@extends("layouts.default")

@section("title", "Galeria de Dúvidas")

@section("content")

    <main class="fd-galeria-de-duvidas">

        <section>
            <div class="container">

                <div class="row">
                    <div class="col-12 col-md-8">
                        <h1>
                            Galeria de Dúvidas - {!! $lesson->name !!}
                        </h1>
                    </div>
                    <div class="col-12 col-md-4">
                        {!! Form::open(['url' => request()->fullUrl(), 'method' => 'GET']) !!}
                            <div class="fd-group">
                                {!! Form::text('pesquisar', null, ['class' => 'fd-input', 'placeholder' => 'Pesquise a sua dúvida']) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>

                <div id="accordion-duvidas" role="tablist">
                    <div class="row">
                        @foreach($gallery as $k => $gl)
                            @if ($k % 2 == 0)
                                <div class="col-12 col-md-6">
                                    <div class="card">
                                        <div class="card-header" role="tab" id="heading1">
                                            <a class="" data-toggle="collapse" href="#collapse1" role="button" aria-expanded="true" aria-controls="collapse1">
                                                {!! $gl->text !!}
                                                <span class="fd-collapse-status">
                                                    <i class="fas fa-minus" style="width: 12px"></i>
                                                </span>
                                            </a>
                                        </div><!-- end [ CARD-HEADER ] -->
                                        <div id="collapse1" class="collapse show" role="tabpanel" aria-labelledby="heading1" data-parent="#accordion-duvidas">
                                            <div class="card-body">
                                                <div class="answer">
                                                    {!! $gl->answer ? $gl->answer : '<p>Aguardando resposta</p>' !!}
                                                </div>
                                            </div><!-- end [ CARD-BODY ] -->
                                            @if ($gl->Teacher)
                                                <div class="card-footer">
                                                    <p><span>Professor(a): {!! $gl->Teacher->name !!}</span><span class="data-duvida">Data: {!! dateToSql($gl->updated_at, 'Y-m-d H:i:s', 'd/m/Y') !!}</span></p>
                                                </div>
                                            @endif
                                        </div><!-- end [ COLLAPSE ] -->
                                    </div><!-- end [ CARD ] -->
                                </div>
                            @else
                                <div class="col-12 col-md-6">
                                    <div class="card">
                                        <div class="card-header" role="tab" id="heading2">
                                            <a class="" data-toggle="collapse" href="#collapse2" role="button" aria-expanded="true" aria-controls="collapse2">
                                                {!! $gl->text !!}
                                                <span class="fd-collapse-status">
                                                    <i class="fas fa-minus" style="width: 12px"></i>
                                                </span>
                                            </a>
                                        </div><!-- end [ CARD-HEADER ] -->
                                        <div id="collapse2" class="collapse show" role="tabpanel" aria-labelledby="heading2" data-parent="#accordion-duvidas">
                                            <div class="card-body">
                                                <div class="answer">
                                                    {!! $gl->answer ? $gl->answer : '<p>Aguardando resposta</p>' !!}
                                                </div>
                                            </div><!-- end [ CARD-BODY ] -->
                                            @if ($gl->Teacher)
                                                <div class="card-footer">
                                                    <p><span>Professor(a): {!! $gl->Teacher->name !!}</span><span class="data-duvida">Data: {!! dateToSql($gl->updated_at, 'Y-m-d H:i:s', 'd/m/Y') !!}</span></p>
                                                </div>
                                            @endif
                                        </div><!-- end [ COLLAPSE ] -->
                                    </div><!-- end [ CARD ] -->
                                </div>
                            @endif
                        @endforeach
                    </div><!-- end /.cols -->
                </div><!-- end /.row -->
            </div><!-- end /.container -->
        </section>

    </main>

@endsection