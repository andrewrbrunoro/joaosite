@extends("layouts.default")

@section("title", $team->Course->name)

@section('content')
    <curso inline-template :lessons='{!! $learn !!}'>
        <main class="course" id="course">

            <?php /* DRAG AND DROP POPUP */ ?>
            <div id="course-schedule-popup">
                <div class="fd-modal-schedule">
                    <section class="course-schedule">
                        <div>
                            <a href="javascript:void(0)" class="cronograma-popup-close">FECHAR</a>

                            <div class="course-schedule-header"></div>

                            <aulas inline-template :aulas="curso.aulas" :aula="aula">
                                <div class="course-schedule-content">
                                    <div class="line bg-gray">
                                        <p>Aulas disponíveis</p>
                                        <div class="form-group">
                                            <input type="text" placeholder="Procure pela aula desejada" class="form-control">
                                            <img src="{!! url('assets/images/svg/svg/magnifying-glass.svg') !!}" alt="" class="fd-svg">
                                        </div>
                                        <a href="" class="download">
                                            <img src="{!! url('assets/images/svg/svg/download.svg') !!}" alt="Baixe o Cronograma do curso" class="fd-svg">
                                            <span class="d-none d-md-block">Cronograma do Curso</span>
                                        </a>
                                    </div>
                                    <div class="line" v-for="(a, index) in aulas">
                                        <div class="line-title">
                                            <h3>@{{ a.aula }} - <small>@{{ a.professor }}</small></h3>
                                        </div>
                                        <div class="line-whatched">
                                            <img v-if="a.qtdes_assistido > 0" src="{!! url('assets/images/svg/verification-mark.svg') !!}" alt="" class="fd-svg">
                                            <span class="d-none d-lg-inline-block">Assistida @{{ a.qtdes_assistido }} vez(es)</span>
                                        </div>
                                        <div class="line-fav">
                                            <a href="javascript:void(0)" v-on:click="favorite(a)">
                                                <div v-show="a.favorito">
                                                    <i class="fas fa-star"></i>
                                                    <span class="d-none d-lg-inline-block">Desvaforitar Aula</span>
                                                </div>
                                                <div v-show="!a.favorito">
                                                    <i class="far fa-star"></i>
                                                    <span class="d-none d-lg-inline-block">Favoritar Aula</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="line-watch-again">
                                            <a v-bind:href="a.url" class="btn fd-button fd-button-gold">assistir aula</a>
                                        </div>
                                    </div>
                                </div>
                            </aulas>


                        </div>
                    </section>
                </div><!-- end [ FD-MODAL-SCHEDULE ] -->
            </div><!-- end [ COURSE-SCHEDULE-POPUP ] -->
            <? /* DRAG AND DROP */ ?>

            <section class="course-title">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-xl-9">
                            <h1>
                                <strong>Curso @{{ curso.tipo }}:</strong>
                                @{{ curso.nome }}
                            </h1>
                        </div><!-- end /.cols -->
                        <div class="col-12 col-xl-3">
                            <p class="course-percentage">
                                <i class="fas fa-desktop"></i> Você já assistiu <span>@{{ curso.total }}% do curso</span>
                            </p>
                        </div>
                    </div><!-- end [ END ROW ] -->
                    <hr />
                </div><!-- end [ CONTANIER ] -->
            </section><!-- end [ COURSE-TITLE ] -->

            <section class="course-class">
                <div class="container">
                    <div class="row course-class-title">
                        <div class="col-12 col-md-7 col-lg-8">
                            <h2><strong>Aula 01:</strong> @{{ curso.atual.aula }}</h2>
                            <a href="javascript:void(0)" v-on:click="favorite(curso.atual)" class="add-to-fav">
                                <div v-show="curso.atual.favorito">
                                    <i class="fas fa-star"></i>
                                    <span class="d-none d-lg-inline-block">Desvaforitar Aula</span>
                                </div>
                                <div v-show="!curso.atual.favorito">
                                    <i class="far fa-star"></i>
                                    <span class="d-none d-lg-inline-block">Favoritar Aula</span>
                                </div>
                            </a>
                        </div><!-- end /.cols -->
                        <div class="col-12 col-md-5 col-lg-4">
                            <div class="flex-content">
                                <a href="" class="btn fd-button fd-button-white"><i class="fas fa-angle-left"></i> Aula anterior</a>
                                <a href="" class="btn fd-button fd-button-gold">próxima aula <i class="fas fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div><!-- end [ ROW ] -->
                </div><!-- end [ CONTAINER ] -->
            </section><!-- end [ COURSE-CLASS ] -->

            <section class="course-video">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="video-container">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <div class="embed-responsive-item">
                                        <iframe v-bind:src="curso.atual.video" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="slides-content" style="width: 0;" v-if="curso.atual.material">
                                <iframe v-bind:src="'http://docs.google.com/gview?url=' + curso.atual.material + '&embedded=true'"
                                        style="width:100%;" frameborder="0"></iframe>
                            </div>
                            <div class="iframe-container" style="width: 0;" v-if="curso.atual.material">
                                <iframe v-bind:src="'http://docs.google.com/gview?url=' + curso.atual.material + '&embedded=true'"
                                        style="width:100%;height:100%" frameborder="0"></iframe>
                            </div>
                            <div class="anotacoes" id="anotations-side">
                                <div class="textarea-content">
                                    <a href="#" data-target="#anotations-side" class="fd-close-anotations">FECHAR</a>
                                    <textarea class="redactor"></textarea>
                                    <div class="fd-row justify-content-end">
                                        <a href="javascript:void(0)" class="btn fd-button fd-button-black btn-print" target="_blank">IMPRIMIR</a>
                                    </div>
                                    <p class="info">Atenção! As anotações não ficam salvas na Plataforma Davi André, cabendo ao usuário salvá-las em seu computador pessoal.</p>
                                </div>
                            </div>
                        </div>
                    </div><!-- end [ ROW ] -->
                </div><!-- end [ CONTAINER ] -->
            </section><!-- end [ COURSE-VIDEO ] -->

            <section class="course-options">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div id="funcionalidades">
                                <div class="row">
                                    <div class="col-12 col-xl-4 d-none">
                                        <a href="#" class="btn fd-button fd-button-gold" data-course-options="true">ver funcionalidades</a>
                                    </div>
                                    <div class="col-12 col-md-8 offset-md-2">





                                        <div class="flex-content">

                                            <?php /* CRONOGRAMA */?>
                                            <div class="fd-item">
                                                <div class="fd-dropup" id="schedule">
                                                    <a href="#" class="fd-dropup-close"></a>
                                                    <h3>Cronograma</h3>
                                                    <p>
                                                        Selecione a posição que fica melhor para você!
                                                    </p>
                                                    <a href="#course-schedule" class="btn"
                                                       v-on:click="setConfig('lesson_status', 1, {!! $team->id !!})"
                                                       data-toggle="collapse" role="button" aria-expanded="false" aria-controls="course-schedule">abaixo do vídeo</a>
                                                    <a href="#course-schedule-popup"
                                                       v-on:click="setConfig('lesson_status', 0, {!! $team->id !!})"
                                                       class="btn cronograma-popup d-none d-lg-block">pop up</a>
                                                </div>
                                                <a href="javascript:void(0)" data-target="#schedule"
                                                   data-toggle="tooltip"
                                                   data-html="false"
                                                   data-placement="bottom"
                                                   title="Aulas Disponíveis">
                                                    <img src="{!! url('assets/images/svg/svg/001-small-calendar.svg') !!}" class="fd-svg"/>
                                                </a>
                                            </div><!-- end [ FD-ITEM ] -->

                                            <?php /* ACENDA / APAGUE A LUZ */ ?>
                                            <div class="fd-item hover">
                                                <div class="fd-dropup" id="change-lights">
                                                    <h3>Acenda | Apague a luz</h3>
                                                    <p>
                                                        Recomenda-se que, em ambientes claros ou durante o dia, atela esteja configurada para fundo branco e, durante a noite, em qua a luminosidade natural é menor, que esteja escuro
                                                    </p>
                                                </div>
                                                <a href="javascript:void(0)"
                                                   v-on:click="setConfig('light', 0, {!! $team->id !!})"
                                                   id="change-lights-button" data-toggle="tooltip" data-html="false" data-placement="bottom" title="Acender | Apagar a Luz">
                                                    <img src="{!! url('assets/images/svg/svg/013-light-bulb-outline.svg') !!}" class="fd-svg"/>
                                                </a>
                                            </div><!-- end [ FD-ITEM ] -->


                                            <div class="fd-item">
                                                <a href="" data-toggle="tooltip" data-html="false" data-placement="bottom" title="Dificuldades Técnicas">
                                                    <img src="{!! url('assets/images/svg/svg/012-settings.svg') !!}" alt="" class="fd-svg">
                                                </a>
                                            </div><!-- end [ FD-ITEM ] -->

                                            <?php /* DÚVIDAS */ ?>
                                            <div class="fd-item">
                                                <div class="fd-dropup" id="doubts">
                                                    <a href="#" class="fd-dropup-close"></a>
                                                    <h3>Dúvidas da Aula</h3>
                                                    <p>
                                                        Utilize o espaço abaixo para enviar sua dúvida ao (à) professor(a). Seja objetivo(a) e não fuja da temática da aula. Antes, verifique se sua dúvida já não está resolvida na galeria de dúvidas e aproveite para estudar com as respostas.
                                                    </p>
                                                    <form action="javascript:void(0)"
                                                          method="POST"
                                                          id="form-report-question"
                                                          data-message-title="Sua dúvida foi enviada!"
                                                          data-message-text="Assim que tivermos a resposta do(a)  professor(a), encaminharemos a você. Agora, volte aos estudos!"
                                                    >
                                                        <div class="form-group">
                                                            <textarea name="texto-duvida" class="form-control" placeholder="Escreva neste espaço a sua dúvida"></textarea>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col">
                                                                <a href="{!! route('front.team.gallery-questions', $lesson_slug) !!}" class="btn float-md-right" >galeria de dúvidas</a>
                                                            </div>
                                                            <div class="col">
                                                                <button type="submit" class="btn black float-md-left">Enviar Dúvida</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <div class="msg"></div>
                                                </div>
                                                <a href="javascript:void(0)" data-target="#doubts" data-toggle="tooltip" data-html="false" data-placement="bottom" title="Dúvidas de Aulas">
                                                    <img src="{!! url('assets/images/svg/svg/011-round-help-button.svg') !!}" alt="" class="fd-svg">
                                                </a>
                                            </div><!-- end [ FD-ITEM ] -->

                                            <?php /* ESCOLHA DO PLAYER */ ?>
                                            <div class="fd-item hover">
                                                <div class="fd-dropup" id="choose-one">
                                                    <h3>
                                                        Alterar Velocidade do Player
                                                    </h3>
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat quasi quidem quod ut veritatis. Atque consequatur dolor dolorem harum id itaque necessitatibus odit quia rerum sed sint, temporibus vitae. Quis!
                                                    </p>
                                                </div>
                                                <a href="javascript:void(0)" data-target="#choose-one" data-toggle="tooltip" data-html="false" data-placement="bottom" title="Alterar velocidade do player de vídeo">
                                                    <img src="{!! url('assets/images/svg/svg/010-arrows.svg') !!}" alt="" class="fd-svg">
                                                </a>
                                            </div><!-- end [ FD-ITEM ] -->


                                            <?php /*
                                        <div class="fd-item">
                                            <a href="" data-toggle="tooltip" data-html="false" data-placement="bottom" title="Velocidade do Vídeo">
                                                <img src="/assets/images/svg/svg/009-fast-foward-circular-button.svg" alt="" class="fd-svg">
                                            </a>
                                        </div><!-- end [ FD-ITEM ] -->
                                        */ ?>


                                            <?php /* AVALIAÇÃO DA AULA */?>
                                            <div class="fd-item">
                                                <div class="fd-dropup" id="evaluation">
                                                    <a href="#" class="fd-dropup-close"></a>
                                                    <h3>Avaliação de Aula</h3>
                                                    <p>
                                                        Selecione a sua opinião sobre esta aula:
                                                    </p>
                                                    <form action="javascript:void(0)" method="POST" id="form-rating">
                                                        <div class="evaluation-grade">
                                                            @foreach($availableRatings as $rt)
                                                                <div>
                                                                    <input type="radio"
                                                                           id="eval-{!! $rt["value"] !!}"
                                                                           name="rating"
                                                                           {!! $rt["value"] == ($lessonEvaluated ? $lessonEvaluated->points : "") ? " checked" : "" !!}
                                                                           value="{!! $rt["value"] !!}"
                                                                           required="required"
                                                                    />
                                                                    <label for="eval-{!! $rt["value"] !!}">
                                                                        <img src="{!! url('assets/images/svg/svg/' . $rt['icon']) !!}" alt="{!! $rt["name"] !!}" class="fd-svg" />
                                                                        <span>{!! $rt["name"] !!}</span>
                                                                    </label>
                                                                </div>
                                                            @endforeach
                                                        </div><!-- end [ RADIO BUTTONS ] -->
                                                        <div class="form-group">
                                                            <label>Se quiser, nos envie uma mensagem coma sua opinião, críticas ou elogios:</label>
                                                            <textarea name="avaliacao-de-aula" class="form-control">{!! $lessonEvaluated ? $lessonEvaluated->text : "" !!}</textarea>
                                                        </div>
                                                        <button type="submit" class="btn float-right">Enviar Avaliação</button>
                                                    </form>
                                                </div>

                                                <a href="javascript:void(0)" data-target="#evaluation" data-toggle="tooltip" data-html="false" data-placement="bottom" title="Avaliação da Aula">
                                                    <img src="{!! url('assets/images/svg/svg/008-happy.svg') !!}" alt="" class="fd-svg">
                                                </a>
                                            </div><!-- end [ FD-ITEM ] -->

                                            <?php /* EXERCÍCIOS */ ?>
                                            <div class="fd-item">
                                                <div class="fd-dropup" id="exercises">
                                                    <a href="#" class="fd-dropup-close"></a>
                                                    <h3>Exercícios</h3>
                                                    <div class="fd-row">
                                                        <p>Questões para impressão</p>
                                                        <a href="http://www.daviandre.com.br/wp-content/uploads/2017/02/Politica_de_acesso_e_requisitos.pdf" target="_blank" class="btn">Nova aba</a>
                                                        <a href="" class="btn">Download</a>
                                                    </div>
                                                    <div class="fd-row">
                                                        <p>Gabarito para impressão</p>
                                                        <a href="http://www.daviandre.com.br/wp-content/uploads/2017/02/Politica_de_acesso_e_requisitos.pdf" target="_blank" class="btn">Nova aba</a>
                                                        <a href="" class="btn">Download</a>
                                                    </div>
                                                    <div class="fd-row">
                                                        <p>Questões para impressão</p>
                                                        <a href="" target="_blank" class="btn">Entrar</a>
                                                    </div>
                                                </div>
                                                <a href="javascript:void(0)" data-target="#exercises" data-toggle="tooltip" data-html="false" data-placement="bottom" title="Exercícios">
                                                    <img src="{!! url('assets/images/svg/svg/007-docs.svg') !!}" alt="" class="fd-svg">
                                                </a>
                                            </div><!-- end [ FD-ITEM ] -->


                                            <?php /* MATERIAL DE APOIO */ ?>
                                            <div class="fd-item">
                                                <div class="fd-dropup" id="support-material">
                                                    <a href="#" class="fd-dropup-close"></a>
                                                    <h3>Material de Apoio</h3>

                                                    <div class="fd-row fd-row-t">
                                                        <div class="th"></div>
                                                        <div class="th">
                                                            <p>nova aba</p>
                                                        </div>
                                                        <div class="th d-none d-lg-block">
                                                            <p>ao lado</p>
                                                        </div>
                                                        <div class="th">
                                                            <p>download</p>
                                                        </div>
                                                    </div>
                                                    <div class="fd-row fd-row-c">
                                                        <div class="td">Sumário da aula</div>
                                                        <div class="td"><a v-bind:href="curso.atual.material" target="_blank" class="square type-1"></a></div>
                                                        <div class="td d-none d-lg-block"><a href="#" class="square type-2 sumario"></a></div>
                                                        <div class="td"><a href="#" class="square type-3"></a></div>
                                                    </div>
                                                    <div class="fd-row fd-row-c">
                                                        <div class="td">Slides</div>
                                                        <div class="td"><a v-bind:href="curso.atual.material" target="_blank" class="square type-1"></a></div>
                                                        <div class="td d-none d-lg-block"><a href="#" class="square type-2 slides"></a></div>
                                                        <div class="td"><a href="#" class="square type-3"></a></div>
                                                    </div>
                                                </div>
                                                <a href="javascript:void(0)" data-target="#support-material" data-toggle="tooltip" data-html="false" data-placement="bottom" title="Material de Apoio">
                                                    <img src="{!! url('assets/images/svg/svg/006-down-arrow.svg') !!}" alt="" class="fd-svg">
                                                </a>
                                            </div><!-- end [ FD-ITEM ] -->


                                            <?php /* DESEMPENHO */ ?>
                                            <div class="fd-item">
                                                <div class="fd-dropup" id="performance">
                                                    <a href="#" class="fd-dropup-close"></a>
                                                    <h3>Desempenho</h3>
                                                    <p>
                                                        Confira o seu desempenho nos excercícios
                                                    </p>
                                                    <div class="fd-row fd-row-t">
                                                        <div class="th">
                                                            <p>Avaliação</p>
                                                        </div>
                                                        <div class="th d-none d-md-inline-block">
                                                            <p>nota</p>
                                                        </div>
                                                        <div class="th">
                                                            <p>detalhes</p>
                                                        </div>
                                                    </div>
                                                    <div class="fd-row fd-row-c">
                                                        <div class="td">Campo de Treinamento</div>
                                                        <div class="td d-none d-md-inline-block">0,00/10,00</div>
                                                        <div class="td"><a href="" class="circle"><i class="fas fa-plus"></i></a></div>
                                                    </div>
                                                    <div class="fd-row fd-row-c">
                                                        <div class="td">Direitos e garantias fundamentais</div>
                                                        <div class="td d-none d-md-inline-block">3,50/10,00</div>
                                                        <div class="td"><a href="" class="circle"><i class="fas fa-plus"></i></a></div>
                                                    </div>
                                                    <div class="fd-row fd-row-c">
                                                        <div class="td">Princípios Fundamentais</div>
                                                        <div class="td d-none d-md-inline-block">2,50/10,00</div>
                                                        <div class="td"><a href="" class="circle"><i class="fas fa-plus"></i></a></div>
                                                    </div>
                                                </div>
                                                <a href="javascript:void(0)" data-target="#performance" data-toggle="tooltip" data-html="false" data-placement="bottom" title="Desempenho">
                                                    <img src="{!! url('assets/images/svg/svg/005-list-circular-button.svg') !!}" alt="" class="fd-svg">
                                                </a>
                                            </div><!-- end [ FD-ITEM ] -->


                                            <?php /* ÚLTIMOS ACESSOS */ ?>
                                            <div class="fd-item">
                                                <div class="fd-dropup" id="last-hits">
                                                    <a href="#" class="fd-dropup-close"></a>
                                                    <h3>Últimos Acessos</h3>

                                                    <historico-acesso inline-template>
                                                        <ul>
                                                            @foreach($lastAccesses as $lastAccess)
                                                                <li>Acesso {!! $lastAccess->Lesson->name !!} <br> {!! dateToSql($lastAccess->updated_at, 'Y-m-d H:i:s', 'd/m/Y H:i:s') !!}</li>
                                                            @endforeach
                                                        </ul>
                                                    </historico-acesso>

                                                </div>
                                                <a href="javascript:void(0)" data-target="#last-hits" data-toggle="tooltip" data-html="false" data-placement="bottom" title="Últimos Acessos">
                                                    <img src="{!! url('assets/images/svg/svg/004-pc-monitor.svg') !!}" alt="" class="fd-svg">
                                                </a>
                                            </div><!-- end [ FD-ITEM ] -->


                                            <?php /* ANOTAÇÕES */?>
                                            <div class="fd-item">
                                                <div class="fd-dropup" id="anotations">
                                                    <a href="#" class="fd-dropup-close"></a>
                                                    <h3>Anotação</h3>
                                                    <p>
                                                        Faça as anotações da sua aula e escolha a posição que ficar melhor para você!
                                                    </p>
                                                    <a href="javascript:void(0)" class="btn bottom">abaixo do vídeo</a>
                                                    <a href="javascript:void(0)" class="btn side">ao lado do vídeo</a>
                                                </div>
                                                <a href="javascript:void(0)" data-target="#anotations" data-toggle="tooltip" data-html="false" data-placement="bottom" title="Anotações" >
                                                    <img src="{!! url('assets/images/svg/svg/003-edit.svg') !!}" alt="" class="fd-svg">
                                                </a>
                                            </div><!-- end [ FD-ITEM ] -->

                                            <?php /* REPORTAR ERRO */ ?>
                                            <div class="fd-item">
                                                <div class="fd-dropup" id="error-report">
                                                    <a href="#" class="fd-dropup-close"></a>
                                                    <h3>Reportar Erro</h3>
                                                    <p>
                                                        Encontrou algum problema técnico na aula?<br />
                                                        Conte-nos o que é e vamos corrigir.
                                                    </p>
                                                    <form action="javascript:void(0)"
                                                          method="POST"
                                                          id="form-report-error"
                                                          data-message-title="Obrigado"
                                                          data-message-text="Sua mensagem com o erro foi reportado!"
                                                    >
                                                        <div class="form-group">
                                                            <textarea name="reportar-erro" class="form-control" placeholder="Escreva neste espaço o erro"></textarea>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col">
                                                                <button type="submit" class="btn float-md-right">Reportar</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <div class="msg"></div>
                                                </div>
                                                <a href="javascript:void(0)" data-target="#error-report" data-toggle="tooltip" data-html="false" data-placement="bottom" title="Reportar Erro">
                                                    <img src="{!! url('assets/images/svg/svg/002-warning.svg') !!}" alt="" class="fd-svg">
                                                </a>
                                            </div><!-- end [ FD-ITEM ] -->

                                        </div>
                                    </div>
                                </div>
                            </div><!-- end [ #FUNCIONALIDADES ] -->

                            <div class="fd-row video-size-options">
                                <span class="text-uppercase">visualizações</span>
                                <a href="javascript:void(0)" class="active" data-props="width-2-1">
                                    <img src="{!! url('assets/images/icons/w-2-3.png') !!}"/>
                                </a>
                                <a href="javascript:void(0)" data-props="width-1-2">
                                    <img src="{!! url('assets/images/icons/w-1-3.png') !!}"/>
                                </a>
                                <a href="javascript:void(0)" data-props="width-1-1">
                                    <img src="{!! url('assets/images/icons/w-1-1.png') !!}"/>
                                </a>
                                <a href="javascript:void(0)" data-props="width-100-100">
                                    <img src="{!! url('assets/images/icons/w-100-100.png') !!}"/>
                                </a>
                            </div><!-- end [ VIDEO-SIZE-OPTIONS ] -->

                        </div>
                    </div><!-- end [ ROW ] -->
                </div><!-- end [ CONTAINER ] -->
            </section><!-- end [ COURSE-OPTIONS ] -->

            <div id="course-schedule" class="collapse">
                <section class="course-schedule">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="course-schedule-header">
                                </div>

                                <aulas inline-template :aulas="curso.aulas">
                                    <div class="course-schedule-content">
                                        <div class="line bg-gray">
                                            <p>Aulas disponíveis</p>
                                            <div class="form-group">
                                                <input type="text" placeholder="Procure pela aula desejada" class="form-control">
                                                <img src="{!! url('assets/images/svg/svg/magnifying-glass.svg') !!}" alt="" class="fd-svg">
                                            </div>
                                            <a href="" class="download">
                                                <img src="{!! url('assets/images/svg/svg/download.svg') !!}" alt="Baixe o Cronograma do curso" class="fd-svg">
                                                <span class="d-none d-md-block">Cronograma do Curso</span>
                                            </a>
                                        </div>
                                        <div class="line" v-for="(a, index) in aulas">
                                            <div class="line-title">
                                                <h3>@{{ a.aula }} - <small>@{{ a.professor }}</small></h3>
                                            </div>
                                            <div class="line-whatched">
                                                <img v-if="a.qtdes_assistido > 0" src="{!! url('assets/images/svg/verification-mark.svg') !!}" alt="" class="fd-svg">
                                                <span class="d-none d-lg-inline-block">Assistida @{{ a.qtdes_assistido }} vez(es)</span>
                                            </div>
                                            <div class="line-fav">
                                                <a href="javascript:void(0)" v-on:click="favorite(a)">
                                                    <div v-show="a.favorito">
                                                        <i class="fas fa-star"></i>
                                                        <span class="d-none d-lg-inline-block">Desvaforitar Aula</span>
                                                    </div>
                                                    <div v-show="!a.favorito">
                                                        <i class="far fa-star"></i>
                                                        <span class="d-none d-lg-inline-block">Favoritar Aula</span>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="line-watch-again">
                                                <a v-bind:href="a.url" class="btn fd-button fd-button-gold">assistir aula</a>
                                            </div>
                                        </div>
                                    </div>
                                </aulas>
                            </div>
                        </div>
                    </div><!-- end [ CONTAINER ] -->
                </section><!-- end [ COURSE-SHCEDULE ] -->

            </div><!-- end [ #COURSE-SCHEDULE ] -->

            <div id="anotations-bottom" class="d-none">
                <div class="container">
                    <div class="textarea-content">
                        <a href="#" data-target="#anotations-bottom" class="fd-close-anotations">FECHAR</a>
                        <textarea class="redactor" id="main-redactor"></textarea>
                        <div class="fd-row justify-content-end">
                            <a href="javascript:void(0)" class="btn fd-button fd-button-black btn-print" target="_blank">IMPRIMIR</a>
                        </div>
                        <p class="info">Atenção! As anotações não ficam salvas na Plataforma Davi André, cabendo ao usuário salvá-las em seu computador pessoal.</p>
                    </div>
                </div>
            </div>

            <section class="course-another">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <hr />
                            <a href="" class="btn fd-button fd-button-white text-uppercase">
                                ver outros cursos
                            </a>
                        </div><!-- end [ COLS ] -->
                    </div><!-- end [ ROW ] -->
                </div><!-- end [ CONTAINER ] -->
            </section><!-- end [ COURSE-ANOTHER ] -->

        </main>

    </curso>
@stop

@section("js")
    <script type="text/javascript">
        var TEAM_ID = '{!! $team->id !!}',
            LESSON_ID = '{!! $lesson_id !!}';
    </script>
    {!! Html::script('assets/js/components/learn.js') !!}
@endsection

