@extends("layouts.default")

@section("title", "Degustação")

@section("content")

    <main class="fd-cursos free-content" data-page="default">

        <div class="container">
            <div class="fd-row">
                <h1>Conteúdo Gratuito</h1>
                <p>Vejas os conteúdos disponibilizados para você.</p>
                <a href="" class="link">Clique aqui e veja o tutorial de podcast.</a>

                <h2>Veja o vídeo de apresentação</h2>

                <a href="javascript:void(0)" class="video-apresentacao" data-video data-video-id="DhxtMa6_tyo">
                    <figure>
                        <img src="assets/images/demonstrativo.jpg" class="img-fluid"/>
                    </figure>
                    <img src="assets/images/svg/play.svg" class="fd-svg"/>
                </a>
            </div>
        </div>

        <section>
            <?php /* CURSOS DA POLICIA */;?>
            <div class="container">
                <div class="fd-row">
                    <h2>Degustação</h2>
                    <div class="owl-carousel owl-theme" data-carousel-page="home-cursos" data-position="cursos">

                        @foreach($degustacoes as $degustacao)
                            <a href="{!! url("{$degustacao->Course->slug}/{$degustacao->TeamType->slug}/{$degustacao->slug}") !!}" class="item fd-card">
                                <div class="fd-card-background" style="background-image: url('assets/images/cursos/pc-sc-001-300x180.jpg')"></div>
                                <div class="fd-card-content">
                                    <h3>
                                        {!! $degustacao->Course->name !!}
                                        <small>Curso de Degustação</small>
                                    </h3>
                                </div>
                                <div class="fd-card-footer">
                                    <div class="btn fd-button">veja mais</div>
                                    <p><strong>GRÁTIS</strong></p>
                                </div>
                            </a>
                        @endforeach

                    </div><!-- end /.owl-carousel -->
                </div><!-- end /.fd-row -->
            </div><!-- end /.container -->


        </section><!-- end fd-home-cursos -->
    </main>


@endsection


@section("js")
    <script type="text/javascript">
        let fdCarousel = $('[data-position="cursos"]');
        fdCarousel.owlCarousel({
            nav: false,
            navText: ['<img src="' + APP_ROOT + 'assets/images/icons/carousel-icon.png" class="prev">', '<img src="' + APP_ROOT + 'assets/images/icons/carousel-icon.png" class="next">'],
            loop: false,
            margin: 20,
            responsive: {
                0: {
                    items: 1
                },
                576: {
                    items: 2,
                    dots: false,
                    nav: true
                },
                992: {
                    items: 3
                }
            }
        });
    </script>
@endsection