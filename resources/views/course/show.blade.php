@extends("layouts.default")

@section("title", $team->Course->name)

@section("content")

    <main class="fd-cursos" data-page="interna">

        <section class="fd-banner">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="fd-banner-content" style="background-image: url('{!! url('assets/images/backgrounds/carousel.jpg') !!}');">
                            <div>
                                <h1>{!! $team->Course->name !!} - {!! $team->name !!}</h1>
                                <p>{!! $team->TeamType->name !!} - {!! $countLessons !!} aulas</p>
                                <h4><span>R$</span> {!! $team->buy_price !!}</h4>
                                @if (!$registration)
                                    {!! Form::open(['route' => 'front.bag.add.team']) !!}
                                        <button type="submit" name="team_id" value="{!! $team->id !!}" class="btn fd-button fd-button-gold">COMPRAR</button>
                                    {!! Form::close() !!}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="fd-detail">

            <div class="container">
                <div class="row">
                    <div class="fd-content">

                        <p class="has-after">
                            Detalhes do Curso
                        </p>

                        {!! $team->Course->commercial_call !!}
                    </div><!-- end /.col -->
                    <aside>
                        <h4><span>R$</span>{!! $team->buy_price !!}</h4>
                        <p>
                            <small>À vista com 5% de desconto ou sem 12x sem juros</small>
                        </p>

                        @if (!$registration)
                            <a href="" class="btn fd-button fd-button-gold">MATRICULE-SE</a>
                        @else
                            <a href="{!! request()->fullUrl() !!}/estudar" class="btn fd-button fd-button-gold">ESTUDAR</a>
                        @endif
                        <ul>
                            <li>
                                <figure>
                                    <img src="{!! url('assets/images/icons/calendar.png') !!}" alt="">
                                </figure>
                                <p><strong>Início:</strong> Imediato</p>
                            </li>
                            <li>
                                <figure>
                                    <img src="{!! url('assets/images/icons/carga-horaria.png') !!}" alt="">
                                </figure>
                                <p><strong>Carga Horária:</strong> {!! $countLessons !!} aulas</p>
                            </li>
                            <li>
                                <figure>
                                    <img src="{!! url('assets/images/icons/modalidade.png') !!}" alt="">
                                </figure>
                                <p><strong>Modalidade:</strong> {!! $team->TeamType->name !!}</p>
                            </li>
                            <li>
                                <figure>
                                    <img src="{!! url('assets/images/icons/duracao.png') !!}" alt="">
                                </figure>
                                <p><strong>Duração:</strong> Até a data da prova</p>
                            </li>
                            <li>
                                <figure>
                                    <img src="{!! url('assets/images/icons/salario.png') !!}" alt="">
                                </figure>
                                <p><strong>Salário Inicial:</strong> R$ 5.721,30</p>
                            </li>
                        </ul>
                        <a href="" class="btn fd-button fd-button-gold outline-gold">veja o projeto do curso</a>
                        <a href="" class="btn fd-button fd-button-white">veja o edital anterior</a>

                        <h5>
                            <strong>Ficou com alguma dúvida</strong>
                            <a href="{!! route('front.contact.index') !!}">Acesse aqui as perguntas frequentes</a>
                        </h5>
                    </aside>
                </div><!-- end /.row -->
            </div><!-- end /.container -->
        </section><!-- end /.fd-detail -->

        @if (false)
        <section>
            <div class="container">
                <div class="fd-row">
                    <h2><strong>Combo Delegado de Polícia</strong></h2>
                    <div class="owl-carousel owl-theme" data-carousel-page="home-cursos" data-position="cursos">

                        <a href="#curso-interna" class="item fd-card" data-tag="combo">
                            <div class="fd-card-background" style="background-image: url('{!! url('assets/images/cursos/goe-plus.jpg') !!}')"></div>
                            <div class="fd-card-content">
                                <h3>
                                    GOE Plus
                                    <small>40 Missões - 1.605 questões</small>
                                </h3>
                            </div>
                            <div class="fd-card-footer">
                                <div class="btn fd-button">veja mais</div>
                                <p>R$ <strong>342,00</strong></p>
                            </div>
                        </a>
                    </div><!-- end /.owl-carousel -->
                </div><!-- end /.fd-row -->
            </div><!-- end /.container -->

            <div class="container">
                <div class="fd-row">
                    <h2>Outros cursos que você pode se interessar</h2>
                    <div class="owl-carousel owl-theme" data-carousel-page="home-cursos" data-position="cursos">

                        <a href="#curso-interna" class="item fd-card">

                            <div class="fd-card-background" style="background-image: url('{!! url('assets/images/cursos/goe-plus.jpg') !!}')"></div>
                            <div class="fd-card-content">
                                <h3>
                                    GOE Plus
                                    <small>40 Missões - 1.605 questões</small>
                                </h3>
                            </div>
                            <div class="fd-card-footer">
                                <div class="btn fd-button">veja mais</div>
                                <p>R$ <strong>342,00</strong></p>
                            </div>
                        </a>

                    </div><!-- end /.owl-carousel -->
                </div><!-- end /.fd-row -->
            </div><!-- end /.container -->
        </section><!-- end /.fd-combo -->
        @endif
    </main>

@endsection