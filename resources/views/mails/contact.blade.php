<strong>Nome:</strong> {!! $name !!} <br>
<strong>E-mail:</strong> {!! $email !!} <br>

@foreach($extra as $key => $data)
    @if (strlen($data) > 20)
        <strong>{!! ucfirst($key) !!}:</strong> <br> {!! $data !!} <br>
    @else
        <strong>{!! ucfirst($key) !!}:</strong> {!! $data !!} <br>
    @endif
@endforeach
