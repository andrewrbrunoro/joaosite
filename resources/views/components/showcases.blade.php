@if (isset($showcases) && $showcases->count())

    @foreach($showcases as $showcase)

        <div class="container">
            <div class="fd-row">
                <h2>{!! $showcase['curso'] !!}</h2>
                <div class="owl-carousel owl-theme" data-carousel-page="home-cursos" data-position="cursos">

                    @foreach($showcase['turmas'] as $curso)
                        <a href="{!! $curso["link"] !!}" class="item fd-card">
                            {{--<div class="fd-card-background" v-bind:style="{ backgroundImage : 'url('+v.imagem+')' }"></div>--}}
                            <div class="fd-card-content">
                                <h3>
                                    {!! $curso['titulo'] !!}
                                    <small>{!! $curso['tipo'] !!} - {!! $curso['q_aulas'] !!}</small>
                                </h3>
                            </div>
                            <div class="fd-card-footer">
                                <div class="btn fd-button">veja mais</div>
                                <p>{!! $curso['valor_label'] !!}</p>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>

    @endforeach

@endif