@extends("layouts.default")

@section("title", "Contato")

@section("content")

    <main class="fd-contato">

        <section>
            <div class="container">

                <div class="row">

                    <div class="col-12 col-sm-10 col-md-8">
                        <a class="btn fd-collapse-button collapsed border-bottom" data-toggle="collapse" href="#perguntas-frequentes">
                            <h3>
                                Perguntas Frequentes
                                <small>Clique e veja se o que você procura está aqui.</small>
                            </h3>
                            <span class="fd-collapse-status">
                                <i class="fas fa-plus"></i>
                            </span>
                        </a>

                        <div class="collapse fd-collapse" id="perguntas-frequentes">
                            <div id="accordion-contato" role="tablist">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <a class="collapsed" data-toggle="collapse" href="#collapseOne" role="button" aria-expanded="true" aria-controls="collapseOne">
                                            Tem telefone para atendimento
                                            <span class="fd-collapse-status">
                                                <i class="fas fa-plus" style="width: 12px"></i>
                                            </span>
                                        </a>
                                    </div>

                                    <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion-contato">
                                        <div class="card-body">
                                            <div>
                                                <p>
                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- end /.card -->

                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <a class="collapsed" data-toggle="collapse" href="#collapseTwo" role="button" aria-expanded="true" aria-controls="collapseTwo">
                                            Degustação - como faço?
                                            <span class="fd-collapse-status">
                                            <i class="fas fa-plus" style="width: 12px"></i>
                                        </span>
                                        </a>
                                    </div>

                                    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion-contato">
                                        <div class="card-body">
                                            <div>
                                                <p>
                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- end /.card -->

                            </div>
                        </div><!-- end /.collapse -->
                    </div><!-- end /.cols -->
                </div><!-- end /.row -->

                <div class="row">
                    <div class="col-12 col-sm-10 col-md-8">
                        <a class="btn fd-collapse-button" data-toggle="collapse" href="#contato" role="button" aria-expanded="false" aria-controls="collapseExample">
                            <h3>
                                Entre em contato
                                <small>Caso não tenha encontrado o que procurava, envie-nos uma mensagem</small>
                            </h3>
                            <span class="fd-collapse-status">
                            <i class="fas fa-minus"></i>
                        </span>
                        </a>
                        <div class="collapse show fd-collapse">
                            {!! Form::open(["id" => "contato"]) !!}
                                <div class="fd-group">
                                    {!! Form::text("nome", null, [
                                            "class" => "form-control fd-input",
                                            "required",
                                            "data-rule-required" => "true",
                                            "data-msg-required" => "Campo obrigatório",
                                            "autocomplete" => "off"
                                    ]) !!}<!-- end /input -->
                                    <label class="fd-label">Seu nome</label>
                                </div>
                                <div class="fd-group">
                                    {!! Form::text("assunto", null, [
                                            "class" => "form-control fd-input",
                                            "required",
                                            "data-rule-required" => "true",
                                            "data-msg-required" => "Campo obrigatório",
                                            "autocomplete" => "off"
                                    ]) !!}<!-- end /input -->
                                    <label class="fd-label">Assunto</label>
                                </div>
                                <div class="fd-group">
                                    {!! Form::text("curso", null, ["class" => "form-control fd-input", "autocomplete" => "off"]) !!}
                                    <!-- end /input -->
                                    <label class="fd-label">Curso matrículado, caso seja aluno</label>
                                </div>
                                <div class="fd-group">
                                    {!! Form::text("email", null, [
                                            "class" => "form-control fd-input",
                                            "required",
                                            "data-rule-required" => "true",
                                            "data-msg-required" => "Campo obrigatório",
                                            "data-rule-email" => "true",
                                            "data-msg-email" => "Preencha com um e-mail válido.",
                                            "autocomplete" => "off"
                                    ]) !!}<!-- end /input -->
                                    <label class="fd-label">E-mail</label>
                                </div>
                                <div class="fd-group">
                                    {!! Form::textarea("mensagem", null, [
                                        "class" => "form-control fd-input",
                                        "required",
                                        "data-rule-required" => "true",
                                        "data-msg-required" => "Campo obrigatório"
                                    ]) !!}
                                    <label class="fd-label">Mensagem</label>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button type="submit" class="btn fd-button fd-button-gold">enviar mensagem</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}<!-- end /.fd-validate -->
                        </div><!-- end /#contato -->
                    </div><!-- end /.cols -->
                </div><!-- end /.fd-row -->
            </div><!-- end /.container -->
        </section>

    </main>

@endsection

@section("js")
    {!! Html::script("assets/js/contact.js") !!}
@endsection