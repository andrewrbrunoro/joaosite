@extends("layouts.default")

@section("title", "Pagamento")

@section("content")
    <cart inline-template :total="200" pagamento-padrao="CREDITO">
        <main class="fd-cart">

            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Compra de Curso</h2>
                        <hr>
                        <h4><strong>02:</strong> Escolha sua forma de pagamento</h4>
                    </div><!-- end [ COL ] -->
                </div><!-- end [ ROW ] -->


                <div id="cart">

                    <div class="row" id="cart-step-2">

                        <div class="col-12 table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Curso</th>
                                    <th>Valor do Curso</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data->PurchaseItem as $row)
                                    <tr>
                                        <td class="text">
                                            <figure class="image">
                                                <img src="{!! url('assets/images/cursos/brigada_rs.jpg') !!}"
                                                     alt="{!! $row->name !!}">
                                            </figure>
                                            <p>
                                                <strong>{!! $row->name !!}</strong>
                                            </p>
                                        </td>
                                        <td class="value">
                                            <p>
                                                {!! $row->style_value !!}
                                            </p>
                                        </td>
                                        <td class="options">
                                            {!! Form::open(['route' => 'front.cart.remove', 'method' => 'DELETE']) !!}
                                            <button class="btn" type="submit" name="item_id" value="{!! $row->id !!}">
                                                <i class="far fa-trash-alt"></i>
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td class="value">
                                        <p>Cupom: </p>
                                    </td>
                                    <td class="value">
                                        <p>Total:
                                            <small>
                                                <small>R$</small> {!! $data->total_brl !!}</small>
                                        </p>
                                    </td>
                                </tr>
                                </tbody>
                            </table><!-- end [ TABLE ] -->
                        </div><!-- end [ COL-12 TABLE-RESPONSIVE -->

                        @if ($errors->count())
                            <div class="col-12">
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{!! $error !!}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif

                        <div class="col-12">
                            {!! Form::open(["route" => "front.cart.payment", "id" => "form-pagamento"]) !!}
                            {!! Form::hidden('senderHash', null) !!}
                            {!! Form::hidden('creditCardToken', null) !!}
                            <div class="row">

                                <div class="col-12 col-sm-4 col-md-3">
                                    <div class="bg-form method-type">
                                        <p class="text-center text-muted"><strong>ESCOLHA A FORMA DE
                                                PAGAMENTO</strong></p>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="paymentMethod" value="creditCard"
                                                       @click="pagamento = 'CREDITO'"
                                                       :checked="pagamento === 'CREDITO'"/>
                                                <img src="{!! url('assets/images/icons/card-1.jpg') !!}">
                                            </label>
                                        </div><!-- end [ RADIO ] -->
                                        {{--<div class="radio">--}}
                                        {{--<label>--}}
                                        {{--<input type="radio" name="method"/>--}}
                                        {{--<img src="/assets/images/icons/card-2.jpg">--}}
                                        {{--</label>--}}
                                        {{--</div><!-- end [ RADIO ] -->--}}
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="paymentMethod" value="boleto"
                                                       @click="pagamento = 'BOLETO'" :checked="pagamento === 'BOLETO'"/>
                                                <img src="{!! url('assets/images/icons/card-3.jpg') !!}"/>
                                            </label>
                                        </div><!-- end [ RADIO ] -->
                                    </div><!-- end [ BG-FORM METHOD-TYPE ] -->
                                    <p class="color-white">
                                        Esta compra está sendo realizada no <strong>Brasil</strong>
                                        <img src="{!! url('assets/images/1483998903_Brazil_flat.png') !!}" alt="">
                                    </p>
                                </div><!-- end [ COLS ] -->
                                <div class="col-12 col-sm-8 col-md-9">
                                    <div class="bg-form">

                                        @include("cart.elements.card")

                                        <div class="col-xs-12" v-if="pagamento === 'BOLETO'">
                                            <div class="my-line">
                                                <strong>Após finalizar o pedido, será exibido na tela de confirmação
                                                    o link para o boleto.</strong>
                                            </div>
                                            <br/><br/>
                                        </div><!-- end VUE CURRENT METHOD BOLET -->

                                    </div><!-- end [ BG-FORM ] -->
                                </div><!-- end [ COLS ] -->

                            </div><!-- end [ ROW ] -->
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="btn fd-button fd-button-gold float-right mt-5">
                                        @{{ aguarde ? 'AGUARDE...' : 'FINALIZAR PEDIDO' }}
                                    </button>
                                </div>
                            </div><!-- end [ ROW ] -->
                            {!! Form::close() !!}
                        </div><!-- end [ COL-12 ] -->
                    </div><!-- end [ ROW#CART-STEP-2 ] -->

                </div><!-- end [ #CART ] -->


            </div><!-- end [ CONTAINER ] -->
        </main>
    </cart>
@endsection

@section("top-js")
    {!! Html::script($pagseguro_library) !!}
@endsection

@section("js")
    <script type="text/javascript">
        PagSeguroDirectPayment.setSessionId('{!! $session_js !!}');
        $(document).ready(function () {
            let inputHash = $('input[name="senderHash"]');
            let senderHash = setInterval(function () {
                if (inputHash.val() === "") {
                    inputHash.val(PagSeguroDirectPayment.getSenderHash());
                } else {
                    clearInterval(senderHash);
                }
            }, 300);
        });
        Vue.component('cart', {
            props: {
                total: {
                    required: true,
                    default: 0.00
                },
                pagamentoPadrao: {
                    validator: function (value) {
                        return ['CREDITO', 'BOLETO', 'DEBITO'].indexOf(value) !== -1;
                    },
                    default: "CREDITO"
                }
            },
            data: function () {
                return {
                    aguarde: false,
                    pagamento: this.pagamentoPadrao,
                    cartoes: [],
                    parcelas: [],
                    numero: "",
                    bandeira: "",
                    cvv: "",
                    cvvSize: 3,
                    mes: "",
                    ano: ""
                }
            },
            methods: {
                setBandeira: function (value) {
                    this.bandeira = value;
                    return this;
                },
                getBandeira: function(){
                    return this.bandeira;
                },
                setCvvSize: function (value) {
                    this.cvvSize = value;
                    return this;
                },
                getCvvSize: function () {
                    return this.cvvSize;
                },
                /**
                 * Passa as parcelas como array, contendo os campos { quantity, installmentAmount, totalAmount, interestFree }
                 * @param array Array
                 */
                setParcelas: function (array) {
                    let self = this;
                    this.parcelas = [];
                    array.map(function(current){
                        if (current.quantity === 1) {
                            self.parcelas.push({
                                text: "À vista de " + self.moeda(current.totalAmount),
                                value: JSON.stringify(current)
                            });
                        } else {
                            self.parcelas.push({
                                text: ( current.quantity + "x de " + self.moeda(current.installmentAmount) ) + (current.interestFree === true ? " s/juros" : " c/juros"),
                                value: JSON.stringify(current)
                            });
                        }
                    });
                },
                /**
                 * Passo o parâmetro como objeto (resposta do pagseguro) e adiciono todos que estão disponíveis
                 * @param cartoes Object
                 * @returns {cart.methods}
                 */
                setCartoes: function (cartoes) {
                    /** transformo o cartões em array para pegar apenas as keys **/
                    Object.keys(cartoes).map(function (i) {
                        /** pego o cartão que está sendo lido atualmente adiciono na variável current **/
                        let current = cartoes[i];
                        /** se disponível, adiciona na listagem de cartões **/
                        if (current.status === "AVAILABLE") {
                            /** Limito a quantidade de dados **/
                            this.cartoes.push({
                                displayName: current.displayName,
                                name: current.name,
                                image: "https://stc.pagseguro.uol.com.br" + current.images.MEDIUM.path
                            });
                        }
                    }.bind(this));
                    return this;
                },
                /**
                 * Busco na API do Pagseguro os cartões
                 */
                cartoesDisponiveis: function () {
                    let self = this;
                    PagSeguroDirectPayment.getPaymentMethods({
                        amount: self.total,
                        success: function (r) {
                            console.log(r);
                            /** Petodos os metodos de pagamento **/
                            let paymentMethods = r.paymentMethods;
                            /** Filtro os cartões **/
                            self.setCartoes(paymentMethods.CREDIT_CARD.options);
                        },
                        error: function (r) {
                            console.log('error', r);
                        }
                    });
                    return this;
                },
                /**
                 * Passa o formulário que vai ser submetido, isso é feito pois existe um delay de entrega no Token
                 * @param form Form
                 */
                compraCartao: function (form) {
                    let self = this;
                    PagSeguroDirectPayment.createCardToken({
                        cardNumber: self.numero,
                        brand: self.bandeira,
                        cvv: self.cvv,
                        expirationMonth: self.mes,
                        expirationYear: self.ano,
                        success: function(r) {
                            $('input[name="creditCardToken"]').val(r.card.token);
                            form.submit();
                        },
                        error: function (r) {
                            console.log('error', r);
                        }
                    });
                    return this;
                },
                /**
                 * Busca as parcelas referente a bandeira que está sendo passada
                 * @param bandeira String
                 */
                parcelasDisponiveis: function (bandeira) {
                    let self = this;
                    PagSeguroDirectPayment.getInstallments({
                        amount: self.total,
                        brand: bandeira,
                        success: function (r) {
                            r = r.installments;
                            self.setParcelas(r[bandeira]);
                        },
                        error: function(r) {
                            console.log('error', r);
                        }
                    });
                    return this;
                },
                /**
                 * Converete o valor (float) para moedal BRL
                 * @param value
                 * @returns {string}
                 */
                moeda: function (value) {
                    return "R$ " + value
                        .toFixed(2)
                        .replace(".", ",")
                        .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
                },
                submitHandler: function (form) {
                    let self = this;
                    /** Evita o flood de envio **/
                    if (!this.aguarde) {
                        this.aguarde = true;
                        switch (this.pagamento) {
                            case "CREDITO":
                                self.compraCartao(form);
                                break;
                            case "BOLETO":
                                form.submit();
                                break;
                        }
                    } else {
                        alert("Calma!!!");
                    }
                    return false;
                }
            },
            watch: {
                "numero": function (val, old_val) {
                    if (val.length === 6) {
                        let self = this;
                        PagSeguroDirectPayment.getBrand({
                            cardBin: val,
                            success: function (r) {
                                r = r.brand;
                                self.setBandeira(r.name)
                                    .setCvvSize(r.cvvSize)
                                    .parcelasDisponiveis(r.name);
                            },
                            error: function(r) {
                                console.log('error', r);
                            }
                        })
                    }
                }
            },
            mounted: function () {

                this.cartoesDisponiveis();

                let self = this;
                let $formPagamento = $("#form-pagamento");

                $formPagamento.validate({
                    submitHandler: self.submitHandler
                });
            }
        });
    </script>
@endsection