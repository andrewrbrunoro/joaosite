<div class="col-12" v-if="pagamento === 'CREDITO'">

    <div class="radio radio-inline">
        <label data-toggle="tooltip" data-placement="top" title="Master Card" v-for="(cartao, o) in cartoes">
            <input type="radio" name="paymentOption" />
            <img :src="cartao.image"/>
        </label>
    </div><!-- end [ RADIO RADIO-INLINE ] -->

    <br/><br/>

    <div class="row">
        <div class="col-12">
            <div class="form-group{!! $errors->first('creditCardHolderName') ? ' has-error' : '' !!}">
                <label for="creditCardHolderName">
                    Nome <small class="text-danger">obrigatório</small>
                </label>
                {!! Form::text('creditCardHolderName', null, [
                'class' => 'form-control', 'required', 'data-msg-required' => 'Campo obrigatório',
                'data-rule-composto' => 'true', 'data-msg-composto' => 'Digite o nome completo',
                'placeholder' => 'Nome (Impresso no cartão)']) !!}
                {!! $errors->first("creditCardHolderName") ? $errors->first('creditCardHolderName', '<span class="text-danger">:message</span>') : "<small>Impresso no cartão</small>" !!}
            </div>
        </div>
    </div><!-- end [ ROW ] -->

    <div class="row">
        <div class="col-6">
            <div class="form-group{!! $errors->first('creditCardHolderCPF') ? ' has-error' : '' !!}">
                <label for="creditCardHolderCPF">
                    CPF <small class="text-danger">obrigatório</small>
                </label>
                {!! Form::text('creditCardHolderCPF', null, [
                    'class' => 'form-control', 'required', 'data-msg-required' => 'Campo obrigatório',
                    'data-rule-cpf' => 'true', 'data-msg-cpf' => 'Digite um CPF válido',
                    'v-mask' => '"###.###.###-##"', 'placeholder' => '000.000.000-00']) !!}
                {!! $errors->first("creditCardHolderCPF") ? $errors->first('creditCardHolderCPF', '<span class="text-danger">:message</span>') : "<small>CPF do Títular</small>" !!}
            </div>
        </div>
        <div class="col-6">
            <div class="form-group{!! $errors->first('creditCardHolderBirthDate') ? ' has-error' : '' !!}">
                <label for="creditCardHolderBirthDate">
                    Data de nascimento <small class="text-danger">obrigatório</small>
                </label>
                {!! Form::text('creditCardHolderBirthDate', null, ['class' => 'form-control', 'required', 'data-msg-required' => 'Campo obrigatório', 'v-mask' => '"##/##/####"', 'placeholder' => '00/00/0000']) !!}
                {!! $errors->first('creditCardHolderBirthDate', '<span class="text-danger">:message</span>') !!}
            </div>
        </div>
    </div><!-- end [ ROW ] -->

    <div class="row">
        <div class="col-6">
            <label for="number">
                Número do cartão <small class="text-danger">obrigatório</small>
            </label>
            <input type="text" class="form-control" required="required" data-msg-required="Campo obrigatório" v-model="numero" />
        </div>
        <div class="col-3">
            <label for="month">
                Mês <small class="text-danger">obrigatório</small>
            </label>
            <select class="form-control" required data-msg-required="Campo obrigatório" v-model="mes">
                @foreach(meses() as $value => $month)
                    <option value="{!! $value !!}">{!! $month !!}</option>
                @endforeach
            </select>
        </div>
        <div class="col-3">
            <label for="">
                Ano <small class="text-danger">obrigatório</small>
            </label>
            <select class="form-control" required data-msg-required="Campo obrigatório" v-model="ano">
                @for ($i = date('Y'); $i < (date('Y') + 8); $i++)
                    <option value="{!! $i !!}">{!! $i !!}</option>
                @endfor
            </select>
        </div>
    </div><!-- end [ ROW ] -->

    <div class="row">
        <div class="col-6">
            <label for="">
                Parcelas <small class="text-danger">obrigatório</small>
            </label>
            <select name="installments" class="form-control" required="required" data-msg-required="Campo obrigatório">
                <option :value="parcela.value" v-for="parcela in parcelas">
                    @{{ parcela.text }}
                </option>
            </select>
        </div>
        <div class="col-4">
            <label for="">
                Código de Segurança <small class="text-danger">obrigatório</small>
            </label>
            <input type="text" class="form-control" required="required" data-msg-required="Campo obrigatório" v-model="cvv" />
            <small>Verso do cartão</small>
        </div>
    </div><!-- end [ ROW ] -->
</div><!-- end VUE CURRENT METHOD CREDIT CARD -->