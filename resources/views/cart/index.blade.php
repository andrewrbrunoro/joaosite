@extends("layouts.default")

@section("title", "Carrinho")

@section("content")
    <main class="fd-cart">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>Compra de Curso</h2>
                    <hr>
                    <h4><strong>01:</strong> Confira o que você está comprando</h4>
                </div><!-- end [ COL ] -->
            </div><!-- end [ ROW ] -->

            <div class="row" id="cart-step-1">
                <div class="col-12 table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Curso</th>
                            <th>Valor do Curso</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if ($data && $data->PurchaseItem->count())
                            @foreach($data->PurchaseItem as $row)
                                <tr>
                                    <td class="text">
                                        <figure class="image">
                                            <img src="{!! url('assets/images/cursos/brigada_rs.jpg') !!}" alt="{!! $row->name !!}">
                                        </figure>
                                        <p>
                                            <strong>{!! $row->name !!}</strong>
                                        </p>
                                    </td>
                                    <td class="value">
                                        <p>
                                            {!! $row->style_value !!}
                                        </p>
                                    </td>
                                    <td class="options">
                                        {!! Form::open(['route' => 'front.cart.remove', 'method' => 'DELETE']) !!}
                                        <button class="btn" type="submit" name="item_id" value="{!! $row->id !!}">
                                            <i class="far fa-trash-alt"></i>
                                        </button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="3">
                                    <p class="text-white">Nenhum Curso no momento, <a href="{!! route('front.home.index') !!}">clique aqui</a> e veja nossos cursos</p>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table><!-- end [ TABLE ] -->
                </div><!-- end [ COL-12 TABLE-RESPONSIVE -->

                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-md-5 offset-md-2 order-md-2">
                            <div class="cupom">
                                <h3>Você possui cupom de desconto?</h3>
                                <div>
                                    <form class="form-inline" action="javascript:void(0)" method="POST">
                                        <input
                                                type="text"
                                                name="pesquisa"
                                                class="form-control"
                                                required="required"
                                                data-rule-required="true"
                                                data-msg-required="Informe o curso"
                                        > <!-- end [ INPUT[CUPOM] ] -->

                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </div>
                            <img src="{!! url('assets/images/cards.gif') !!}" alt="PagSeguro, VISA, Maestro, MasterCard, AMEX, Boleto" class="mt-3 d-block mx-auto">
                        </div>
                        <div class="col-12 col-md-5 order-md-1">
                            <div class="total">
                                <h3>Total</h3>
                                <ul>
                                    <li>
                                        <span>Subtotal</span>
                                        <span>R$ {!! $data->sub_total_brl !!}</span>
                                    </li>
                                    <li>
                                        <span>Cupom</span>
                                        <span>R$ 0,00</span>
                                    </li>
                                    <li>
                                        <span>Total</span>
                                        <span>R$ {!! $data->total_brl !!}</span>
                                    </li>
                                </ul>
                            </div><!-- end [ TOTAL ] -->
                        </div><!-- end [ COL ] -->
                    </div><!-- end [ ROW ] -->

                    <div class="row">
                        <div class="col-12">
                            <a href="{!! route('front.cart.payment') !!}" class="btn fd-button fd-button-gold float-right">PRÓXIMA ETAPA <i class="fas fa-angle-right"></i></a>
                        </div>
                    </div>
                </div><!-- end [ COL-12 ] -->

            </div><!-- end [ ROW#CART-STEP-1 ] -->
        </div><!-- end [ CONTAINER ] -->
    </main>
@endsection