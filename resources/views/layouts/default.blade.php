<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-language" content="pt-br" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title', 'Página sem título') | {!! setting('nome') !!}</title>

    <!-- BEIGN: META NAME -->
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Fator Digital - https://www.fatordigital.com.br">
    <meta name="robots" content="all">
    <meta name="revisit-after" content="5 day">
    <meta name="identifier-url" content="">
    <meta name="language" content="Portuguese" />
    <!-- END: META NAME -->

    <!-- BEGIN: GOOGLE+ TAGS -->
    <link rel="author" href="https://plus.google.com/(Google+_Profile)/posts"/>
    <link rel="publisher" href="https://plus.google.com/(Google+_Page_Profile)"/>
    <meta itemprop="name" content="">
    <meta itemprop="description" content="">
    <meta itemprop="image" content="">
    <!-- END: GOOGLE+ TAGS -->

    <!-- BEGIN: TWITTER CARD -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="Conta do Twitter do site (incluindo arroba)">
    <meta name="twitter:title" content="Título da página">
    <meta name="twitter:description" content="Descrição da página. No máximo 200 caracteres">
    <meta name="twitter:creator" content="Conta do Twitter do autor do texto (incluindo arroba)">
    <!-- imagens largas para o Twitter Summary Card precisam ter pelo menos 280x150px -->
    <meta name="twitter:image" content="http://www.example.com/image.jpg">
    <!-- END: TWITTER CARD -->

    <!-- BEGIN: OG TAGS -->
    <meta property="og:image" content="">
    <meta property="og:site_name" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="1260">
    <meta property="og:image:height" content="630">
    <meta property="og:type" content="website">
    <!-- END: OG TAGS -->

    <!-- BEGIN: META TAGS DE GEOLOCALIZACAO -->
    <meta name="geo.region" content="BR-RS" />
    <meta name="geo.placename" content="Porto Alegre" />
    <meta name="geo.position" content="-29.560652;-49.904135" />
    <meta name="ICBM" content="-29.560652, -49.904135" />
    <!-- END: META TAGS DE GEOLOCALIZACAO -->

    <!-- BEGIN: CARREGAMENTO DOS FAVICONS -->
    <link rel="apple-touch-icon" sizes="57x57" href="{!! url('assets/images/favicon/apple-icon-57x57.png') !!}">
    <link rel="apple-touch-icon" sizes="60x60" href="{!! url('assets/images/favicon/apple-icon-60x60.png') !!}">
    <link rel="apple-touch-icon" sizes="72x72" href="{!! url('assets/images/favicon/apple-icon-72x72.png') !!}">
    <link rel="apple-touch-icon" sizes="76x76" href="{!! url('assets/images/favicon/apple-icon-76x76.png') !!}">
    <link rel="apple-touch-icon" sizes="114x114" href="{!! url('assets/images/favicon/apple-icon-114x114.png') !!}">
    <link rel="apple-touch-icon" sizes="120x120" href="{!! url('assets/images/favicon/apple-icon-120x120.png') !!}">
    <link rel="apple-touch-icon" sizes="144x144" href="{!! url('assets/images/favicon/apple-icon-144x144.png') !!}">
    <link rel="apple-touch-icon" sizes="152x152" href="{!! url('assets/images/favicon/apple-icon-152x152.png') !!}">
    <link rel="apple-touch-icon" sizes="180x180" href="{!! url('assets/images/favicon/apple-icon-180x180.png') !!}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{!! url('assets/images/favicon/android-icon-192x192.png') !!}">
    <link rel="icon" type="image/png" sizes="32x32" href="{!! url('assets/images/favicon/favicon-32x32.png') !!}">
    <link rel="icon" type="image/png" sizes="96x96" href="{!! url('assets/images/favicon/favicon-96x96.png') !!}">
    <link rel="icon" type="image/png" sizes="16x16" href="{!! url('assets/images/favicon/favicon-16x16.png') !!}">
    <link rel="manifest" href="{!! url('assets/images/favicon/manifest.json') !!}">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="msapplication-TileImage" content="{!! url('assets/images/favicon/ms-icon-144x144.png') !!}">
    <meta name="theme-color" content="#000000">
    <!-- END: CARREGAMENTO DOS FAVICONS -->

    <!-- BEGIN: FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,800" rel="stylesheet">
    <!-- END: FONTS -->

    <!-- BEGIN: CSS -->
    {!! Html::style('assets/vendors/fatordigital.com.br/bootstrap.min.css') !!}
    {!! Html::style('assets/vendors/owl-carousel/css/owl.theme.default.min.css') !!}
    {!! Html::style('assets/vendors/owl-carousel/css/owl.carousel.min.css') !!}
    {!! Html::style('assets/vendors/sweetalert/sweetalert.min.css') !!}
    {!! Html::style('assets/vendors/fatordigital.com.br/animate.css') !!}
    {!! Html::style('assets/vendors/jquery/jquery-ui.css') !!}
    {!! Html::style('assets/vendors/redactor/redactor.min.css') !!}
    {!! Html::style('assets/css/main.css') !!}
    <!-- END: CSS -->

    @yield("css")

    <!-- BEGIN: SCRIPTS -->
    <script>
        var APP_ROOT = '{!! url('/') !!}/',
            FULL_URL = '{!! request()->fullUrl() !!}';
    </script>
    {!! Html::script('assets/vendors/font-awesome/5.0.6.js') !!}
    <!-- END: SCRIPTS -->

    @yield("top-js")

</head>
<body>

<div id="app">

    @include("layouts.elements.navbar")

    @yield("content")

    <footer>

        <div class="container flex-container">
            <a href="{!! url('/') !!}">
                <figure>
                    <img src="{!! url('assets/images/logos/daviandre.png') !!}" alt="" />
                </figure>
            </a>
            <p>
                Todos os direitos reservados – 2018 | Davi André Costa Silva EIRELE ME – CNPJ 14.206.108.0001-40
            </p>

            <ul>
                <li>
                    <a href="" target="_blank">
                        <img src="{!! url('assets/images/icons/facebook.png') !!}" alt="Facebook - Davi André"/>
                    </a>
                </li>
                <li>
                    <a href="" target="_blank">
                        <img src="{!! url('assets/images/icons/youtube.png') !!}" alt="Youtube - Davi André"/>
                    </a>
                </li>
                <li>
                    <a href="" target="_blank">
                        <img src="{!! url('assets/images/icons/instagram.png') !!}" alt="Instagram - Davi André"/>
                    </a>
                </li>
            </ul>

        </div>
        <div class="container">
            <section class="fd_signature">
                <a href="https://www.fatordigital.com.br" target="_blank" title="Fator Digital">
                    Fator Digital <div id="fator_signature"></div>
                </a>
            </section>
        </div>
    </footer>
</div>

<!-- BEGIN: SCRIPTS -->
{!! Html::script('assets/vendors/fatordigital.com.br/jquery.min.js') !!}
{!! Html::script('assets/vendors/vue/vue.js') !!}
{!! Html::script('assets/vendors/axios/axios.js') !!}
{!! Html::script('assets/vendors/fatordigital.com.br/jqueryui.min.js') !!}
{!! Html::script('assets/vendors/jquery/jquery.ui.touch-punch.min.js') !!}
{!! Html::script('assets/vendors/popper/popper.min.js') !!}
{!! Html::script('assets/vendors/fatordigital.com.br/bootstrap.min.js') !!}
{!! Html::script('assets/vendors/mask/mask.min.js') !!}
{!! Html::script('assets/vendors/jquery-validate/jquery.validate.min.js') !!}
{!! Html::script('assets/vendors/swiffy/swiffy-v7.4.js') !!}
{!! Html::script('assets/vendors/fatordigital.com.br/fator.js') !!}
{!! Html::script('assets/vendors/owl-carousel/js/owl.carousel.min.js') !!}
{!! Html::script('assets/vendors/sweetalert/sweetalert.min.js') !!}
{!! Html::script('assets/vendors/jquery/jquery.match-height.js') !!}
{!! Html::script('assets/vendors/redactor/redactor.min.js') !!}
{!! Html::script('assets/vendors/vue-mask/vue-mask.min.js') !!}
{!! Html::script('assets/js/validator.js') !!}

@yield("js")

{!! Html::script('assets/js/main.js') !!}

@if (session()->has("success"))
    <script type="text/javascript">
        loadSwal(0, "{!! setting('nome') !!}", "{!! session("success") !!}", 'success.png');
    </script>
@elseif(session()->has("error"))
    <script type="text/javascript">
        loadSwal(1, 'Erro', "{!! session("error") !!}", 'error.png');
    </script>
@endif

<script>
    $(document).ready(function(){
        var stage = new swiffy.Stage(document.getElementById('fator_signature'),
            swiffyobject, {});
        stage.setBackground(null);
        stage.start();
    });
</script>
<!-- END: SCRIPTS -->
</body>
</html>
