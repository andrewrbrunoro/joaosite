<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="#" data-target=".fd-mobile">
            <i class="fa fa-bars"></i> MENU
        </a>
        <a class="navbar-brand logo" href="{!! url('/') !!}">
            <img src="{!! url('assets/images/logos/daviandre.png') !!}" alt="{!! setting('nome') !!}"/>
        </a>
        <div class="d-none d-lg-flex">
            <ul class="navbar-nav">

                <li class="nav-item">
                    <a class="nav-link active" href="{!! route('front.home.index') !!}">cursos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{!! route('front.content.free') !!}">conteúdo gratuito</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{!! route("front.contact.index") !!}">contato</a>
                </li>
                <li class="nav-item">
                    <form class="form-inline" action="javascript:void(0)" method="POST">
                        <input
                                type="text"
                                name="pesquisa"
                                class="form-control"
                                placeholder="Pesquise pelo curso desejado"
                                required="required"
                                data-rule-required="true"
                                data-msg-required="Informe o curso"
                        > <!-- end [ INPUT[PESQUISA] ] -->

                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </li>
            </ul>
        </div><!-- end [ NAVBAR-COLLAPSE ] -->
        <ul class="navbar-nav ml-auto fd-login">
            <li class="nav-item">
                @if (auth()->guest())
                    <a class="navbar-brand" href="#" data-target=".fd-login-menu">
                        <img src="{!! url('assets/images/icons/user.png') !!}" alt="Faça seu Login"/> login
                    </a>
                @else
                    <a href="#" data-target=".user-options" class="navbar-brand user-header">
                        <figure class="user-img">
                            <img src="{!! url('assets/images/icons/user.png') !!}" alt="{!! auth()->user()->name !!}">
                        </figure>
                        <h6>Olá, <strong>{!! auth()->user()->name !!}</strong></h6>
                    </a><!-- end [ USER-HEAD ] -->
                @endif
            </li>
        </ul>
    </div><!-- end [ CONTAINER ] -->
</nav><!-- end [ NAVBAR ] -->


<nav class="navbar fd-mobile">
    <a href="#" data-target=".fd-mobile" class="fd-close-menu">FECHAR</a>
    <ul class="navbar-nav">
        <li>
            <a class="navbar-brand" href="#" data-target=".fd-mobile"><i class="fa fa-bars"></i> MENU</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{!! route('front.home.index') !!}">Cursos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{!! route('front.content.free') !!}">Conteúdo Gratuito</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#vitoriosos">Vitoriosos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#professores">Professores</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#noticias">Notícias</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#contato">Contato</a>
        </li>
        <li class="nav-item d-lg-none">
            <form class="form-inline" action="javascript:void(0)" method="POST">
                <input
                        type="text"
                        name="pesquisa"
                        class="form-control"
                        placeholder="Pesquise pelo curso desejado"
                        required="required"
                        data-rule-required="true"
                        data-msg-required="Informe o curso"
                > <!-- end [ INPUT[PESQUISA] ] -->

                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
        </li>
    </ul><!-- end [ NAVBAR-NAV ] -->

    <ul class="nav-bottom">
        <li>
            <a href="">Políticas de Privacidade</a>
        </li>
        <li>
            <a href="">Polítivas Antipiratarias</a>
        </li>
        <li>
            <a href="">Requisítos de Acesso</a>
        </li>
        <li>
            <a href="">Cancelamento</a>
        </li>
    </ul><!-- end [ UL - NAV-BOTTOM ] -->
</nav><!-- end [ FD-MOBILE ] -->

@if (auth()->guest())
    <nav class="fd-login-menu{!! $errors->first('email') || $errors->first('password') ? ' active' : '' !!}">
        <a href="#" data-target=".fd-login-menu" class="fd-close-menu">FECHAR</a>
        <ul class="navbar-nav">
            <li>
                <a class="navbar-brand" href="#" data-target=".fd-login-menu">
                    <img src="{!! url('assets/images/icons/user.png') !!}" alt="Login"/> Login
                </a>
            </li>
        </ul>
        {!! Form::open(['route' => 'front.auth.login', 'class' => 'fd-login-validate']) !!}
            <div class="fd-group">
                {!! Form::text('email', null, ['class' => 'fd-input', 'required', 'data-rule-required' => 'true', 'data-msg-required' => 'Preencha o campo corretamente']) !!}
                {!! $errors->first('email', '<label class="error">:message</label>') !!}
                <label class="fd-label">Seu Usuário</label>
                <img src="{!! url('assets/images/svg/avatar.svg') !!}" alt="Usuário" class="fd-svg">
            </div>
            <div class="fd-group">
                {!! Form::password('password', ['class' => 'fd-input', 'required', 'data-rule-required' => 'true', 'data-msg-required' => 'Preencha o campo corretamente']) !!}
                {!! $errors->first('password', '<label class="error">:message</label>') !!}
                <label class="fd-label">Sua senha</label>
                <img src="{!! url('assets/images/svg/open-lock.svg') !!}" alt="Senha" class="fd-svg">
            </div>
            <div class="fd-row">
                <a href="#cadastrar" class="btn fd-button fd-button-white">cadastrar</a>
                <button type="submit" class="btn fd-button fd-button-gold">entrar</button>
            </div>
            <a href="" class="link float-right">Esqueceu sua senha?</a>
        {!! Form::close() !!}
    </nav><!-- end [ FD-LOGIN-MENU ] -->
@endif

@if (auth()->check())
    <nav class="user-options">
        <a href="#" data-target=".user-options" class="fd-close-menu">FECHAR</a>
        <div class="user-options-content">
            <div class="user-header">
                <figure class="user-img" data-notification="1">
                    <img src="{!! url('assets/images/icons/user.png') !!}" alt="Usuário">
                </figure>
                <h6>Olá, <strong>{!! auth()->user()->name !!}</strong></h6>
            </div><!-- end [ USER-HEAD ] -->

            <div class="user-body">
                <p class="text-muted">
                    Gerenciador
                </p>
                <hr/>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <nav>
                                <div class="nav nav-tabs nav-justified" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" href="#meus-dados"
                                       role="button" aria-controls="nav-home" aria-selected="true">
                                        <img src="{!! url('assets/images/svg/settings-work-tool.svg') !!}" class="fd-svg"/>
                                    </a>
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile"
                                       role="tab" aria-controls="nav-profile" aria-selected="false">
                                        <img src="{!! url('assets/images/svg/pallete.svg') !!}" class="fd-svg"/>
                                    </a>
                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact"
                                       role="tab" aria-controls="nav-contact" aria-selected="false">
                                        <img src="{!! url('assets/images/svg/conversation-speech-bubbles.svg') !!}"
                                             class="fd-svg"/>
                                    </a>
                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-con"
                                       role="tab" aria-controls="nav-contact" aria-selected="false">
                                        <img src="{!! url('assets/images/svg/headphone-symbol.svg') !!}" class="fd-svg"/>
                                    </a>
                                    <a class="nav-item nav-link" id="nav-contact-tab" href="{!! route('front.auth.logout') !!}">
                                        <img src="{!! url('assets/images/svg/power-button-off.svg') !!}" class="fd-svg"/>
                                    </a>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                     aria-labelledby="nav-home-tab">...
                                </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                     aria-labelledby="nav-profile-tab">...
                                </div>
                                <div class="tab-pane fade" id="nav-contact" role="tabpanel"
                                     aria-labelledby="nav-contact-tab">

                                    <h5>AJUDA</h5>
                                    <p><strong>Não encontrou o que procura?</strong></p>
                                    <p>Que cursos ou professor você gostaria de ver na Plataforma Davi André?</p>
                                    <form action="" class="fd-validate">
                                        <div class="fd-group">
                                            <input type="text"
                                                   name="nome"
                                                   class="fd-input"
                                                   required="required"
                                                   data-rule-required="true"
                                                   data-msg-required="Preencha o campo corretamente"
                                            >
                                            <label class="fd-label">Nome</label>
                                        </div>
                                        <div class="fd-group">
                                            <input type="email"
                                                   name="email"
                                                   class="fd-input"
                                                   required="required"
                                                   data-rule-required="true"
                                                   data-msg-required="Preencha o campo corretamente"
                                                   data-email-required="true"
                                                   data-msg-email="Preencha o campo corretamente"
                                            >
                                            <label class="fd-label">E-mail</label>
                                        </div>
                                        <div class="fd-group">
                                        <textarea name="mensagem"
                                                  class="fd-input"
                                                  required="required"
                                                  data-rule-required="true"
                                                  data-msg-required="Preencha o campo corretamente"
                                        >
                                        </textarea>
                                            <label class="fd-label">Sua Mensagem</label>
                                        </div>
                                        <button type="submit" class="btn fd-button">Enviar</button>
                                    </form>
                                </div>
                            </div>
                        </div><!-- end [ COL-12 ] -->
                    </div><!-- end [ ROW ] -->
                </div><!-- end [ CONTAINER ] -->
            </div><!-- end [ USER-BODY ] -->

        </div>
    </nav><!-- end [ USER-OPTIONS ] -->
@endif