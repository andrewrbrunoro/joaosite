@extends("layouts.default")

@section("title", "Portal")

@section("content")


    <main class="fd-home">
        @if (isset($banners) && $banners->count())
            <header class="fd-header">
                <div class="container">
                    <div class="owl-carousel owl-theme" data-carousel-page="home" data-position="main">
                        @foreach($banners as $banner)
                            <a href="{!! $banner->url !!}" class="item">
                                <figure class="d-none d-md-block">
                                    <img src="{!! $banner->image !!}" alt="{!! $banner->alt !!}">
                                </figure>
                                @if ($banner->mobile)
                                    <figure class="d-md-none">
                                        <img src="{!! url('assets/images/backgrounds/carousel-mobile2.jpg') !!}" alt="">
                                    </figure>
                                @endif
                            </a>
                        @endforeach
                    </div>
                </div>
            </header>
        @endif

        <section class="fd-home-cursos">

            <vitrine-home />
            @include("components.showcases", compact("showcases"))

        </section><!-- end fd-home-cursos -->
    </main>

@endsection

@section("js")
    {!! Html::script('assets/js/components/vitrine.js') !!}
@endsection