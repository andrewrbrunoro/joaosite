Vue.use(VueTheMask);
new Vue({
    el : '#app',
    data: function () {
        return {
            global: {
                curso: {},
                vitrine: {}
            }
        }
    }
});

let _swal_timer = 5000000;

let _swal = [
    {
        type: 'success',
        timer: _swal_timer,
        size: 700,
        background: '#1e1e1e',
        position: '',
        button_color: '#E04569'
    },
    {
        type: 'error',
        timer: 50000000,
        size: 700,
        background: '#1e1e1e',
        position: '',
        button_color: '#E04569'
    }
];

function loadSwal($indice, $title, $message, $image) {
    swal({
        type: '',
        title: $title,
        html: $message,
        imageUrl: APP_ROOT + 'assets/images/swal/' + $image,
        position: _swal[$indice].position,
        padding: 100,
        showConfirmButton: false,
        background: _swal[$indice].background,
        width: _swal[$indice].size,
        timer: _swal[$indice].timer,
        confirmButtonColor: _swal[$indice].button_color,
        showCloseButton: true
    });
}

let BRMask = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    mask_options = {
        onKeyPress: function (val, e, field, options) {
            field.mask(BRMask.apply({}, arguments), options);
        }
    };

function loadSVG(s, c) {
    if (s) {
        $(c).each(function () {
            let $img = $(this),
                imgID = $img.attr('id'),
                imgClass = $img.attr('class'),
                imgURL = $img.attr('src');

            $.get(imgURL, function (data) {
                let $svg = $(data).find('svg');
                if (typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                if (typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass + ' replaced-svg');
                }
                $svg = $svg.removeAttr('xmlns:a');
                if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                    $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
                }
                $img.replaceWith($svg);
            }, 'xml');
        });
    }
}

function popUp(url, name, width, height) {

    let frog = window.open(url, name, "width=" + width + ",height=" + height + ",scrollbars=1,resizable=1");

    let text = document.form.input.value;

    let html = "<html><head></head><body>Hello, <b>" + text + "</b>.";
    html += "How are you today?</body></html>";

    frog.document.open();
    frog.document.write(html);
    frog.document.close();

}

function printContent(t, n) {

    let w = window.open('', 'PRINT', 'height=400,width=600');

    w.document.write('<html><head><title>Anotação: ' + n + '</title>');
    w.document.write('</head><body >');
    w.document.write('<h4>Anotação ' + n + '</h4>');
    w.document.write(t);
    w.document.write('</body></html>');

    w.document.close();
    w.focus();

    w.print();
    w.close();

    return true;
}

$(document).ready(function () {

    let redactor = $('.redactor');
    redactor.redactor({
        buttonsHide: ['format', 'link', 'deleted', 'html'],
        minHeight: 250,
        maxHeight: 250,
        keyupCallback: function(e)
        {
            let elementIndex = redactor.index(this.$element);
            redactor.not(':eq('+elementIndex+')').redactor('code.set', this.code.get());
        }
    });

    // TROCA A TAG IMG POR SVG
    loadSVG(true, '.fd-svg');

    // FUNÇÃO DE CLICK PARA ABERTURA/FECHAMENTO DOS MENUS
    let openmenu = $('a.navbar-brand[data-target]').not('.logo');
    openmenu.on('click', function (event) {
        event.preventDefault();
        let _this = $(this),
            target = _this.data('target');
        openmenu.removeClass('active');
        $(this).addClass('active');
        $(target).toggleClass('active');
    });


    let closeMenu = $('.fd-close-menu');
    closeMenu.click( function(event) {
        event.preventDefault();
        let t = $(this),
            target = $(t).data('target');
        $(target).removeClass('active');
        $(openmenu).removeClass('active');
    });

    let closeAnotations = $('.fd-close-anotations');
    closeAnotations.click( function(event) {
        event.preventDefault();
        let t = $(this),
            target = $(t).data('target');

        if(target === "#anotations-bottom"){
            $(target).addClass('d-none');
        } else {
            $(target).removeAttr('style');
            $('.video-container').removeAttr('style');
        }
    });

    $("#img-input").change(function () {

        let input = this;
        let output = $('#img-output');

        if (input.files && input.files[0]) {
            let reader = new FileReader();

            reader.onload = function (e) {
                output.attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    });

    // CAROUSEL CONFIG
    // passada por data attr
    setTimeout(function () {

        let fdCarousel = $('.owl-carousel[data-position="cursos"]'),
            fdCarouselHome = $('.owl-carousel[data-position="main"]');


        fdCarouselHome.owlCarousel({
            nav: false,
            loop: true,
            dots: true,
            margin: 0,
            items: 1
        });




        //  OPACITY ON HOVER
        let $el = $('a[data-video-id]');
        $el.hover(
            function () {
                $(this).addClass("is-active");
            }, function () {
                $(this).removeClass("is-active");
            }
        );


        //  INCLUSÃO DO MODAL COM VIDEO
        $el.click(function (e) {
            e.preventDefault();

            let iframe = "https://www.youtube.com/embed/" + $(this).data('video-id');
            let dropdack = $('<div class="fd-dropback"></div>');
            let close = '<a href="javascript:void(0)" class="fd-dropback-close"></a>';
            let embed = '<div class="container"><div class="fd-embed">' +
                close +
                '<div class="embed-responsive embed-responsive-16by9">' +
                '<iframe class="embed-responsive-item" src=' + iframe + ' frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>' +
                '</div>' +
                '</div></div>';


            dropdack.append(embed);

            $('main').after(dropdack);
            dropdack.animate({'opacity': 1}, 250);

            dropdack.click(function () {
                $(this).animate({'opacity': 0}, 250, function () {
                    $(this).remove();
                });
            });
        });

    }, 200);




    // VERIFICACAO DE CONTEUDO NOS INPUTS DO FORM
    let inputs = $('.fd-input');
    inputs.blur(function () {
        $(this).val() ? $(this).addClass("used") : $(this).removeClass("used")
    });

    // MASCARAS DE INPUT VIA DATA ATTR

    $('[data-fd-mask="date"]').mask('00/00/0000');
    $('[data-fd-mask="phone"]').mask(BRMask, mask_options);
    $('[data-fd-mask="cep"]').mask('00000-000');
    $('[data-fd-mask="number"]').mask('00000');


    // TOOLTIP CONFIGS
    if($(window).width() > 767){
        $('[data-toggle="tooltip"]').tooltip({
            trigger: 'hover'
        });
    }

    // MODAL FORM
    let modalForm = $('.fd-modal-curso');
    let closeModal = $('.fd-modal-curso-close');

    closeModal.click(function () {
        $('.fd-backdrop-curso').delay(250).fadeOut(350);
        modalForm.fadeOut(350);
    });


    // MUDANCA DO ICONE DE ADD AOS FAVORITOS
    let q = $('#course a.add-to-fav');
    if (!q.hasClass('added')) {
        q.hover(
            function () {
                $(this).find('.fa-star').attr('data-prefix', 'fas');
            }, function () {
                $(this).find('.fa-star').attr('data-prefix', 'far');
            }
        );
    }
    q.click(function () {
        $(this).addClass('added');
        $(this).find('.fa-star').attr('data-prefix', 'fas');
    });


    // HABILITAR E DESABILITAR FUNCOES DA VIDEO AULA
    let courseOptions = $('.btn[data-course-options]');

    courseOptions.click(function (event) {
        event.preventDefault();
        let data = $(this).data('course-options');
        // let target = $('.course-options .fd-dropup > a');
        let target = $(this).parents('.course-options').find('.fd-dropup > a');
        if (data == true) {
            target.toggleClass('disabled');
        }
    });

    // DROPUP CUSTOM FUNCTION
    let dropup = $('.fd-dropup');
    let dropupAction = dropup.find('>a');

    dropupAction.click(function (e) {
        e.preventDefault();

        if (!$(this).parent(dropup).hasClass('active')) {
            $(this).parent(dropup).addClass('active');
        } else {
            dropup.removeClass('active');
        }
    });

});


// AREA DO ALUNO
$(document).ready(function () {
    let video = $('.course-video .embed-responsive-item');
    let target = $('#app');
    let options = $('.course-options .col-12');
    let inputs = $('input.form-control, input[type="radio"]');






    // INCLUI UM BACKGROUND ATRAZ DO VIDEO E
    // ALTERA O Z-INDEX DO VIDEO E OPCOES
    let cl = $('#change-lights-button');
    cl.click(function () {

        $(this).toggleClass('active');


        if (!$(this).hasClass('active')) {
            $('.class-dropback').remove();
            options.removeAttr('styles');
            video.removeAttr('styles');
        } else {
            target.append('<div class="class-dropback"></div>');
            options.css({'z-index': '9951'});
            video.css({'z-index': '9951'});
        }
    });

    $('.class-dropback').click(function () {
        $(this).remove();
        options.removeAttr('styles');
        video.removeAttr('styles');
    });


    // DROPUP PARA A AVALIAÇÃO DA AULA
    let a = $('.fd-dropup.evaluation form');
    a.each(function () {
        let $this = $(this);
        $this.validate({

            // REGRAS DE VALIDAÇÃO
            rules: {
                eval: {
                    required: true
                }
            },
            messages: {
                eval: {
                    required: "Escolha uma opção acima"
                }
            },

            // SUBMISSÃO DO FORMULÁRIO
            submitHandler: function (form) {
                let dados = $(form).serialize(),
                    btn = $(form).find('button[type="submit"]');

                btn.text('Enviando ...').addClass('disabled');
                loadSwal(0, 'Sua avaliação foi enviada', 'Agrademos pela sua avaliação, ela é muito importante para cada vez mais melhorarmos nossas aulas.', 'success.png');

                $.ajax({
                    type: 'POST',
                    // url: APP_ROOT + 'mail.php',
                    data: dados,
                    dataType: 'json',

                    success: function (data) {
                        // gtag('event', 'page_view', {
                        //     'page_path': '/goal/...'
                        // });
                        setTimeout(function () {
                            inputs.val('');
                            btn.text('ENVIAR AVALIAÇÃO').removeClass('disabled');
                            loadSwal(0, 'Sucesso', data.msg, 'success.png');
                        }, 500);
                    },
                    error: function (error) {
                        error = error.responseJSON;
                        setTimeout(function () {
                            btn.text('INSCREVA-SE').removeClass('disabled');
                            loadSwal(1, 'Erro', error.msg, 'error.png');
                        }, 5000)
                    }
                });

                return false;
            }
        });
    });



    // EXIBE O DROPUP CLICADO E FECHA OS DEMAIS
    $('.fd-item > a').click(function (e) {

        e.preventDefault();

        let p = $('.fd-item > .fd-dropup');
        let t = $(this).data('target');

        if (!$(t).hasClass('active')) {
            $(p).removeClass('active');
            $(t).addClass('active');
        } else {
            $(t).removeClass('active');
            $(p).removeClass('active');
        }

    });

    // CRIA O POPUP
    $('.fd-dropup .popup').click(function () {

        let url = $(this).data('popup-url');
        let name = $(this).data('popup-title');

        popUp(url, name, 500, 800);
    });


    // CARREGA O MODAL COM AS INFORMACOES E INCLUI O RESIZE E DRAG AND DROP
    // COM O RESPECTIVO CONTEÚDO DAS AULAS
    let course_modal = $('#course-schedule-popup');
    let course_modal_open = $('.cronograma-popup');
    let course_modal_close = $('.cronograma-popup-close');

    course_modal_open.click(function () {
        course_modal
            .fadeIn(350)
            .draggable({
                start: function () {
                    $(this).css({
                        transform: "none",
                        top: $(this).offset().top + "px",
                        left: $(this).offset().left + "px"
                    });
                }
            }).resizable();

        course_modal_close.click(function () {
            course_modal.fadeOut(350);
        });
    });

    // MATCH HEIGHT CONFIG
    // SETA A MESMA ALTURA NOS ELEMENTOS QUE ESTÃO NA MESMA .ROW E POSSUAM A CLASSE .fd-eq
    let fd_eq = $('.fd-eq');
    let fd_eq_opt = {
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    };
    fd_eq.matchHeight(fd_eq_opt);


    // ALTERA A LARGURA DO VÍDEO E O ELEMENTO ATIVO AO SEU LADO ( MATERIAL DE APOIO, ANOTAÇÕES... )
    let videoSize = $('.video-size-options a');

    videoSize.on('click', function () {
        let _this = $(this);

        videoSize.removeClass('active');
        _this.addClass('active');

        let video = $('.video-container'),
            iframe = $('.iframe-container'),
            data = _this.data('props'),
            time = 500;

        video.addClass('active');
        iframe.addClass('active');
        let vSize = 66,
            iSize = 33;

        // VERIFICAÇÃO DE QUAL TAMANHO DEVERÁ SER EXIBIDO
        if (data === 'width-2-1') {
            vSize = 66;
            iSize = 33;
        }
        if (data === 'width-1-2') {
            vSize = 33;
            iSize = 66;
        }
        if (data === 'width-1-1') {
            vSize = 50;
            iSize = 50;
        }
        if (data === 'width-100-100') {
            vSize = 100;
            iSize = 100;
        }

        video.animate({'width': vSize+'%'}, time);
        iframe.animate({'width': iSize+'%'}, time);
    });


    let slides = $('#support-material .slides');
    let sumario = $('#support-material .sumario');
    let anotationsSide = $('#anotations .side');
    let anotationsBottom = $('#anotations .bottom');

    function cleanAttr() {
        $('.video-container').removeAttr('style');
        $('.iframe-container').removeAttr('style');
        $('#funcionalidades').removeAttr('style');
        $('.slides-content').removeAttr('style');
        $('.anotacoes').removeAttr('style');
        $('.video-size-options').removeAttr('style').removeClass('d-flex justify-content-end');
    }

    slides.click(function (e) {
        e.preventDefault();

        let _this = $(this);
        cleanAttr();
        sumario.removeClass('is-active');
        _this.parents('#support-material').removeClass('active');

        if (!_this.hasClass('is-active')) {
            _this.addClass('is-active');
            $('.video-container').css({'float': 'left'});
            $('.slides-content').css({
                'width': '33%',
                'display': 'block',
                'float': 'left'
            }).addClass('is-active');
            $('#funcionalidades').css({
                'float': 'left',
                'width': '66%'
            });
            $('.video-size-options').css({
                'float': 'left',
                'width': '33%',
            }).addClass('d-flex justify-content-end');
        } else {
            _this.removeClass('is-active');
        }

    });

    sumario.click(function (e) {
        e.preventDefault();

        let _this = $(this);
        cleanAttr();
        slides.removeClass('is-active');
        _this.parents('#support-material').removeClass('active');

        if (!_this.hasClass('is-active')) {
            _this.addClass('is-active');
            $('.video-container').css({'float': 'left'});
            $('.iframe-container').css({
                'width': '33%',
                'display': 'block',
                'float': 'left'
            }).addClass('is-active');
            $('#funcionalidades').css({
                'float': 'left',
                'width': '66%'
            });
            $('.video-size-options').css({
                'float': 'left',
                'width': '33%',
            }).addClass('d-flex justify-content-end');
        } else {
            _this.removeClass('is-active');
        }
    });

    anotationsSide.click(function (e) {
        e.preventDefault();

        let _this = $(this);
        cleanAttr();
        slides.removeClass('is-active');

        if (!_this.hasClass('is-active')) {
            $('.video-container').css({'float': 'left'});
            $('#anotations-side').css({
                'width': '33%',
                'display': 'block',
                'float': 'left'
            });
            $('#anotations-bottom').addClass('d-none');
            $('.iframe-container, .slides-content').removeClass('is-active');
        }

    });


    anotationsBottom.click(function (e) {
        e.preventDefault();
        let target = $('#anotations-bottom');

        $('.anotacoes').removeAttr('style');
        target.removeClass('d-none');

        if(!$('.slides-content').hasClass('is-active')){
            $('.video-container').removeAttr('style');
        }
        if(!$('.iframe-container').hasClass('is-active')){
            $('.video-container').removeAttr('style');
        }
    });

    $('.btn-print').click(function () {

        let r = $('#main-redactor').redactor('code.get');
        printContent(r, 'Aula 01');
    });
});

$(document).ready(function () {

    let login_form = $('.fd-login-menu .fd-login-validate');
    login_form.validate({
        submitHandler: function (form) {
            let btn = $(form).find('button');
            btn.html('<i class="fas fa-sync fa-spin"></i> <span style="margin-left: 5px;">AGUARDE</span>');
            return true;
        }
    });


    let fdCarousel = $('[data-position="cursos"]');
    fdCarousel.owlCarousel({
        nav: false,
        navText: ['<img src="' + APP_ROOT + 'assets/images/icons/carousel-icon.png" class="prev">', '<img src="' + APP_ROOT + 'assets/images/icons/carousel-icon.png" class="next">'],
        loop: false,
        margin: 20,
        responsive: {
            0: {
                items: 1
            },
            576: {
                items: 2,
                dots: false,
                nav: true
            },
            992: {
                items: 3
            }
        }
    });
});