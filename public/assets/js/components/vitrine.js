Vue.component('vitrine-home', {
    template:   `
    <div>
        <div class="container" v-for="vitrine in vitrines">
            <div class="fd-row">
                <h2>{{ vitrine.curso }}</h2>
                <div class="owl-carousel owl-theme" data-carousel-page="home-cursos" data-position="cursos">

                    <a v-bind:href="v.link" class="item fd-card" v-for="(v) in vitrine.turmas">
                        <div class="fd-card-background" v-bind:style="{ backgroundImage : 'url('+v.imagem+')' }"></div>
                        <div class="fd-card-content">
                            <h3>{{ v.titulo }}
                                <small v-if="v.tipo != ''">{{ v.tipo }} - {{v.q_aulas }}</small>
                                <small v-else>{{ v.tipo }} - {{v.q_aulas }}</small>
                            </h3>
                        </div>
                        <div class="fd-card-footer">
                            <div class="btn fd-button">veja mais</div>
                            <p v-html="v.valor_label"></p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>`,
    data: function(){
        return {
            vitrines: []
        };
    },
    created: function() {
        axios.get(APP_ROOT + "api/vitrines").then(function(r){
            this.vitrines = r.data;
        }.bind(this));
    },
    mounted: function () {
        setTimeout(function(){
            let fdCarousel = $('[data-position="cursos"]');
            fdCarousel.owlCarousel({
                nav: false,
                navText: ['<img src="' + APP_ROOT + 'assets/images/icons/carousel-icon.png" class="prev">', '<img src="' + APP_ROOT + 'assets/images/icons/carousel-icon.png" class="next">'],
                loop: false,
                margin: 20,
                responsive: {
                    0: {
                        items: 1
                    },
                    576: {
                        items: 2,
                        dots: false,
                        nav: true
                    },
                    992: {
                        items: 3
                    }
                }
            });
        }, 800);
    }
});