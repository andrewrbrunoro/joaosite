$(document).ready(function(){
    $("#form-report-error").validate({
        submitHandler: function(form) {
            let btn = $(form).find('button[type="submit"]');

            // Deixa o botão desabilitado até retornar o ajax
            btn.text('Enviando ...').addClass('disabled');

            axios.post(APP_ROOT + 'reportar/erro/aula', {
                team_id: TEAM_ID,
                lesson_id: LESSON_ID,
                url: FULL_URL,
                text: $('textarea[name="reportar-erro"]').val()
            }).then(function(r){
                loadSwal(0, 'Muito Obrigado', r.data.message, 'success.png');
                form.reset();
                btn.text('Reportar').removeClass('disabled');
            }).catch(function(r) {
                r = r.response;
                loadSwal(1, 'Erro', r.data.message, 'error.png');
                btn.text('Reportar').removeClass('disabled');
            });
            return false;
        }
    });

    $("#form-report-question").validate({
        submitHandler: function(form) {
            let btn = $(form).find('button[type="submit"]');

            // Deixa o botão desabilitado até retornar o ajax
            btn.text('Enviando ...').addClass('disabled');

            axios.post(APP_ROOT + 'reportar/duvida/aula', {
                lesson_id: LESSON_ID,
                text: $('textarea[name="texto-duvida"]').val()
            }).then(function(r){
                loadSwal(0, 'Muito Obrigado', r.data.message, 'success.png');
                form.reset();
                btn.text('Enviar Dúvida').removeClass('disabled');
            }).catch(function(r) {
                r = r.response;
                loadSwal(1, 'Erro', r.data.message, 'error.png');
                btn.text('Enviar Dúvida').removeClass('disabled');
            });
            return false;
        }
    });

    $("#form-rating").validate({
        submitHandler: function(form) {
            let btn = $(form).find('button[type="submit"]');

            // Deixa o botão desabilitado até retornar o ajax
            btn.text('Enviando ...').addClass('disabled');

            axios.post(APP_ROOT + 'avaliar/aula', {
                lesson_id: LESSON_ID,
                rating: $('input[name="rating"]:checked').val(),
                text: $('textarea[name="avaliacao-de-aula"]').val()
            }).then(function(r){
                loadSwal(0, 'Muito Obrigado', r.data.message, 'success.png');
                btn.text('Enviar Avaliação').removeClass('disabled');
            }).catch(function(r) {
                r = r.response;
                loadSwal(1, 'Erro', r.data.message, 'error.png');
                btn.text('Enviar Avaliação').removeClass('disabled');
            });
            return false;
        }
    });
});

Vue.component('curso', {
    props: {
        lessons: {
            type: Object,
            default: function () {
                return {}
            }
        }
    },
    data: function(){
        return {
            curso: this.lessons,
            aula: {},
            isActive: false
        };
    },
    methods: {
        setConfig: function (key, value, team_id) {
            axios.post(APP_ROOT + 'configuracao/turma', {
                team_id: team_id,
                key: key,
                value: value
            });
        },
        favorite: function (aula) {
            axios.post(APP_ROOT + "favoritar/aula", {
                team_id: TEAM_ID,
                lesson_id: aula.id
            }).then(function(r){
                aula.favorito = r.data.status;
            });
        }
    }
});

Vue.component('aulas', {
    props: ["aulas"],
    methods: {
        favorite: function (aula) {
            axios.post(APP_ROOT + "favoritar/aula", {
                team_id: TEAM_ID,
                lesson_id: aula.id
            }).then(function(r){
                aula.favorito = r.data.status;
            });
        }
    }
});