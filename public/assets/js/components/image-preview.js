Vue.component('image-preview', {
    data: function () {
        return {
            image: null
        }
    },
    methods: {
        preview: function (e) {
            // Pego o evento do input
            let files = e.target.files || e.dataTransfer.files;
            if (files && files[0]) {
                // Pego o primeiro item do array
                let firstFile = files[0];
                // Instâncio o FileReader
                let reader = new FileReader();
                // Adiciona a class no self
                let self = this;
                // Pega o evento de carregamento
                reader.addEventListener("load", function (a) {
                    self.image = reader.result;
                }, false);

                if (firstFile) {
                    reader.readAsDataURL(firstFile);
                }
            }
        }
    }
});