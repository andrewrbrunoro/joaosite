<?php

$path = realpath($_SERVER['DOCUMENT_ROOT']) . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'editor' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;

$dir = '/files/editor/images/';

if ($_FILES['file']['type'] == 'image/png' || $_FILES['file']['type'] == 'image/jpg' || $_FILES['file']['type'] == 'image/gif' || $_FILES['file']['type'] == 'image/jpeg' || $_FILES['file']['type'] == 'image/pjpeg')
{
    $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
    $filename = md5(date('YmdHis')) . '.' . $ext;

    $file = $path . $filename;

    @move_uploaded_file($_FILES['file']['tmp_name'], $file);

    $array = array(
        'filelink' => 'http://' . $_SERVER['HTTP_HOST'] .  $dir . $filename
    );

    echo stripslashes(json_encode($array));
}