<?php

$path = realpath($_SERVER['DOCUMENT_ROOT']) . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'editor' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR;

$dir = '/files/editor/files/';

$file_parts = explode( '.', $_FILES['file']['name'] );
$filename = $file_parts[0];
$filetype = $file_parts[ count( $file_parts ) - 1 ];

if(file_exists($path . DIRECTORY_SEPARATOR . $_FILES['file']['name'])){
    $_FILES['file']['name'] = slug($filename).'-'.time().'.'.$filetype;
}else{
    $_FILES['file']['name'] = slug($filename).'.'.$filetype;
}

@move_uploaded_file($_FILES['file']['tmp_name'], $path . $_FILES['file']['name']);

$array = array(
    'filelink' => 'http://' . $_SERVER['HTTP_HOST'] . $dir . $_FILES['file']['name'],
    'filename' => $_FILES['file']['name'],
);

echo stripslashes(json_encode($array));


function slug($str, $separator = '-'){
    setlocale(LC_ALL, 'pt_BR.ISO-8859-1');
    $str = @iconv('UTF-8', 'ISO-8859-1//IGNORE//TRANSLIT', $str);

    $str = strtolower($str);

    // Código ASCII das vogais
    $ascii['a'] = range(224, 230);
    $ascii['e'] = range(232, 235);
    $ascii['i'] = range(236, 239);
    $ascii['o'] = array_merge(range(242, 246), array(240, 248));
    $ascii['u'] = range(249, 252);

    // Código ASCII dos outros caracteres
    $ascii['b'] = array(223);
    $ascii['c'] = array(231);
    $ascii['d'] = array(208);
    $ascii['n'] = array(241);
    $ascii['y'] = array(253, 255);

    foreach ($ascii as $key=>$item) {
        $acentos = '';
        foreach ($item AS $codigo) $acentos .= chr($codigo);
        $troca[$key] = '/['.$acentos.']/i';
    }

    $str = preg_replace(array_values($troca), array_keys($troca), $str);

    // Slug?
    if ($separator) {
        // Troca tudo que não for letra ou número por um caractere ($slug)
        $str = preg_replace('/[^a-z0-9]/i', $separator, $str);
        // Tira os caracteres ($slug) repetidos
        $str = preg_replace('/' . $separator . '{2,}/i', $separator, $str);
        $str = trim($str, $separator);
    }
    return $str;
}