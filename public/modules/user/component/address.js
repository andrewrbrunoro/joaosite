var $address = Vue.extend({
    props: {
        share: {
            type: Object,
            default: function () {
                return {};
            }
        }
    },
    data: function () {
        return {
            address: {
                cep: $('[name="UserAddress[zip_code]"]').val(),
                state: $('[name="UserAddress[state]"]').val(),
                district: $('[name="UserAddress[district]"]').val(),
                city: $('[name="UserAddress[city]"]').val(),
                street: $('[name="UserAddress[street]"]').val()
            }
        }
    },
    watch: {
        'address.cep': function (new_val, old_val) {
            if (/^[0-9]{8}$/.test(new_val.replace(/\D/g, ''))) {

                $.get('https://viacep.com.br/ws/' + new_val.replace(/\D/g, '') + '/json/', function(r){
                    if (r.erro == true) {
                        toastr.error('CEP inválido ou não encontrado.');
                    } else {
                        this.address.cep = r.cep;
                        this.address.state = r.uf;
                        this.address.district = r.bairro;
                        this.address.city = r.localidade;
                        this.address.street = r.logradouro;
                    }
                }.bind(this));

                // this.$http.get('https://viacep.com.br/ws/' + new_val.replace(/\D/g, '') + '/json/').then(function (success) {
                //     var r = success.body;
                //     if (r.erro == true) {
                //         toastr.error('CEP inválido ou não encontrado.');
                //     } else {
                //         this.address.cep = r.cep;
                //         this.address.state = r.uf;
                //         this.address.district = r.bairro;
                //         this.address.city = r.localidade;
                //         this.address.street = r.logradouro;
                //     }
                // });
            }
        }
    }
});
Vue.component('address-component', $address);