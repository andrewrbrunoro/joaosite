function storePrivateTransport(form) {
    createButton($(form).find('button[type="submit"]'));

    /* Só executa se o navegador não estiver processando nada */
    if (wait == false) {

        /* Ativa processamento */
        startLoading();

        /* Remove todos os erros */
        removeAllErrors();

        /* Faz a requisição pro servidor para salvar os dados */
        axios.post(form.action, $(form).serialize()).then(function (response) {
            endLoading('Salvo com sucesso');
        }).catch(function (error) {
            try {
                var r = error.response.data.errors;
                Object.keys(r).map(function (element_name) {
                    setError(element_name, r[element_name][0]);
                });
            } catch (e) {
                swal('Um erro inesperado aconteceu, por favor, atualiza a página, caso o erro persita, entre em contato conosco.');
            }
            endLoading(error.response.data.message);
        });
    }
    return false;
}