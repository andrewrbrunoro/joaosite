var $content = Vue.extend({
    props: {
        elements: Array
    },
    data: function () {
        return {
            element: ''
        }
    },
    methods: {
        elementType: function (e) {
            var target = e.target||e.srcElement;
            this.setElement(target.value);
        },
        setElement: function (id) {
            this.elements.map(function(a) {
                if (a.id == id) {
                    this.element = a.slug;
                    if (a.element_type == 'textarea') {
                        setTimeout(function(){
                            $('.content-redactor-editor').redactor({
                                minHeight: 500,
                                toolbarFixed: true,
                                toolbarFixedTopOffset: 80, // pixels
                                fileUpload: CMS_URL + 'upload/file',
                                imageUpload: CMS_URL + 'upload/image'
                            })
                        }, 100);
                    }
                }
            }.bind(this));
        }
    },
    mounted: function (){
        var $contentType = $("#ContentType");
        if ($contentType.val() != "")
            this.setElement($contentType.val());
    }
});
Vue.component('content-manage', $content);


$(document).ready(function(){

    var $range = $(".js-range-slider"),
        $from = $(".js-from"),
        $to = $(".js-to"),
        range,
        min = 0,
        max = 12,
        from,
        to;

    var updateValues = function () {
        $from.prop("value", from);
        $to.prop("value", to);
    };

    $range.ionRangeSlider({
        type: "single",
        min: min,
        max: max,
        prettify_enabled: false,
        grid: true,
        grid_num: 10,
         step: 0.5,
                postfix: " Horas",
                prettify: true,
                hasGrid: true,
        onChange: function (data) {
            from = data.from;
            to = data.to;
            
            updateValues();
        }
    });

    range = $range.data("ionRangeSlider");

    var updateRange = function () {
        range.update({
            from: from,
            to: to
        });
    };


})