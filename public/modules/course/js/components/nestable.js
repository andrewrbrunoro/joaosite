var $list = Vue.extend({
    props: {
        contentsProp: {
            type: Array,
            default: function () {
                return [];
            }
        }
    },
    data: function () {
        return {
            query_search: '',
            not_read_ids: [],
            search_result: [],
            search_query: '',
            contents: this.contentsProp
        }
    },
    methods: {
        // Procura pelo conteúdo
        getContents: function () {
            var self = this;
            axios.get(CMS_URL + 'conteudo/procurar', {
                params: {
                    q: self.search_query
                }
            }).then(function (r) {
                var data = r.data.data;
                if (data.total > 0) {
                    self.search_result = data.data;
                } else {
                    self.search_result = [];
                }
            }).catch(function (error) {
                alert("Um erro inesperado aconteceu, por favor, informe o link atual e abra um chamado.");
            });
        },
        // Adiciona o conteúdo na listagem de conteúdos
        setContent: function (content) {
           // event.preventDefault();
            // Adiciona o conteudo no array
            this.contents.unshift(content);
            // Adiciona o ID para não aparecer mais na listagem de conteúdo
            this.not_read_ids.push(content.id);
            // Adiciono um timeOut pra dar tempo do DOM identificar que existe novos elementos
            setTimeout(function () {
                $('.date-picker').datepicker({
                    format: "dd/mm/yyyy",
                    todayHighlight: true
                });
            }, 200);

            if (this.contents.length < 3) {
                this.search_query = '*';
                this.getContents();
            }
        },
        // Como estamos trabalhando com dados não concretos, é necessário atualizar as informações em RT para o usuário
        // Soluciona o problema, quando adiciona um novo conteúdo, mantém os dados
        updateContentValue: function (index, key) {
            var target = event.target || event.srcElement;
            if (this.contents[index]) {
                this.contents[index][key] = target.value;
            }
        },
        // Remove o conteúdo que foi adicionado
        destroyContent: function (id) {
            this.not_read_ids = this.not_read_ids.filter(function(current) {
                return current != id;
            });
            this.contents = this.contents.filter(function(content){
                return content.id != id;
            });
        }
    },
    mounted: function () {
        $('.dd').nestable({
            handleClass: 'arb-drag'
        });

        if (this.contents.length > 0) {
            this.contents.map(function(content){
                this.not_read_ids.push(parseInt(content.id));
            }.bind(this));
        }

        if (this.contents.length < 2) {
            this.search_query = '*';
            this.getContents();
        }
    }
});
Vue.component('nestable', $list);