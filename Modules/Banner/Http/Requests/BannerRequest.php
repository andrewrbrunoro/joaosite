<?php

namespace Modules\Banner\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannerRequest extends FormRequest
{
    public function attributes()
    {
        return [
            'image' => 'Imagem',
            'url' => 'Link',
            'release_date' => 'Data de lançamento',
            'expiration_date' => 'Data de expiração'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PATCH') {
            $return = [
                'image' => 'image|custom_dimensions:width=' . config('banner.banner_width') . ',height=' . config('banner.banner_height'),
            ];
        } else {
            $return = [
                'image' => 'required|image|custom_dimensions:width=' . config('banner.banner_width') . ',height=' . config('banner.banner_height'),
            ];
        }

        # Valida se é uma data válida, se vir preenchida do formulário
        if ($this->request->get('release_date') != '')
            $return += ['release_date' => 'date_format:d/m/Y'];

        # Valida se é uma data válida, se vir preenchida do formulário
        if ($this->request->get('expiration_date') != '')
            $return += ['expiration_date' => 'date_format:d/m/Y'];

        # Valida se é URL se vim preenchida do formulário
        if ($this->request->get('url') != '')
            $return += ['url' => 'url'];

        return $return;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
