<?php


Route::group([
    'prefix' => 'banners',
    'as' => 'banner.',
    'namespace' => 'Modules\Banner\Http\Controllers'
], function ($r) {

    $r->get('/', 'BannerController@index')->name('index');

    $r->get('criar', 'BannerController@create')->name('create');
    $r->post('criar', 'BannerController@store')->name('store');


    $r->get('{id}/editar', 'BannerController@edit')->name('edit');
    $r->patch('{id}/editar', 'BannerController@update')->name('update');

    $r->delete('{id}/deletar', 'BannerController@destroy')->name('destroy');
});
