<?php

namespace Modules\Banner\Http\Controllers;

use App\Helpers\Redactor;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Banner\Entities\Banner;
use Modules\Banner\Http\Requests\BannerRequest;

class BannerController extends Controller
{

    public function index(Banner $banner)
    {
        $data = $banner->orderBy('created_at', 'desc')
            ->paginate(20);

        return view('banner::index', compact('data'));
    }

    public function create()
    {
        return view('banner::create');
    }

    public function store(BannerRequest $request, Banner $banner, Redactor $redactor)
    {
        # Inicio a transação
        DB::beginTransaction();
        try {

            # Pego da imagem, a largura e altura
            list($width, $height) = getimagesize($request->file('image')->getPathname());

            # Salvo o arquivo
            $redactor->setFileName($request->file('image')->getClientOriginalName())
                ->setQuality(90);

            # Adiciono no request, para utilizar como variável de inserção, pois ta vindo os outros dados do formulário
            $request->merge([
                'name' => $redactor->getFileName(),
                'type' => $request->file('image')->getClientMimeType(),
                'alt' => $redactor->getFileName() . ' - ' . setting('nome'),
                'width' => $width,
                'height' => $height
            ]);

            # Salvo o banner
            $bannerQuery = $banner->create($request->all());

            # Salvo o arquivo no upload
            $redactor->setDir($banner->path_image . $bannerQuery->id, false)
                ->requestIntervationImage('image', [['width' => 100, 'name' => 'thumb']]);

            # Limpa qualquer cache nomeado com banners
            cache()->forget('banners');

            # Tudo certo, confirma a transação
            DB::commit();
            return redirect()->route('banner.index')->with('success', 'Novo Banner inserido com sucesso.');
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->withInput($request->all())->with('error', $e->getMessage());
        }
    }

    public function show()
    {
        return view('banner::show');
    }

    public function edit($id, Banner $banner)
    {
        try {
            # Procura pelo banner
            $edit = $banner->find($id);
            # Verifica se o banner existe
            if (!$edit)
                throw new \Exception("Registro não encontrado");

        } catch (\Exception $e) {
            return redirect()->route('banner.index')->with('error', $e->getMessage());
        }
        return view('banner::edit', compact('edit'));
    }

    public function update($id, BannerRequest $request, Banner $banner, Redactor $redactor)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {
            # Procura pelo banner
            $edit = $banner->find($id);

            # Verifica se o banner existe
            if (!$edit)
                return redirect()->route('banner.index')->with('error', "Registro não encontrado");

            # Se vir o arquivo, atualiza ele
            if ($request->hasFile('image')) {
                # Pego da imagem, a largura e altura
                list($width, $height) = getimagesize($request->file('image')->getPathname());

                # Salvo o arquivo
                $redactor->setFileName($request->file('image')->getClientOriginalName())
                    ->setQuality(90);

                # Adiciono no request, para utilizar como variável de inserção, pois ta vindo os outros dados do formulário
                $request->merge([
                    'name' => $redactor->getFileName(),
                    'type' => $request->file('image')->getClientMimeType(),
                    'alt' => $redactor->getFileName() . ' - ' . setting('nome'),
                    'width' => $width,
                    'height' => $height
                ]);
            }

            # Salvo o banner
            $edit->update($request->all());

            # Se vir o arquivo, atualiza ele
            if ($request->hasFile('image')) {
                # Salvo o arquivo no upload
                $redactor->setDir($banner->path_image . $edit->id, false)
                    ->requestIntervationImage('image', [['width' => 100, 'name' => 'thumb']]);
            }

            # Limpa qualquer cache nomeado com banners
            cache()->forget('banners');

            # Tudo certo, finaliza a transação
            DB::commit();
            return redirect()->route('banner.index')->with('success', "<strong>Banner</strong> editado com sucesso");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->route('banner.index')->with('error', $e->getMessage());
        }
    }

    public function destroy($id, Banner $banner)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Procura o banner
            $exist = $banner->find($id);
            # Verifica se o banner existe
            if (!$exist)
                throw new \Exception("Registro não encontrado");

            # Remove o banner, apenas com softDelete
            $exist->delete();

            # Inicia a transação
            DB::commit();
            return redirect()->route("banner.index")->with("success", "Banner removido com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->route("banner.index")->with("error", $e->getMessage());
        }
    }
}
