<?php

namespace Modules\Banner\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{

    use SoftDeletes;

    public $path_image = 'assets/uploads/banners/';

    protected $fillable = ["name", "slug", "type", "alt", "width", "height", "url", "release_date", "expiration_date", "status"];

    protected $appends = ["thumb"];

    /**
     * @var array
     *
     * Informo pra model, qual será os campos no formato DATA
     */
    protected $dates = ["release_date", "expiration_date"];

    /**
     * @return string
     *
     * Pega o status e transforma em um HTML
     */
    public function getStatusLabelAttribute()
    {
        switch ($this->status) {
            case 0:
                return '<label class="badge badge-danger">Inativo</label>';
                break;
            case 1:
                return '<label class="badge badge-primary">Ativo</label>';
                break;
            default:
                return '<label class="badge badge-danger">Inativo</label>';
                break;
        }
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     *
     * Pega a imagem
     */
    public function getBannerImageAttribute()
    {
        # Declara a variável imagem com os dados da imagem
        $image = $this->path_image . $this->id . "/" . $this->name;
        # Se existir, retorna a url completa
        if (is_file(public_path($image)))
            return url($image);
        else {
            return "";
        }
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     *
     * URL da thumb se ela existir
     */
    public function getThumbAttribute()
    {
        # Declara a variável imagem com os dados da imagem
        $image = $this->path_image . $this->id . "/thumb-" . $this->name;
        # Se existir, retorna a url completa
        if (is_file(public_path($image)))
            return url($image);
        else {
            return "";
        }
    }

    /**
     * @param $value
     * @return string
     *
     * Converte a data SQL para a da configuração
     */
    public function getReleaseDateAttribute($value)
    {
        if ($value)
            return dateToSql($value, 'Y-m-d H:i:s', config('app.date_format'));
        return "";
    }

    /**
     * @param $value
     * @return string
     *
     * Converte a data SQL para a da configuração
     */
    public function getExpirationDateAttribute($value)
    {
        if ($value)
            return dateToSql($value, 'Y-m-d H:i:s', config('app.date_format'));
        return "";
    }

    /**
     * @param $value
     *
     * Converto a data para a da configuração
     */
    public function setReleaseDateAttribute($value)
    {
        if ($value && typeDate($value, config('app.date_format')))
            $this->attributes['release_date'] = dateToSql($value, config('app.date_format'));
        else {
            $this->attributes['release_date'] = null;
        }
    }

    /**
     * @param $value
     *
     * Converto a data para a da configuração
     */
    public function setExpirationDateAttribute($value)
    {
        if ($value && typeDate($value, config('app.date_format')))
            $this->attributes['expiration_date'] = dateToSql($value, config('app.date_format'));
        else {
            $this->attributes['expiration_date'] = null;
        }
    }

}
