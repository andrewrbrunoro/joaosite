@extends('dashboard::layouts.master')

@section('title', 'Novo Banner')

@section('content')
    <div class="row">
        {!! Form::open(['route' => 'banner.store', 'files' => true]) !!}
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5>Novo Banner</h5>
            </div>

            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('banner.index') !!}" class="btn btn-outline btn-warning">
                            <i class="fa fa-arrow-left"></i> Listar todos os banners
                        </a>
                    </div>
                </div>

                <hr>


                @include('banner::form')
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop
