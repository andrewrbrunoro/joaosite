@extends('dashboard::layouts.master')

@section('title', 'Banners')

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Banners</h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-12">
                        <a href="{!! route('banner.create') !!}" class="btn btn-outline btn-success">
                            <i class="fa fa-plus"></i> Cadastrar Banner
                        </a>
                        <a href="{!! request()->fullUrl() !!}" class="btn btn-outline btn-default">
                            <i class="fa fa-repeat"></i> Atualizar
                        </a>
                    </div>
                </div>

                <hr>

                <div class="row">

                    <div class="col-lg-12">
                        <table class="table table-responsive table-bordered table-stripped">
                            <thead>
                            <tr>
                                <td>#</td>
                                <td>Thumb</td>
                                <td>Nome</td>
                                <td>Link</td>
                                <td>Data de lançamento</td>
                                <td>Data de expiração</td>
                                <td>Status</td>
                                <td>Opções</td>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($data->count())
                                @foreach($data as $row)
                                    <tr>
                                        <td>{!! $row->id !!}</td>
                                        <td>
                                            <img src="{!! $row->thumb !!}" alt="{!! $row->name !!}" class="img-responsive" />
                                        </td>
                                        <td>{!! $row->name !!}</td>
                                        <td>{!! $row->url !!}</td>
                                        <td>{!! $row->release_date !!}</td>
                                        <td>{!! $row->expiration_date !!}</td>
                                        <td>{!! $row->statusLabel !!}</td>
                                        <td>
                                            {!! Form::open(['route' => ['banner.destroy', $row->id], 'method' => 'DELETE', 'onsubmit' => 'beforeConfirm(this, event)']) !!}
                                            <a href="{!! route('banner.edit', $row->id) !!}" class="btn btn-outline btn-info btn-xs">
                                                <i class="fa fa-pencil"></i> Editar
                                            </a>
                                            <button class="btn btn-outline btn-danger btn-xs" type="submit">
                                                <i class="fa fa-trash"></i> Deletar
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8">Nenhum post no momento, <a href="{!! route('banner.create') !!}">clique aqui</a> adicione um post</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>
@stop

