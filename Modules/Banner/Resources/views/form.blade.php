<div class="row">

    <image-preview inline-template>
        <div class="col-lg-12">
            <div class="form-group">
                @if (isset($edit) && $edit->banner_image != "")
                    <img src="{!! $edit->banner_image !!}" id="preview-image" class="img-responsive" v-if="!image" />
                    <img v-bind:src="image" id="preview-image" class="img-responsive" />
                @else
                    <img v-bind:src="image" id="preview-image" class="img-responsive" />
                @endif
            </div>
            <div class="form-group">
                <div class="form-group{!! $errors->first('image') ? ' has-error' : '' !!}">
                    <label for="image">
                        Imagem Banner ({!! config('banner.banner_width') !!}w x {!! config('banner.banner_height') !!}h) <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    {!! Form::file('image', ['class' => 'form-control', (isset($edit) ? '' : 'required'), 'placeholder' => 'Imagem', 'v-on:change' => 'preview']) !!}
                    {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
            <hr>
        </div>
    </image-preview>

    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="clear">
                        Status <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('status', 1, true) !!} Ativo
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('status', 0, false) !!} Inativo
                    </label>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-lg-12">
        <div class="form-group{!! $errors->first('url') ? ' has-error' : '' !!}">
            <label for="url">
                Link <label class="badge badge-info">Opcional</label>
            </label>
            {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'Link: ex( https://google.com.br, '.url('/').' )']) !!}
            {!! $errors->first('url', '<span class="help-block">:message</span>') !!}
        </div>
    </div>

    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-3">
                <div class="form-group{!! $errors->first('release_date') ? ' has-error' : '' !!}">
                    <label for="release_date">
                        Data de lançamento <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::text('release_date', null, ['class' => 'form-control date-picker', 'v-mask' => '"##/##/####"', 'placeholder' => 'Data de lançamento']) !!}
                    {!! $errors->first('release_date', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group{!! $errors->first('expiration_date') ? ' has-error' : '' !!}">
                    <label for="expiration_date">
                        Data de expiração <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::text('expiration_date', null, ['class' => 'form-control date-picker', 'v-mask' => '"##/##/####"', 'placeholder' => 'Data de expiração']) !!}
                    {!! $errors->first('expiration_date', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-lg-6 col-sm-6">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check"></i> Salvar a postagem
        </button>
    </div>
</div>

@section('script')
    {!! Html::script('assets/js/components/image-preview.js') !!}
@endsection