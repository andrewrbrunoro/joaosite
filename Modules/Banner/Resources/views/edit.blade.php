@extends('dashboard::layouts.master')

@section('title', 'Editar Banner')

@section('content')
    <div class="row">
        {!! Form::model($edit, ['route' => ['banner.update', $edit->id], 'files' => true, 'method' => 'PATCH']) !!}
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5>Banner - <strong class="text-success">{!! $edit->name !!}</strong></h5>
            </div>

            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('banner.index') !!}" class="btn btn-outline btn-warning">
                            <i class="fa fa-arrow-left"></i> Listar todos os banners
                        </a>
                    </div>
                </div>

                <hr>

                @include('banner::form')
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop
