<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');

            $table->string('type', 50)->nullable();
            $table->string('alt')->nullable();
            $table->string('width', 20)->nullable();
            $table->string('height', 20)->nullable();

            $table->text('url')->nullable();

            $table->dateTime('release_date')->nullable();
            $table->dateTime('expiration_date')->nullable();

            $table->tinyInteger('status')->default(1);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
