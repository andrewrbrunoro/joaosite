<?php

namespace Modules\Notification\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Modules\Notification\Emails\NotificationMail;
use Modules\Notification\Entities\Notification;
use Modules\Notification\Entities\NotificationEmail;

/**
 * Class NotificationEmail
 * @package Modules\Notification\Jobs
 *
 * QUEUE para envio das notificações
 */
class NotificationEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    protected $notification;

    protected $notificationEmail;

    /**
     * NotificationEmail constructor.
     * @param Notification $notification
     * @param NotificationEmail $notificationEmail
     *
     */
    public function __construct(Notification $notification, NotificationEmail $notificationEmail)
    {
        $this->notification = $notification;
        $this->notificationEmail = $notificationEmail;
    }

    public function handle()
    {
        Mail::to($this->notificationEmail->email)->send(new NotificationMail($this->notification, $this->notificationEmail));
    }

}
