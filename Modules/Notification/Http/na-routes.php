<?php

# na.notification (na = not auth)
Route::group([
    'namespace' => 'Modules\Notification\Http\Controllers',
    'prefix' => 'service/notify',
    'middleware' => 'settings',
    'as' => 'na.notification.'
], function($r) {

    $r->get('read/{code}', 'NotificationEmailController@readNotification')->name('lida');

});