<?php
Route::group([
    'namespace' => 'Modules\Notification\Http\Controllers',
    'prefix' => 'notificacao',
    'middleware' => ['settings'],
    'as' => 'notification.'
], function ($r) {

    $r->get('/', 'NotificationController@index')->name('index');

    $r->get('/criar', 'NotificationController@create')->name('create');
    $r->post('/criar', 'NotificationController@store')->name('store');

    $r->get('/{id}/visualizar', 'NotificationController@show')->name('show');

});

