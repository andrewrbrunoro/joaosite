<?php

namespace Modules\Notification\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NotificationRequest extends FormRequest
{

    public function attributes()
    {
        return [
            'title' => 'Título',
            'subject' => 'Assunto',
            'text' => 'Conteúdo'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->session()->flash('emails', collect($this->request->get('emails')));
        return [
            'title' => 'required|max:255',
            'subject' => 'required|max:120',
            'text' => 'required',
            'emails.*.name' => 'required',
            'emails.*.email' => 'required|email'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
