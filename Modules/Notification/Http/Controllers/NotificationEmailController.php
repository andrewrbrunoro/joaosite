<?php

namespace Modules\Notification\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Notification\Entities\NotificationEmail;

class NotificationEmailController extends Controller
{

    public function readNotification($code, NotificationEmail $notificationEmail)
    {
        # Pega a imagem dot-white que é uma imagem inútil apenas para não dar erro no e-mail
        $path = public_path("assets/images/dot-white.png");
        $file = \File::get($path);
        $type = \File::mimeType($path);

        # Inicia a transação
        DB::beginTransaction();
        try {

            # Procuro a notificação do usuário pelo código
            $edit = $notificationEmail->whereCode($code)
                ->whereRead(0)
                ->first();

            # Se existir, eu atualizo e coloco como e-mail lido
            if ($edit)
                # Adiciono como lido
                $edit->update(['read' => 1]);

            # Conclui a transação
            DB::commit();
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
        }
        # A resposta é baseada na imagem
        return response()->stream(function() use ($file){
            echo $file;
        }, 200, ["Content-Type" => $type]);
    }

}
