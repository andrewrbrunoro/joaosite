<?php

namespace Modules\Notification\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Notification\Entities\Notification;
use Modules\Notification\Entities\NotificationEmail;
use Modules\Notification\Http\Requests\NotificationRequest;
use Modules\Notification\Jobs\NotificationEmailJob;

class NotificationController extends Controller
{
    public function index(Notification $notification)
    {
        # Listo todas as notificações
        $data = $notification->orderBy('created_at', 'desc')
            ->paginate(20);

        return view('notification::index', compact('data'));
    }

    public function create()
    {
        return view('notification::create');
    }

    public function store(NotificationRequest $notificationRequest, Notification $notification, NotificationEmail $notificationEmail)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Cria a notificação
            $notificationQuery = $notification->create($notificationRequest->all());

            # Lê todos os e-mails e salva na relação
            foreach ($notificationRequest->get('emails') as $email) {
                $notificationEmailSave = new NotificationEmail(["email" => $email['email'], "name" => $email['name']]);
                $notificationQuery->NotificationEmails()->saveMany([$notificationEmailSave]);
                # Coloca na fila para ser processado
                dispatch(new NotificationEmailJob($notificationQuery, $notificationEmailSave));
            }

            # Conclui a transação
            DB::commit();
            return redirect()->route("notification.index")->with("success", "Notificação criada com sucesso");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->route("notification.index")->with("error", $e->getMessage());
        }
    }

    public function show($id, Notification $notification)
    {
        try {
            # Procuro a notificação referente ao ID
            $show = $notification->find($id);
            # Se não existir, envia para a exceção
            if (!$show)
                throw new \Exception("Registro não encontrado.");

        } catch (\Exception $e) {
            return redirect()->route("notification.index")->with("error", $e->getMessage());
        }
        return view('notification::show', compact('show'));
    }
}
