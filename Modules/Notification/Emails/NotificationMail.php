<?php

namespace Modules\Notification\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\Notification\Entities\Notification;
use Modules\Notification\Entities\NotificationEmail;

class NotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $Notification;

    public $NotificationEmail;

    public function __construct(Notification $notification, NotificationEmail $notificationEmail)
    {
        $this->Notification = $notification;
        $this->NotificationEmail = $notificationEmail;
    }

    public function build()
    {
        return $this->from(env('MAIL_FROM'), env('MAIL_NAME'))
            ->subject($this->Notification->subject)
            ->view('notification::mails.notification');
    }
}
