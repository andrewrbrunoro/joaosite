@extends('dashboard::layouts.master')

@section('title', 'Nova Notificação')

@section('content')
    <div class="row">
        {!! Form::open(['route' => 'notification.store']) !!}
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5>Nova Notificação</h5>
            </div>

            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('notification.index') !!}" class="btn btn-outline btn-warning">
                            <i class="fa fa-arrow-left"></i> Listar todas as notificações
                        </a>
                    </div>
                </div>

                <hr>


                @include('notification::form')
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop
