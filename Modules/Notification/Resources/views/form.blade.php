<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('title') ? ' has-error' : '' !!}">
                    <label for="title">
                        Título <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Título']) !!}
                    {!! $errors->first('title', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('subject') ? ' has-error' : '' !!}">
                    <label for="subject">
                        Assunto <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    {!! Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'Assunto']) !!}
                    {!! $errors->first('subject', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>

    <notification-email inline-template :rows='{!! session()->has('emails') ? session('emails') : collect([]) !!}' :error='{!! $errors !!}'>
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>
                            Nome do Destinatário
                        </label>
                        <input type="text" class="form-control" name="notification_email_name" placeholder="Nome" v-model="name" />
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>
                            E-mail do Destinatário
                        </label>
                        <input type="text" class="form-control" name="notification_email_email" placeholder="E-mail" v-model="email" />
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <button class="btn btn-success" type="button" v-on:click="setDestiny">
                            <i class="fas fa-plus"></i> Salvar Destinatário
                        </button>
                    </div>
                </div>
                <div class="col-lg-12 table-responsive">
                    <table class="table table-bordered table-stripped">
                        <thead>
                            <tr>
                                <td>Nome do Destinatário</td>
                                <td>E-mail do Destinatário</td>
                                <td width="11%">Opções</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-if="lists.length <= 0">
                                <td colspan="3">Adicione um destinatário</td>
                            </tr>
                            <tr v-for="(list, index) in lists" v-bind:class="error['emails.' + index + '.email'] || error['emails.' + index + '.name'] ? 'bg-danger' : ''">
                                <td>@{{ list.name }}</td>
                                <td>@{{ list.email }}</td>
                                <td>
                                    <input type="hidden" v-bind:name="'emails['+ index +'][name]'" :value="list.name" />
                                    <input type="hidden" v-bind:name="'emails['+ index +'][email]'" :value="list.email" />
                                    <button class="btn btn-primary btn-xs" type="button" v-on:click="edit(list, index)">
                                        <i class="fas fa-pencil-alt"></i>
                                    </button>
                                    <button class="btn btn-xs btn-danger" type="button" v-on:click="destroy(index)">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </notification-email>

    <div class="col-lg-12">
        <div class="form-group{!! $errors->first('text') ? ' has-error' : '' !!}">
            <label for="text">
                Conteúdo <label class="badge badge-danger">Obrigatório</label>
            </label>
            {!! Form::textarea('text', null, ['class' => 'form-control redactor-editor', 'placeholder' => 'Conteúdo']) !!}
            {!! $errors->first('text', '<span class="text-danger">:message</span>') !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6 col-sm-6">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check"></i> Salvar a Notificação
        </button>
    </div>
</div>

@section('script')
    <script type="text/javascript">
        Vue.component('notification-email', {
            props: {
                error: Array | Object,
                rows: {
                    type: Array,
                    default: function () {
                        return []
                    }
                }
            },
            data: function () {
                return {
                    name: '',
                    email: '',
                    lists: this.rows
                }
            },
            methods: {
                validate: function () {
                    removeAllErrors();
                    let errors = 0;
                    if (this.name == '') {
                        setError('notification_email_name', "Campo Obrigatório.");
                        errors++;
                    }
                    if (this.email == '') {
                        setError('notification_email_email', "Campo Obrigatório.");
                        errors++;
                    }
                    return errors === 0;
                },
                edit: function (object, id) {
                    this.name = object.name;
                    this.email = object.email;

                    this.destroy(id);
                },
                destroy: function (id) {
                    this.lists = this.lists.filter(function(current, index) {
                        return index != id;
                    });
                },
                setDestiny: function () {
                    if (this.validate()) {
                        this.lists.unshift({name: this.name, email: this.email});
                        this.clear();
                    }
                },
                clear: function () {
                    this.name = '';
                    this.email = '';
                }
            }
        });
    </script>
@endsection