@extends('dashboard::layouts.master')

@section('title', 'Nova Notificação')

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5>Visualizando a notificação - <strong class="text-success">{!! $show->title !!}</strong></h5>
            </div>

            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('notification.index') !!}" class="btn btn-outline btn-warning">
                            <i class="fa fa-arrow-left"></i> Listar todas as notificações
                        </a>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="forum-title bg-primary">
                            Dados da notificação
                        </h3>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>
                                        Título
                                    </label>
                                    <br>
                                    <strong class="text-navy">
                                        {!! $show->title !!}
                                    </strong>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label>
                                    Assunto
                                </label>
                                <br>
                                <strong class="text-navy">
                                    {!! $show->subject !!}
                                </strong>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>
                                        Conteúdo
                                    </label>
                                    <br>
                                    {!! $show->text !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 table-responsive">
                        <h3 class="forum-title bg-primary">
                            E-mails
                        </h3>
                        <table class="table table-bordered table-stripped">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Nome</td>
                                    <td>E-mail</td>
                                    <td>Status</td>
                                    <td>
                                        Data/Hora de leitura
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            @if ($show->NotificationEmails->count())
                                @foreach($show->NotificationEmails as $notificationEmail)
                                    <tr>
                                        <td>
                                            {!! $notificationEmail->id !!}
                                        </td>
                                        <td>
                                            {!! $notificationEmail->name !!}
                                        </td>
                                        <td>
                                            {!! $notificationEmail->email !!}
                                        </td>
                                        <td>
                                            {!! $notificationEmail->read ? '<strong class="text-success">E-mail aberto</strong>' : '<strong class="text-danger">Aguardando...</strong>' !!}
                                        </td>
                                        <td>
                                            @if ($notificationEmail->read == 1)
                                                {!! dateToSql($notificationEmail->updated_at, 'Y-m-d H:i:s', 'd/m/Y H:i:s') !!}
                                            @else
                                                <strong class="text-danger">Aguardando...</strong>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5"></td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
