@extends('dashboard::layouts.master')

@section('title', 'Notificações')

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Notificações</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-12">
                        <a href="{!! route('notification.create') !!}" class="btn btn-outline btn-success">
                            <i class="fa fa-plus"></i> Cadastrar Notificação
                        </a>
                        <a href="{!! request()->fullUrl() !!}" class="btn btn-outline btn-default">
                            <i class="fas fa-repeat"></i> Atualizar
                        </a>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-lg-12 table-responsive">
                        <table class="table table-bordered table-stripped">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Título</td>
                                    <td>Assunto</td>
                                    <td>Resumo</td>
                                    <td>Lidos/Enviados</td>
                                    <td>
                                        Opções
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            @if ($data->count())
                                @foreach($data as $row)
                                    <tr>
                                        <td>{!! $row->id !!}</td>
                                        <td>{!! $row->title !!}</td>
                                        <td>{!! $row->subject !!}</td>
                                        <td>{!! $row->resume !!}</td>
                                        <td>
                                            <label class="badge badge-info" title="Total de e-mails lidos dessa notificação">{!! $row->NotificationReadEmails->count() !!}</label>
                                            /
                                            <label class="badge badge-danger" title="Total de e-mails enviados dessa notificação">{!! $row->NotificationEmails->count() !!}</label>
                                        </td>
                                        <td>
                                            <a href="{!! route('notification.show', $row->id) !!}"
                                               class="btn btn-outline btn-xs btn-success"
                                               title="Visualizar e-mails e dados da notificação">
                                                <i class="fas fa-eye"></i> Visualizar
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6">Nenhuma notificação no momento, <a href="{!! route('notification.create') !!}">clique aqui</a> e crie uma notificação</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
