<?php

namespace Modules\Notification\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use SoftDeletes;

    protected $fillable = ["title", "subject", "text"];

    public function getResumeAttribute()
    {
        return str_limit(strip_tags($this->text), 20);
    }

    public function NotificationReadEmails()
    {
        return $this->hasMany(NotificationEmail::class, 'notification_id')->where('read', '=', 1);
    }

    public function NotificationEmails()
    {
        return $this->hasMany(NotificationEmail::class, 'notification_id')->orderBy('read', 'desc');
    }
}
