<?php

namespace Modules\Notification\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotificationEmail extends Model
{
    use SoftDeletes;

    protected $fillable = ["notification_id", "name", "email", "read", "code"];

    public function setEmailAttribute($value)
    {
        # Crio um código baseado na hora atual para que não tenha problema que o usuário atualize as notificações manualmente por HTTP
        $this->attributes['code'] = base64_encode(bcrypt(Carbon::now()));
        $this->attributes['email'] = $value;
    }

    public function Notification()
    {
        return $this->belongsTo(Notification::class, 'notification_id');
    }
}
