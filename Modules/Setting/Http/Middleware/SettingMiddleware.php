<?php

namespace Modules\Setting\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Modules\Setting\Entities\Setting;
use Ramsey\Uuid\Uuid;

class SettingMiddleware
{

    public function handle(Request $request, Closure $next)
    {
        try {
            $this->loadSettings();
            $this->loadCookie();
        } catch (\Exception $e) {
            return redirect()->back()->with("error", "Um erro interno aconteceu, por favor, aguarde um instante.");
        }
        return $next($request);
    }

    /**
     * @return array|\Illuminate\Cache\CacheManager|mixed
     * @throws \Exception
     *
     * Carrega todas as configurações da tabela `settings` e cria um padrão [key] => valor que será acessível pela helper do módulo
     */
    public function loadSettings()
    {
        $Setting = new Setting();
        if (!cache()->has('settings')) {
            $cache = [];
            foreach ($Setting->all() as $item) {
                if (isJson($item->value)) {
                    $cache[$item->key] = json_decode($item->value);
                } else {
                    $cache[$item->key] = $item->value;
                }
            }
            cache()->put('settings', $cache, Carbon::now()->addDay());
        } else {
            $cache = cache('settings');
        }
        return $cache;
    }

    /**
     * Crio um token no cookie pro usuário
     */
    public function loadCookie()
    {
        # Se não encontrar o cookie `token_cookie`
        if (!Cookie::get("uuid")) {
            # Crio o token e limito ele pra 60 caracteres
            $token = Uuid::uuid4();
            # Gravo o cookie
            Cookie::queue(Cookie::forever("uuid", $token->getHex()));
        }
    }

}
