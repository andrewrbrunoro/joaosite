<?php
if (!function_exists('setting')) {
    function setting($key)
    {
        if (cache()->has('settings')) {
            $settings = cache('settings');
            return isset($settings[$key]) ? $settings[$key] : (env('APP_DEBUG') == true ? $key . ' não encontrado.' : '');
        } else {
            return env('APP_DEBUG') == true ? 'CACHE inativo ou não inserido valores na tabela settings' : '';
        }
    }
}