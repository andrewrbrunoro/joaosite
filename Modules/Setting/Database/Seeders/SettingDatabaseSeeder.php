<?php

namespace Modules\Setting\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Setting\Entities\Setting;

class SettingDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $setting = new Setting();

        $exist = $setting->where("key", "=", "nome")->count();
        if ($exist == 0) {
            $setting->create([
                'key' => 'nome',
                'value' => 'Defina o nome do sistema na configurações',
                'public' => 1,
                'status' => 1
            ]);
        }

        $exist = $setting->where("key", "=", "autor")->count();
        if ($exist == 0) {
            $setting->create([
                'key' => 'autor',
                'value' => 'Andrew Rodrigues Brunoro',
                'public' => 0,
                'status' => 1
            ]);
        }

        $exist = $setting->where("key", "=", "sistema.suporte.email")->count();
        if ($exist == 0) {
            $setting->create([
                'key' => 'sistema.suporte.email',
                'value' => 'andrewrbrunoro@gmail.com',
                'public' => 0,
                'status' => 1
            ]);
        }

        // $this->call("OthersTableSeeder");
    }
}
