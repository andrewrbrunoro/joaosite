<?php

return [
    'name' => 'Blog',
    'banner_width' => 1920,
    'banner_height' => 519,
    'thumb_width' => 120,
    'thumb_height' => 120
];
