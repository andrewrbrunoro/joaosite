<?php

Route::group([
    'prefix' => 'blog',
    'as' => 'blog.',
    'namespace' => 'Modules\Blog\Http\Controllers'
], function ($r) {

    $r->get('/', 'BlogController@index')->name('index');

    $r->get('criar', 'BlogController@create')->name('create');
    $r->post('criar', 'BlogController@store')->name('store');


    $r->get('{id}/editar', 'BlogController@edit')->name('edit');
    $r->patch('{id}/editar', 'BlogController@update')->name('update');

    $r->delete('{id}/deletar', 'BlogController@destroy')->name('destroy');
});

Route::group([
    'prefix' => 'categorias',
    'as' => 'post_category.',
    'namespace' => 'Modules\Blog\Http\Controllers'
], function ($r) {

    $r->get('/', 'PostCategoryController@index')->name('index');

    $r->get('criar', 'PostCategoryController@create')->name('create');
    $r->post('criar', 'PostCategoryController@store')->name('store');

    $r->get('{id}/editar', 'PostCategoryController@edit')->name('edit');
    $r->patch('{id}/editar', 'PostCategoryController@update')->name('update');

    $r->delete('{id}/deletar', 'PostCategoryController@destroy')->name('destroy');

});