<?php

namespace Modules\Blog\Http\Controllers;

use App\Helpers\FileHelper;
use App\Helpers\Redactor;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Blog\Entities\Post;
use Modules\Blog\Entities\PostCategory;
use Modules\Blog\Entities\PostCategoryPost;
use Modules\Blog\Entities\PostImage;
use Modules\Blog\Http\Requests\PostRequest;

class BlogController extends Controller
{

    public function index(Post $post)
    {
        $data = $post->orderBy('created_at', 'desc')
            ->paginate(20);

        return view('blog::index', compact('data'));
    }

    public function create(PostCategory $postCategory)
    {
        # Listagem de todas as categorias
        $postCategories = $postCategory->whereStatus(1)
            ->orderBy('name')
            ->get()
            ->pluck('name', 'id');


        return view('blog::create', compact('postCategories'));
    }

    public function store(PostRequest $request, Post $post, FileHelper $fileHelper)
    {
        # inicio a transação
        DB::beginTransaction();
        try {
            # Crio a postagem
            $postQuery = $post->create($request->all());

            # Adiciona a imagem
            $this->add_image($request, $postQuery, $fileHelper);

            # Se receber a relação com uma categoria, vincula
            if ($request->get('category_id') != '') {
                $category = PostCategory::find($request->get('category_id'));
                if (!$category)
                    throw new \Exception("Categoria não encontrada.");
                $postQuery->PostCategory()->attach(['post_category_id' => $request->get('category_id')]);
            }

            # Salvo a transação
            DB::commit();
            # Retorna com sucesso
            return redirect()->route('blog.index')->with('success', "Post <strong>{$postQuery->name}</strong>, foi criado com sucesso.");
        } catch (Exception $e) {
            # cancelo a transação
            DB::rollback();
            # retorno com a mensagem de erro
            return redirect()->back()->withInput($request->all())->with('error', $e->getMessage());
        }
    }

    public function show()
    {
        return view('blog::show');
    }

    public function edit($id, Post $post, PostCategory $postCategory)
    {
        try {
            # Listagem de todas as categorias
            $postCategories = $postCategory->whereStatus(1)
                ->orderBy('name')
                ->get()
                ->pluck('name', 'id');

            # Procuro pelo registro
            $edit = $post->with('PostCategory')->find($id);
            # Não existir, vai para o exception
            if (!$edit)
                throw new \Exception("Registro não encontrado.");
        } catch (\Exception $e) {
            return redirect()->route('blog.index')->with('error', $e->getMessage());
        }
        return view('blog::edit', compact('edit', 'postCategories'));
    }

    public function update($id, Post $post, PostRequest $request, FileHelper $fileHelper)
    {
        try {
            # Procuro pelo registro
            $edit = $post->with('PostCategory')->find($id);
            # Não existir, vai para o exception
            if (!$edit)
                return redirect()->route('blog.index')->with('error', 'Registro não encontrado.');

            # Se vir imagem, remove ela antes
            if ($request->hasFile('image') && $edit->banner_image_public_path != false) {
                unlink($edit->banner_image_public_path);
                $edit->PostImage->delete();
            }

            # Adiciona a imagem
            $this->add_image($request, $edit, $fileHelper);

            # Salvo a transação
            DB::commit();
            return redirect()->route('blog.index')->with('success', "Post {$edit->name}, editado com sucesso");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            # Retorna para atualização
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * @param $request
     * @param $post
     * @param $fileHelper
     *
     * Diminui a quantidade de código e reutilizar código
     */
    private function add_image($request, $post, $fileHelper)
    {
        # Se receber a imagem
        if ($request->hasFile('image')) {
            # Campo imagme
            $image = $request->file('image');
            # Largura e altura da imagem
            list($width, $height) = getimagesize($image->getPathname());
            # Cria um objeto da imagem
            $postImage = new PostImage(array(
                'key' => 'banner',
                'name' => $fileHelper->setFileName($image->getClientOriginalName())->getFileName(),
                'alt' => request()->has('name') ? request()->get('name') : '',
                'type' => $image->getClientMimeType(),
                'size' => $image->getSize(),
                'width' => $width,
                'height' => $height
            ));
            # Salva o arquivo
            $fileHelper->setInput('image')
                ->setDir('assets/uploads/blog/' . $post->id)
                ->setQuality(80)
                ->requestIntervationImage();

            # Relaciono a imagem ao POST
            $post->PostImage()->save($postImage);
        }
    }

    /**
     * @param $id
     * @param Post $post
     * @param PostCategoryPost $postCategory
     * @return \Illuminate\Http\RedirectResponse
     *
     * Remove o POST e suas relações
     */
    public function destroy($id, Post $post, PostCategoryPost $postCategory)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Procura pelo registro
            $exist = $post->find($id);
            if (!$exist)
                throw new \Exception("Registro não encontrado");

            # Se existir imagem, remove ela do banco do upload
            if ($exist->banner_image_public_path != false) {
                # Remove a imagem
                unlink($exist->banner_image_public_path);
                # Remove a imagem do banco
                $exist->PostImage->delete();
            }

            # Remove a Categoria
            $postCategory->wherePostId($exist->id)->delete();

            # Finalmente, deleta o post
            $exist->delete();

            # Finaliza a transação
            DB::commit();
            return redirect()->route('blog.index')->with('success', "POST {$exist->name}, removido com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->route('blog.index')->with('error', $e->getMessage());
        }
    }
}
