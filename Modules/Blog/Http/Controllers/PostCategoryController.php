<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Blog\Entities\PostCategory;
use Modules\Blog\Http\Requests\PostCategoryRequest;

class PostCategoryController extends Controller
{

    public function index(PostCategory $postCategory)
    {
        $data = $postCategory->with(['Posts' => function ($q) {
            return $q->count();
        }])->orderBy('created_at', 'desc')
            ->paginate(20);

        return view('blog::post_category.index', compact('data'));
    }

    public function create(PostCategory $postCategory)
    {
        $postCategories = $postCategory->whereStatus(1)
            ->orderBy('name')
            ->get()
            ->pluck('name', 'id');

        return view('blog::post_category.create', compact('postCategories'));
    }


    public function store(PostCategoryRequest $request, PostCategory $postCategory)
    {
        # inicia a transação
        DB::beginTransaction();
        try {
            # Salva a nova categoria
            $postCategoryQuery = $postCategory->create($request->all());
            # tudo certo, confirma a transação
            DB::commit();
            # retorna com successo
            return redirect()->route('post_category.create')->with('success', "<strong>{$postCategoryQuery->name}</strong>, está disponível.");
        } catch (\Exception $e) {
            # cancela a transação
            DB::rollback();
            # retorna para página de requisição com erro
            return redirect()->back()->with("error", "Não foi possível salvar a Nova Categoria.");
        }
    }

    public function show()
    {
        return view('blog::show');
    }

    public function edit($id, PostCategory $postCategory)
    {
        try {
            # procura pelo registro
            $edit = $postCategory->find($id);
            if (!$edit)
                throw new \Exception("Registro não encontrado");

            # Pego todas as categorias para utilizar de vínculo
            $postCategories = $postCategory->whereStatus(1)
                ->where('id', '!=', $id)
                ->orderBy('name')
                ->get()
                ->pluck('name', 'id');

        } catch (\Exception $e) {
            # Qualquer erro entra aqui, porém ,espeicífica que não existe.
            return redirect()->route('post_category.index')->with('error', $e->getMessage());
        }
        return view('blog::post_category.edit', compact('edit', 'postCategories'));
    }

    public function update($id, PostCategory $postCategory, PostCategoryRequest $request)
    {
        # inicia a transação
        DB::beginTransaction();
        try {
            # procura pelo registro
            $postCategoryQuery = $postCategory->find($id);
            if (!$postCategoryQuery)
                throw new \Exception("Registro não encontrado");

            # salva o registro
            $postCategoryQuery->update($request->all());
            # retorna com sucesso
            DB::commit();
            return redirect()->route('post_category.index')->with("success", "<strong>{$postCategoryQuery->name}</strong>, foi atualizado");
        } catch (\Exception $e) {
            # cancela a transação
            DB::rollback();
            # retorna para página de requisição com o erro
            return redirect()->back()->with("error", $e->getMessage());
        }
    }

    public function destroy($id, PostCategory $postCategory)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {
            # Verifico se a categoria existe
            $delete = $postCategory->find($id);
            # Se não existir, envia pra listagem de categorias
            if (!$delete)
                return redirect()->route("post_category.index")->with("error", "Registro não encontrado.");
            # Deleto o registro
            $delete->delete();
            # Conclui a transação
            DB::commit();
            return redirect()->route("post_category.index")->with("success", "Categoria removida com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->route("post_category.index")->with("error", $e->getMessage());
        }
    }
}
