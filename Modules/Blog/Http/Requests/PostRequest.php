<?php

namespace Modules\Blog\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PostRequest extends FormRequest
{

    public function attributes()
    {
        return [
            'name' => 'Nome',
            'category_id' => 'Categoria',
            'slug' => 'URL',
            'text' => 'Conteúdo',
            'image' => 'Imagem Banner',
            'video' => 'Vídeo',
            'thumb' => 'Thumb'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $return = [
            'name' => 'required|min:5|max:255',
            'slug' => 'unique:posts,slug,' . getSegmentFromEnd($this->segments(), 2) . ',id|max:255',
            'image' => 'nullable|required|image|custom_dimensions:width=' . config('blog.banner_width') . ',height=' . config('blog.banner_height'),
            'video' => 'nullable|url'
        ];

        return $return;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


}
