<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostCategory extends Model
{
    use  SoftDeletes;

    protected $fillable = ["parent_id", "name", "slug", "status"];


    /**
     * @param $value
     *
     * Caso não seja enviado o parent_id, seta como default 0
     */
    public function setParentIdAttribute($value)
    {
        if (!$value)
            $this->attributes['parent_id'] = 0;
        else {
            $this->attributes['parent_id'] = $value;
        }
    }

    /**
     * @return mixed
     *
     * Converte a data de criação do registro para BR
     */
    public function getCreatedAttribute()
    {
        if ($this->created_at != '') {
            return $this->created_at->format('d/m/Y H:i:s');
        }
    }

    /**
     * @param $value
     *
     * Salva ou cria a slug, depende se o usuário enviou o valor
     */
    public function setSlugAttribute($value)
    {
        if (!$value) {
            $value = str_slug(trim(strip_tags($value)), '-');
        }
        $this->attributes['slug'] = $value;
    }

    /**
     * @return string
     *
     * Pega o status e transforma em um HTML
     */
    public function getStatusLabelAttribute()
    {
        switch ($this->status) {
            case 0:
                return '<label class="badge badge-danger">Inativo</label>';
                break;
            case 1:
                return '<label class="badge badge-primary">Ativo</label>';
                break;
            default:
                return '<label class="badge badge-danger">Inativo</label>';
                break;
        }
    }

    /**
     * Todos os posts que estão vinculado as categoria
     */
    public function Posts()
    {
        return $this->hasMany(PostCategoryPost::class, 'post_category_id');
    }

    public function Childs()
    {
        return $this->hasMany(PostCategory::class, 'parent_id', 'id');
    }
}
