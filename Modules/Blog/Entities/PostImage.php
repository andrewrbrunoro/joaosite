<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class PostImage extends Model
{

    protected $fillable = ["post_id", "name", "alt", "type", "size", "width", "height"];

}
