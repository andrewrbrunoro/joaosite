<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    public $path_image = "assets/uploads/blog/";

    protected $fillable = ["name", "slug", "resume", "text", "status", "release_date", "expiration_date"];

    protected $appends = ["category_id", "banner_image"];

    /**
     * @return string
     *
     * Pega a categoria que está vinculada
     */
    public function getCategoryIdAttribute()
    {
        if ($this->PostCategory->count()) {
            return $this->PostCategory->first()->pivot->post_category_id;
        }
        return "";
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     *
     * Pega a imagem
     */
    public function getBannerImageAttribute()
    {
        # Se existir uma imagem registrada
        if ($this->PostImage) {
            # Pega a imagem
            $image = $this->path_image . "{$this->id}/{$this->PostImage->name}";

            # Se a imagem existir na pasta
            if (is_file(public_path($image)))
                return url($image);
        }
        return "";
    }

    /**
     * @return bool|string
     *
     * Retorna o caminho da imagme com public_path
     */
    public function getBannerImagePublicPathAttribute()
    {
        # Se existir uma imagem registrada
        if ($this->PostImage) {
            # Pega a imagem
            $image = $this->path_image . "{$this->id}/{$this->PostImage->name}";

            # Se a imagem existir na pasta
            if (is_file(public_path($image)))
                return public_path($image);
        }
        return false;
    }

    /**
     * @return string
     *
     * Pega o status e transforma em um HTML
     */
    public function getStatusLabelAttribute()
    {
        switch ($this->status) {
            case 0:
                return '<label class="badge badge-danger">Inativo</label>';
                break;
            case 1:
                return '<label class="badge badge-primary">Ativo</label>';
                break;
            default:
                return '<label class="badge badge-danger">Inativo</label>';
                break;
        }
    }

    /**
     * @param $value
     *
     * Limpo a string
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = trim(strip_tags($value));
    }

    /**
     * @param $value
     *
     * Salva ou cria a slug, depende se o usuário enviou o valor
     */
    public function setSlugAttribute($value)
    {
        # Se o campo estiver vazio, utiliza o name para criar a slug
        if (!$value) {
            $value = str_slug($this->attributes['name'], '-');
        }
        $this->attributes['slug'] = $value;
    }

    public function PostCategory()
    {
        return $this->belongsToMany(PostCategory::class, 'post_category_posts', 'post_id', 'post_category_id')->orderBy('created_at', 'desc');
    }

    public function PostImage()
    {
        return $this->hasOne(PostImage::class, 'post_id')->orderBy('created_at', 'desc');
    }


}