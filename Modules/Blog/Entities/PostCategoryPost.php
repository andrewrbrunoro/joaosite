<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostCategoryPost extends Model
{
    use SoftDeletes;

    protected $fillable = ["post_id", "post_category_id"];
}
