$(document).ready(function () {

    // Pego o input com id #slug e adiciono um evento ao keypress
    $('#slug').on('keyup', function () {
        // O input que irá receber o valor slugify
        $('#slug-target').val(string_to_slug($(this).val()));
    });

});