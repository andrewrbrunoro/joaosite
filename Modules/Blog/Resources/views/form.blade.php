<div class="row">

    <image-preview inline-template>
        <div class="col-lg-12">
            <div class="form-group">
                @if (isset($edit) && $edit->banner_image != "")
                    <img src="{!! $edit->banner_image !!}" id="preview-image" class="img-responsive" v-if="!image" />
                    <img v-bind:src="image" id="preview-image" class="img-responsive" />
                @else
                    <img v-bind:src="image" id="preview-image" class="img-responsive" />
                @endif
            </div>
            <div class="form-group">
                <div class="form-group{!! $errors->first('image') ? ' has-error' : '' !!}">
                    <label for="image">
                        Imagem Banner ({!! config('blog.banner_width') !!}w x {!! config('blog.banner_height') !!}h) <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::file('image', ['class' => 'form-control', 'placeholder' => 'Imagem', 'v-on:change' => 'preview']) !!}
                    {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
            <hr>
        </div>
    </image-preview>

    {{--<image-preview inline-template>--}}
        {{--<div class="col-lg-12">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-6">--}}
                    {{--<div class="form-group">--}}
                        {{--<img v-bind:src="image" id="preview-image" class="img-responsive" />--}}
                    {{--</div>--}}
                    {{--<div class="form-group{!! $errors->first('thumb') ? ' has-error' : '' !!}">--}}
                        {{--<label for="thumb">--}}
                            {{--Thumb ({!! config('blog.thumb_width') !!}w x {!! config('blog.thumb_height') !!}h) <label class="badge badge-info">Opcional</label>--}}
                        {{--</label>--}}
                        {{--{!! Form::file('thumb', ['class' => 'form-control', 'placeholder' => 'Thumb', 'v-on:change' => 'preview']) !!}--}}
                        {{--{!! $errors->first('thumb', '<span class="help-block">:message</span>') !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<hr>--}}
        {{--</div>--}}
    {{--</image-preview>--}}

    <div class="col-lg-12">

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="clear">
                        Status <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('status', 1, true) !!} Ativo
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('status', 0, false) !!} Inativo
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                    <label for="name">
                        Título <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'slug', 'placeholder' => 'Título']) !!}
                    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('slug') ? ' has-error' : '' !!}">
                    <label for="slug">
                        URl <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    <div class="input-group">
                        <span class="input-group-addon">{!! url('/') !!}/</span>
                        {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'URl', 'id' => 'slug-target']) !!}
                    </div>
                    {!! $errors->first('slug', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('category_id') ? ' has-error' : '' !!}">
                    <label for="category_id">
                        Categoria <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::select('category_id', $postCategories, null, ['class' => 'form-control', 'placeholder' => 'Categoria']) !!}
                    {!! $errors->first('category_id', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div>
        
        <div class="form-group{!! $errors->first('video') ? ' has-error' : '' !!}">
            <label for="video">
                Vídeo <label class="badge badge-info">Opcional</label>
            </label>
            {!! Form::text('video', null, ['class' => 'form-control', 'placeholder' => 'Vídeo']) !!}
            {!! $errors->first('video', '<span class="text-danger">:message</span>') !!}
        </div>
        

        <div class="form-group{!! $errors->first('text') ? ' has-error' : '' !!}">
            <label for="text">
                Conteúdo <label class="badge badge-info">Opcional</label>
            </label>
            {!! Form::textarea('text', null, ['class' => 'form-control redactor-editor', 'placeholder' => 'Digite aqui o conteúdo da postagem.']) !!}
            {!! $errors->first('text', '<span class="text-danger">:message</span>') !!}
        </div>
    </div>

</div>

<div class="row">
    <div class="col-lg-6 col-sm-6">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check"></i> Salvar Post
        </button>
    </div>
</div>

@section('script')
    {!! Html::script(Module::asset('blog:js/crud.js')) !!}
    {!! Html::script('assets/js/components/image-preview.js') !!}
@endsection