@extends('dashboard::layouts.master')

@section('title', 'Novo Post')

@section('content')
    <div class="row">
        {!! Form::open(['route' => 'blog.store', 'files' => true]) !!}
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5>Novo Post</h5>
            </div>

            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('blog.index') !!}" class="btn btn-outline btn-warning">
                            <i class="fa fa-arrow-left"></i> Listar todas as postagens
                        </a>
                    </div>
                </div>

                <hr>


                @include('blog::form')
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop
