@extends('dashboard::layouts.master')

@section('title', 'Blog')

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Blog - <strong class="text-success">Posts</strong></h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-12">
                        <a href="{!! route('blog.create') !!}" class="btn btn-outline btn-success">
                            <i class="fa fa-plus"></i> Cadastrar Posts
                        </a>
                        <a href="{!! request()->fullUrl() !!}" class="btn btn-outline btn-default">
                            <i class="fa fa-repeat"></i> Atualizar
                        </a>
                    </div>
                </div>

                <hr>

                <div class="row">

                    <div class="col-lg-12">
                        <table class="table table-responsive table-bordered table-stripped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nome</th>
                                <th>URl</th>
                                <th>Status</th>
                                <th width="15%">Opções</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($data->count())
                                @foreach($data as $row)
                                    <tr>
                                        <td>{!! $row->id !!}</td>
                                        <td>{!! $row->name !!}</td>
                                        <td>{!! url("/{$row->slug}") !!}</td>
                                        <td>{!! $row->statusLabel !!}</td>
                                        <td>
                                            {!! Form::open(['route' => ['blog.destroy', $row->id], 'method' => 'DELETE', 'onsubmit' => 'beforeConfirm(this, event)']) !!}
                                            <a href="{!! route('blog.edit', $row->id) !!}" class="btn btn-outline btn-info btn-xs">
                                                <i class="fa fa-pencil"></i> Editar
                                            </a>
                                            <button class="btn btn-outline btn-danger btn-xs" type="submit">
                                                <i class="fa fa-trash"></i> Deletar
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">Nenhum post no momento, <a href="{!! route('blog.create') !!}">clique aqui</a> adicione um post</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>
@stop

