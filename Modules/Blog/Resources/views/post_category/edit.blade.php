@extends('dashboard::layouts.master')

@section('title', 'Edita a Categoria')

@section('content')
    <div class="row">
        {!! Form::model($edit, ['route' => ['post_category.update', $edit->id], 'method' => 'PATCH']) !!}
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5>Blog - <strong class="text-success">Nova Categoria</strong></h5>
            </div>

            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('post_category.index') !!}" class="btn btn-outline btn-warning">
                            <i class="fa fa-arrow-left"></i> Listar todas as categorias
                        </a>
                    </div>
                </div>

                <hr>


                @include('blog::post_category.form')
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop
