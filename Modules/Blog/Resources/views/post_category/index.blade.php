@extends('dashboard::layouts.master')

@section('title', 'Blog Categorias')

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Blog - <strong class="text-success">Categorias</strong></h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-12">
                        <a href="{!! route('post_category.create') !!}" class="btn btn-outline btn-success">
                            <i class="fa fa-plus"></i> Cadastrar Categoria
                        </a>
                        <a href="{!! request()->fullUrl() !!}" class="btn btn-outline btn-default">
                            <i class="fa fa-repeat"></i> Atualizar
                        </a>
                    </div>
                </div>

                <hr>

                <div class="row">

                    <div class="col-lg-12">
                        <table class="table table-responsive table-bordered table-stripped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nome</th>
                                    <th>URl</th>
                                    <th>Status</th>
                                    <th width="15%">Opções</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if ($data->count())
                                @foreach($data as $row)
                                    <tr>
                                        <td>{!! $row->id !!}</td>
                                        <td>{!! $row->name !!}</td>
                                        <td>
                                            <a target="_blank" href="{!! url("/{$row->slug}") !!}">{!! url("/{$row->slug}") !!}</a>
                                        </td>
                                        <td>
                                            {!! $row->statusLabel !!}
                                        </td>
                                        <td>
                                            {!! Form::open(['route' => ['post_category.destroy', $row->id], 'method' => 'DELETE', 'onsubmit' => 'beforeConfirm(this,event)']) !!}
                                            <a href="{!! route('post_category.edit', $row->id) !!}" class="btn btn-outline btn-info btn-xs">
                                                <i class="fa fa-pencil"></i> Editar
                                            </a>
                                            <button class="btn btn-outline btn-danger btn-xs" type="submit">
                                                <i class="fa fa-trash"></i> Deletar
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">Nenhuma categoria no momento, <a href="{!! route('post_category.create') !!}">clique aqui</a> adicione uma categoria</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>
@stop

