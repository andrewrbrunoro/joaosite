<div class="row">

    <div class="col-xs-12">

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="clear">
                        Status <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('status', 1, true) !!} Ativo
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('status', 0, false) !!} Inativo
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                    <label for="name">
                        Título <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'slug', 'placeholder' => 'Título']) !!}
                    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('slug') ? ' has-error' : '' !!}">
                    <label for="slug">
                        URl <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    <div class="input-group">
                        <span class="input-group-addon">{!! url('/') !!}/</span>
                        {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'URl', 'id' => 'slug-target']) !!}
                    </div>
                    {!! $errors->first('slug', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('parent_id') ? ' has-error' : '' !!}">
                    <label for="parent_id">
                        Categoria que será relacionada <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::select('parent_id', $postCategories, null, ['class' => 'form-control', 'placeholder' => 'Categoria']) !!}
                    {!! $errors->first('parent_id', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div>

        <hr>
    </div>

</div>

<div class="row">
    <div class="col-lg-6 col-sm-6">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check"></i> Salvar a categoria
        </button>
    </div>
</div>

@section('script')
    {!! Html::script(Module::asset('blog:js/crud.js')) !!}
@endsection
