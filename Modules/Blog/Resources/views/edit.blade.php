@extends('dashboard::layouts.master')

@section('title', 'Editar Post')

@section('content')
    <div class="row">
        {!! Form::model($edit, ['route' => ['blog.update', $edit->id], 'files' => true, 'method' => 'PATCH']) !!}
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5>Post - <strong class="text-success">{!! $edit->name !!}</strong></h5>
            </div>

            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('blog.index') !!}" class="btn btn-outline btn-warning">
                            <i class="fa fa-arrow-left"></i> Listar todas as postagens
                        </a>
                    </div>
                </div>

                <hr>

                @include('blog::form')
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop
