<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupAction extends Model
{
    use SoftDeletes;

    protected $fillable = ["group_id", "action_id"];
}
