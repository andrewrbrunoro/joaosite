<?php

namespace Modules\User\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'group_id', 'email', 'password', 'status', 'token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['created'];

    /**
     * Cria um token único de 30 caracteres
     */
    public function setNameAttribute($value)
    {
        if (request()->isMethod('POST')) {
            $this->attributes['token'] = base64_encode(str_limit(bcrypt(Carbon::now() . $this->attributes['email']), 30, ''));
        }
        $this->attributes['name'] = $value;
    }

    public function setCpfAttribute()
    {

    }

    public function UserAddresses()
    {
        return $this->hasMany(UserAddress::class, 'user_id');
    }

    public function UserAddress()
    {
        return $this->hasOne(UserAddress::class, 'user_id')->orderBy('created_at', 'desc');
    }

    public function getCreatedAttribute()
    {
        if ($this->created_at != '') {
            return $this->created_at->format('d/m/Y H:i:s');
        }
    }

    public function setPasswordAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['password'] = bcrypt($value);
        } else {
            unset($this->attributes['password']);
            unset($this->attributes['password_confirmation']);
        }
    }
}
