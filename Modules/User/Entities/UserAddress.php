<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAddress extends Model
{

    use SoftDeletes;

    protected $fillable = ["user_id", "zip_code", "street", "street_number", "district", "city", "state", "country", "collection", "status"];
    

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id')->withDefault();
    }

}
