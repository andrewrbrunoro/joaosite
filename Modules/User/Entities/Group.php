<?php

namespace Modules\User\Entities;

use App\Traits\Slug;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{

    use SoftDeletes, Slug;

    protected $fillable = ["name", "slug", "redirect", "system_access"];

}
