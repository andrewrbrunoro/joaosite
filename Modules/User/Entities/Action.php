<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{

    protected $fillable = ["route", "action", "method", "uri", "middleware", "domain"];

    protected $appends = ["full_uri"];

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     *
     * Retorna o LINK completo
     */
    public function getFullUriAttribute()
    {
        return url($this->uri);
    }

}
