<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $return['name'] = 'required|regex:/^[a-zA-Z ]+$/u|min:3';
        if ($this->method() == 'PATCH') {
            $return['email'] = 'required|email|unique:users,email,' . getSegmentFromEnd($this->segments(), 2) . ',id';
            if ($this->request->get('password') != '') {
                $return['password'] = 'required|min:3|confirmed';
                $return['password_confirmation'] = 'required|min:3';
            }
        }

        if ($this->method() == 'POST') {
            $return['password'] = 'required|min:3|confirmed';
            $return['password_confirmation'] = 'required|min:3';
            $return['email'] = 'required|email|unique:users,email';
        }

        if ($this->request->has('UserAddress') && $this->request->get('UserAddress')['zip_code'] != '') {
            $return['UserAddress.*'] = 'required';
        }
        return $return;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'name.required' => 'O Nome é obrigatório.',
            'name.regex' => 'Digite apenas letras.',
            'name.min' => 'Tamanho mínimo de 3 caracteres.',
            'email.required' => 'O E-mail é obrigatório.',
            'email.email' => 'Digite um e-mail válido.',
            'email.unique' => 'Este e-mail já está cadastrado.',
            'password.required' => 'A senha é obrigatória.',
            'password.min' => 'Tamanho mínimo de 3 caracteres.',
            'password.confirmed' => 'As senhas não estão iguais.',
            'password_confirmation.required' => 'Confirmar a senha é obrigatório.',
            'password_confirmation.min' => 'Tamanho mínimo de 3 caracteres.',
            'UserAddress.*' => 'Campo obrigatório'
        ];
    }
}
