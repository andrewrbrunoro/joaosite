<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;
use Modules\User\Entities\User;
use Modules\User\Entities\UserAddress;
use Modules\User\Http\Requests\UserRequest;

class UserController extends Controller
{

    public function index(User $user)
    {
        $data = $user->orderBy('id', 'desc');
        return view('user::index', ['data' => $data->paginate(15)]);
    }

    public function create()
    {
        return view('user::create');
    }


    public function store(UserRequest $request, User $user)
    {
        # Inícia a transação com o banco de dados
        DB::beginTransaction();
        try {
            # Cria o usuário
            $store = $user->create($request->all());

            # Verifica se está passando endereço
            if ($request->get('UserAddress')['zip_code'] != '') {
                # Relaciona o endereço com o usuário / adicionar o endereço como cobrança é temporário
                $store->UserAddresses()->saveMany([new UserAddress($request->get('UserAddress') + ['collection' => 1])]);
            }

            # Tudo certo, comita a transação
            DB::commit();
        } catch (\Exception $e) {
            # A transação falhou, retorna os dados
            DB::rollback();
            return redirect()->back()->with("error", "Não foi possível adicionar o usuário <strong>{$request->get('name')}</strong>, tente novamente.");
        }
        return redirect()->back()->with("success", "Usuário <strong>{$request->get('name')}</strong>, foi cadastrado com sucesso.");
    }

    public function show()
    {
        return view('user::show');
    }

    public function edit($id, User $user)
    {
        try {
            $edit = $user->with('UserAddress')->find($id);
            if (!$edit)
                throw new Exception("Usuário <strong>#{$id}</strong> não encontrado.");
        } catch (Exception $e) {
            return redirect()->route('user.index')->with("error", $e->getMessage());
        }
        return view('user::edit', compact('edit'));
    }

    public function update($id, UserRequest $request, User $user)
    {
        # Inícia a transação com o banco de dados
        DB::beginTransaction();
        try {

            # Verifica se o usuário existe
            $update = $user->with('UserAddress')
                ->find($id);

            # Se não existir, retorna avisando que o ID não foi encontrado
            if (!$update)
                throw new Exception("Usuário <strong>{$request->get('name')}</strong> não foi encontrado.");

            # Atuliza os dados
            $update->update($request->except(['email']));

            # Verifica se está passando endereço
            if ($request->filled('UserAddress')) {
                # Versão inicial, atualiza apenas um endereço
                if($update->UserAddress){
                    $update->UserAddress->update($request->get('UserAddress') + ['collection' => 1]);
                }else{
                    $update->UserAddress()->save(new UserAddress($request->get('UserAddress') + ['collection' => 1]));
                }
            }

            # Tudo certo, comita a transação
            DB::commit();
        } catch (\Exception $e) {
            # A transação falhou, retorna os dados
            DB::rollback();
            return redirect()->back()->with("error", $e->getMessage());
        }
        return redirect()->back()->with("success", "Usuário <strong>{$request->get('name')}</strong> foi atualizado.");
    }

    public function destroy($id, User $user)
    {
        DB::beginTransaction();
        try {
            $exist = $user->with('UserAddress')->find($id);
            if (!$exist)
                throw new Exception("Usuário <strong>#{$id}</strong> não encontrado.");

            $exist->UserAddress->delete();
            $exist->delete();
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->route('user.index')->with("error", $e->getMessage());
        }
        DB::commit();
        return redirect()->route('user.index')->with("success", "Usuário {$exist->name} foi deletado.");
    }
}
