<?php

namespace Modules\User\Http\Controllers;

use GuzzleHttp\Psr7\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\User\Entities\Action;
use Modules\User\Entities\Group;
use Modules\User\Entities\GroupAction;
use Modules\User\Http\Requests\GroupRequest;
use Modules\User\Http\Requests\GroupUpgradeRequest;

class GroupController extends Controller
{

    public function index(Group $group)
    {
        # Paginação
        $data = $group->orderBy('id', 'desc')->paginate(20);

        return view("user::group.index", compact("data"));
    }

    public function show($id, Action $action, GroupAction $groupAction)
    {
        # Adiciona o ID do grupo em uma sessão para não existir tentiva de atualização por request terceiro
        session()->put('group_show_id', $id);

        # Permissões que já existem no GRUPO
        $group_actions = $groupAction->where('group_id', '=', $id)
            ->pluck('action_id')
            ->toArray();

        # Permissões disponíveis
        $data = $action->where('uri', 'like', '%admin%')
            ->paginate(50);

        return view("user::group.show", compact('data', 'group_actions'));
    }

    /**
     * @param GroupUpgradeRequest $request
     * @param GroupAction $groupAction
     * @return \Illuminate\Http\JsonResponse
     *
     * Irá receber uma requisição ajax com método POST e irá vincular o grupo com a permissão
     */
    public function groupUpgrade(GroupUpgradeRequest $request, GroupAction $groupAction)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {
            # Verifico se a liberação da permissão está vindo do gerenciador do grupo
            if (!preg_match('/admin\/grupo\/([0-9])\/permissoes/', $request->server('HTTP_REFERER')))
                throw new \Exception("Tentiva inválida.");

            # Verifico se existir a sessão do grupo
            if (!session()->has('group_show_id'))
                throw new \Exception("Não foi possível salvar o registro, tente novamente, caso o erro persista entre em contato conosco.");

            # Verifico se relação já não existe
            $exist = $groupAction->where([
                'group_id' => session('group_show_id'),
                'action_id' => $request->get('action_id')
            ]);

            # Se existir
            if ($exist->count() > 0) {
                # Mensagem de bloqueio
                $message = "Permissão bloqueada com sucesso.";
                # Deleto a ação do grupo
                $exist->delete();
            } else {
                # Mensagem de liberação
                $message = "Permissão liberada com sucesso.";
                # Libero a ação pro grupo
                $groupAction->create([
                    'group_id' => session('group_show_id'),
                    'action_id' => $request->get('action_id')
                ]);
            }

            # Conclui a transação
            DB::commit();
            return response()->json(["error" => false, "message" => $message], 200);
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return response()->json(["error" => true, "message" => $e->getMessage()], 400);
        }
    }

    public function create(Action $action)
    {

        # Lista todas as rotas que estão apontada para INDEX
        $actions = $action->where('route', 'like', '%.index%')
            ->where('method', '=', 'GET|HEAD')
            ->get()
            ->pluck('full_uri', 'route');

        return view("user::group.create", compact("actions"));
    }

    public function store(GroupRequest $groupRequest, Group $group)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Crio o grupo
            $group->create($groupRequest->all());

            # Conclui a transação
            DB::commit();
            return redirect()->route("group.index")->with("success", "Grupo <strong>{$groupRequest->get('name')}</strong>, criado com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->with("error", $e->getMessage());
        }
    }

    public function edit($id, Group $group, Action $action)
    {
        try {

            # Procuro o Grupo pelo ID
            $edit = $group->find($id);

            # Se não existir, envio pra exception
            if (!$edit)
                throw new \Exception("Registro não encontrado.");

            # Lista todas as rotas que estão apontada para INDEX
            $actions = $action->where('route', 'like', '%.index%')
                ->where('method', '=', 'GET|HEAD')
                ->get()
                ->pluck('full_uri', 'route');


        } catch (\Exception $e) {
            return redirect()->route("group.index")->with("error", $e->getMessage());
        }
        return view("user::group.edit", compact("edit", "actions"));
    }

    public function update($id, GroupRequest $request, Group $group)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Procuro o Grupo pelo ID
            $edit = $group->find($id);

            # Se não existir, envio pra exception
            if (!$edit)
                return redirect()->route("group.index")->with("error", "Registro não encontrado.");

            # Atualiza o Grupo
            $edit->update($request->all());

            # Conclui a transação
            DB::commit();
            return redirect()->route("group.index")->with("success", "Grupo <strong>{$request->get('name')}</strong> atualizado com sucesso");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->with("error", $e->getMessage());
        }
    }

    public function destroy($id, Group $group)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {
            # Verifico se o Grupo existe
            $exist = $group->find($id);
            # Se não existir, envio pra exception
            if (!$exist)
                throw new \Exception("Registro não encontrado.");

            # Deleto o Grupo
            $exist->delete();

            # Conclui a transação
            DB::commit();
            return redirect()->route("group.index")->with("success", "<strong>Grupo</strong> deletado com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->route("group.index")->with("error", $e->getMessage());
        }
    }

}
