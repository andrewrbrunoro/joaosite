<?php

Route::group([
    'namespace' => 'Modules\User\Http\Controllers',
    'prefix' => 'usuarios',
    'as' => 'user.'
], function ($r) {
    $r->get('/', 'UserController@index')->name('index');
    $r->get('{id}/visualizar', 'UserController@show')->name('show');

    $r->get('{id}/editar', 'UserController@edit')->name('edit');
    $r->patch('{id}/editar', 'UserController@update')->name('update');

    $r->get('criar', 'UserController@create')->name('create');
    $r->post('criar', 'UserController@store')->name('store');

    $r->delete('{id}', 'UserController@destroy')->name('destroy');
});


Route::group([
    'namespace' => 'Modules\User\Http\Controllers',
    'prefix' => 'grupo',
    'as' => 'group.'
], function ($r) {
    
    $r->get('/', 'GroupController@index')->name('index');
    $r->get('{id}/permissoes', 'GroupController@show')->name('show');
    $r->post('/group-upgrade', 'GroupController@groupUpgrade')->name('group_upgrade');

    $r->get('{id}/editar', 'GroupController@edit')->name('edit');
    $r->patch('{id}/editar', 'GroupController@update')->name('update');

    $r->get('criar', 'GroupController@create')->name('create');
    $r->post('criar', 'GroupController@store')->name('store');

    $r->delete('{id}', 'GroupController@destroy')->name('destroy');
});
