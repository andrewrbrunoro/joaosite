<div class="row">

    <div class="col-lg-12">
        <h3 class="forum-title bg-primary">Dados do usuário</h3>
    </div>


    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="clear">
                        Status <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('status', 1, true) !!} Ativo
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('status', 0, false) !!} Inativo
                    </label>
                </div>
            </div>
        </div>
    </div>


    @if (isset($edit))

        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                        <label for="name">
                            Nome <label class="badge badge-danger">Obrigatório</label>
                        </label>
                        {!! Form::text('name', null, ['class' => 'form-control', 'required', 'placeholder' => 'Nome']) !!}
                        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-6 col-sm-6">
                    <div class="form-group{!! $errors->first('email') ? ' has-error' : '' !!}">
                        <label for="email">
                            E-mail <label class="badge badge-inverse">Não editável</label>
                        </label>
                        {!! Form::text('email', null, ['class' => 'form-control', 'readonly', 'placeholder' => 'E-mail']) !!}
                        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group{!! $errors->first('password') ? ' has-error' : '' !!}">
                        <label for="password">
                            Senha <label class="badge badge-info">Opcional</label>
                        </label>
                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Senha']) !!}
                        {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group{!! $errors->first('password_confirmation') ? ' has-error' : '' !!}">
                        <label for="password_confirmation">
                            Repetir senha <label class="badge badge-info">Opcional</label>
                        </label>
                        {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Repetir senha']) !!}
                        {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
    @else

        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                        <label for="name">
                            Nome <label class="badge badge-danger">Obrigatório</label>
                        </label>
                        {!! Form::text('name', null, ['class' => 'form-control', 'required', 'placeholder' => 'Nome']) !!}
                        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-6 col-sm-6">
                    <div class="form-group{!! $errors->first('email') ? ' has-error' : '' !!}">
                        <label for="email">
                            E-mail <label class="badge badge-danger">Obrigatório</label>
                        </label>
                        {!! Form::text('email', null, ['class' => 'form-control', 'required', 'placeholder' => 'E-mail']) !!}
                        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-6">
            <div class="form-group{!! $errors->first('password') ? ' has-error' : '' !!}">
                <label for="password">
                    Senha <label class="badge badge-danger">Obrigatório</label>
                </label>
                {!! Form::password('password', ['class' => 'form-control', 'required', 'placeholder' => 'Senha']) !!}
                {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
            </div>
        </div>
        <div class="col-lg-6 col-sm-6">
            <div class="form-group{!! $errors->first('password_confirmation') ? ' has-error' : '' !!}">
                <label for="password_confirmation">
                    Repetir senha <label class="badge badge-danger">Obrigatório</label>
                </label>
                {!! Form::password('password_confirmation', ['class' => 'form-control', 'required', 'placeholder' => 'Repetir senha']) !!}
                {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
            </div>
        </div>
    @endif

    <div class="col-lg-12">
        <h3 class="forum-title bg-primary">Endereço</h3>
    </div>

    <address-component inline-template>
        <div>
            <div class="col-lg-3">
                <div class="form-group{!! $errors->first('UserAddress.zip_code') ? ' has-error' : '' !!}">
                    <label for="UserAddress[zip_code]">
                        CEP <label class="badge badge-info">opcional</label>
                    </label>
                    {!! Form::text('UserAddress[zip_code]', null, ['class' => 'form-control', 'v-mask' => "'#####-###'", 'placeholder' => 'Digite aqui o CEP do usuário', 'v-model' => 'address.cep']) !!}
                    {!! $errors->first('UserAddress.zip_code', '<span class="help-block text-danger">:message</span>') !!}
                </div>
            </div>

            <div class="col-lg-3" v-show="address.cep != ''">
                <div class="form-group{!! $errors->first('UserAddress.state') ? ' has-error' : '' !!}">
                    <label for="UserAddress[state]">
                        Estado <label class="badge badge-danger">opcional</label>
                    </label>
                    {!! Form::select('UserAddress[state]', getStates(), null, ['class' => 'form-control select', 'v-model' => 'address.state']) !!}
                    {!! $errors->first('UserAddress.state', '<span class="help-block text-danger">:message</span>') !!}
                </div>
            </div>

            <div class="col-lg-3" v-show="address.cep != ''">
                <div class="form-group{!! $errors->first('UserAddress.district') ? ' has-error' : '' !!}">
                    <label for="UserAddress[district]">
                        Bairro <label class="badge badge-danger">opcional</label>
                    </label>
                    {!! Form::text('UserAddress[district]', null, ['class' => 'form-control', 'placeholder' => 'Bairro', 'v-model' => 'address.district']) !!}
                    {!! $errors->first('UserAddress.district', '<span class="help-block text-danger">:message</span>') !!}
                </div>
            </div>

            <div class="col-lg-3" v-show="address.cep != ''">
                <div class="form-group{!! $errors->first('UserAddress.city') ? ' has-error' : '' !!}">
                    <label for="UserAddress[city]">
                        Cidade <label class="badge badge-danger">opcional</label>
                    </label>
                    {!! Form::text('UserAddress[city]', null, ['class' => 'form-control', 'placeholder' => 'Cidade', 'v-model' => 'address.city']) !!}
                    {!! $errors->first('UserAddress.city', '<span class="help-block text-danger">:message</span>') !!}
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="col-lg-3" v-show="address.cep != ''">
                <div class="form-group{!! $errors->first('UserAddress.street') ? ' has-error' : '' !!}">
                    <label for="UserAddress[street]">
                        Logradouro <label class="badge badge-danger">opcional</label>
                    </label>
                    {!! Form::text('UserAddress[street]', null, ['class' => 'form-control', 'placeholder' => 'Logradouro', 'v-model' => 'address.street']) !!}
                    {!! $errors->first('UserAddress.street', '<span class="help-block text-danger">:message</span>') !!}
                </div>
            </div>

            <div class="col-lg-3" v-show="address.cep != ''">
                <div class="form-group{!! $errors->first('UserAddress.street_number') ? ' has-error' : '' !!}">
                    <label for="UserAddress[street_number]">
                        Número <label class="badge badge-danger">opcional</label>
                    </label>
                    {!! Form::text('UserAddress[street_number]', null, ['class' => 'form-control', 'placeholder' => 'Exemplo: 222, Apto 12 / 33']) !!}
                    {!! $errors->first('UserAddress.street_number', '<span class="help-block text-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </address-component>

</div>

<div class="row">
    <div class="col-lg-6 col-sm-6">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check"></i> Salvar os dados
        </button>
    </div>
</div>

@section('script')
    {!! Html::script(Module::asset('user:component/address.js')) !!}
@endsection