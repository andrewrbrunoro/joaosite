<div class="row">

    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="clear">
                        Acesso ao Sistema <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('system_access', 1, false) !!} Ativo
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('system_access', 0, true) !!} Inativo
                    </label>
                </div>
            </div>
        </div>
        <hr>
    </div>

    <div class="col-lg-12 col-xs-12">
        <div class="row">
            <div class="col-lg-6 col-xs-6">
                <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                    <label for="name">
                        Nome <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                    {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
            <div class="col-lg-6 col-xs-6">
                <div class="form-group{!! $errors->first('redirect') ? ' has-error' : '' !!}">
                    <label for="redirect">
                        Página <label class="badge badge-info">Opcional</label> <small>(Será acessada após fazer o login no sistema)</small>
                    </label>
                    {!! Form::select('redirect', $actions, null, ['class' => 'form-control', 'placeholder' => 'Página']) !!}
                    {!! $errors->first('redirect', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-6 col-sm-6">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check"></i> Salvar Grupo
        </button>
    </div>
</div>
