@extends('dashboard::layouts.master')

@section('title', 'Vincular Permissões')

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Vincular Permissões</h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('group.index') !!}" class="btn btn-outline btn-warning">
                            <i class="fa fa-arrow-left"></i> Listar todos os Grupo
                        </a>
                        <a href="{!! request()->fullUrl() !!}" class="btn btn-outline btn-default">
                            <i class="fa fa-repeat"></i> Atualizar
                        </a>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-lg-12 col-xs-12 table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Permissões</th>
                                <th>Metodo</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($data) && $data->count())
                                @foreach($data as $row)
                                    <tr>
                                        <td>
                                            <input type="checkbox"{!! in_array($row->id, $group_actions) ? ' checked' : '' !!} class="action" value="{!! $row->id !!}"/>
                                        </td>
                                        <td>{!! url($row->uri) !!}</td>
                                        <td>{!! $row->method !!}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                        @if (isset($data))
                            {!! $data->links() !!}
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.action').on('click', function () {
                var self = $(this);
                axios.post(CMS_URL + "grupo/group-upgrade", {
                    action_id: self.val()
                }).then(function (r) {
                    toastr.success(r.data.message);
                }).catch(function (r) {
                    r = r.response.data;
                    toastr.error(r.message);
                });
            });
        });
    </script>
@endsection