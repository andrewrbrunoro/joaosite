@extends('dashboard::layouts.master')

@section('title', 'Grupos')

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Grupos</h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('group.create') !!}" class="btn btn-outline btn-success">
                            <i class="fa fa-plus"></i> Cadastrar Grupo
                        </a>
                        <a href="{!! request()->fullUrl() !!}" class="btn btn-outline btn-default">
                            <i class="fa fa-repeat"></i> Atualizar
                        </a>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-lg-12 col-xs-12 table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th>Nome</th>
                                    <th>Página</th>
                                    <th title="Acesso ao Sistema">A. Sistema</th>
                                    <th width="25%">Opções</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($data) && $data->count())
                                    @foreach($data as $row)
                                        <tr>
                                            <td>{!! $row->id !!}</td>
                                            <td>{!! $row->name !!}</td>
                                            <td>{!! $row->redirect ? route($row->redirect) : '' !!}</td>
                                            <td>
                                                {!! $row->system_access ? "Sim" : "Não" !!}
                                            </td>
                                            <td>
                                                {!! Form::open(['route' => ['group.destroy', $row->id], 'method' => 'DELETE', 'onsubmit' => 'beforeConfirm(this, event)']) !!}
                                                <a href="{!! route('group.show', $row->id) !!}" class="btn btn-xs btn-outline btn-success">
                                                    <i class="fas fa-lock-open"></i> Permissões
                                                </a>
                                                <a href="{!! route('group.edit', $row->id) !!}" class="btn btn-outline btn-info btn-xs">
                                                    <i class="fa fa-pencil"></i> Editar
                                                </a>
                                                <button class="btn btn-outline btn-danger btn-xs" type="submit">
                                                    <i class="fa fa-trash"></i> Deletar
                                                </button>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5">Nenhum Grupo registrado no momento, <a href="{!! route('group.create') !!}">clique aqui</a> para cadastrar.</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>

                        @if (isset($data))
                            {!! $data->links() !!}
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop
