@extends('dashboard::layouts.master')

@section('title', 'Cadastrar Grupo')

@section('content')
    <div class="row">
        {!! Form::open(['route' => 'group.store', 'method' => 'post']) !!}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastrar Grupo</h5>
                </div>

                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="{!! route('group.index') !!}" class="btn btn-outline btn-warning">
                                <i class="fa fa-arrow-left"></i> Listar todos os Grupo
                            </a>
                        </div>
                    </div>
                    <hr>
                    @include('user::group.form')
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@stop
