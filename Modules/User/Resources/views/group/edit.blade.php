@extends('dashboard::layouts.master')

@section('title', 'Editar Grupo')

@section('content')
    <div class="row">
        {!! Form::model($edit, ['route' => ['group.update', $edit->id], 'method' => 'PATCH']) !!}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Grupo - <strong></strong></h5>
                </div>

                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="{!! route('group.index') !!}" class="btn btn-outline btn-warning">
                                <i class="fa fa-arrow-left"></i> Listar todos os Grupo
                            </a>
                        </div>
                    </div>
                    <hr>
                    @include('user::group.form')
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@stop
