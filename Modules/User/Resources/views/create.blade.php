@extends('dashboard::layouts.master')

@section('title', 'Cadastrar usuário')

@section('content')
    <div class="row">
        {!! Form::open(['route' => 'user.store', 'method' => 'post']) !!}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastrar usuário</h5>
                </div>

                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="{!! route('user.index') !!}" class="btn btn-outline btn-warning">
                                <i class="fa fa-arrow-left"></i> Listar todos os usuários
                            </a>
                        </div>
                    </div>
                    <hr>
                    @include('user::form')
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@stop
