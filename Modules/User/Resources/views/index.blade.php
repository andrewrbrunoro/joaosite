@extends('dashboard::layouts.master')

@section('title', 'Usuários')

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Usuários cadastrados</h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('user.create') !!}" class="btn btn-outline btn-success">
                            <i class="fa fa-plus"></i> Cadastrar usuário
                        </a>
                        <a href="{!! request()->fullUrl() !!}" class="btn btn-outline btn-default">
                            <i class="fa fa-repeat"></i> Atualizar
                        </a>
                    </div>
                </div>

                <hr>

                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>Data de cadastro</th>
                            <th>Opções</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($data) && $data->count())
                            @foreach($data as $row)
                                <tr>
                                    <td>{!! $row->id !!}</td>
                                    <td>{!! $row->name !!}</td>
                                    <td>{!! $row->email !!}</td>
                                    <td>{!! $row->created !!}</td>
                                    <td>
                                        {!! Form::open(['route' => ['user.destroy', $row->id], 'method' => 'DELETE', 'onsubmit' => 'beforeConfirm(this, event)']) !!}
                                        <a href="{!! route('user.edit', $row->id) !!}" class="btn btn-outline btn-info btn-xs">
                                            <i class="fa fa-pencil"></i> Editar
                                        </a>
                                        <button class="btn btn-outline btn-danger btn-xs" type="submit">
                                            <i class="fa fa-trash"></i> Deletar
                                        </button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">Nenhum usuário registrado no momento, <a href="{!! route('user.create') !!}">clique aqui</a> para cadastrar um novo usuário</td>
                            </tr>
                        @endif
                    </tbody>
                </table>

                @if (isset($data))
                    {!! $data->links() !!}
                @endif

            </div>
        </div>
    </div>
@stop
