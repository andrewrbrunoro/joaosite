<?php

return [
    'name' => 'User',
    'groups' => [
        "Administrador",
        "Editor",
        "Professor",
        "Parceiro",
        "Operador de vídeo"
    ]
];
