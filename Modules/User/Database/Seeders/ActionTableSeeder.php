<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;
use Modules\User\Entities\Action;

class ActionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->setup();
        // $this->call("OthersTableSeeder");
    }

    private function setup()
    {
        $action = new Action();
        $routes = Route::getRoutes();
        foreach ($routes as $route) {
            $exist = $action->whereUri($route->uri)->count();
            if ($exist == 0) {
                $action->create([
                    "route" => $route->getName(),
                    "action" => isset($route->action['controller']) ? $route->action['controller'] : '',
                    "method" => implode('|', $route->methods),
                    "uri" => $route->uri,
                    "middleware" => isset($route->action['middleware']) ? implode(',', $route->action['middleware']) : '',
                    "domain" => ''
                ]);
            }
        }
    }
}
