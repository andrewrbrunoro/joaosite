<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\User\Entities\Group;

class GroupTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        # Instâncio a model
        $group = new Group();
        # Leio os grupos que serão criados
        foreach (config('user.groups') as $item) {
            $group->create(["name" => $item, "redirect" => "dashboard.index", "system_access" => 1]);
        }
        // $this->call("OthersTableSeeder");
    }
}
