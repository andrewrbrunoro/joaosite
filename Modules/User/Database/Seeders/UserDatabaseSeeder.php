<?php

namespace Modules\User\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\User\Entities\User;

class UserDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $user = new User();

        if (!$user->whereEmail("admin@sistema.com.br")->count()) {
            $user->create([
                'name' => 'Administrador',
                'email' => 'admin@sistema.com.br',
                'password' => 540120
            ]);
        }

        $this->call(ActionTableSeeder::class);
        $this->call(GroupTableSeeder::class);
    }
}
