<?php

namespace Modules\Auth\Entities;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    protected $fillable = ["name", "email", "password"];

    /**
     * @param $value
     *
     * Encrypta a senha
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

}
