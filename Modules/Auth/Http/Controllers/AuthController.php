<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Exception;
use Modules\Auth\Entities\User;
use Modules\Auth\Http\Requests\AuthRegisterRequest;
use Modules\Auth\Http\Requests\AuthRequest;

class AuthController extends Controller
{

    # Rota para onde o usuário será direicionado após autenticação
    private $route_auth = 'dashboard.index';

    # Rota para onde será direicionado ao ser deslogado
    private $route_logout = 'auth.index';

    public function index()
    {
        return view('auth::index');
    }

    /**
     * @param AuthRequest $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * Verifica se está passando os dados corretos
     */
    public function login(AuthRequest $request)
    {
        # Se não autenticar, retorna com erros
        if (!auth()->attempt($request->only(['email', 'password']))) {
            return redirect()->back()->withErrors([
                'email' => 'Verifique se digitou o e-mail corretamente.',
                'password' => 'Verifique se digitou a senha corretamente.']);
        }

        # Redireciona se passar na autenticação
        return redirect()->route($this->route_auth);
    }

    /**
     * @param AuthRegisterRequest $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     *
     * Se estiver passando os dados corretamente, cadastra o usuário
     */
    public function register(AuthRegisterRequest $request, User $user)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {
            # Cria o usuário
            $QueryUser = $user->create([
                'name' => $request->get('register_name'),
                'email' => $request->get('register_email'),
                'password' => $request->get('register_password')
            ]);
            # Se tudo deu certo, salva o mesmo
            DB::commit();

            # Autentica o usuário pelo ID
            auth()->loginUsingId($QueryUser->id);

            # Redireciona para o dashboard autenticado
            return redirect()->route($this->route_auth)->with('success', setting('sistema.cadastro.agradecimento'));
        } catch (Exception $e) {
            # Retorna e não finaliza o cadastro
            DB::rollback();
            return redirect()->back()->with('error', 'Não foi possível finalizar seu cadastro, tente novamente.');
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     *
     * Remove a sessão do usuário e retorna para a área do login
     */
    public function logout()
    {
        auth()->logout();
        return redirect()->route($this->route_logout)->with('success', 'Volte logo, estaremos esperando por você :)');
    }


}
