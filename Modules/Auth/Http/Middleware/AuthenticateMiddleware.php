<?php

namespace Modules\Auth\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthenticateMiddleware
{

    private $route_login = 'auth.login';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        # Se for um usuário anônimo, redireciona ou responde por falta de permissão
        if (auth()->guest()) {
            # Se for ajax, não autorizado header
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                # Retorna para a página de login
                return redirect()->route($this->route_login);
            }
        }

        return $next($request);
    }
}
