<?php

namespace Modules\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthRegisterRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'register_name' => 'required',
            'register_email' => 'required|email|unique:users,email',
            'register_password' => 'required|min:6|confirmed',
            'register_password_confirmation' => 'required|min:6',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'register_name.required' => 'Campo obrigatório.',
            'register_email.required' => 'Campo obrigatório.',
            'register_email.unique' => 'Este e-mail já está cadastrado.',
            'register_email.email' => 'E-mail inválido.',
            'register_password.required' => 'Campo obrigatório.',
            'register_password_confirmation.required' => 'Campo obrigatório.',
            'register_password.min' => 'Mínimo 6 caracteres.',
            'register_password.confirmed' => 'Confirme a senha.',
            'register_password_confirmation.min' => 'Mínimo 6 caracteres.',

        ];
    }
}
