<?php

$namespace = 'Modules\Auth\Http\Controllers';

Route::get('/autenticacao', $namespace.'\AuthController@index')
    ->middleware('guest')
    ->name('auth.index');

Route::post('/autenticacao', $namespace.'\AuthController@login')
    ->middleware('guest')
    ->name('auth.login');

Route::post('/autenticacao/registro', $namespace.'\AuthController@register')
    ->middleware('guest')
    ->name('auth.register');

Route::get('/autenticacao/logout', $namespace.'\AuthController@logout')
    ->name('auth.logout');

//Route::group([
//    'namespace' => 'Modules\Auth\Http\Controllers',
//    'prefix' => 'autenticacao',
////    'middleware' => ['guest'],
//    'as' => 'auth.'
//], function ($r) {
//
////    $r->get('/', 'AuthController@index')->name('index');
//    $r->post('/', 'AuthController@login')->name('login');
//
//    $r->post('/registro', 'AuthController@register')->name('register');
//
//    Route::get('/logout', 'AuthController@logout')->name('logout');
//});
