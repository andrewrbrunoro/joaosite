@extends('auth::layouts.master')

@section('content')
    <div class="wrapper">
        <div id="formContent">
            <!-- Tabs Titles -->
            <h2 class="active tab" data-ref="auth">CONECTAR-SE</h2>

            <!-- Login Form -->
            <div id="auth" class="tab-content fadeIn">
                <!-- Icon -->
                <div class="fadeIn first">
                    <img src="{!! Module::asset('auth:images/icon.svg') !!}" id="icon" alt="Avatar"/>
                </div>
                {!! Form::open(['route' => 'auth.login']) !!}

                {!! Form::text('email', null, ['placeholder' => 'E-mail', 'required', 'class' => 'fadeIn second']) !!}
                {!! $errors->first('email', '<span class="has-error">:message</span>') !!}

                {!! Form::password('password', ['placeholder' => 'Senha', 'required', 'class' => 'fadeIn third']) !!}
                {!! $errors->first('password', '<span class="has-error">:message</span>') !!}

                <br>
                <input type="submit" value="CONECTAR"/>

                {!! Form::close() !!}
            </div>
            <!-- Remind Passowrd -->
            <div id="formFooter">
                <a class="underlineHover" href="#">Esqueceu a senha?</a>
            </div>

        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            // Declara todas as tabs
            var $tabs = $('.tab');
            // Declara todos os conteúdos referente as tabs
            var $contents = $('.tab-content');
            // Quando clicar, remove o efeito de display none do conteúdo da tab sendo clicada
            $tabs.click(function () {
                // Pega o id declarado no data
                var ref = $(this).data('ref');
                // Declara o conteúdo que irá aparecer
                var current = $('#' + ref);
                // Esconde todos os conteúdos
                $contents.addClass('hide');
                // Remove todas as tabs ativas
                $tabs.removeClass('active');
                // Remove o hide do conteúdo referente a TAB
                current.removeClass('hide');
                // Adiciona todas as tabs o efeito de underline
                $tabs.addClass('underlineHover');
                // Adiciona o active e remove da tab que está ativa o underline
                $(this).addClass('active').removeClass('underlineHover');
                // Adiciona no histórico do browser a tab corrent
                localStorage.setItem('tab', ref);
            });
            if (localStorage.getItem('tab') != '') {
                $('[data-ref="' + localStorage.getItem('tab') + '"]').trigger('click');
            }
        });
    </script>
@endsection