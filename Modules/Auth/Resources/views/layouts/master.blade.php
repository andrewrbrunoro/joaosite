<!DOCTYPE html>
<html lang="{!! config()->get('lang') !!}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Autenticação | {!! setting('nome') !!}</title>

    {!! Html::style(Module::asset('auth:css/login.css?' . time())) !!}
</head>
<body style="background-image: url({!! Module::asset('auth:images/pexels-photo-733852.jpeg') !!});">

@yield('content')

{!! Html::script('assets/vendors/jquery/jquery-2.1.1.js') !!}
@yield('js')

</body>
</html>