<?php

Route::group(['middleware' => 'web', 'prefix' => 'winner', 'namespace' => 'Modules\Winner\Http\Controllers'], function()
{
    Route::get('/', 'WinnerController@index');
});
