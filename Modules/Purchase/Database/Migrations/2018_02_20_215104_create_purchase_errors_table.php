<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_errors', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('purchase_id')->nullable();
            $table->integer('user_id')->nullable();

            $table->string('cookie')->nullable();

            $table->text('url')->nullable();
            $table->text('referer')->nullable();

            $table->text('message');

            $table->longText('exception')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_errors');
    }
}
