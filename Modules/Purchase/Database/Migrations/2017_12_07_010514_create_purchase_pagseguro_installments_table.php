<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasePagseguroInstallmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_pagseguro_installments', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('pagseguro_id');
            $table->integer('installment');

            $table->decimal('amount', 10, 2)->default(0.00);

            $table->tinyInteger('interest_free');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_pagseguro_installments');
    }
}
