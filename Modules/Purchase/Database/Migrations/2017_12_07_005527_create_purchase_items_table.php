<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchase_id');

            $table->string('table')->nullable();
            $table->integer('item_id')->nullable();

            $table->string('name', 191);
            $table->string('slug', 255)->nullable();

            $table->decimal('price', 10, 2)->default(0.00);
            $table->decimal('sale', 10, 2)->default(0.00);

            $table->integer('quantity')->default(1);

            $table->longText('url')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_items');
    }
}
