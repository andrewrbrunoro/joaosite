<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasePagsegurosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_pagseguros', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('purchase_id');

            $table->decimal('gross_amount', 10, 2)->default(0.00);
            $table->decimal('discount_amount', 10, 2)->default(0.00);
            $table->decimal('fee_amount', 10, 2)->default(0.00);
            $table->decimal('net_amount', 10, 2)->default(0.00);
            $table->decimal('extra_amount', 10, 2)->default(0.00);

            $table->dateTimeTz('date')->nullable();

            $table->string('code', 191)->nullable();
            $table->string('reference', 10)->nullable();
            $table->string('recovery_code', 191)->nullable();

            $table->integer('type')->nullable();
            $table->integer('status')->nullable();
            $table->integer('payment_method_type')->nullable();
            $table->integer('payment_method_code')->nullable();

            $table->longText('payment_link')->nullable();

            $table->integer('installments')->default(1);

            $table->integer('count_items')->default(1);

            $table->longText('success')->nullable();
            $table->longText('error')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_pagseguros');
    }
}
