<?php

namespace Modules\Purchase\Helpers;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;
use Modules\Purchase\Entities\PaymentMethod;
use Modules\Purchase\Entities\Purchase;
use Modules\Purchase\Entities\PurchaseHistory;
use Modules\Purchase\Entities\PurchaseItem;
use Modules\Purchase\Entities\PurchaseStatus;
use Modules\User\Entities\User;

class PurchaseHelper
{

    # Validação
    private $validator;

    # Variável da MessageBag
    private $bag;

    # Para adicionar um item no carrinho é necessário passar os valores
    private $item_validator = [
        'table' => 'required',
        'item_id' => 'required',
        'name' => 'required',
        'slug' => 'required',
        'price' => 'required',
        'url' => 'required|min:10'
    ];

    # Variável do pedido
    private $purchase;

    # Dados do usuário
    private $user = null;

    private $user_create = null;

    private $discount = 0.00;

    public function __construct(MessageBag $bag)
    {
        $this->bag = $bag;
    }

    /**
     * @param array $validator
     * @return PurchaseHelper
     *
     * Caso seja necessário fazer uma validação customizada
     */
    public function setItemValidator(array $validator): self
    {
        $this->item_validator = $validator;
        return $this;
    }

    /**
     * @param User $user
     * @return PurchaseHelper
     *
     * Seta o usuário que será vinculado ao pedido
     */
    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @param User $user
     * @return PurchaseHelper
     *
     * Seta o usuário que será vinculado ao pedido como Criador
     */
    public function setUserCreate(User $user): self
    {
        $this->user_create = $user;
        return $this;
    }

    /**
     * @return PurchaseHelper
     *
     * Cria um pedido ou retorna o que está ativo para o usuário
     */
    public function setOrGetPurchase(): self
    {
        # Instância a MODEL de Pedidos
        $purchase = new Purchase();

        # Se o usuário não for anônimo, procura pelo ID dele
        if (!auth()->guest()) {
            $query = $purchase->where('user_id', '=', auth()->user()->id)
                ->where('closed', '=', 0)->first();
        } else {
            $query = $purchase->where('user_ip', '=', request()->server('REMOTE_ADDR'))
                ->where('closed', '=', 0)->first();
        }

        # Se não existir, cria, se não pega o que já existe
        if (!$query) {
            DB::beginTransaction();
            try {
                # Adiciona o IP do usuário que ta criando o pedido
                $save['user_ip'] = request()->server('REMOTE_ADDR');
                # Usuário que está criando o pedido
                if ($this->user_create != null)
                    $save['user_create_id'] = $this->user_create->id;
                # Usuário que está vinculado ao pedido
                if ($this->user != null)
                    $save['user_id'] = $this->user->id;

                # Salva o pedido
                $query = $purchase->create($save);
                # Salva os dados se tudo estiver certo
                DB::commit();
            } catch (\Exception $e) {
                # Não salva os dados se existir erro
                DB::rollback();
                $this->setError($e->getMessage());
            }
        }
        # Seta o pedido
        $this->setPurchase($query);
        return $this;
    }

    /**
     * @param Purchase $purchase
     * @return PurchaseHelper
     *
     * Seta o pedido
     */
    public function setPurchase(Purchase $purchase): self
    {
        $this->purchase = $purchase;
        return $this;
    }

    /**
     * @return Collection
     *
     * Retorna os dados do pedido
     */
    public function getPurchase(): Collection
    {
        return $this->purchase;
    }

    /**
     * @return mixed
     *
     * Retorna a URL do pedido para visualização
     */
    public function getPurchaseUrlShow(): string
    {
        return route('purchase.show', $this->purchase->id);
    }

    /**
     * @return mixed
     *
     * Retorna a URL do pedido para edição
     */
    public function getPurchaseUrlEdit(): string
    {
        return route('purchase.edit', $this->purchase->id);
    }

    /**
     * Fecha o pedido
     */
    public function close()
    {
        # Inicia a transação com o banco de dados
        DB::beginTransaction();
        try {
            # Variável declarada para dar desconoto
            $valor = 0.00;
            # Se o desconto for setado, diminui o valor total do carrinho com o desconto
            if ($this->discount > 0) {
                $valor = $this->purchase->total - $this->discount;
            }

            if ($valor < 0)
                throw new Exception('O desconto máximo para este carrinho é de R$ ');

            # Atualiza o pedido setando o campo "Closed como verdadeiro ou 1"
            $this->purchase->update(['closed' => true, 'total_with_discount' => $valor]);
            # Confirma a atualização
            DB::commit();
        } catch (Exception $e) {
            # Retorna a transação, algo deu errado
            DB::rollback();
            $this->setError($e->getMessage());
        }
    }

    /**
     * @param array $item
     * @return PurchaseHelper
     *
     * Insere o item no carrinho
     */
    public function setItem(array $item): self
    {
        # Só adiciona o item se existir um pedido
        if ($this->purchase) {
            # Se for válido o item, insere
            if ($this->validateItem($item) === false) {
                # Inicia uma transação de dados
                DB::beginTransaction();
                try {
                    # Víncula o item com o pedido
                    if (!$this->purchase->Items()->saveMany([new PurchaseItem($item)])) {
                        $this->setError('Falha ao adicionar o item no carrinho.');
                    } else {
                        # Comita a transação
                        DB::commit();
                    }
                } catch (Exception $e) {
                    # Retorna a transação, algo deu errado
                    DB::rollback();
                    # Adiciona o erro na Bag da class
                    $this->setError($e->getMessage());
                }
            } else {
                # Adiciona o erro na Bag da class
                $this->setError('O item está inválido, verifique as regras para inserção.');
            }
        } else {
            # Como não foi encontrado o pedido, gera ou pega o que existe ativo
            $this->setOrGetPurchase();
            # Adiciona no carrinho
            $this->setItem($item);
        }
        return $this;
    }

    /**
     * @param int $id
     * @return PurchaseHelper
     *
     * Seta o status do Pedido no histórcio dele
     */
    public function setHistoryPurchase(int $id): self
    {
        try {
            # Verifica se existe um pedido
            if ($this->purchase) {
                # Instância a model de status de pedido
                $purchaseStatus = new PurchaseStatus();
                # Verifica se existe o ID do status
                $status = $purchaseStatus->find($id);
                # Se não existir seta o erro
                if (!$status) {
                    $this->setError("O Status #{$id} não foi encontrado em nossos registros.");
                } else {
                    # Incrementa o status como histórico do pedido
                    $this->purchase->PurchaseHistories()->saveMany([new PurchaseHistory(['purchase_status_id' => $id])]);
                }
            } else {
                # Como não foi encontrado o pedido, gera ou pega o que existe ativo
                $this->setOrGetPurchase();
                # Adiciona o histórcio novamente
                $this->setPurchaseStatus($id);
            }
        } catch (Exception $e) {
            # Retorna a transação, algo deu errado
            DB::rollback();
            # Adiciona o erro na Bag da class
            $this->setError($e->getMessage());
        }
        return $this;
    }

    /**
     * @param $value
     * @return PurchaseHelper
     *
     * Adiciona o valor na variável e adiciona no final do pedido
     */
    public function setDiscountValue($value): self
    {
        $this->discount = $value;
        return $this;
    }

    /**
     * @param int $id
     * @return PurchaseHelper
     *
     * Seta o metodo de pagamento ao pedido
     */
    public function setPurchasePaymentMethod(int $id): self
    {
        try {
            # Verifica se existe um pedido
            if ($this->purchase) {
                # Instância a model de Tipo de pagamento
                $paymentMethod = new PaymentMethod();
                # Procura pelo metodo
                $method = $paymentMethod->find($id);
                # Encontrou, se sim, adiciona ao pedido
                if ($method) {
                    # Atualiza o pedido e adiciona o metodo de pagamento
                    $this->purchase->update(['payment_method_id' => $id]);
                } else {
                    $this->setError('Método de pagamento não encontrado.');
                }
            } else {
                # Como não foi encontrado o pedido, gera ou pega o que existe ativo
                $this->setOrGetPurchase();
                # Adiciona o histórcio novamente
                $this->setPurchasePaymentMethod($id);
            }
        } catch (Exception $e) {
            # Retorna a transação, algo deu errado
            DB::rollback();
            # Adiciona o erro na Bag da class
            $this->setError($e->getMessage());
        }
        return $this;
    }

    /**
     * @param array $item
     * @return bool
     *
     * Valida o item que será inserido
     */
    public function validateItem(array $item): bool
    {
        $this->validator = \Validator::make($item, $this->item_validator);
        return $this->validator->fails();
    }

    /**
     * @param $message
     * @param string $key
     *
     * Adiciona erro na BAG
     */
    private function setError($message, $key = '')
    {
        $this->bag->add(!empty($key) ? $key : $this->bag->count(), $message);
    }

    /**
     * @param bool $first
     * @return array|string
     *
     * Retorna os errors que estão na MesssageBag do validator
     */
    public function getErrors($first = false)
    {
        if ($first)
            return $this->bag->first();
        else {
            return $this->bag->all();
        }
    }

    /**
     * @return bool
     *
     * Verifica se existe erros
     */
    public function getStatus()
    {
        return $this->bag->count() > 0 || $this->validator->fails();
    }


}