<?php

namespace Modules\Purchase\Helpers;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cookie;
use Modules\Purchase\Traits\PagseguroReturn;

class PagseguroHelper
{
    use PagseguroReturn;

    private $payment_method = null;

    private $receiver_email;

    private $shipping_cost = "0.00";

    private $shipping_type = 3;

    private $session_id;

    private $currency = "BRL";

    private $country = "BRA";

    private $status = "sandbox";

    private $email = null;

    private $token = null;

    private $data = [];

    private $url = [
        "production" => [
            "https://pagseguro.uol.com.br",
            "https://ws.pagseguro.uol.com.br",
            "https://stc.pagseguro.uol.com.br",
            "https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"
        ],
        "sandbox" => [
            "https://sandbox.pagseguro.uol.com.br",
            "https://ws.sandbox.pagseguro.uol.com.br",
            "https://stc.sandbox.pagseguro.uol.com.br",
            "https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"
        ]
    ];

    public function __construct()
    {
        # Seto automaticamente os dados que existem
        $this->setPagseguroEmail(env("PAGSEGURO_EMAIL"))
            ->setPagseguroToken(env("PAGSEGURO_TOKEN"));
    }

    /**
     * @param string $value
     * @return PagseguroHelper
     * E-mail que vai receber o pagamento
     */
    public function setPagseguroReceiverEmail(string $value): self
    {
        $this->receiver_email = $value;
        return $this;
    }

    /**
     * @return mixed
     * E-mail pagamento
     */
    public function getPagseguroReceiverEmail()
    {
        return $this->receiver_email;
    }

    /**
     * @param string $value
     * @return PagseguroHelper
     * Moeda
     */
    public function setPagseguroCurrency(string $value): self
    {
        $this->currency = $value;
        return $this;
    }

    /**
     * @return mixed
     * Moeda que vai ser utilizada
     */
    public function getPagseguroCurrency()
    {
        return $this->currency;
    }

    /**
     * @param $value
     * @return $this
     * Adiciona o E-mail de configuração do Pagseguro
     */
    public function setPagseguroEmail(string $value): self
    {
        $this->email = $value;
        return $this;
    }

    /**
     * @return mixed
     * Retorna o valor do e-mail
     */
    public function getPagseguroEmail(): string
    {
        return $this->email;
    }

    /**
     * @param $value
     * @return $this
     * Adiciona o Token de configuração do Pagseguro
     */
    public function setPagseguroToken(string $value): self
    {
        $this->token = $value;
        return $this;
    }

    /**
     * @return mixed
     * Retorna o valor do Token
     */
    public function getPagseguroToken(): string
    {
        return $this->token;
    }

    /**
     * @param $value
     * @return $this
     * @throws \Exception
     *
     * Troca o status de transação (teste ou produção)
     */
    public function setStatus(string $value): self
    {
        # valido se o status é sandbox ou production
        if (!in_array($value, ["sandbox", "production"]))
            throw new \Exception("Por favor, informe `sandbox` ou `production` na função `setStatus`");

        $this->status = $value;
        return $this;
    }

    /**
     * @return string
     * Retorna o status
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $value
     * @return PagseguroHelper
     * Adiciono o país padrão
     */
    public function setCountry(string $value): self
    {
        $this->country = $value;
        return $this;
    }

    /**
     * @return mixed
     * Retorna o país
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $value
     * @return PagseguroHelper
     * Adicino o métotod de pagamento
     */
    public function setPagseguroMethod(string $value): self
    {
        # Verifico se o metodo de pagamento está disponível
        if (!in_array($value, ["creditCard", "boleto", "eft"]))
            $this->payment_method = $value;
        return $this;
    }

    /**
     * @return null
     * Retorna o metodo de pagamento
     */
    public function getPaymentMethod()
    {
        return $this->payment_method;
    }

    /**
     * @param $take
     * @param string $url
     * @return string
     * @throws \Exception
     *
     * Pega a URl referente a index da `url` e concatena com a $url passada
     */
    public function getUrl(int $take, string $url = ""): string
    {
        if (!isset($this->url[$this->getStatus()][$take]))
            throw new \Exception("{$take} não encontrado nas variável de `urls`");
        return $this->url[$this->getStatus()][$take] . $url;
    }

    /**
     * @param string $value
     * @return PagseguroHelper
     * Adiciona o ID da sessão
     */
    public function setSessionID(string $value): self
    {
        $this->session_id = $value;
        return $this;
    }

    /**
     * @return string
     * Retorna o id da sessão
     */
    public function getSessionID(): string
    {
        return $this->session_id;
    }

    /**
     * @param float $value
     * @return PagseguroHelper
     * Valor do frete
     */
    public function setShippingCost(float $value): self
    {
        $this->shipping_cost = $value;
        return $this;
    }

    /**
     * @return float
     * Valor da entrega
     */
    public function getShippingCost()
    {
        return $this->shipping_cost;
    }

    /**
     * @param int $value
     * @return PagseguroHelper
     * 1 = Encomenda normal (PAC)
     * 2 = SEDEX
     * 3 = Tipo de frete não especificado
     */
    public function setShippingType(int $value): self
    {
        $this->shipping_type = $value;
        return $this;
    }

    /**
     * @return int
     * Tipo de entrega
     */
    public function getShippingType()
    {
        return $this->shipping_type;
    }

    /**
     * @param array $data
     * @return array
     * Dados do cartão de crédito de acordo com a regrade do Pagseguro
     */
    public function clearedCreditData(array $data): array
    {
        return [
            "creditCardHolderName" => isset($data["creditCardHolderName"]) ? str_limit($data["creditCardHolderName"], 50, '') : "",
            "creditCardHolderCPF" => isset($data["creditCardHolderCPF"]) ? str_to_number($data["creditCardHolderCPF"]) : "",
            "creditCardHolderBirthDate" => isset($data["creditCardHolderBirthDate"]) ? $data["creditCardHolderBirthDate"] : ""
        ];
    }

    /**
     * @throws \Exception
     * Crio a sessão com o pagseguro para efetuar o pagamento
     */
    public function session()
    {
        if (!Cookie::get("pagseguro_session_id")) {
            $client = new Client();
            $response = $client->request("POST", $this->getUrl(1, "/v2/sessions"), [
                "debug" => false,
                "form_params" => [
                    "email" => $this->getPagseguroEmail(),
                    "token" => $this->getPagseguroToken()
                ]
            ]);
            if ($response->getStatusCode() === 200) {
                $body = $response->getBody();
                # Ativo o debug do xml, apenas para visualizar os erros do xml
                libxml_use_internal_errors(true);
                # Faço a leitura da string
                $xml = simplexml_load_string($body->getContents());
                # Se não for um xml valido
                if (!$xml)
                    throw new \Exception("Não foi possível criar um sessão para a sua compra, tente novamente mais tarde.");
                else {
                    # Transformo o xml em json e em seguida ele vira um objeto
                    $xml = json_decode(json_encode($xml));
                    # Se não vir um ID de sessão
                    if (!$xml->id)
                        throw new \Exception("Não foi possível criar um sessão para a sua compra, tente novamente, caso erro persista, tente novamente mais tarde.");
                    else {
                        # Salvo o token do pagseguro do usuário por 2 horas
                        cookie("pagseguro_session_id", (string)$xml->id, 120);
                        # Adiciono o id da sessão
                        $this->setSessionID((string)$xml->id);
                    }
                }
            } else {
                throw new \Exception($response->getStatusCode());
            }
        } else {
            $this->setSessionID(Cookie::get("pagseguro_session_id"));
        }
        return $this;
    }

    /**
     * @param array $item
     * @param int $count
     * @return PagseguroHelper
     * @throws \Exception
     * Adiciona o item no pedido
     */
    public function setItem(array $item, $count = 1): self
    {
        $validator = validator($item, [
            "item_id" => "required",
            "name" => "required",
            "price" => "required",
            "quantity" => "required"
        ]);

        if ($validator->fails())
            throw new \Exception($validator->fails(), 1);

        $this->data += [
            "itemId{$count}" => str_limit($item["item_id"], 100, ''),
            "itemDescription{$count}" => str_limit($item["name"], 100, ''),
            "itemAmount{$count}" => $item["price"],
            "itemQuantity{$count}" => $item["quantity"]
        ];

        return $this;
    }

    /**
     * @param array $data
     * @return PagseguroHelper
     * @throws \Exception
     */
    public function setSender(array $data): self
    {
        $validator = validator($data, [
            "senderName" => "required|max:50",
            "senderCPF" => "required",
            "senderAreaCode" => "required|max:2",
            "senderPhone" => "required",
            "senderEmail" => "required|email",
            "senderHash" => "required"
        ], [
            "senderHash.*" => "Atualize a sua página e tente novamente, caso o erro persista, entre em contato conosco."
        ], [
            "senderName" => "Nome de registro",
            "senderCPF" => "CPF de registro",
            "senderAreaCode" => "DDD de registro",
            "senderPhone" => "Telefone de registro",
            "senderEmail" => "E-mail de registro"
        ]);

        # se falhar a validação, envia os erros pra exceção [ se 1 = array, 0 = string ]
        if ($validator->fails())
            throw new \Exception($validator->errors(), 1);

        $this->data += $data;
        return $this;
    }

    /**
     * @param array $data
     * @return PagseguroHelper
     * @throws \Exception
     * 1 = Encomenda normal (PAC)
     * 2 = SEDEX
     * 3 = Tipo de frete não especificado
     */
    public function setShipping(array $data): self
    {
        # Adiciono o valor e o tipo de entrega
        # Pra utilizar a entrega com valores utilizar a função ->setShippingType e setShippingCost
        $data["shippingType"] = $this->getShippingType();
        $data["shippingCost"] = $this->getShippingCost();
        # Pra utilizar a entrega com valores utilizar a função ->setShippingType e setShippingCost
        # Adiciono o valor e o tipo de entrega

        $validator = validator($data, [
            "shippingAddressStreet" => "required",
            "shippingAddressNumber" => "required",
            "shippingAddressDistrict" => "required",
            "shippingAddressPostalCode" => "required",
            "shippingAddressCity" => "required|min:2",
            "shippingAddressState" => "required|max:2",
            "shippingType" => "required",
            "shippingCost" => "required"
        ]);

        # se falhar a validação, envia os erros pra exceção [ se 1 = array, 0 = string ]
        if ($validator->fails())
            throw new \Exception($validator->errors(), 1);

        $this->data += $data;
        return $this;
    }

    /**
     * @param array $data
     * @return PagseguroHelper
     * @throws \Exception
     */
    public function setBillingAddress(array $data): self
    {
        $validator = validator($data, [
            "billingAddressStreet" => "required",
            "billingAddressNumber" => "required",
            "billingAddressDistrict" => "required",
            "billingAddressPostalCode" => "required",
            "billingAddressCity" => "required|min:2",
            "billingAddressState" => "required|max:2"
        ]);

        # se falhar a validação, envia os erros pra exceção [ se 1 = array, 0 = string ]
        if ($validator->fails())
            throw new \Exception($validator->errors(), 1);

        $this->data += $data;
        return $this;
    }

    /**
     * @param array $data
     * @return PagseguroHelper
     * @throws \Exception
     */
    public function setCredit(array $data): self
    {
        $validator = validator($data, [
            "creditCardToken" => "required",
            "creditCardHolderName" => "required",
            "creditCardHolderCPF" => "required|cpf",
            "creditCardHolderBirthDate" => "required|date_format:d/m/Y",
            "creditCardHolderAreaCode" => "required|max:2",
            "creditCardHolderPhone" => "required"
        ], [
            "creditCardToken.required" => "Atualize a sua página e tente novamente, caso o erro persista, entre em contato conosco."
        ], [
            "creditCardHolderName" => "Nome",
            "creditCardHolderCPF" => "CPF",
            "creditCardHolderBirthDate" => "Data de nascimento",
            "creditCardHolderAreaCode" => "DDD",
            "creditCardHolderPhone" => "Telefone"
        ]);

        # se falhar a validação, envia os erros pra exceção [ se 1 = array, 0 = string ]
        if ($validator->fails())
            throw new \Exception($validator->errors(), 1);

        $this->data += $data;
        return $this;
    }

    /**
     * @param string $value
     * @return PagseguroHelper
     * @throws \Exception
     * {"quantity":1,"installmentAmount":200,"totalAmount":200,"interestFree":true}
     */
    public function setInstallment(string $value): self
    {
        $value = json_decode($value, true);
        $validator = validator($value, [
            "quantity" => "required",
            "installmentAmount" => "required"
        ]);

        # se falhar a validação, envia os erros pra exceção [ se 1 = array, 0 = string ]
        if ($validator->fails())
            throw new \Exception($validator->errors(), 1);

        $this->data["installmentQuantity"] = $value["quantity"];
        $this->data["installmentValue"] = $value["installmentAmount"];

        return $this;
    }

    /**
     * @param $data
     * @return $this
     * @throws \Exception
     */
    public function validate($data)
    {
        $validator = validator($data, [
            "paymentMethod" => "required"
        ]);

        # se falhar a validação, envia os erros pra exceção [ se 1 = array, 0 = string ]
        if ($validator->fails())
            throw new \Exception($validator->errors(), 1);

        $this->setBillingAddress($data)
            ->setShipping($data)
            ->setSender($data);

        switch ($data["paymentMethod"]) {
            case "creditCard":
                $this->setInstallment($data['installments'])->setCredit($data)->output();
                break;
            case "boleto":
                $this->output();
                break;
            case "eft":
                break;
            default:
                throw new \Exception("Tipo de pagamento {$data['paymentMethod']} não encontrado.");
        }

        return $this;
    }

    public function getData()
    {
        $data = collect($this->data);
        return $data;
    }

    public function output()
    {
        $this->setPagseguroReceiverEmail($this->getPagseguroEmail());

        $this->data["reference"] = session("purchase_id");
        $this->data["receiverEmail"] = $this->getPagseguroReceiverEmail();
        $this->data["currency"] = $this->getPagseguroCurrency();
        $this->data["paymentMethod"] = $this->getPaymentMethod();
        $this->data["shippingAddressCountry"] = $this->getCountry();
        $this->data["billingAddressCountry"] = $this->getCountry();
        $this->data["email"] = $this->getPagseguroEmail();
        $this->data["token"] = $this->getPagseguroToken();

        $client = new Client();
        $response = $client->request("POST", $this->getUrl(1, '/v2/transactions'), [
            "form_params" => $this->getData()->toArray()
        ]);

        dd($response->getBody());

    }

}