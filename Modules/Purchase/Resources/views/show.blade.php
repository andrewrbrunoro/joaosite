@extends('dashboard::layouts.master')

@section('title', 'Pedido #'.$purchase->id)

@section('content')

    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Pedido #{!! $purchase->id !!}</h5>
            </div>
            <div class="ibox-content">
                <div class="row">

                    <div class="col-lg-12">
                        @if (!$purchase->without_status)
                            <div class="alert alert-danger">
                                <p>
                                    <strong>O PEDIDO NÃO TEM HISTÓRICO DE STATUS!</strong>  <br >
                                    Faça um vínculo de um status para este pedido, caso ao alterar o status o erro permanecer, por favor, contatar a equipe da <strong>{!! setting('sistema.autor') !!}</strong>
                                </p>
                            </div>
                        @endif
                        @if ($purchase->PurchaseObservations->count())
                            @php
                            $observation = $purchase->PurchaseObservation;
                            @endphp
                            <div class="alert alert-success">
                                <strong>Última observação</strong>
                                <p>{!! $observation->text !!}</p>
                            </div>
                        @endif
                    </div>


                    <div class="col-lg-12">
                        <div class="row">

                            <div class="col-lg-6">
                                <h3 class="forum-title bg-primary">
                                    Dados pessoais
                                </h3>
                                <strong>Nome</strong> <br>
                                {!! $purchase->User->name !!} <br>
                                <strong>E-mail</strong> <br>
                                {!! $purchase->User->email !!}
                                <hr>

                                <h3 class="forum-title bg-primary">
                                    Status do pedido
                                </h3>

                                <table class="table table-responsive table-bordered">
                                    <thead>
                                    <tr>
                                        <td>Status</td>
                                        <td>Data/Hora</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if ($purchase->PurchaseHistories->count())
                                        @foreach($purchase->PurchaseHistories as $history)
                                            <tr>
                                                <td>
                                                    <label class="{!! $history->PurchaseStatus->badge !!}">{!! $history->PurchaseStatus->name !!}</label>
                                                </td>
                                                <td class="bg-success">{!! $history->created !!}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="2">
                                                O pedido não passou por nenhum status no momento.
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>


                                {!! Form::open(['route' => ['purchase.change_status', $purchase->id]]) !!}
                                <div class="form-group{!! $errors->first('purchase_status_id') ? ' has-error' : '' !!}">
                                    <label for="change_status">
                                        Alterar status <label class="badge badge-info">Opcional</label>
                                    </label>
                                    <div class="input-group">
                                        {!! Form::select('purchase_status_id', $statuses, null, ['class' => 'form-control', 'required', 'placeholder' => 'Selecione o novo status']) !!}
                                        {!! $errors->first('purchase_status_id', '<span class="text-danger">:message</span>') !!}
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Alterar status</button>
                                        </span>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>

                            <div class="col-lg-6">
                                {{--{!! Mostra o endereço de entrega se o mesmo existir !!}--}}
                                @if ($purchase->User->UserAddresses->count())
                                    @php
                                        $address = $purchase->User->UserAddress;
                                    @endphp
                                    <!-- Endereço de Entrega -->
                                    <h3 class="forum-title bg-primary">
                                        Endereço de Entrega e Cobrança <i class="fa fa-send-o"></i>
                                    </h3>
                                    <strong>CEP</strong> <br>
                                    {!! $address->zip_code !!} <br>
                                    <strong>Edenreço</strong> <br>
                                    {!! $address->street !!}, {!! $address->street_number !!} <br>
                                    <strong>Bairro</strong> <br>
                                    {!! $address->district !!} <br>
                                    <strong>Cidade</strong> <br>
                                    {!! $address->city !!} <br>
                                    <strong>Estado</strong> <br>
                                    {!! $address->state !!} <br>
                                    <!-- Endereço de Entrega -->

                                    {{--<hr>--}}

                                    <!-- Endereço de cobrança -->
                                    {{--<h3 class="forum-title bg-primary">--}}
                                        {{--Endereço de Conbrança <i class="fa fa-dollar"></i>--}}
                                    {{--</h3>--}}
                                    {{--<strong>Nome</strong> <br>--}}
                                    {{--{!! $purchase->User->name !!} <br>--}}
                                    {{--<strong>E-mail</strong> <br>--}}
                                    {{--{!! $purchase->User->email !!}--}}
                                    <!-- Endereço de cobrança -->
                                @endif
                            </div>

                        </div>
                        <hr>
                    </div>
                    <div class="col-lg-12">
                        <h3 class="forum-title bg-primary">
                            <i class="fas fa-shopping-cart"></i> Produtos
                        </h3>
                        <table class="table table-responsive table-bordered">
                            <thead>
                                <tr>
                                    <td>Produto Nome</td>
                                    <td>Quantidade</td>
                                    <td>Preço</td>
                                    <td>Preço promoção</td>
                                </tr>
                            </thead>
                            <tbody>
                            @if ($purchase->Items->count())
                                @foreach($purchase->Items as $item)
                                    <tr>
                                        @if ($item->url)
                                            <td>
                                                <a href="{!! $item->url !!}" target="_blank">{!! $item->name !!}</a>
                                            </td>
                                        @else
                                            <td>
                                                {!! $item->name !!}
                                            </td>
                                        @endif
                                        <td>{!! $item->quantity !!}</td>
                                        <td>R$ {!! $item->price_brl !!}</td>
                                        <td>R$ {!! $item->sale_brl !!}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">Nenhum produto neste pedido.</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                        <hr>
                    </div>

                    @if ($purchase->PurchaseErrors->count())
                    <div class="col-lg-12">
                        <h3 class="forum-title bg-danger">
                            <i class="fas fa-times"></i> ERROS
                        </h3>
                        <table class="table table-responsive table-bordered">
                            <thead>
                            <tr>
                                <th>Mensagem</th>
                                <th>URL</th>
                                <th>Referente</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($purchase->PurchaseErrors as $item)
                                    <tr>
                                        <td>{!! $item->message !!}</td>
                                        <td>{!! $item->url !!}</td>
                                        <td>{!! $item->referer !!}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <hr>
                    </div>
                    @endif

                    <div class="col-lg-12">
                        {!! Form::open(['route' => ['purchase.add_observation', $purchase->id]]) !!}
                            <div class="form-group{!! $errors->first('text') ? ' has-error' : '' !!}">
                                <label for="text">
                                    Observação <label class="badge badge-info">Opcional</label>
                                </label>
                                {!! Form::textarea('text', null, ['class' => 'form-control', 'rows' => 2, 'style' => 'resize: none;', 'placeholder' => 'Observação']) !!}
                                {!! $errors->first('text', '<span class="text-danger">:message</span>') !!}
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-plus"></i> Adicionar observação
                                </button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection