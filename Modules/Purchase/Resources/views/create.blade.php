@extends('dashboard::layouts.master')

@section('title', 'Cadastrar pedido')

@section('content')
    <div class="row">
        {!! Form::open(['route' => 'purchase.store', 'method' => 'post']) !!}
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Cadastrar pedido</h5>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('purchase.index') !!}" class="btn btn-outline btn-warning">
                            <i class="fa fa-arrow-left"></i> Listar todos os pedidos
                        </a>
                    </div>
                </div>
                <hr>
                @include('purchase::form')
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop
