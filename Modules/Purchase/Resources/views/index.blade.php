@extends('dashboard::layouts.master')

@section('title', 'Pedidos')

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Pedidos cadastrados</h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('purchase.create') !!}" class="btn btn-outline btn-success">
                            <i class="fa fa-plus"></i> Cadastrar pedido
                        </a>
                        <a href="{!! request()->fullUrl() !!}" class="btn btn-outline btn-default">
                            <i class="fa fa-repeat"></i> Atualizar
                        </a>
                    </div>
                </div>

                <hr>

                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>#Pedido</th>
                        <th>Usuário</th>
                        <th>Status Atual</th>
                        <th>Total</th>
                        <th>Sub Total</th>
                        <th>Total com desconto</th>
                        <th>Data do pedido</th>
                        <th>Opções</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($data) && $data->count())
                        @foreach($data as $row)
                            <tr{!! !$row->without_status ? ' class="bg-danger" title="O Pedido não contém um status, verifique o ocorrido."' : '' !!}>
                                <td>{!! $row->id !!}</td>
                                <td>{!! $row->User->name !!}</td>
                                @if ($row->without_status == true)
                                    <td>
                                        <label class="{!! $row->PurchaseLastHistory->PurchaseStatus->badge !!}">
                                            {!! $row->PurchaseLastHistory->PurchaseStatus->name !!}
                                        </label>
                                    </td>
                                @else
                                    <td>
                                        Pedido sem status
                                    </td>
                                @endif
                                <td>R$ {!! $row->total_brl !!}</td>
                                <td>R$ {!! $row->sub_total_brl !!}</td>
                                <td>R$ {!! $row->total_with_coupon_brl !!}</td>
                                <td>{!! $row->created !!}</td>
                                <td>
                                    {{--{!! Form::open(['route' => ['purchase.destroy', $row->id], 'method' => 'DELETE', 'onsubmit' => 'beforeConfirm(this, event)']) !!}--}}
                                    <a href="{!! route('purchase.show', $row->id) !!}" class="btn btn-outline btn-xs btn-success">
                                        <i class="fa fa-eye"></i> Visualizar
                                    </a>
                                    <?php /* ?>
                                    <a href="{!! route('purchase.edit', $row->id) !!}" class="btn btn-outline btn-info btn-xs">
                                        <i class="fa fa-pencil"></i> Editar
                                    </a>
                                    <button class="btn btn-outline btn-danger btn-xs" type="submit">
                                        <i class="fa fa-trash"></i> Deletar
                                    </button>
                                    <?php */?>
                                    {{--{!! Form::close() !!}--}}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8">Nenhum pedido registrado no momento, <a href="{!! route('purchase.create') !!}">clique aqui</a> para cadastrar um pedido manualmente.</td>
                        </tr>
                    @endif
                    </tbody>
                </table>

                @if (isset($data))
                    {!! $data->links() !!}
                @endif

            </div>
        </div>
    </div>
@stop