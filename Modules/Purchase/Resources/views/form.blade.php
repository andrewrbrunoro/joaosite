@section('css')
    {!! Html::style('assets/') !!}
@endsection
<div class="row">
    <div class="col-lg-12">

        <h4 class="forum-title text-white bg-primary">Dados principais do pedido</h4>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('payment_method_id') ? ' has-error' : '' !!}">
                    <label for="payment_method_id">
                        Forma de pagamento <label class="badge badge-danger">obrigatório</label>
                    </label>
                    {!! Form::select('payment_method_id', $methods, null, ['class' => 'form-control', 'placeholder' => 'Selecione a forma de pagamento']) !!}
                    {!! $errors->first('payment_method_id', '<span class="help-block">:message</span>') !!}
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('purchase_status_id') ? ' has-error' : '' !!}">
                    <label for="payment_method_id">
                        Status do pedido <label class="badge badge-danger">obrigatório</label>
                    </label>
                    {!! Form::select('purchase_status_id', $statuses, null, ['class' => 'form-control', 'placeholder' => 'Selecione o status do pedido']) !!}
                    {!! $errors->first('purchase_status_id', '<span class="help-block">:message</span>') !!}
                </div>
            </div>

        </div>
        <hr>
    </div>

    <div class="col-lg-12">

        <h4 class="forum-title text-white bg-primary">Complementos do pedido</h4>

        <div class="row">

            <div class="col-lg-12">
                <div class="form-group{!! $errors->first('user_id') ? ' has-error' : '' !!}">
                    <label for="user_id">
                        Usuário <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::select('user_id', $users, null, ['class' => 'form-control', 'placeholder' => 'Selecione o usuário']) !!}
                    {!! $errors->first('user_id', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div>
        <hr/>
    </div>

    <purchase-products inline-template :products='{!! session()->has('products') ? session('products') : json_encode([]) !!}'>
        <div class="col-lg-12">

            <div class="alert alert-danger{!! !$errors->first('products') ? ' hide' : '' !!}">
                {!! $errors->first('products', ':message') !!}
            </div>

            <div class="alert alert-info" v-if="products.length == 0">
                <p>Ao menos um produto é necessário</p>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group{!! $errors->first('product_id') ? ' has-error' : '' !!}">
                        <label for="product_id">
                            Produto <label class="badge badge-danger">obrigatório</label>
                        </label>
                        {!! Form::text('product_id', null, ['class' => 'form-control', 'v-model' => 'product.name', 'placeholder' => 'Digite o nome do Produto']) !!}
                        {!! $errors->first('product_id', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-2">
                    <div class="form-group{!! $errors->first('product_price') ? ' has-error' : '' !!}">
                        <label for="product_price">
                            Preço <label class="badge badge-info">Opcional</label>
                        </label>
                        {!! Form::text('product_price', null, ['class' => 'form-control', 'v-money' => "{decimal: ',', thousands: '.', precision: 2, masked: false}", 'v-model' => 'product.price', 'placeholder' => 'Preço']) !!}
                        {!! $errors->first('product_price', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-2">
                    <div class="form-group{!! $errors->first('product_sale') ? ' has-error' : '' !!}">
                        <label for="product_sale">
                            Promoção <label class="badge badge-info">Opcional</label>
                        </label>
                        {!! Form::text('product_sale', null, ['class' => 'form-control', 'v-money' => "{decimal: ',', thousands: '.', precision: 2, masked: false}", 'v-model' => 'product.sale', 'placeholder' => 'Preço']) !!}
                        {!! $errors->first('product_sale', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-2">
                    <div class="form-group{!! $errors->first('quantity') ? ' has-error' : '' !!}">
                        <label for="quantity">
                            Quantidade <label class="badge badge-info">Opcional</label>
                        </label>
                        {!! Form::text('quantity', null, ['class' => 'form-control', 'v-model' => 'product.quantity', 'placeholder' => 'Quantidade']) !!}
                        {!! $errors->first('quantity', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
            </div>

            <div class="row pull-right">
                <div class="col-lg-12">
                    <button type="button" @click="saveProduct" title="Clique aqui para adicionar o produto no pedido" class="btn btn-outline btn-success">
                        Adicionar produto <i class="fa fa-plus"></i>
                    </button>
                    <button type="button" @click="clearProduct" title="Clique aqui e irá limpar os campos referente ao produto" class="btn btn-outline btn-info">
                        Limpar os campos <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-lg-12">
                    <hr>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">

                    <h4 class="forum-title text-white bg-primary">Produtos adicionado no pedido</h4>

                    <table class="table table-responsive table-bordered">
                        <thead>
                        <tr>
                            <td>Produto Nome</td>
                            <td>Quantidade</td>
                            <td>Preço</td>
                            <td>Preço promoção</td>
                            <td width="5%">Ações</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-if="products.length > 0" v-for="(product, index) in products">
                            <td>@{{ product.name }}</td>
                            <td>@{{ product.quantity }}</td>
                            <td>@{{ product.price }}</td>
                            <td>@{{ product.sale }}</td>
                            <td>
                                <button type="button" class="btn btn-xs btn-danger" title="Remover o produto">
                                    <i class="fa fa-times"></i>
                                </button>
                            </td>
                            <input type="hidden" name="products[]" v-bind:value="JSON.stringify(product)"/>
                        </tr>
                        <tr v-if="products.length == 0">
                            <td colspan="5">Nenhum produto registrado no momento</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </purchase-products>

</div>

<div class="row">
    <div class="col-lg-6 col-sm-6">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check"></i> Salvar pedido
        </button>
    </div>
</div>

@section('script')
    {!! Html::script(Module::asset('purchase:js/vendors/jquery-scobar/jquery.mask.min.js')) !!}
    {!! Html::script(Module::asset('purchase:js/main.js')) !!}
    <script type="text/javascript">
        var $purchase_products = Vue.extend({
            props: {
                products: {
                    type: Array,
                    default: []
                }
            },
            data: function () {
                return {
                    product: {
                        name: null,
                        quantity: 1,
                        price: '0,00',
                        sale: '0,00'
                    }
                }
            },
            methods: {
                /**
                 * Valida o produto
                 * @returns {boolean}
                 */
                validateProduct: function () {
                    var product = this.product;
                    /* Remove todos os erros */
                    removeAllErrors();
                    /* Verifica se o campo nome do produto está vazio */
                    if (!product.name) {
                        /* Seta o erro avisando que precisa ser preenchido */
                        setError('product_id', 'Nome do produto é obrigatório.');
                        return false;
                    }
                    return true;
                },
                clearProduct: function () {
                    this.product = {
                        name: null,
                        quantity: 1,
                        price: '0,00',
                        sale: '0,00'
                    }
                },
                /**
                 * Salva o produto se estiver válido
                 */
                saveProduct: function () {
                    if (this.validateProduct()) {
                        var products = this.product;
                        this.products.unshift(products);
                        this.clearProduct();
                    }
                }
            }
        });
        Vue.component('purchase-products', $purchase_products);
    </script>
@endsection