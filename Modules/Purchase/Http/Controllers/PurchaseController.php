<?php

namespace Modules\Purchase\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Dashboard\Helpers\PusherHelper;
use Modules\Purchase\Entities\PaymentMethod;
use Modules\Purchase\Entities\Purchase;
use Modules\Purchase\Entities\PurchaseError;
use Modules\Purchase\Entities\PurchaseHistory;
use Modules\Purchase\Entities\PurchaseItem;
use Modules\Purchase\Entities\PurchaseObservation;
use Modules\Purchase\Entities\PurchaseStatus;
use Modules\Purchase\Helpers\PurchaseHelper;
use Modules\Purchase\Http\Requests\ChangeStatusRequest;
use Modules\Purchase\Http\Requests\ObservationRequest;
use Modules\Purchase\Http\Requests\PurchaseRequest;
use Modules\User\Entities\User;

class PurchaseController extends Controller
{

    private $temp_validate = [
        'name' => 'required'
    ];


    public function index(Purchase $purchase)
    {
        $data = $purchase->with('PurchaseLastHistory')->has('User')->orderBy('id', 'desc');
        return view('purchase::index', ['data' => $data->paginate(15)]);
    }

    public function create(PaymentMethod $paymentMethod, PurchaseStatus $purchaseStatus, User $user)
    {
        # Todos os metódos ativos
        $methods = $paymentMethod->whereStatus(1)
            ->get()->pluck('name', 'id');

        # Todos os status de pagamento
        $statuses = $purchaseStatus->whereStatus(1)
            ->get()->pluck('name', 'id');

        # Todos os usuário, menos o que está acessando
        $users = $user->where('id', '!=', auth()->user()->id)
            ->get()->pluck('name', 'id');

        return view('purchase::create', compact('methods', 'statuses', 'users'));
    }

    public function store(PurchaseRequest $request, PurchaseHelper $purchaseHelper, PusherHelper $pusherHelper)
    {
        # Inicia a transação com o banco de dados
        DB::beginTransaction();
        try {

            # Adiciona a validação temporária do item
            $purchaseHelper->setItemValidator($this->temp_validate);

            # Se não passar o usuário, seta o criado como o usuário referente ao pedido
            if ($request->get('user_id') == '')
                $purchaseHelper->setUser(auth()->user());
            else {
                # Usuário
                $User = new User();
                # Seta o usuário referente ao que está sendo passado por vínculo
                $purchaseHelper->setUser($User->find($request->get('user_id')));
            }

            # Seta o usuário que está criando o pedido
            $purchaseHelper->setUserCreate(auth()->user());

            # Leio todos os items, caso algum de errado irá entrar diretamente na exception
            foreach ($request->get('products') as $item) {
                # Converte o item que está vindo em formato json string para array
                # Caso o json estiver inválido, irá entrar na exception
                $item = json_decode($item, true);
                # Adiciona o item no carrinho
                $purchaseHelper->setItem($item);
            }

            # Atualiza o metódo de pagamento
            # Seta o histórico do pedido de acordo com o status setado pelo o usuário
            # Fecha o pedido
            $purchaseHelper->setPurchasePaymentMethod($request->get('payment_method_id'))
                ->setHistoryPurchase($request->get('purchase_status_id'))
                ->close();

            # Não foi possível criar o pedido
            if ($purchaseHelper->getStatus())
                throw new Exception($purchaseHelper->getErrors(true));

            # Está tudo okay, finaliza
            DB::commit();

            # Cria um pusher avisando que um pedido foi criado
            $pusherHelper->setMessage('Neste momento acaba de ser gerado um pedido manual, <a href="' . $purchaseHelper->getPurchaseUrlEdit() . '" class="text-success font-bold">clique aqui</a> para ser redirecionado para o pedido.')
                ->trigger('purchases', 'alert');

            return redirect()->route('purchase.create')->with('success', 'Pedido manual gerado com sucesso.');
        } catch (Exception $e) {

            # Instância uma collection
            $items = collect([]);
            # Verifica se foi passado os produtos, se sim, lê todos eles e transforma em array (eles estão vindo em json)
            if ($request->has('products') && $request->get('products')) {
                foreach ($request->get('products') as $key => $item) {
                    $json = array_map(function($value) {
                        return str_replace("'", "&apos;", $value);
                    }, json_decode($item, true));
                    $items->push($json);
                }
            }
            # Adiciona em uma flash session que se caso o usuário atualizar a página via HTTP ele remove os dados
            session()->flash('products', $items->toJson());

            # Retorna caso tenha algo dado errado
            DB::rollback();
            return redirect()->back()->with('error', $e->getMessage())->withInput($request->all());
        }
    }

    public function show($id, Purchase $purchase, PurchaseStatus $purchaseStatus)
    {
        try {
            # Procura pelo pedido
            $purchase = $purchase->with(['PurchaseHistories', 'PurchaseObservation', 'User', 'PurchaseLastHistory', 'Items'])
                ->find($id);

            # Se não existir avisa que o pedido não existe
            if (!$purchase)
                throw new Exception("Pedido #{$id} não encontrado.");

        } catch (Exception $e) {
            return redirect()->route('purchase.index')->with('error', $e->getMessage());
        }

        # Todos os status de pagamento
        $statuses = $purchaseStatus->whereStatus(1)
            ->get()->pluck('name', 'id');

        return view('purchase::show', compact('purchase', 'statuses'));
    }


    /**
     * @param $id
     * @param ObservationRequest $request
     * @param Purchase $purchase
     * @return mixed
     *
     * Adiciona observação ao pedido
     */
    public function addObservation($id, ObservationRequest $request, Purchase $purchase)
    {
        # Inícia a transação com o banco de dados
        DB::beginTransaction();
        try {

            # Procura pelo pedido
            $Purchase = $purchase->find($id);
            # Se não encontrar, vai para a exception
            if (!$Purchase)
                throw new Exception('Pedido não encontrado.');

            # Víncula a observação ao pedido
            $Purchase->PurchaseObservations()->saveMany([new PurchaseObservation($request->only(['text']))]);

            # Tudo certo, salva a observação
            DB::commit();
        } catch (Exception $e) {
            # Retorna a transação, algo deu errado
            DB::rollback();
            return redirect()->back()->with('error', $e->getMessage());
        }
        return redirect()->back()->with('success', 'Observação adicionada com sucesso.');
    }

    /**
     * @param $id
     * @param ChangeStatusRequest $request
     * @param Purchase $purchase
     *
     * Adiciona o novo status no histórico
     */
    public function changeStatus($id, ChangeStatusRequest $request, Purchase $purchase)
    {
        # Inícia a transação
        DB::beginTransaction();
        try {

            # Proura o pedido
            $Purchase = $purchase->find($id);
            # Não existe, envia pra exception
            if (!$Purchase)
                throw new Exception('Pedido não encontrado.');

            # Salva o status no histórcio
            $Purchase->PurchaseHistories()->saveMany([new PurchaseHistory($request->only('purchase_status_id'))]);

            # Tudo certo, salva o histórcio
            DB::commit();

        } catch (Exception $e) {
            # Retorna a transação, algo deu errado
            DB::rollback();
            return redirect()->back()->with('error', $e->getMessage());
        }
        return redirect()->back()->with('success', 'O pedido recebeu um novo status e foi adicionado no seu histórico com sucesso.');
    }

    public function edit()
    {
        return view('purchase::edit');
    }

    public function update(Request $request)
    {
    }

    public function destroy()
    {
    }
}
