<?php

namespace Modules\Purchase\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PurchaseRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'products' => 'required|array',
            'purchase_status_id' => 'required',
            'payment_method_id' => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function failedValidation(Validator $validator)
    {
        # Instância uma collection
        $items = collect([]);
        # Verifica se foi passado os produtos, se sim, lê todos eles e transforma em array (eles estão vindo em json)
        if ($this->request->has('products') && $this->request->get('products')) {
            foreach ($this->request->get('products') as $key => $item) {
                $json = array_map(function($value) {
                    return str_replace("'", "&apos;", $value);
                }, json_decode($item, true));
                $items->push($json);
            }
        }
        # Adiciona em uma flash session que se caso o usuário atualizar a página via HTTP ele remove os dados
        $this->session()->flash('products', $items->toJson());

        return parent::failedValidation($validator);
    }

    public function messages()
    {
        return [
            'purchase_status_id.required' => 'Campo obrigatório.',
            'products.required' => 'Forneça ao menos 1 produto para o cadastro do pedido.',
            'payment_method_id.required' => 'Campo obrigatório.'
        ];
    }
}
