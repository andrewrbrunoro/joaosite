<?php

Route::group([
    'namespace' => 'Modules\Purchase\Http\Controllers',
    'prefix' => 'pedidos',
    'as' => 'purchase.'
], function ($r) {
    $r->get('/', 'PurchaseController@index')->name('index');
    $r->get('{id}/visualizar', 'PurchaseController@show')->name('show');

    $r->get('{id}/editar', 'PurchaseController@edit')->name('edit');
    $r->patch('{id}/editar', 'PurchaseController@update')->name('update');

    $r->get('criar', 'PurchaseController@create')->name('create');
    $r->post('criar', 'PurchaseController@store')->name('store');

    $r->delete('{id}', 'PurchaseController@destroy')->name('destroy');

    $r->post('{id}/adicionar/observacao', 'PurchaseController@addObservation')->name('add_observation');

    $r->post('{id}/alterar/status', 'PurchaseController@changeStatus')->name('change_status');
});
