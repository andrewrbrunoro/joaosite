<?php

namespace Modules\Purchase\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseHistory extends Model
{
    use SoftDeletes;

    protected $fillable = ["purchase_id", "purchase_status_id"];

    public function getCreatedAttribute()
    {
        if ($this->created_at != '') {
            return $this->created_at->format('d/m/Y H:i:s');
        }
    }

    public function PurchaseStatus()
    {
        return $this->belongsTo('\Modules\Purchase\Entities\PurchaseStatus')->withDefault();
    }

}
