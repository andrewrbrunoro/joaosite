<?php

namespace Modules\Purchase\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{
    use SoftDeletes;

    protected $fillable = [
        "user_id", "user_create_id", "payment_method_id",
        "cookie", "closed", "sub_total", "total", "total_with_coupon"
    ];

    # Obriga retorna se existe um histórcio de pedido
    protected $appends = ['without_status'];

    /**
     * @return bool
     * Verifica se o pedido tem um histórico
     */
    public function getWithoutStatusAttribute()
    {
        return $this->PurchaseHistories->count() > 0;
    }

    /**
     * @return mixed
     * Converte a data que está vindo do pedido para d/m/Y
     */
    public function getCreatedAttribute()
    {
        if ($this->created_at != '') {
            return $this->created_at->format('d/m/Y H:i:s');
        }
    }

    /**
     * @return mixed
     *
     * Converte o float para moeda Real R$
     */
    public function getTotalBrlAttribute()
    {
        return bcoToCoin($this->total);
    }

    /**
     * @return mixed
     *
     * Converte o float para moeda Real R$
     */
    public function getTotalWithCouponBrlAttribute()
    {
        return bcoToCoin($this->total_with_coupon);
    }

    /**
     * @return mixed
     *
     * Converte o float para moeda Real R$
     */
    public function getSubTotalBrlAttribute()
    {
        return bcoToCoin($this->sub_total);
    }

    /**
     * @param $value
     *
     * Converte a moeda Real para float decimal 10,2
     */
    public function setTotalAttribute($value)
    {
        $this->attributes['total'] = coinToBco($value);
    }

    /**
     * @param $value
     *
     * Converte a moeda Real para float decimal 10,2
     */
    public function setSubTotalAttribute($value)
    {
        $this->attributes['sub_total'] = coinToBco($value);
    }

    /**
     * @param $value
     *
     * Converte a moeda Real para float decimal 10,2
     */
    public function setSaleAttribute($value)
    {
        $this->attributes['sale'] = coinToBco($value);
    }

    /**
     * @param $value
     *
     * Converte a moeda Real para float decimal 10,2
     */
    public function setTotalWithDiscountAttribute($value)
    {
        $this->attributes['total_with_discount'] = coinToBco($value);
    }

    public function User()
    {
        return $this->belongsTo('\Modules\User\Entities\User')->withDefault();
    }

    public function PurchaseHistories()
    {
        return $this->hasMany('\Modules\Purchase\Entities\PurchaseHistory')->orderBy('id', 'desc');
    }

    public function PurchaseLastHistory()
    {
        return $this->hasOne('\Modules\Purchase\Entities\PurchaseHistory', 'purchase_id')->latest();
    }

    public function PaymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_method_id')->withDefault();
    }

    public function Items()
    {
        return $this->hasMany(PurchaseItem::class, 'purchase_id')->orderBy('created_at', 'desc');
    }

    public function PurchaseObservations()
    {
        return $this->hasMany(PurchaseObservation::class, 'purchase_id');
    }

    public function PurchaseObservation()
    {
        return $this->hasOne(PurchaseObservation::class, 'purchase_id')->orderBy('created_at', 'desc');
    }

    public function PurchaseErrors()
    {
        return $this->hasMany(PurchaseError::class, 'purchase_id')->orderBy('created_at', 'desc');
    }

}
