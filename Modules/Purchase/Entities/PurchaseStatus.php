<?php

namespace Modules\Purchase\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseStatus extends Model
{
    use SoftDeletes;

    protected $fillable = ["name", "slug", "debit_stock", "credit_stock", "notification_email", "status"];
}
