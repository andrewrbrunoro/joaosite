<?php

namespace Modules\Purchase\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseObservation extends Model
{

    use SoftDeletes;

    protected $fillable = ["purchase_id", "user_id", "text"];

    public static function boot()
    {
        parent::boot();

        /**
         * Adiciona sempre o ID do usuário que está criando a observação
         */
        static::creating(function ($model) {
            $model->user_id = auth()->user()->id;
        });
    }

    /**
     * @param $value
     *
     * Limpa qualquer html do texto
     */
    public function setTextAttribute($value)
    {
        $this->attributes['text'] = strip_tags(nl2br($value), '<br><br/><strong><b>');
    }

}
