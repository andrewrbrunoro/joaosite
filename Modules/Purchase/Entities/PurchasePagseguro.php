<?php

namespace Modules\Purchase\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchasePagseguro extends Model
{
    use SoftDeletes;

    protected $fillable = [
        "purchase_id", "gross_amount", "discount_amount", "fee_amount", "net_amount", "extra_amount",
        "date", "code", "reference", "recovery_code", "type", "status", "payment_method_type",
        "payment_method_code", "payment_link", "installments", "count_items", "success", "error"
    ];
}
