<?php

namespace Modules\Purchase\Entities;

class PurchaseItemObserver
{
    private $purchase;

    private $total = 0.00;

    private $sub_total = 0.00;

    public function setTotal(float $total, $quantity = 1): self
    {
        $this->total += ($total * $quantity);
        return $this;
    }

    public function setSubTotal(float $subTotal, $quantity = 1): self
    {
        $this->sub_total += ($subTotal * $quantity);
        return $this;
    }

    public function setPurchase(Purchase $purchase, $quantity = 1): self
    {
        $this->purchase = $purchase;
        return $this;
    }

    public function created(PurchaseItem $purchaseItem)
    {
        # Seta o pedido
        $this->setPurchase($purchaseItem->Purchase);

        # Adiciona os valores que o pedido já tem
        $this->setTotal($this->purchase->total);
        $this->setSubTotal($this->purchase->sub_total);

        # Adiciona os valores do item
        $this->setTotal($purchaseItem->original_price, $purchaseItem->quantity);
        $this->setSubTotal($purchaseItem->price, $purchaseItem->quantity);

        # Salva os novos valores do pedido
        $this->purchase->update([
            'total' => $this->total,
            'sub_total' => $this->sub_total
        ]);
    }

}