<?php

namespace Modules\Purchase\Entities;

use Illuminate\Database\Eloquent\Model;

class PurchaseError extends Model
{
    protected $fillable = [];
}
