<?php

namespace Modules\Purchase\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseItem extends Model
{
    use SoftDeletes;

    protected $fillable = [
        "purchase_id", "table", "item_id", "quantity",
        "name", "slug", "price", "sale", "url", "user_remove_cart"
    ];

    protected $appends = ['original_price'];

    /**
     * @return mixed
     *
     * Converte o preço para moeda Real
     */
    public function getPriceBrlAttribute()
    {
        return bcoToCoin($this->price);
    }

    /**
     * @return mixed
     * 
     * Converte a promoção para moeda Real
     */
    public function getSaleBrlAttribute()
    {
        return bcoToCoin($this->sale);
    }

    /**
     * Se estiver em promoção, vai retornar o valor de promoção
     */
    public function getOriginalPriceAttribute()
    {
        if ($this->sale > 0) {
            return $this->sale;
        } else {
            return $this->price;
        }
    }

    # Converte em valor decimal 10,2
    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = coinToBco($value);
    }

    public function setSaleAttribute($value)
    {
        $this->attributes['sale'] = coinToBco($value);
    }
    # Converte em valor decimal 10,2

    public function Purchase()
    {
        return $this->belongsTo(Purchase::class, 'purchase_id');
    }
}
