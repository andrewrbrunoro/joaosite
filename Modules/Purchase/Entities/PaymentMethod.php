<?php

namespace Modules\Purchase\Entities;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{

    protected $fillable = ["pagseguro_code", "name", "slug", "image", "status"];

}
