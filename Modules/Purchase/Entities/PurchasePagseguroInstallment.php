<?php

namespace Modules\Purchase\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchasePagseguroInstallment extends Model
{
    use SoftDeletes;

    protected $fillable = ["pagseguro_id", "installment", "amount", "interest_free"];
}
