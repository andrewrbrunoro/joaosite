@extends('dashboard::layouts.master')

@section('title', 'Leads, Contato')

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Leads, Contato</h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-12">
                        @if (request()->has("status"))
                            <a href="{!! route('lead.index') !!}" class="btn btn-outline btn-warning">
                                Listar todos os Leads <i class="fas fa-arrow-left"></i>
                            </a>
                        @endif
                        @if ($wait_count > 0)
                            <a href="{!! route('lead.index', ['status' => 0]) !!}" class="btn btn-danger">
                                Aguardando resposta <label class="badge badge-inverse">{!! $wait_count !!}</label>
                            </a>
                        @endif
                        <a href="{!! route('common_question.index') !!}" class="btn btn-outline btn-success">
                            <i class="fas fa-question-circle"></i> Pergunta Frequente
                        </a>
                        <a href="{!! request()->fullUrl() !!}" class="btn btn-outline btn-default">
                            <i class="fa fa-repeat"></i> Atualizar
                        </a>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-responsive table-bordered">
                            <thead>
                            <tr>
                                <td>#Lead</td>
                                <td>Nome</td>
                                <td>E-mail</td>
                                <td>Categoria</td>
                                <td>Status</td>
                                <td>Data/Hora</td>
                                <td>Ações</td>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($leads->count())
                                @foreach($leads as $lead)
                                    <tr>
                                        <td>{!! $lead->id !!}</td>
                                        <td>{!! $lead->name !!}</td>
                                        <td>{!! $lead->email !!}</td>
                                        <td>{!! $lead->LeadCategory->name !!}</td>
                                        <td>{!! $lead->status_reason !!}</td>
                                        <td>{!! $lead->created !!}</td>
                                        <td>
                                            <a href="{!! route('lead.show', $lead->id) !!}" class="btn btn-xs btn-outline btn-success">
                                                <i class="fa fa-eye"></i> Visualizar
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="7">Nenhum registro no momento.</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop
