@extends('dashboard::layouts.master')

@section('title', 'Lead #' . $lead->id)

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Lead #{!! $lead->id !!}</h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('lead.index') !!}" class="btn btn-outline btn-warning">
                            <i class="fa fa-arrow-left"></i> Listar todos os contatos
                        </a>
                    </div>
                </div>
                <hr>
                <div class="row">

                    <div class="col-lg-12">
                        @if ($lead->LeadObservations->count())
                            @php
                            $observation = $lead->LeadObservation;
                            @endphp
                            <div class="alert alert-success">
                                <strong>Última observação - {!! $lead->created !!}</strong>
                                <p>{!! $observation->text !!}</p>
                            </div>
                        @endif
                    </div>

                    <div class="col-lg-6">
                        <h3 class="forum-title bg-primary">
                            Dados principais
                        </h3>
                        <p>
                            <strong>Nome:</strong> {!! $lead->name !!} <br>
                            <strong>E-mail:</strong> {!! $lead->email !!}
                        </p>

                        @if ($lead->parent_id == 0)
                            <h3 class="forum-title bg-primary">
                                Status deste contato
                            </h3>

                            {!! Form::model($lead, ['route' => ['lead.change_status', $lead->id]]) !!}
                            <div class="form-group{!! $errors->first('status') ? ' has-error' : '' !!}">
                                <div class="input-group">
                                    {!! Form::select('status', [0 => 'Aguardando resposta', 1 => 'Finalizado'], null, ['class' => 'form-control', 'required', 'placeholder' => 'Selecione o novo status']) !!}
                                    {!! $errors->first('status', '<span class="text-danger">:message</span>') !!}
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Alterar
                                            status
                                        </button>
                                    </span>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        @endif
                    </div>

                    <div class="col-lg-6">
                        <h3 class="forum-title bg-primary">
                            Dados do formulário
                        </h3>
                        @if ($lead->LeadFormDatas->count())
                            <p>
                                @foreach($lead->LeadFormDatas as $formData)
                                    <strong>{!! $formData->name !!}:</strong> {!! $formData->value !!}<br>
                                @endforeach
                            </p>
                        @else
                            <p>
                                Apenas os dados principais foram preenchidos.
                            </p>
                        @endif
                    </div>


                    <div class="col-lg-12">
                        <hr>
                    </div>
                </div>

                @if ($lead->parent_id == 0 && false)
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="forum-title bg-danger">
                                Tentativas
                            </h3>
                            <table class="table table-responsive table-bordered table-striped">
                                <thead>
                                <tr>
                                    <td>#Lead</td>
                                    <td>Campos preenchidos</td>
                                    <td>Observação</td>
                                    <td>Data/Hora</td>
                                    <td>Ações</td>
                                </tr>
                                </thead>
                                <tbody>
                                @if ($lead->LeadParents->count())
                                    @foreach($lead->LeadParents as $parent)
                                        <tr>
                                            <td>{!! $parent->id !!}</td>
                                            <td>{!! str_limit($parent->LeadFormDatas->pluck('value', 'name'), 30) !!}</td>
                                            <td>{!! str_limit($parent->LeadObservation->text, 30) !!}</td>
                                            <td>{!! $parent->created !!}</td>
                                            <td>
                                                <a href="{!! route('lead.show', $parent->id) !!}" class="btn btn-xs btn-outline btn-success">
                                                    <i class="fa fa-eye"></i> Visualizar
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7">Nenhum registro no momento.</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif

                <div class="row">
                    {!! Form::open(['route' => ['lead.add_observation', $lead->id]]) !!}
                    <div class="col-lg-12">
                        <div class="form-group{!! $errors->first('text') ? ' has-error' : '' !!}">
                            <label for="text">
                                Observação <label class="badge badge-info">Opcional</label>
                            </label>
                            {!! Form::textarea('text', null, ['class' => 'form-control', 'rows' => 2, 'required', 'placeholder' => 'Observação', 'style' => 'resize: none']) !!}
                            {!! $errors->first('text', '<span class="text-danger">:message</span>') !!}
                        </div>
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-check"></i> Salvar observação
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
