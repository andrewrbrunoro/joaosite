<div class="row">

    <div class="col-lg-12 col-xs-12">
        <div class="form-group{!! $errors->first('title') ? ' has-error' : '' !!}">
            <label for="title">
                Título <label class="badge badge-danger">Obrigatório</label>
            </label>
            {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Título']) !!}
            {!! $errors->first('title', '<span class="text-danger">:message</span>') !!}
        </div>
    </div>

    <div class="col-lg-12 col-xs-12">
        <div class="form-group{!! $errors->first('answer') ? ' has-error' : '' !!}">
            <label for="answer">
                Resposta <label class="badge badge-danger">Obrigatório</label>
            </label>
            {!! Form::textarea('answer', null, ['class' => 'form-control redactor-editor', 'placeholder' => 'Resposta']) !!}
            {!! $errors->first('answer', '<span class="text-danger">:message</span>') !!}
        </div>
        <hr>
    </div>

    <div class="col-lg-6 col-sm-6">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check"></i> Salvar os Dados
        </button>
    </div>
</div>