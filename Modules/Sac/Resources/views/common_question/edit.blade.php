@extends('dashboard::layouts.master')

@section('title', 'Editar Pergunta Frequente')

@section('content')
    <div class="row">
        {!! Form::model($edit, ['route' => ['common_question.update', $edit->id], 'method' => 'PATCH']) !!}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Pergunta Frequente - <strong></strong></h5>
                </div>

                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="{!! route('common_question.index') !!}" class="btn btn-outline btn-warning">
                                <i class="fa fa-arrow-left"></i> Listar todas os Pergunta Frequente
                            </a>
                        </div>
                    </div>
                    <hr>
                    @include('sac::common_question.form')
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@stop
