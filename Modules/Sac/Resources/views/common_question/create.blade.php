@extends('dashboard::layouts.master')

@section('title', 'Cadastrar Pergunta Frequente')

@section('content')
    <div class="row">
        {!! Form::open(['route' => 'common_question.store', 'method' => 'post']) !!}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastrar Pergunta Frequente</h5>
                </div>

                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="{!! route('common_question.index') !!}" class="btn btn-outline btn-warning">
                                <i class="fa fa-arrow-left"></i> Listar todas os Pergunta Frequente
                            </a>
                        </div>
                    </div>
                    <hr>
                    @include('sac::common_question.form')
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@stop
