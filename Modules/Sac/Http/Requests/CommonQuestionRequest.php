<?php

namespace Modules\Sac\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommonQuestionRequest extends FormRequest
{
    public function attributes()
    {
        return [
            'title' => 'Título',
            'answer' => 'Resposta'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:191',
            'answer' => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
