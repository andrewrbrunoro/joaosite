<?php

namespace Modules\Sac\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeadObservationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'text.required' => 'Campo obrigatório.'
        ];
    }
}
