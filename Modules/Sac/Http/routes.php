<?php

Route::group([
    'prefix' => 'leads',
    'as' => 'lead.',
    'namespace' => 'Modules\Sac\Http\Controllers'
], function ($r) {

    $r->get('/', 'SacController@index')->name('index');
    $r->get('{id}/visualizar', 'SacController@show')->name('show');

    $r->post('{id}/adicionar/observacao', 'SacController@addObservation')->name('add_observation');
    $r->post('{id}/alterar/status', 'SacController@changeStatus')->name('change_status');

});


Route::group([
    'prefix' => 'pergunta-frequente',
    'as' => 'common_question.',
    'namespace' => 'Modules\Sac\Http\Controllers'
], function ($r) {

    $r->get('/', 'CommonQuestionController@index')->name('index');

    $r->get('criar', 'CommonQuestionController@create')->name('create');
    $r->post('criar', 'CommonQuestionController@store')->name('store');


    $r->get('{id}/editar', 'CommonQuestionController@edit')->name('edit');
    $r->patch('{id}/editar', 'CommonQuestionController@update')->name('update');

    $r->delete('{id}/deletar', 'CommonQuestionController@destroy')->name('destroy');

});