<?php

namespace Modules\Sac\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Sac\Entities\Lead;
use Modules\Sac\Entities\LeadObservation;
use Modules\Sac\Http\Requests\ChangeStatusRequest;
use Modules\Sac\Http\Requests\LeadObservationRequest;

class SacController extends Controller
{

    public function index(Request $request, Lead $lead)
    {
        # Lê todos os leads e página por 20
        $leads = $lead->whereParentId(0)
            ->orderBy('id', 'desc');

        if ($request->has("status"))
            $leads->whereStatus($request->get("status"));

        $leads = $leads->paginate(20);

        # Conta quantos contatos está aguardando uma resposta
        $wait_count = $lead->whereParentId(0)
            ->whereStatus(0)->count();

        # passa para view os dados
        return view('sac::index', compact('leads', 'wait_count'));
    }

    public function show($id, Lead $Lead)
    {
        try {
            # Procura o lead pelo ID
            $lead = $Lead->with(['LeadCategory', 'LeadFormDatas'])->find($id);
            # Se não existir, fornece o exception com a mensagem
            if (!$lead)
                throw new Exception("O Lead #{$id}, não foi encontrado.");

        } catch (Exception $e) {
            return redirect()->route('lead.index')->with('error', $e->getMessage());
        }
        return view('sac::show', compact('lead'));
    }

    /**
     * @param $id
     * @param LeadObservationRequest $leadObservationRequest
     * @param Lead $Lead
     * @return mixed
     *
     * Adiciona a observação na tabela de histórico do contato
     */
    public function addObservation($id, LeadObservationRequest $leadObservationRequest, Lead $Lead)
    {
        # Inicia a transação com o banco de dados
        DB::beginTransaction();
        try {

            # Procura o lead pelo ID
            $lead = $Lead->find($id);
            # Se não existir, fornece o exception com a mensagem
            if (!$lead)
                throw new Exception("O Lead #{$id}, não foi encontrado.");

            $lead->LeadObservations()->saveMany([
                new LeadObservation(['text' => $leadObservationRequest->get('text')])
            ]);

            # Salva o registro no banco
            DB::commit();

            return redirect()->back()->with('success', 'Observação adiciona no histórico deste contato.');
        } catch (Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * @param $id
     * @param ChangeStatusRequest $changeStatusRequest
     * @param Lead $Lead
     * @return mixed
     *
     * Altera o status do contato
     */
    public function changeStatus($id, ChangeStatusRequest $changeStatusRequest, Lead $Lead)
    {
        DB::beginTransaction();
        try {

            # Procura o lead pelo ID
            $lead = $Lead->find($id);
            # Se não existir, fornece o exception com a mensagem
            if (!$lead)
                throw new Exception("O Lead #{$id}, não foi encontrado.");

            # Altera o status do contato
            $lead->update(['status' => $changeStatusRequest->get('status')]);

            # Salva o registro no banco
            DB::commit();

            return redirect()->back()->with('success', 'O status deste contato foi alterado.');
        } catch (Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

}
