<?php

namespace Modules\Sac\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Sac\Entities\CommonQuestion;
use Modules\Sac\Http\Requests\CommonQuestionRequest;

class CommonQuestionController extends Controller
{
    public function index(CommonQuestion $commonQuestion)
    {
        # Paginação
        $data = $commonQuestion->paginate(20);
        return view("sac::common_question.index", compact("data"));
    }

    public function create()
    {
        return view("sac::common_question.create");
    }

    public function store(CommonQuestion $commonQuestion, CommonQuestionRequest $request)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Crio a pergunta frequente
            $commonQuestion->create($request->all());

            # Conclui a transação
            DB::commit();
            return redirect()->route("common_question.create")->with("success", "Pergunta Frequente: <strong>{$request->get('title')}</strong>, criada com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->with("error", $e->getMessage());
        }
    }

    public function edit($id, CommonQuestion $commonQuestion)
    {
        try {
            # Procuro pela pergunta frequente
            $edit = $commonQuestion->find($id);
            # Se não existir, envio pra exceção
            if (!$edit)
                throw new \Exception("Registro não encontrado.");

        } catch (\Exception $e) {
            return redirect()->route("common_question.index")->with("error", $e->getMessage());
        }

        return view("sac::common_question.edit", compact("edit"));
    }

    public function update($id, CommonQuestion $commonQuestion, CommonQuestionRequest $request)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Procuro pela pergunta frequente
            $edit = $commonQuestion->find($id);
            # Se não existir, envio para a listagem de pergunta frequente
            if (!$edit)
                return redirect()->route("common_question.index")->with("error", "Registro não encontrado.");

            # Atualizo os dados
            $edit->update($request->all());

            # Conclui a transação
            DB::commit();
            return redirect()->route("common_question.index")->with("success", "Pergunta Frequente atualizada com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->with("error", $e->getMessage());
        }
    }

    public function destroy($id, CommonQuestion $commonQuestion)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Verifico se a Pergunta Frequente existe
            $exist = $commonQuestion->find($id);
            # Se não existir, envio pra execeção
            if (!$exist)
                throw new \Exception("Registro não encontrado.");

            # Deleto o registro
            $exist->delete();

            # Conclui a transação
            DB::commit();
            return redirect()->route("common_question.index")->with("success", "Pergunta Frequente removida com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->route("common_question.index")->with("error", $e->getMessage());
        }
    }
}
