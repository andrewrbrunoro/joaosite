<?php

namespace Modules\Sac\Jobs;

use App\Mail\ContactEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Modules\Sac\Entities\Lead;
use Modules\Sac\Entities\LeadCategory;
use Modules\Sac\Entities\LeadFormData;

class LeadJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    public $name;

    public $email;

    public $category;

    public $extra;

    public $subject;

    /**
     * LeadJob constructor.
     * @param $name
     * @param $email
     * @param $subject
     * @param $categorySlug
     * @param $extra
     *
     * É obrigatório vir o E-MAIL e NOME no Request
     */
    public function __construct($name, $email, $subject, $categorySlug, $extra = [])
    {
        $this->name = $name;
        $this->email = $email;
        $this->subject = $subject;
        $this->category = LeadCategory::whereSlug($categorySlug)->first();
        $this->extra = $extra;
    }

    public function handle()
    {
        DB::beginTransaction();
        try {

            $leadQuery = Lead::create([
                "lead_category_id" => $this->category->id,
                "name" => $this->name,
                "email" => $this->email
            ]);

            foreach ($this->extra as $name => $value) {
                $leadQuery->LeadFormDatas()->saveMany([
                    new LeadFormData(["name" => $name, "value" => $value])
                ]);
            }

            DB::commit();
            Mail::to(setting('lead.email'))->send(new ContactEmail($this->name, $this->email, $this->subject, $this->category->cc, $this->extra));
        } catch (\Exception $e) {
            DB::rollback();
        }
    }
}
