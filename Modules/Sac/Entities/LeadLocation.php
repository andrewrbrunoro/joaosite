<?php

namespace Modules\Sac\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeadLocation extends Model
{

    use SoftDeletes;

    protected $fillable = ["lead_id", "ip", "hostname", "city", "state", "country", "lat_lng", "org"];
}
