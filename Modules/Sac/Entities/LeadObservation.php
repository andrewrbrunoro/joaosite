<?php

namespace Modules\Sac\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeadObservation extends Model
{
    use SoftDeletes;

    protected $fillable = ["user_id", "lead_id", "text"];

    public static function boot()
    {
        parent::boot();

        # Adiciona o ID do usuário antes de salvar
        self::creating(function ($model) {
            $model->user_id = auth()->user()->id;
        });
    }

    /**
     * @return mixed
     * Converte a data que está vindo da observação para d/m/Y
     */
    public function getCreatedAttribute()
    {
        if ($this->created_at != '') {
            return $this->created_at->format('d/m/Y H:i:s');
        }
    }

}
