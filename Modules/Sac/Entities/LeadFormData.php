<?php

namespace Modules\Sac\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeadFormData extends Model
{

    use SoftDeletes;

    protected $fillable = ["lead_id", "name", "value"];

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = trim(strip_tags($value));
    }
}
