<?php

namespace Modules\Sac\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lead extends Model
{

    use SoftDeletes;

    protected $fillable = ["lead_category_id", "parent_id", "name", "email", "status", "error"];

    protected $appends = ['status_reason'];

    /**
     * @return mixed
     * Converte a data que está vindo do pedido para d/m/Y
     */
    public function getCreatedAttribute()
    {
        if ($this->created_at != '') {
            return $this->created_at->format('d/m/Y H:i:s');
        }
    }

    /**
     * @return string
     *
     * Retorna o status do leads com label
     */
    public function getStatusReasonAttribute()
    {
        switch ($this->status) {
            case 0:
                return '<label class="badge badge-danger">Aguardando resposta</label>';
                break;
            case 1:
                return '<label class="badge badge-success">Finalizado</label>';
                break;
            default:
                return '<label class="badge badge-danger">Aguardando resposta</label>';
                break;
        }
    }

    public function LeadParents()
    {
        return $this->hasMany(Lead::class, 'parent_id');
    }

    public function LeadCategory()
    {
        return $this->belongsTo(LeadCategory::class, 'lead_category_id')->withDefault();
    }

    public function LeadFormData()
    {
        return $this->hasOne(LeadFormData::class, 'lead_id')->orderBy('created_at', 'desc');
    }

    public function LeadFormDatas()
    {
        return $this->hasMany(LeadFormData::class, 'lead_id')->orderBy('value');
    }

    public function LeadObservation()
    {
        return $this->hasOne(LeadObservation::class, 'lead_id')->orderByDesc('created_at');
    }

    public function LeadObservations()
    {
        return $this->hasMany(LeadObservation::class, 'lead_id');
    }
}
