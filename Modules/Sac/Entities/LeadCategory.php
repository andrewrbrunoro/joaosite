<?php

namespace Modules\Sac\Entities;

use App\Traits\Slug;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeadCategory extends Model
{

    use SoftDeletes, Slug;

    protected $fillable = ["name", "slug"];
}
