<?php

namespace Modules\Sac\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommonQuestion extends Model
{
    use SoftDeletes;

    protected $fillable = ["title", "answer"];
}
