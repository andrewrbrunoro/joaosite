<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead_locations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('lead_id');

            $table->string('ip', 60)->nullable();
            $table->string('hostname', 120)->nullable();
            $table->string('city', 120)->nullable();
            $table->string('state', 120)->nullable();
            $table->string('country', 60)->nullable();

            $table->string('lat_lng', 60)->nullable();
            $table->string('org', 120)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lead_locations');
    }
}
