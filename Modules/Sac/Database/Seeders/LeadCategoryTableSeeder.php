<?php

namespace Modules\Sac\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Sac\Entities\LeadCategory;

class LeadCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $leadCategory = new LeadCategory();

        if ($leadCategory->where("slug", "=", "contato")->count() < 1) {
            $leadCategory->create([
                "name" => "Contato"
            ]);
        }
    }
}
