<?php

namespace Modules\Api\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Api\Entities\CourseShowcase;
use Modules\Api\Entities\Module;
use Modules\Api\Entities\ModuleLesson;

class ShowcaseController extends Controller
{

    /**
     * @param Request $request
     * @param CourseShowcase $courseShowcase
     * @param ModuleLesson $moduleLesson
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     *
     * Leio as vitrines com a relação em PIVOT
     */
    public function getShowcases(Request $request, CourseShowcase $courseShowcase, ModuleLesson $moduleLesson)
    {
        # Procuro as vitrines disponíveis
        # A vitrine só vai existir SE ( Existir cursos na relação E que os cursos tenham turmas como PRINCIPAL )
        $showcases = $courseShowcase->has('VitrineCursos')
            ->with(['VitrineCursos' => function($q) {
                $q->with(["Turma" => function($q) {
                    $q->select(["id", "course_id", "team_type_id", "name", "slug", "price", "sale"]);
                }])->select(["courses.id", "name", "slug"]);
            }])->get(["id", "name"]);

        # Instâncio uma collect para poder manipular os dados com facilidade
        $data = collect([]);
        # Faço a leitura nas vitrines
        foreach ($showcases as $showcase) {
            $courses = [];
            # Leitura do Curso que está na relação
            foreach ($showcase->VitrineCursos as $row) {
                # Pra diminuir o uso de instância, eu seto os dados da turma na variável "team"
                $team = $row->Turma;
                # Crio o array com os dado do curso
                $courses[] = [
                    "titulo" => $row->name,
                    "imagem" => "http://davi-andre.fd/assets/images/cursos/edital-concurso-policia-civil-ma.jpg",
                    "valor_label" => "R$ <strong>{$team->price}</strong>",
                    "tipo" => $team->TeamType->name,
                    "q_aulas" => "{$team->countLessons} aulas",
                    "link" => url("{$row->slug}/{$team->TeamType->slug}/{$team->slug}")
                ];
            }


            $data->push([
                'id' => $showcase->id,
                'curso' => $showcase->name,
                'turmas' => $courses
            ]);
        }
        return response()->json($data);
    }

}
