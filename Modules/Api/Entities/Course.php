<?php

namespace Modules\Api\Entities;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [];

    public function Turma()
    {
        return $this->hasOne(Team::class, 'course_id', 'id')->wherePrincipal(1);
    }
}
