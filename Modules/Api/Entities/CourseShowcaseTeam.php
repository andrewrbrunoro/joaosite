<?php

namespace Modules\Api\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseShowcaseTeam extends Model
{

    use SoftDeletes;

    protected $fillable = [];

    public function Curso()
    {
        return $this->belongsTo(Course::class, "course_id");
    }

}
