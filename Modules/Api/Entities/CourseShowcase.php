<?php

namespace Modules\Api\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseShowcase extends Model
{
    use SoftDeletes;

    protected $fillable = [];

    public function VitrineCursos()
    {
        return $this->belongsToMany(Course::class, 'course_showcase_teams', 'course_showcase_id', 'course_id')
            ->has('Turma')
            ->with('Turma')
            ->whereNull('course_showcase_teams.deleted_at');
    }
}
