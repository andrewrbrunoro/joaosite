<?php

namespace Modules\Api\Entities;

use App\AttributesPivot;
use App\Traits\Price;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    use SoftDeletes, Price;

    protected $fillable = [];

    public function getCountLessonsAttribute()
    {
        $modules = TeamModule::where('team_id', '=', $this->id)
            ->pluck('id');

        return ModuleLesson::whereIn('module_id', $modules)->count();
    }

    public function TeamType()
    {
        return $this->belongsTo(TeamType::class, 'team_type_id')->withDefault();
    }

    public function TeamModules()
    {
        return $this->belongsToMany(Module::class, 'team_modules', 'team_id', 'module_id')
            ->whereNull('team_modules.deleted_at')
            ->orderBy('team_modules.order')
            ->using(AttributesPivot::class)
            ->withPivot('order', 'price', 'sale', 'release_date', 'expiration_date');
    }

}
