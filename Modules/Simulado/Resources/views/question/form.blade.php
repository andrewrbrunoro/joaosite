<div class="row">

    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="clear">
                        Status <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('status', 1, true) !!} Ativo
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('status', 0, false) !!} Inativo
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-xs-12">

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('formation_area_id') ? ' has-error' : '' !!}">
                    <label for="formation_area_id">
                        Área de Formação <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::select('formation_area_id', $formationAreas, null, ['class' => 'form-control', 'placeholder' => 'Área de Formação']) !!}
                    {!! $errors->first('formation_area_id', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('action_area_id') ? ' has-error' : '' !!}">
                    <label for="action_area_id">
                        Área de Atuação <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::select('action_area_id', $actingAreas, null, ['class' => 'form-control', 'placeholder' => 'Área de Atuação']) !!}
                    {!! $errors->first('action_area_id', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('discipline_id') ? ' has-error' : '' !!}">
                    <label for="discipline_id">
                        Disciplina <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::select('discipline_id', $disciplines, null, ['class' => 'form-control', 'placeholder' => 'Disciplina']) !!}
                    {!! $errors->first('discipline_id', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('level_id') ? ' has-error' : '' !!}">
                    <label for="level_id">
                        Nível <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::select('level_id', $levels, null, ['class' => 'form-control', 'placeholder' => 'Nível']) !!}
                    {!! $errors->first('level_id', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
        </div>

    </div>

    <div class="col-lg-6 col-sm-6">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check"></i> Salvar os Dados
        </button>
    </div>
</div>