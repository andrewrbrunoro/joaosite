@extends('dashboard::layouts.master')

@section('title', 'Cadastrar Questão')

@section('content')
    <div class="row">
        {!! Form::open(['route' => 'question.store', 'method' => 'post']) !!}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastrar Questão</h5>
                </div>

                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="{!! route('question.index') !!}" class="btn btn-outline btn-warning">
                                <i class="fa fa-arrow-left"></i> Listar todas as Questões
                            </a>
                        </div>
                    </div>
                    <hr>
                    @include('simulado::question.form')
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@stop
