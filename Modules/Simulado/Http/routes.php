<?php

/**
 * Rotas do Simulado
 */
Route::group([
    'prefix' => 'questoes',
    'as' => 'question.',
    'namespace' => 'Modules\Simulado\Http\Controllers'
], function ($r) {

    $r->get('/', 'SimuladoController@index')->name('index');

    $r->get('criar', 'SimuladoController@create')->name('create');
    $r->post('criar', 'SimuladoController@store')->name('store');


    $r->get('{id}/editar', 'SimuladoController@edit')->name('edit');
    $r->patch('{id}/editar', 'SimuladoController@update')->name('update');

    $r->delete('{id}/deletar', 'SimuladoController@destroy')->name('destroy');
});