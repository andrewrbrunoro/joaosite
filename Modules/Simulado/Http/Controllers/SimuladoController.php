<?php

namespace Modules\Simulado\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Course\Entities\Discipline;
use Modules\Simulado\Entities\ActingArea;
use Modules\Simulado\Entities\FormationArea;
use Modules\Simulado\Entities\Level;
use Modules\Simulado\Entities\Question;

class SimuladoController extends Controller
{

    public function index(Question $question)
    {
        # Paginação das questões
        $data = $question->paginate(20);

        return view('simulado::question.index', compact('data'));
    }

    public function create(Discipline $discipline, ActingArea $actingArea, FormationArea $formationArea, Level $level)
    {
        # Lista as disciplinas
        $disciplines = $discipline->whereStatus(1)
            ->pluck('name', 'id');

        # Lista as Áreas de atuação
        $actingAreas = $actingArea->pluck('name', 'id');

        # Lista as Áreas de Formação
        $formationAreas = $formationArea->pluck('name', 'id');

        # Lista os Levels
        $levels = $level->pluck('name', 'id');

        return view('simulado::question.create', compact('disciplines', 'actingAreas', 'formationAreas', 'levels'));
    }

    public function store(Request $request)
    {
    }

    public function show()
    {
        return view('simulado::question.show');
    }

    public function edit()
    {
        return view('simulado::question.edit');
    }

    public function update(Request $request)
    {
    }

    public function destroy()
    {
    }
}
