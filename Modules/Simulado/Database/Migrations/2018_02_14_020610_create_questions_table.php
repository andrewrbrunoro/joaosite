<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_type_id');

            $table->text('subject');

            $table->longText('question');
            $table->longText('comment')->nullable();

            $table->decimal('point', 10, 2)->nullable()->default(0.00);

            $table->tinyInteger('eliminate_point')->default(0);

            $table->text('correction_video')->nullable();
            $table->string('correction_file')->nullable();

            $table->integer('discipline_id')->nullable();
            $table->integer('level_id')->nullable();
            $table->integer('formation_area_id')->nullable();
            $table->integer('action_area_id')->nullable();

            $table->tinyInteger('status')->default(1);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
