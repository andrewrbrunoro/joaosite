<?php

namespace Modules\Podcast\Entities;

use App\Traits\Date;
use App\Traits\Slug;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Podcast extends Model
{
    use SoftDeletes, Slug, Date;

    protected $fillable = ["name", "slug", "play_list", "release_date", "expiration_date"];


}
