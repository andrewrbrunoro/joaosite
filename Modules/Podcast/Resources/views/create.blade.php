@extends('dashboard::layouts.master')

@section('title', 'Cadastrar Podcast')

@section('content')
    <div class="row">
        {!! Form::open(['route' => 'podcast.store', 'method' => 'post']) !!}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastrar Podcast</h5>
                </div>

                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="{!! route('podcast.index') !!}" class="btn btn-outline btn-warning">
                                <i class="fa fa-arrow-left"></i> Listar todos os Podcast
                            </a>
                        </div>
                    </div>
                    <hr>
                    @include('podcast::form')
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@stop
