@extends('dashboard::layouts.master')

@section('title', 'Editar Podcast')

@section('content')
    <div class="row">
        {!! Form::model($edit, ['route' => ['podcast.update', $edit->id], 'method' => 'PATCH']) !!}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar Podcast - <strong></strong></h5>
                </div>

                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="{!! route('podcast.index') !!}" class="btn btn-outline btn-warning">
                                <i class="fa fa-arrow-left"></i> Listar todos os Podcast
                            </a>
                        </div>
                    </div>
                    <hr>
                    @include('podcast::form')
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@stop
