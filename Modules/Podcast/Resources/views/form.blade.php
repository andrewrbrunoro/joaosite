<div class="row">

    <div class="col-lg-12 col-xs-12">

        <div class="row">
            <div class="col-lg-6 col-xs-6">
                <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                    <label for="name">
                        Título <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                    {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="form-group{!! $errors->first('release_date') ? ' has-error' : '' !!}">
                    <label for="release_date">
                        Dt. lançamento <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::text('release_date', null, ['class' => 'form-control', 'placeholder' => 'Dt. lançamento']) !!}
                    {!! $errors->first('release_date', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="form-group{!! $errors->first('expiration_date') ? ' has-error' : '' !!}">
                    <label for="expiration_date">
                        Dt. encerramento <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::text('expiration_date', null, ['class' => 'form-control', 'placeholder' => 'Dt. encerramento']) !!}
                    {!! $errors->first('expiration_date', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
        </div>

        <div class="form-group{!! $errors->first('play_list') ? ' has-error' : '' !!}">
            <label for="play_list">
                Play List <label class="badge badge-danger">Obrigatório</label>
            </label>
            {!! Form::text('play_list', null, ['class' => 'form-control', 'placeholder' => 'Play List']) !!}
            {!! $errors->first('play_list', '<span class="text-danger">:message</span>') !!}
        </div>

        <hr>
    </div>

    <div class="col-lg-6 col-sm-6">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check"></i> Salvar o Podcast
        </button>
    </div>
</div>