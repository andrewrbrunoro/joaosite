<?php

Route::group([
    'prefix' => 'podcasts',
    'as' => 'podcast.',
    'namespace' => 'Modules\Podcast\Http\Controllers'
], function ($r) {

    $r->get('/', 'PodcastController@index')->name('index');

    $r->get('criar', 'PodcastController@create')->name('create');
    $r->post('criar', 'PodcastController@store')->name('store');


    $r->get('{id}/editar', 'PodcastController@edit')->name('edit');
    $r->patch('{id}/editar', 'PodcastController@update')->name('update');

    $r->delete('{id}/deletar', 'PodcastController@destroy')->name('destroy');

});
