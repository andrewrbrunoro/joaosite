<?php

namespace Modules\Podcast\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Podcast\Entities\Podcast;
use Modules\Podcast\Http\Requests\PodcastRequest;

class PodcastController extends Controller
{

    public function index(Podcast $podcast)
    {
        # Paginação
        $data = $podcast->orderBy('created_at', 'desc')->paginate(20);
        return view("podcast::index", compact("data"));
    }

    public function create()
    {
        return view("podcast::create");
    }

    public function store(Podcast $podcast, PodcastRequest $request)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Crio o podcast
            $podcast->create($request->all());

            # Conclui a transação
            DB::commit();
            return redirect()->route("podcast.index")->with("success", "Podcast: <strong>{$request->get('name')}</strong>, criado com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->withInput($request->all())->with("error", $e->getMessage());
        }
    }

    public function edit($id, Podcast $podcast)
    {
        try {

            # procuro o podcast
            $edit = $podcast->find($id);
            # Se não existir, envia pra exceção
            if (!$edit)
                throw new \Exception("Registro não encontrado.");

        } catch (\Exception $e) {
            return redirect()->route("podcast.index")->with("error", $e->getMessage());
        }
        return view("podcast::edit", compact("edit"));
    }

    public function update($id, Podcast $podcast, PodcastRequest $request)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # procuro o podcast
            $edit = $podcast->find($id);
            # Se não existir, envia pra exceção
            if (!$edit)
                return redirect()->route("podcast.index")->with("error", "Registro não encontrado.");

            # atualizo o podcast
            $edit->update($request->all());

            # Conclui a transação
            DB::commit();
            return redirect()->route("podcast.index")->with("success", "Podcast atualizado com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->with("error", $e->getMessage());
        }
    }

    public function destroy($id, Podcast $podcast)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # procuro o podcast
            $delete = $podcast->find($id);
            # Se não existir, envia pra exceção
            if (!$delete)
                return redirect()->route("podcast.index")->with("error", "Registro não encontrado.");

            # Deleto o podcast
            $delete->delete();

            # Conclui a transação
            DB::commit();
            return redirect()->route("podcast.index")->with("success", "Podcast deletado com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->route("podcast.index")->with("error", $e->getMessage());
        }
    }

}
