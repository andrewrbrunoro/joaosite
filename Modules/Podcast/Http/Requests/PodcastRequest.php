<?php

namespace Modules\Podcast\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PodcastRequest extends FormRequest
{
    public function attributes()
    {
        return [
            "name" => "Nome",
            "play_list" => "Play List",
            "release_date" => "Dt. Lançamento",
            "expiration_date" => "Dt. Encerramento"
        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required",
            "play_list" => "required",
            "release_date" => "nullable|date_format:d/m/Y",
            "expiration_date" => "nullable|date_format:d/m/Y"
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
