<?php namespace Modules\Dashboard\Helpers;

use Exception;
use Illuminate\Support\MessageBag;
use Pusher\Pusher;

class PusherHelper
{

    private $pusher;

    private $data;

    private $bag;

    public function __construct(MessageBag $bag)
    {
        $this->bag = $bag;
        $options = array(
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'encrypted' => env('PUSHER_APP_ENCRYPTED')
        );
        $this->pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), $options);
    }

    /**
     * @param string $channel
     * @param string $event
     * @return PusherHelper
     *
     * Efetua o RTC
     */
    public function trigger(string $channel, string $event): self
    {
        try {
            $this->pusher->trigger($channel, $event, $this->data);
        } catch (Exception $e) {
            $this->setError($e->getMessage());
        }
        return $this;
    }

    /**
     * @param string $message
     * @return PusherHelper
     *
     * Adiciona a mensagem para o usuário
     */
    public function setMessage(string $message): self
    {
        $this->data['message'] = $message;
        return $this;
    }

    /**
     * @param $message
     * @param string $key
     *
     * Adiciona erro na BAG
     */
    private function setError($message, $key = '')
    {
        $this->bag->add(!empty($key) ? $key : $this->bag->count(), $message);
    }

    /**
     * @return mixed
     *
     * Retorna os errors que estão na MesssageBag do validator
     */
    public function getErrors()
    {
        return $this->bag->all();
    }

    /**
     * @return bool
     *
     * Verifica se existe erros
     */
    public function getStatus()
    {
        return $this->bag->count() > 0;
    }


}