@extends('dashboard::layouts.master')

@section('title', 'Painel')

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Painel</h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="forum-title bg-primary">Pedidos recentes (Os últimos 5)</h3>
                    </div>
                    <div class="col-lg-12">
                        <table class="table table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th>#Pedido</th>
                                    <th>Status Atual</th>
                                    <th>Total</th>
                                    <th>Sub Total</th>
                                    <th>Total com desconto</th>
                                    <th>Data do pedido</th>
                                    <th>Opções</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if ($purchases->count())
                                @foreach($purchases as $row)
                                    <tr{!! !$row->without_status ? ' class="bg-danger" title="O Pedido não contém um status, verifique o ocorrido."' : '' !!}>
                                        <td>{!! $row->id !!}</td>
                                        @if ($row->without_status == true)
                                            <td>
                                                <label class="{!! $row->PurchaseLastHistory->PurchaseStatus->badge !!}">
                                                    {!! $row->PurchaseLastHistory->PurchaseStatus->name !!}
                                                </label>
                                            </td>
                                        @else
                                            <td>
                                                Pedido sem status
                                            </td>
                                        @endif
                                        <td>R$ {!! $row->total_brl !!}</td>
                                        <td>R$ {!! $row->sub_total_brl !!}</td>
                                        <td>R$ {!! $row->total_with_coupon_brl !!}</td>
                                        <td>{!! $row->created !!}</td>
                                        <td>
                                            <a href="{!! route('purchase.show', $row->id) !!}" class="btn btn-outline btn-xs btn-success">
                                                <i class="fa fa-eye"></i> Visualizar
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="7">Nenhum pedido no momento, <a href="{!! route('purchase.create') !!}">clique aqui</a> para gerar um pedido manualmente.</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>


            </div>
        </div>
    </div>
@stop
