<div class="navbar-collapse collapse" id="navbar">
    <ul class="nav navbar-nav">
        <li class="dropdown">
            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Clientes / Usuários <span class="caret"></span></a>
            <ul role="menu" class="dropdown-menu">
                <li>
                    <a href="{!! route('user.index') !!}">Usuários</a>
                </li>
                <li>
                    <a href="{!! route('notification.index') !!}">Notificações</a>
                </li>
                <li>
                    <a href="{!! route('lead.index') !!}">Lead / Contatos</a>
                </li>
                <li>
                    <a href="{!! route('common_question.index') !!}">Perguntas Frequentes</a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> E-Cursos <span class="caret"></span></a>
            <ul role="menu" class="dropdown-menu">
                <li>
                    <a href="{!! route('course.index') !!}">Cursos</a>
                </li>
                <li>
                    <a href="{!! route('team.index') !!}">Turmas</a>
                </li>
                <li>
                    <a href="{!! route('lesson.index') !!}">Aulas</a>
                </li>
                <li>
                    <a href="{!! route('content.index') !!}">Conteúdos</a>
                </li>
                <li>
                    <a href="{!! route('course.showcase.index') !!}">Vitrines</a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Blog <span class="caret"></span></a>
            <ul role="menu" class="dropdown-menu">
                <li>
                    <a href="{!! route('blog.index') !!}">Posts</a>
                </li>
                <li>
                    <a href="{!! route('post_category.index') !!}">Categorias</a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Complementos <span class="caret"></span></a>
            <ul role="menu" class="dropdown-menu">
                <li>
                    <a href="{!! route('banner.index') !!}">Banners</a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Pedidos <span class="caret"></span></a>
            <ul role="menu" class="dropdown-menu">
                <li>
                    <a href="{!! route('purchase.index') !!}">Ordem de compras</a>
                </li>
            </ul>
        </li>
    </ul>
    <ul class="nav navbar-top-links navbar-right">
        <li>
            <a href="{!! route('auth.logout') !!}">
                <i class="fa fa-sign-out"></i> Sair
            </a>
        </li>
    </ul>
</div>