@if (isset($errors))
    <div class="row">
        @if (session()->has('success'))
            <div class="alert alert-success">
                {!! session('success') !!}
            </div>
        @endif
        @if (session()->has('info'))
            <div class="alert alert-info">
                {!! session('info') !!}
            </div>
        @endif
        @if (session()->has('warning'))
            <div class="alert alert-warning">
                {!! session('warning') !!}
            </div>
        @endif
        @if (session()->has('error'))
            <div class="alert alert-danger">
                {!! session('error') !!}
            </div>
        @endif
        @if ($errors->count() > 0)
            <div class="alert alert-danger">
                <strong>Verifique os campos em destaques.</strong>
            </div>
        @endif
    </div>
@endif