<!DOCTYPE html>
<html lang="{!! config()->get('lang') !!}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>@yield('title') | {!! setting('nome') !!}</title>

    {!! Html::style('assets/vendors/bootstrap/bootstrap.min.css') !!}
    {!! Html::style('assets/vendors/sweetalert/sweetalert.min.css') !!}
    {!! Html::style('assets/vendors/animate/animate.css') !!}

    {!! Html::style('assets/vendors/redactor/redactor.min.css') !!}

    {!! Html::style('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') !!}

    {!! Html::style('assets/vendors/jquery-tag/jquery-ui.css') !!}
    {!! Html::style('assets/vendors/jquery-tag/jquery.tagit.css') !!}

    {!! Html::style('assets/vendors/toastr/toastr.min.css') !!}
    @yield('css')
    {!! Html::style(Module::asset('dashboard:css/main.css')) !!}
    {!! Html::style('assets/vendors/inspina/style.css') !!}

    {!! Html::script('assets/vendors/pusher/pusher.min.js') !!}

    {!! Html::style('assets/vendors/ionRangeSlider/ion.rangeSlider.css') !!}
    {!! Html::style('assets/vendors/ionRangeSlider/ion.rangeSlider.skinNice.css') !!}

    <script>
        var APP_URL = '{!! url('/') !!}/',
            CMS_URL = APP_URL + '{!! env('SYSTEM_CMS_PREFIX') !!}/',
            USER_TOKEN = '{!! auth()->user()->token !!}';

        // Enable pusher logging - don't include this in production
        //        Pusher.logToConsole = true;

        var pusher = new Pusher('{!! env('PUSHER_APP_KEY') !!}', {
            cluster: '{!! env('PUSHER_CLUSTER') !!}',
            encrypted: true
        });
    </script>
</head>

<body class="top-navigation">

<div id="wrapper">

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom white-bg">
            <nav class="navbar navbar-static-top" role="navigation">

                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
                            class="navbar-toggle collapsed" type="button">
                        <i class="fa fa-reorder"></i>
                    </button>
                    <a href="{!! route('dashboard.index') !!}" class="navbar-brand">{!! setting('nome') !!}</a>
                </div>

                @include('dashboard::layouts.elements.menu')

            </nav>
        </div>

        <div class="wrapper wrapper-content">
            <div class="container">

                @include('dashboard::layouts.elements.alerts')

                @yield('content')
            </div>
        </div>

        <div class="clearfix"></div>

        @include('dashboard::layouts.elements.footer')


        {{--<div class="floating-modal">--}}
        {{--@yield("right-modal")--}}
        {{--</div>--}}

    </div>

</div>

{!! Html::script('assets/vendors/jquery/jquery-2.1.1.js') !!}
{!! Html::script('assets/vendors/jquery/jquery-ui.min.js') !!}

{!! Html::script('assets/vendors/font-awesome/5.0.6.js') !!}

{!! Html::script('assets/vendors/bootstrap/bootstrap.min.js') !!}
{!! Html::script('assets/vendors/metis-menu/jquery.metisMenu.js') !!}
{!! Html::script('assets/vendors/slim-scroll/jquery.slimscroll.min.js') !!}
{!! Html::script('assets/vendors/sweetalert/sweetalert.min.js') !!}

{!! Html::script('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') !!}
{!! Html::script('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.pt-BR.min.js') !!}

{!! Html::script('assets/vendors/redactor/redactor.min.js') !!}

{!! Html::script('assets/vendors/jquery-tag/tag-it.min.js') !!}

{!! Html::script('assets/vendors/toastr/toastr.min.js') !!}
{!! Html::script('assets/vendors/pace/pace.min.js') !!}
{!! Html::script('assets/vendors/inspina/inspina.js') !!}
{!! Html::script('assets/vendors/ionRangeSlider/ion.rangeSlider.min.js') !!}

{!! Html::script('assets/vendors/axios/axios.js') !!}
{!! Html::script('assets/vendors/vue/vue.js') !!}
{!! Html::script('assets/vendors/vue-mask/vue-mask.min.js') !!}
{!! Html::script(Module::asset('purchase:js/vendors/vue-mask-money/vue-mask-money.js')) !!}

@yield('script')

{!! Html::script(Module::asset('dashboard:js/main.js')) !!}

@yield('main-js')

<script type="text/javascript">
    Vue.use(VueTheMask);
    Vue.use(VMoney);
    new Vue({
        el: '#wrapper',
        data: function () {
            return {
                // Variável para poder acessar dados externos dos componentes
                // Pra criar um padrão, utilize o nome do módulo em camelCase
                global: {
                    teamModules: {
                        module_id: ""
                    }
                }
            }
        }
    });

    var user_channel = pusher.subscribe(USER_TOKEN);
    user_channel.bind('notification', function (data) {
        toastr.success(data.message);
    });

    var private_channel = pusher.subscribe('private01');
    private_channel.bind('notification', function (data) {
        toastr.info(data.message);
    });

    var channel = pusher.subscribe('purchases');
    channel.bind('alert', function (data) {
        toastr.success(data.message);
    });
    channel.bind('{!! auth()->user()->token !!}', function (data) {
        console.log(data);
        toastr.success(data.message);
    });
</script>


</body>
</html>