$(document).ready(function () {
    $('.redactor-editor').redactor({
        minHeight: 300,
        toolbarFixed: true,
        toolbarFixedTopOffset: 80, // pixels
        fileUpload: CMS_URL + 'upload/file',
        imageUpload: CMS_URL + 'upload/image'
    });

    $('[data-open-modal]').on('click', function (e) {
        e.preventDefault();
        $('.floating-modal').toggleClass('active');
    });

    $('[data-close-modal]').on('click', function(e){
        e.preventDefault();
        $('.floating-modal').toggleClass('active');
    });

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "preventOpenDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "7000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    $('.date-picker').datepicker({
        format: "dd/mm/yyyy",
        todayHighlight: true
    });
});

var wait = false;
var button;
var buttonText;

/**
 * Função para botões de deletar
 * @param form
 * @returns {boolean}
 */
function beforeConfirm (form) {
    if (form) {
        event.preventDefault();
        if (!wait) {

            wait = true;
            form = $(form);

            swal({
                title: 'Você tem certeza?',
                text: "Você deseja realmente remover este registro?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim',
                cancelButtonText: 'Cancelar'
            }).then(function () {
                form.removeAttr('onsubmit');
                form.trigger('submit');
            }, function() {
                wait = false;
            });

        }
    } else {
        return false;
    }
}

/**
 * Passar como parâmetro o atributo name
 * @param element_name
 * @param message
 */
function setError(element_name, message) {
    var element = $(document).find('[name="' + element_name + '"]');
    /* Adiciona a class has-error na div parent */
    element.parent('div').addClass('has-error');
    /* Cria o elemento span após o elemento */
    element.after('<span class="help-block automatic-error"> ' + message + ' </span>');
}

/**
 * Remove todos os erros da página atual
 */
function removeAllErrors() {
    /* Procura no documento tudo que tiver com a classe has-error e remove a classe has-error */
    $(document).find('.has-error').removeClass('has-error');
    /* Procura todos os span com a classe automatic-error e remove do documento */
    $(document).find('.automatic-error').remove();
}

function createButton(element) {
    button = element;
    buttonText = element.html();
}

/**
 * Inicia o loading
 */
function startLoading() {
    wait = true;
    if (button)
        button.html('Aguarde...');
}

/**
 * Finaliza o loading
 */
function endLoading(text) {
    wait = false;

    if (text) {
        button.html(text);
        setTimeout(function(){
            if (button && buttonText != '')
                button.html(buttonText);
        }, 3000);
    } else {
        if (button && buttonText != '')
            button.html(buttonText);
    }

}

/**
 *
 * Slugify
 *
 * @param str
 * @returns {*}
 */
function string_to_slug (str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
}