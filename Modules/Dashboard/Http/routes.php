<?php
Route::group([
    'namespace' => 'Modules\Dashboard\Http\Controllers',
    'as' => 'dashboard.'
], function ($r) {

    $r->get('/', 'DashboardController@index')->name('index');

});
