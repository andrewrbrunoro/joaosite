<?php

namespace Modules\Dashboard\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Route;
use Modules\Purchase\Entities\Purchase;

class DashboardController extends Controller
{

    public function index(Purchase $purchase)
    {
        $purchases = $purchase->orderBy('created_at', 'desc')
            ->limit(5)
            ->get();
        return view('dashboard::index', compact('purchases'));
    }

    public function create()
    {
        return view('dashboard::create');
    }

    public function store(Request $request)
    {
    }

    public function show()
    {
        return view('dashboard::show');
    }

    public function edit()
    {
        return view('dashboard::edit');
    }

    public function update(Request $request)
    {
    }

    public function destroy()
    {
    }
}
