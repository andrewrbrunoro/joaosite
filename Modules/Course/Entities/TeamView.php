<?php

namespace Modules\Course\Entities;

use Illuminate\Database\Eloquent\Model;

class TeamView extends Model
{
    protected $fillable = ["team_id"];
}
