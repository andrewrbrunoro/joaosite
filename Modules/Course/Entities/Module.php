<?php

namespace Modules\Course\Entities;

use App\AttributesPivot;
use App\Traits\Date;
use App\Traits\Price;
use App\Traits\Slug;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module extends Model
{

    use SoftDeletes, Slug;

    protected $fillable = ["name", "slug"];

    public function TeamModules()
    {
        return $this->hasMany(TeamModule::class, 'module_id');
    }

    public function ModuleLessons()
    {
        return $this->belongsToMany(Lesson::class, 'module_lessons', 'module_id', 'lesson_id')
            ->whereNull('module_lessons.deleted_at')
            ->using(AttributesPivot::class)
            ->withPivot('order', 'release_date', 'expiration_date')
            ->orderBy('module_lessons.order', 'asc');
    }

}
