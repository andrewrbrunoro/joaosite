<?php

namespace Modules\Course\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discipline extends Model
{

    use SoftDeletes;

    protected $fillable = ["name", "slug", "status"];

}
