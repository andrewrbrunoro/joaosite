<?php

namespace Modules\Course\Entities;

use App\Traits\Price;
use App\Traits\Slug;
use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{

    use SoftDeletes, Price, Slug, Status;

    public $path_image = "assets/uploads/content/";

    protected $fillable = [
        "content_type_id", "available_weekday","duration","name", "slug", "video",
        "price", "sale", "content", "status"
    ];

    /**
     * Verifica se o arquivo existe
     */
    public function getFileExistsAttribute()
    {
        if ($this->ContentType->element_type == 'file') {
            return is_file(public_path($this->content));
        }
        return true;
    }

    public function ContentType()
    {
        return $this->belongsTo(ContentType::class, 'content_type_id')->withDefault();
    }

}
