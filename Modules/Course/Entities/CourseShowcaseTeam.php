<?php

namespace Modules\Course\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseShowcaseTeam extends Model
{
    use SoftDeletes;

    protected $fillable = ["course_showcase_id", "course_id", "order"];

    public function Course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
}
