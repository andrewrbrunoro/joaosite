<?php

namespace Modules\Course\Entities;

use App\Traits\Date;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ModuleLesson extends Model
{
    use SoftDeletes, Date;

    protected $fillable = ["module_id", "lesson_id", "order", "release_date", "expiration_date"];
}
