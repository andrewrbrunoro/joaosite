<?php

namespace Modules\Course\Entities;

use App\Helpers\DateInterval;
use App\Traits\Date;
use App\Traits\ModelAttribute;
use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{

    use SoftDeletes, Status, Date;

    protected $fillable = [
        "name", "slug", "resume", "status", "days_available",
        "video_call", "commercial_call", "terms"
    ];

    public static function boot()
    {
        parent::boot();

        self::created(function($model){
            cache()->flush();
        });

        self::updated(function($model){
            cache()->flush();
        });

        self::deleted(function($model){
            cache()->flush();
        });
    }


    /**
     * @var array
     *
     * Informo pra model, qual será os campos no formato DATA
     */
    protected $dates = ["release_date", "expiration_date"];

    /**
     * Verifica se o Curso está em uma data válida para utilização
     */
    public function getCourseNameExpirationTextAttribute()
    {
        $dateInterval = new DateInterval();
        $dateInterval->validIntervalDate($this->release_date, $this->expiration_date);
        return "{$this->name} - Curso {$dateInterval->getStatusMessage()}";
    }

    /**
     * @param $value
     *
     * Cria o slug do curso e evita repitir o mesmo
     */
    public function setNameAttribute($value)
    {
        # Seta o slug no atributo para salvar
        $this->attributes['slug'] = slugToMe($value, $this);
        # Seta o nome tratado no atributo
        $this->attributes['name'] = $value;
    }

    public function setResumeAttribute($value)
    {
        if ($value)
            $this->attributes['resume'] = trim(strip_tags($value));
        else {
            $this->attributes['resume'] = null;
        }
    }

}
