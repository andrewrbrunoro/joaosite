<?php

namespace Modules\Course\Entities;

use App\Traits\Date;
use App\Traits\Price;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeamModule extends Model
{

    use SoftDeletes, Date, Price;

    protected $fillable = ["team_id", "module_id", "order", "price", "sale", "release_date", "expiration_date"];

    public function Team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    public function Module()
    {
        return $this->belongsTo(Module::class, 'module_id');
    }

}
