<?php

namespace Modules\Course\Entities;

use App\AttributesPivot;
use App\Traits\Contents;
use App\Traits\Price;
use App\Traits\Slug;
use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{

    use SoftDeletes, Price, Contents, Slug, Status;

    protected $fillable = [
        "course_id", "team_type_id", "name", "slug", "resume",
        "price", "sale", "content", "principal", "status", "principal"
    ];

    public static function boot()
    {
        parent::boot();

        self::created(function($model){
            foreach (["team_{id}", "{id}_count_lessons"] as $key) {
                cache()->forget(str_replace("{id}", $model->id, $key));
            }
        });

        self::updated(function($model){
            foreach (["team_{id}", "{id}_count_lessons"] as $key) {
                cache()->forget(str_replace("{id}", $model->id, $key));
            }
        });

        self::deleted(function($model){
            foreach (["team_{id}", "{id}_count_lessons"] as $key) {
                cache()->forget(str_replace("{id}", $model->id, $key));
            }
        });
    }

    public function Course()
    {
        return $this->belongsTo(Course::class, 'course_id')->withDefault();
    }

    public function TeamType()
    {
        return $this->belongsTo(TeamType::class, 'team_type_id')->withDefault();
    }

    public function TeamModules()
    {
        return $this->belongsToMany(Module::class, 'team_modules', 'team_id', 'module_id')
            ->whereNull('team_modules.deleted_at')
            ->orderBy('team_modules.order')
            ->using(AttributesPivot::class)
            ->withPivot('order', 'price', 'sale', 'release_date', 'expiration_date');
    }

}
