<?php

namespace Modules\Course\Entities;

use App\Traits\Date;
use App\Traits\Slug;
use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseShowcase extends Model
{
    use SoftDeletes, Date, Slug, Status;

    protected $fillable = ["name", "slug", "status", "release_date", "expiration_date"];

    public function CourseShowcaseTeam()
    {
        return $this->hasMany(CourseShowcaseTeam::class, 'course_showcase_id');
    }
}
