<?php

namespace Modules\Course\Entities;

use App\Traits\Slug;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeamType extends Model
{

    use Slug;

    protected $fillable = ["name", "slug", "status"];

}
