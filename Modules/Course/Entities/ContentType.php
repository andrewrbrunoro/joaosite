<?php

namespace Modules\Course\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContentType extends Model
{

    use SoftDeletes;

    protected $fillable = ["name", "slug", "status", "element_type", "validate"];

}
