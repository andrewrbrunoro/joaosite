<?php

namespace Modules\Course\Entities;

use App\Traits\Date;
use App\Traits\Price;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessonContent extends Model
{
    use SoftDeletes, Date, Price;

    protected $fillable = ["lesson_id", "content_id", "order", "price", "sale", "release_date", "expiration_date"];

    public function Content()
    {
        return $this->belongsTo(Content::class, 'content_id')->withDefault();
    }

    public function Lesson()
    {
        return $this->belongsTo(Lesson::class, 'lesson_id')->withDefault();
    }
}
