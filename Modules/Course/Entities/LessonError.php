<?php

namespace Modules\Course\Entities;

use App\Traits\TextClear;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Entities\User;

class LessonError extends Model
{

    use SoftDeletes, TextClear;

    protected $fillable = ["team_id", "user_id", "url", "lesson_id", "text", "answer", "fixed"];

    public function getFixedLabelAttribute()
    {
        return $this->fixed ? '<label class="badge badge-primary">Ajustado</label>' : '<label class="badge badge-danger">Aguardando</label>';
    }

    public function Lesson()
    {
        return $this->belongsTo(Lesson::class, 'lesson_id');
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function Team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

}
