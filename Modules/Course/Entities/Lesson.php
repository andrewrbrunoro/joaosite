<?php

namespace Modules\Course\Entities;

use App\Traits\Contents;
use App\Traits\Price;
use App\Traits\Slug;
use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lesson extends Model
{

    use SoftDeletes, Slug, Price, Contents, Status;

    protected $fillable = ["name", "slug", "resume", "price", "sale", "status", "release_date", "expiration_date"];

    public function getRatingAttribute()
    {
        if ($this->LessonRating->count()) {

            return round($this->LessonRating->sum("points") / $this->LessonRating->count());

        } else {
            return "Ainda não avaliado";
        }
    }

    public function Discipline()
    {
        return $this->belongsTo(Discipline::class, 'discipline_id')->withDefault();
    }

    public function Contents()
    {
        return $this->belongsToMany(Content::class, 'lesson_contents', 'lesson_id', 'content_id')
            ->orderBy('pivot_order', 'asc')
            ->whereNull('lesson_contents.deleted_at')
            ->withPivot('order', 'price', 'sale', 'release_date', 'expiration_date');
    }

    public function LessonError()
    {
        return $this->hasMany(LessonError::class, 'lesson_id');
    }

    public function LessonQuestion()
    {
        return $this->hasMany(LessonQuestion::class, 'lesson_id');
    }

    public function LessonRating()
    {
        return $this->hasMany(LessonRating::class, 'lesson_id');
    }

    public function LessonView()
    {
        return $this->hasMany(LessonView::class, 'lesson_id');
    }

    public function LessonContents()
    {
        return $this->hasMany(LessonContent::class, 'lesson_id');
    }

}
