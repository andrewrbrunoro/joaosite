<?php

namespace Modules\Course\Entities;

use App\Traits\Date;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Entities\User;

class TeamUser extends Model
{
    use SoftDeletes, Date;

    protected $fillable = ["team_id", "user_id", "expiration_date"];

    public function Team()
    {
        return $this->belongsTo(Team::class, "team_id");
    }

    public function User()
    {
        return $this->belongsTo(User::class, "user_id");
    }
}
