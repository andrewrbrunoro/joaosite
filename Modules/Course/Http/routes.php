<?php

/**
 * Rotas do curso
 */
Route::group([
    'prefix' => 'curso',
    'as' => 'course.',
    'namespace' => 'Modules\Course\Http\Controllers'
], function ($r) {

    $r->get('/', 'CourseController@index')->name('index');

    $r->get('criar', 'CourseController@create')->name('create');
    $r->post('criar', 'CourseController@store')->name('store');


    $r->get('{id}/editar', 'CourseController@edit')->name('edit');
    $r->patch('{id}/editar', 'CourseController@update')->name('update');

    $r->delete('{id}/deletar', 'CourseController@destroy')->name('destroy');
});

/**
 * Rotas dos módulos da turma
 */
Route::group([
    'prefix' => 'modulo',
    'as' => 'module.',
    'namespace' => 'Modules\Course\Http\Controllers'
], function ($r) {

    $r->get('/todos/modulos', 'ModuleController@getModules')->name('modules');

});


/**
 * Rotas dos alunos da turma
 */
Route::group([
    'prefix' => 'turma-usuario',
    'as' => 'team_user.',
    'namespace' => 'Modules\Course\Http\Controllers'
], function ($r) {

    $r->get('{id}/matriculados', 'TeamUserController@show')->name('show');

    $r->post('{id}/matriculados', 'TeamUserController@store')->name('store');

    $r->patch('{id}/atualiza-matricula', 'TeamUserController@update')->name('update_registration');

    $r->delete('{id}/matriculados/{registration_id}', 'TeamUserController@destroy')->name('destroy');

});


/**
 * Rotas da Vitrine para cursos
 */
Route::group([
    'prefix' => 'vitrine-cursos',
    'as' => 'course.showcase.',
    'namespace' => 'Modules\Course\Http\Controllers'
], function ($r) {

    $r->get('/', 'ShowcaseController@index')->name('index');

    $r->get('criar', 'ShowcaseController@create')->name('create');
    $r->post('criar', 'ShowcaseController@store')->name('store');


    $r->get('{id}/editar', 'ShowcaseController@edit')->name('edit');
    $r->patch('{id}/editar', 'ShowcaseController@update')->name('update');

    $r->delete('{id}/deletar', 'ShowcaseController@destroy')->name('destroy');

});

/**
 * Rotas da turma
 */
Route::group([
    'prefix' => 'turma',
    'as' => 'team.',
    'namespace' => 'Modules\Course\Http\Controllers'
], function ($r) {

    $r->get('/', 'TeamController@index')->name('index');

    $r->get('criar', 'TeamController@create')->name('create');
    $r->post('criar', 'TeamController@store')->name('store');

    $r->get('{id}/editar', 'TeamController@edit')->name('edit');
    $r->patch('{id}/editar', 'TeamController@update')->name('update');

    $r->delete('{id}/deletar', 'TeamController@destroy')->name('destroy');

});

/**
 * Rotas da turma modulo
 */
Route::group([
    'prefix' => 'turma',
    'as' => 'team.module.',
    'namespace' => 'Modules\Course\Http\Controllers'
], function ($r) {

    $r->get('{id}/modulos', 'TeamModuleController@show')->name('show');
    $r->post('{id}/criar/modulo', 'TeamModuleController@store')->name('store');

    $r->patch('{id}/ordernar/modulos', 'TeamModuleController@reOrder')->name('reOrder');
    # ID = Módulo id
    $r->patch('{id}/ordernar/aulas/modulo', 'TeamModuleController@reOrderLessons')->name('reOrder.lessons');

    $r->patch('{id}/editar/modulo', 'TeamModuleController@update')->name('update');
    $r->delete('{id}/deletar/modulos', 'TeamModuleController@destroy')->name('destroy');

    $r->post('{id}/importar/modulos', 'TeamModuleController@importModules')->name('import.modules');

});

/**
 * Rotas dos conteúdos
 */
Route::group([
    'prefix' => 'conteudo',
    'as' => 'content.',
    'namespace' => 'Modules\Course\Http\Controllers'
], function ($r) {

    $r->get('procurar', 'ContentController@searchContents')->name('search');

    $r->get('/', 'ContentController@index')->name('index');

    $r->get('criar', 'ContentController@create')->name('create');
    $r->post('criar', 'ContentController@store')->name('store');

    $r->get('{id}/editar', 'ContentController@edit')->name('edit');
    $r->patch('{id}/editar', 'ContentController@update')->name('update');

    $r->delete('{id}/deletar', 'ContentController@destroy')->name('destroy');

});

/**
 * Rotas das aulas
 */
Route::group([
    'prefix' => 'aulas',
    'as' => 'lesson.',
    'namespace' => 'Modules\Course\Http\Controllers'
], function ($r) {

    $r->get('/', 'LessonController@index')->name('index');

    $r->get('criar', 'LessonController@create')->name('create');
    $r->post('criar', 'LessonController@store')->name('store');

    $r->get('{id}/editar', 'LessonController@edit')->name('edit');
    $r->patch('{id}/editar', 'LessonController@update')->name('update');

    $r->delete('{id}/deletar', 'LessonController@destroy')->name('destroy');

});


/**
 * Rotas dos Suportes das Aulas
 */
Route::group([
    'prefix' => 'suporte/aula',
    'as' => 'support.lesson.',
    'namespace' => 'Modules\Course\Http\Controllers'
], function ($r) {

    # Ações da página de erros
    $r->get('{lesson_id}/erros', 'SupportLessonController@errors')->name('error.index');
    $r->get('{lesson_id}/erro/{id}/editar', 'SupportLessonController@errorEdit')->name('error.edit');
    $r->patch('{lesson_id}/erro/{id}/editar', 'SupportLessonController@errorUpdate')->name('error.update');
    # Ações da página de erros

});