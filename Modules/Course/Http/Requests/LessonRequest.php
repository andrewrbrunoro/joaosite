<?php

namespace Modules\Course\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LessonRequest extends FormRequest
{

    public function attributes()
    {
        return [
            'name' => 'Nome',
            'resume' => 'Resumo',
            'discipline_id' => 'Disciplina',
            'price' => 'Preço',
            'sale' => 'Promoção'
        ];
    }

    /**
     * Gerencia uma sessão, caso a validação dos dados da aula seja inválido, irá retornar com eles preenchidos
     */
    public function flashContents()
    {
        # Instância uma collection
        $items = collect([]);
        # Verifica se foi passado os produtos, se sim, lê todos eles e transforma em array (eles estão vindo em json)
        if ($this->request->has('contents') && $this->request->get('contents')) {
            foreach ($this->request->get('contents') as $key => $item) {
                $items->push($item);
            }
        }
        # Adiciona em uma flash session que se caso o usuário atualizar a página via HTTP ele remove os dados
        $this->session()->flash('contents', $items->toJson());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        # Verifico se veio o array de conteúdo, caso sim, mantenho eles numa flash (caso tenha dado erro, irá retornar pra view com os mesmo preenchidos)
        $this->flashContents();

        return [
            'name' => 'required|max:255',
            'resume' => 'max:255'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
