<?php

namespace Modules\Course\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeamModuleRequest extends FormRequest
{
    public function attributes()
    {
        return [
            'name' => 'Nome',
            'release_date' => 'Dt. Lançamento',
            'expiration_date' => 'Dt. Expiração'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'release_date' => 'nullable|date_format:d/m/Y',
            'expiration_date' => 'nullable|date_format:d/m/Y'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return bool
     *
     * Retorna a validação em JSON
     */
    public function expectsJson()
    {
        return true;
    }
}
