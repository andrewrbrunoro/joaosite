<?php

namespace Modules\Course\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeamUserRequest extends FormRequest
{
    public function attributes()
    {
        return [
            'user_id' => 'Usuários',
            'expiration_date' => 'Dt. de validade'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|array',
            'expiration_date' => 'nullable|date_format:d/m/Y'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
