<?php

namespace Modules\Course\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeamUserUpgradeRequest extends FormRequest
{
    public function attributes()
    {
        return [
            'registration_id' => 'Matrícula',
            'expiration_date' => 'Dt. de validade'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'registration_id' => 'required',
            'expiration_date' => 'required|date_format:d/m/Y'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function expectsJson()
    {
        return true;
    }
}
