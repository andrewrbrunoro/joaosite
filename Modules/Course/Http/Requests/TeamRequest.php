<?php

namespace Modules\Course\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeamRequest extends FormRequest
{

    public function attributes()
    {
        return [
            'name' => 'Nome',
            'team_type_id' => 'Tipo de Turma',
            'course_id' => 'Curso',
            'price' => 'Preço',
            'sale' => 'Promoção',
            'resume' => 'Resumo',
            'content' => 'Conteúdo'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'team_type_id' => 'required',
            'course_id' => 'required',
            'resume' => 'max:255',
            'principal' => 'required',
            'status' => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
