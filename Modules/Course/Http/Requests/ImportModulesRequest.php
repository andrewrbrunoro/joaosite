<?php

namespace Modules\Course\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class ImportModulesRequest extends FormRequest
{
    public function attributes()
    {
        return [
            'modules' => 'Módulos'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'modules' => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();
        $this->session()->flash("modal", true);
        if ($errors->has("modules"))
            $this->session()->flash("error-modal", "Necessário selecionar o módulo e caso não queira algumas aulas, remova a seleção de cada uma.");

        parent::failedValidation($validator);
    }
}
