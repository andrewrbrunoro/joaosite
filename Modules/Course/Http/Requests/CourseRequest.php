<?php

namespace Modules\Course\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseRequest extends FormRequest
{

    public function attributes()
    {
        return [
            'name' => 'Nome',
            'resume' => 'Resumo',
            'days_available' => 'Dias disponíveis',
            'video_call' => 'Vídeo Comercial',
            'comercial_call' => 'Chamada Comercial',
            'terms' => 'Termos de Aquisição'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $return = [
            'name' => 'required|max:191',
            'resume' => 'max:255',
            'days_available' => 'nullable|numeric',
            'video_call' => 'nullable|url'
        ];

        return $return;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
