<?php

namespace Modules\Course\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class CourseShowcaseRequest extends FormRequest
{
    public function attributes()
    {
        return [
            'name' => 'Nome',
            'release_date' => 'Data de Lançamento',
            'expiration_date' => 'Data de Expiração'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'release_date' => 'nullable|date_format:d/m/Y',
            'expiration_date' => 'nullable|date_format:d/m/Y',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function failedValidation(Validator $validator)
    {
        $teams = collect([]);
        foreach ($this->request->get('courses') as $item) {
            $teams->push(json_decode($item, true));
        }
        $this->session()->flash('courses', $teams);
        parent::failedValidation($validator);
    }
}
