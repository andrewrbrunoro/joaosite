<?php

namespace Modules\Course\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Request;
class ContentRequest extends FormRequest
{
    public function attributes()
    {
        return [
            'name' => 'Nome',
            'content_type_id' => 'Tipo de Conteúdo',
            'video' => 'Vídeo',
            'content' => 'Conteúdo',
            'available_weekday' => 'Dia da semana',
            'duration' => 'Duração',
        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];

        $rules['name'] = 'required';
        $rules['duration'] = 'nullable|integer';
        $rules['video'] = 'nullable|url';
        $rules['available_weekday'] = 'required';

        if (Request::input('content_type_id') == 2) {
              $rules['content'] = 'mimes:pdf';
        }else if (Request::input('content_type_id') == 3) {
              $rules['content'] = 'mimes:ppt';
        }else{
              $rules['content'] = 'required';
        }
        
        return $rules;
       
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
