<?php

namespace Modules\Course\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Course\Entities\Team;
use Modules\Course\Entities\TeamUser;
use Modules\Course\Http\Requests\TeamUserRequest;
use Modules\Course\Http\Requests\TeamUserUpgradeRequest;
use Modules\User\Entities\User;

class TeamUserController extends Controller
{
    public function show($id, Team $Team, TeamUser $teamUser, User $user)
    {
        try {

            # Procuro pela turma
            $team = $Team->find($id);
            # Se ela não existir, envia pra exceção avisando que não encontrou
            if (!$team)
                throw new \Exception("Registro não encontrado.");

            # Lista os usuários que estão na turma
            $data = $teamUser->has('User')
                ->with(['User', 'Team'])
                ->where('team_id', '=', $team->id);

            # Listagem dos usuários
            $listUsers = $user->whereStatus(1)
                ->whereNotIn('id', $data->pluck('user_id')->toArray())
                ->get()
                ->pluck('email', 'id');

            $data = $data->paginate(20);

        } catch (\Exception $e) {
            return redirect()->route("team.index")->with("error", $e->getMessage());
        }
        return view("course::team.users.show", compact("team", "data", "listUsers"));
    }

    public function store($id, Team $team, TeamUser $teamUser, User $user, TeamUserRequest $request)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Procuro pela turma
            $teamExist = $team->find($id);
            # Se não existir, envia pra listagem de turmas
            if (!$teamExist)
                return redirect()->route("team.index")->with("error", "Turma não existe.");

            # Faço o looping em todos os ids
            foreach ($request->get('user_id') as $user_id) {
                # Se o usuário existir, matriculo ele
                if ($user->where('id', '=', $user_id)->count()) {
                    # Verifico se o usuário já não está matriculado
                    if (!$teamUser->where(['team_id' => $id,'user_id' => $user_id])->count()) {
                        # Cria a matricula
                        $teamUser->create([
                            "team_id" => $id,
                            "user_id" => $user_id
                        ]);
                    }
                }
            }

            # Conclui a transação
            DB::commit();
            return redirect()->route("team_user.show", $id)->with("success", "Matrículas geradas com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->route("team_user.show", $id)->with("error", $e->getMessage());
        }
    }

    public function update($id, TeamUserUpgradeRequest $request, Team $team, TeamUser $teamUser)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Verifico se existe a turma
            $teamExist = $team->find($id);
            # Se não existir, envio pra exceção informando não ter encontrado
            if (!$teamExist)
                throw new \Exception("Turma não encontrada.");

            # Verifico se a matricula existe
            $teamUserExist = $teamUser->find($request->get('registration_id'));
            # Se não existir, envio pra exceção informando não ter encontrado
            if (!$teamUserExist)
                throw new \Exception("Matrícula não encontrada.");

            # Atualizo a data de validade
            $teamUserExist->update(['expiration_date' => $request->get("expiration_date")]);

            # Conclui a transação
            DB::commit();
            return response()->json(["error" => false, "message" => "Matricula atualizada com sucesso"], 200);
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return response()->json(["error" => true, "message" => $e->getMessage()], 400);
        }
    }

    public function destroy($id, $registration_id, Team $team, TeamUser $teamUser)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Procuro pela turma
            $teamExist = $team->find($id);
            # Se não existir, envia pra listagem de turmas
            if (!$teamExist)
                return redirect()->route("team.index")->with("error", "Turma não existe.");

            # Verifico se a matricula existe
            $teamUserExist = $teamUser->find($registration_id);
            # Se não existir, envio pra exceção informando não ter encontrado
            if (!$teamUserExist)
                throw new \Exception("Matrícula não encontrada.");

            # Remove a matricula
            $teamUserExist->delete();

            # Conclui a transação
            DB::commit();
            return redirect()->route("team_user.show", $id)->with("success", "Matrícula deletada com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->route("team_user.show", $id)->with("error", $e->getMessage());
        }
    }
}
