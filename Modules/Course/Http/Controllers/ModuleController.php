<?php

namespace Modules\Course\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Course\Entities\Module;

class ModuleController extends Controller
{

    public function getModules(Request $request, Module $module)
    {
        try {

            # Pego os módulos e as relações com o mesmo
            $modules = $module->with('ModuleLessons');
            # Se receber os IDS dos módulos que a turma tem, pega os módulos que não tem
            if($request->has("not"))
                $modules->whereNotIn('id', $request->get("not"));

            # Pega os módulos
            $modules = $modules->get();

            if ($modules) {
                $modules = $modules->sortByDesc(function ($module) {
                    return $module->ModuleLessons->count();
                })->values()->all();
            }

            return response()->json(["error" => false, "data" => $modules], 200);
        } catch (\Exception $e) {
            return response()->json(["error" => true, "message" => $e->getMessage(), "data" => array()], 400);
        }
    }

}
