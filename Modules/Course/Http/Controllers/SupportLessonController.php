<?php

namespace Modules\Course\Http\Controllers;

use App\Helpers\PusherHelper;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Course\Entities\LessonError;
use Modules\Course\Entities\LessonQuestion;
use Modules\Course\Http\Requests\LessonErrorRequest;
use Modules\User\Entities\User;
use Pusher\Pusher;

class SupportLessonController extends Controller
{
    public function questions($lesson_id, LessonQuestion $lq)
    {
        # Paginação das dúvidas
        $data = $lq->where('lesson_id', '=', $lesson_id)
            ->orderBy(['status', 'created_at'])->paginate(20);

    }

    public function errors($lesson_id, LessonError $le)
    {
        # Paginação dos erros
        $data = $le->where('lesson_id', '=', $lesson_id)
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        return view("course::lesson.support.errors.index", compact('data'));
    }

    public function errorEdit($lesson_id, $error_id, LessonError $le)
    {
        try {
            # Procuro o erro
            $edit = $le->find($error_id);
            # Se não existir, envia pra exceção
            if (!$edit)
                throw new \Exception("Registro não encontrado.");
        } catch (\Exception $e) {
            return redirect()->route("support.lesson.error.index", $lesson_id)->with("error", $e->getMessage());
        }
        return view("course::lesson.support.errors.edit", compact("edit"));
    }

    public function errorUpdate($lesson_id, $error_id, PusherHelper $pusherHelper, LessonError $le, LessonErrorRequest $request)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {
            # Procuro o erro
            $edit = $le->find($error_id);
            # Se não existir, envia pra exceção
            if (!$edit)
                throw new \Exception("Registro não encontrado.");

            # Atualiza os Dados
            $edit->update($request->all());

            # Envia a notificação pro usuário
            $pusherHelper->setMessage($request->get("answer"))
                ->notifyUser($edit->user_id);

            $message = "Alteração efetuada com sucesso, ";
            if ($request->get("fixed") == true) {
                $message .= "o erro <strong>foi ajustado</strong>.";
            } else {
                $message .= "o erro <strong>não</strong> foi ajustado ainda.";
            }

            if ($request->get("text") != "") {
                $message .= " Notificação enviada pro usuário com sucesso.";
            }

            # Conclui a transação
            DB::commit();
            return redirect()->back()->with("success", $message);
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->with("error", $e->getMessage());
        }
    }
}
