<?php

namespace Modules\Course\Http\Controllers;

use GuzzleHttp\Psr7\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Course\Entities\Content;
use Modules\Course\Entities\Discipline;
use Modules\Course\Entities\Lesson;
use Modules\Course\Entities\LessonContent;
use Modules\Course\Http\Requests\LessonRequest;

class LessonController extends Controller
{

    public function index(Lesson $lesson)
    {
        # Paginação das aulas
        $data = $lesson->orderBy('created_at', 'desc')
            ->paginate(20);

        return view('course::lesson.index', compact('data'));
    }

    public function create(Discipline $discipline, Content $content)
    {
        # Lista todas as disciplinas
        $disciplines = $discipline->whereStatus(1)
            ->orderBy('name')
            ->get()
            ->pluck('name', 'id');

        # Lista os conteúdos inicialmente
        $contents = $content->whereStatus(1)
            ->orderBy('created_at', 'desc')
            ->limit(10)
            ->get(['id', 'content_type_id', 'name', 'price', 'sale']);

        return view('course::lesson.create', compact('disciplines', 'contents'));
    }

    public function store(LessonRequest $request, Lesson $lesson)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {
            # Crio a aula
            $lessonQuery = $lesson->create($request->except(['contents']));
            # Vincula os conteudos a aula
            if ($request->has('contents') && $request->get('contents')) {
                # Inicio uma variavel que vai tomar conta da ordem
                $i = 0;
                foreach ($request->get('contents') as $content) {
                    # Utilizo a relação do eloquent para relacionar, os dados estão sendo tratados pelas traits
                    $lessonQuery->LessonContents()->saveMany([new LessonContent($content + ['order' => $i])]);
                    # Incremento para adicionar ordem
                    $i++;
                }
            }
            # Inicia a transação
            DB::commit();
            # A transação foi feita com sucesso, elimina os conteúdos da seção
            session()->forget('contents');
            return redirect()->route("lesson.create")->with("success", "Aula <strong>{$request->get('name')}</strong>, criado com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->with("error", $e->getMessage());
        }
    }

    public function show()
    {
        return view('course::lesson.show');
    }

    public function edit($id, Lesson $lesson, Discipline $discipline, Content $content)
    {
        try {
            # Procuro pela Aula
            $edit = $lesson->with('Contents')->find($id);
            # Se não encontrar, envia pra exceção
            if (!$edit)
                throw new \Exception("Registro não encontrado.");

            # utiliza para preencher
            $items = collect([]);
            foreach ($edit->Contents as $content) {
                $items->push([
                    'id' => $content->id,
                    'name' => $content->name,
                    'order' => $content->pivot->order,
                    'price' => $content->pivot->price,
                    'sale' => $content->pivot->sale,
                    'release_date' => dateToSql($content->pivot->release_date, 'Y-m-d H:i:s', config('app.date_format')),
                    'expiration_date' => dateToSql($content->pivot->expiration_date, 'Y-m-d H:i:s', config('app.date_format'))
                ]);
            }

            session()->flash('contents', $items);

            # Lista todas as disciplinas
            $disciplines = $discipline->whereStatus(1)
                ->orderBy('name')
                ->get()
                ->pluck('name', 'id');

            # Lista os conteúdos inicialmente
            $contents = $content->whereStatus(1)
                ->orderBy('created_at', 'desc')
                ->limit(10)
                ->get(['id', 'content_type_id', 'name', 'price', 'sale']);
        } catch (\Exception $e) {
            # Retorna para a listagem dos cursos
            return redirect()->route("lesson.index")->with("error", $e->getMessage());
        }
        return view('course::lesson.edit', compact('edit', 'disciplines', 'contents'));
    }

    public function update($id, LessonRequest $request, Lesson $lesson, LessonContent $lessonContent)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Procuro pela Aula
            $edit = $lesson->with('Contents')->find($id);
            # Se não encontrar, envia pra exceção
            if (!$edit)
                return redirect()->route("lesson.index")->with("error", "Registro não encontrado");

            # Vincula os conteudos a aula
            if ($request->has('contents') && $request->get('contents')) {
                $contents = $edit->Contents->pluck('name', 'id')->toArray();
                $requestContents = $request->get('contents');

                # Atualiza as informações da relação
                $updateLessonContents = array_exist_keys($requestContents, $contents);
                foreach ($updateLessonContents as $updateLessonContent) {

                    # Facilito e adiciono o conteúdo atual em uma variável referente a index
                    $row = $requestContents[$updateLessonContent];

                    # Procuro e atualizo
                    $update = $lessonContent->where('lesson_id', '=', $edit->id)
                        ->where('content_id', '=', $updateLessonContent)->first();

                    if ($update) {
                        # Pego a posição do conteúdo e adiciono na ordem
                        $order = array_search($row['content_id'], array_keys($requestContents));

                        # Atualizo
                        $update->update($row + ['order' => $order]);
                    }
                }

                # Nova relação
                $newLessonContents = array_not_exist_keys($requestContents, $contents);
                foreach ($newLessonContents as $newLessonContent) {

                    # Facilito e adiciono o conteúdo atual em uma variável referente a index
                    $row = $requestContents[$newLessonContent];

                    # Pego a posição do conteúdo e adiciono na ordem
                    $order = array_search($newLessonContent, array_keys($requestContents));

                    # Relaciono o novo conteúdo
                    $edit->LessonContents()->saveMany([new LessonContent($row + ['order' => $order])]);
                }

                # Remove
                $destroyLessonContents = array_diff_key($contents, $requestContents);
                foreach ($destroyLessonContents as $destroyLessonContent => $value) {
                    $lessonContent->where('lesson_id', '=', $edit->id)
                        ->where('content_id', '=', $destroyLessonContent)->delete();
                }
            }


            # Atualizo os dados da aula
            $edit->update($request->except(['contents']));


            # Inicia a transação
            DB::commit();
            # A transação foi feita com sucesso, elimina os conteúdos da seção
            session()->forget('contents');
            return redirect()->route("lesson.edit", $id)->with("success", "Aula <strong>{$request->get('name')}</strong>, atualizado com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->with("error", $e->getMessage());
        }
    }

    public function destroy($id, Lesson $lesson)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {
            # Procuro a aula
            $exist = $lesson->find($id);
            # Verififco se ele existe
            if (!$exist)
                return redirect()->route("lesson.index")->with("error", "Registro não encontrado");
            # Remove a aula
            $exist->delete();
            # Finaliza a transação
            DB::commit();
            return redirect()->route("lesson.index")->with("success", "Aula removida com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->route("lesson.index")->with("error", $e->getMessage());
        }
    }

    public function searchLessons(Request $request, Lesson $lesson)
    {
        $data = $lesson;
        if ($request->has('q')) {
            if ($request->get('q') == '*') {
                $data = $lesson->orderBy('name');
            } else {
                $data = $lesson->where('name', 'like', "%{$request->get('q')}%")
                    ->orWhere('slug', 'like', "%{$request->get('q')}%")
                    ->orWhere('resume', 'like', "%{$request->get('q')}%");
            }
        }

        $results = $data->paginate(20);

        return response()->json(['error' => false, 'data' => $results]);
    }
}
