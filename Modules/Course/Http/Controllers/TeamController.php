<?php

namespace Modules\Course\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Course\Entities\Course;
use Modules\Course\Entities\Team;
use Modules\Course\Entities\TeamType;
use Modules\Course\Http\Requests\TeamRequest;

class TeamController extends Controller
{

    public function index(Team $team)
    {
        # Lista todas as turmas
        $data = $team->with(['Course' => function($q){
            $q->select(['id', 'name']);
        }, 'TeamType' => function($q) {
            $q->select(['id', 'name']);
        }])->orderBy('created_at', 'desc');

        # Se vir o ID do curso, lista apenas referente ao id
        if (request()->filled('curso'))
            $data = $data->whereCourseId(request('curso'));

        # Paginação de 20
        $data = $data->paginate(20);

        return view('course::team.index', compact('data'));
    }

    public function create(TeamType $teamType, Course $course)
    {
        # Cria o select do tipo de turmas [ id => nome ], se não existir retorna []
        $teamTypes = $teamType->whereStatus(1)
            ->orderBy('name')
            ->get()
            ->pluck('name', 'id');

        # Cria o select dos cursos [ id => nome ], se não existir retorna []
        $courses = $course->whereStatus(1)
            ->orderBy('name')
            ->get()
            ->pluck('course_name_expiration_text', 'id');

        return view('course::team.create', compact('teamTypes', 'courses'));
    }

    public function store(TeamRequest $request, Team $team)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {
            # Caso a turma que está sendo criada, seja setada como principal, removo das outras o principal (referente o curso)
            if ($request->get('principal') == 1) {
                $team->where('course_id', '=', $request->get('course_id'))
                    ->wherePrincipal(1)
                    ->update(['principal' => 0]);
            }

            # Apenas cria a turma
            $teamQuery = $team->create($request->all());

            # Inicia a transação
            DB::commit();
            return redirect()->route("team.module.show", $teamQuery->id)->with("success", "Turma <strong>{$request->get('name')}</strong> criada com sucesso. <br> Você foi redirecionado de seção - <strong>Faça o vínculo dos módulos</strong>");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->with("error", "Não foi possível salvar a turma, tente novamente.");
        }
    }

    public function show()
    {
        return view('course::show');
    }

    public function edit($id, Team $team, TeamType $teamType, Course $course)
    {
        try {
            # Procuro pela turma
            $edit = $team->find($id);
            # Se não encontrar, envia para Exception
            if (!$edit)
                throw new \Exception("Registro não encontrado.");

            # Cria o select do tipo de turmas [ id => nome ], se não existir retorna []
            $teamTypes = $teamType->whereStatus(1)
                ->orderBy('name')
                ->get()
                ->pluck('name', 'id');

            # Cria o select dos cursos [ id => nome ], se não existir retorna []
            $courses = $course->whereStatus(1)
                ->orderBy('name')
                ->get()
                ->pluck('course_name_expiration_text', 'id');

        } catch (\Exception $e) {
            # Retorna para a listagem de cursos, avisando o erro
            return redirect()->route("team.index")->with("error", $e->getMessage());
        }
        return view('course::team.edit', compact('edit', 'teamTypes', 'courses'));
    }

    public function update($id, TeamRequest $request, Team $team)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {
            # Procuro pela Turma
            $edit = $team->find($id);
            # Se não encontrar, retorna para a tela de listagem de turmas
            if (!$edit)
                return redirect()->route("team.index")->with("error", "Registro não encontrado");

            # Caso a turma que está sendo criada, seja setada como principal, removo das outras o principal (referente o curso)
            if ($request->get('principal') == 1) {
                $team->where('course_id', '=', $request->get('course_id'))
                    ->wherePrincipal(1)
                    ->update(['principal' => 0]);
            }

            # Atualiza os dados da turma
            $edit->update($request->all());
            # Inicia a transação
            DB::commit();
            return redirect()->back()->with("success", "Turma <strong>{$edit->name}</strong> atualizada com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->with("error", $e->getMessage());
        }
    }

    public function destroy($id, Team $team)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {
            # Consulto pelo curso
            $exist = $team->find($id);
            # Se não encontrar, retorna para a tela de listagem de turmas
            if (!$exist)
                return redirect()->route("team.index")->with("error", "Registro não encontrado");
            # Deleta a turma
            $exist->delete();
            # Inicia a transação
            DB::commit();
            return redirect()->route("team.index")->with("success", "Turma removida com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->route("team.index")->with("error", $e->getMessage());
        }
    }
}
