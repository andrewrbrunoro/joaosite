<?php

namespace Modules\Course\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Course\Entities\Lesson;
use Modules\Course\Entities\Module;
use Modules\Course\Entities\ModuleLesson;
use Modules\Course\Entities\Team;
use Modules\Course\Entities\TeamModule;
use Modules\Course\Http\Requests\ImportModulesRequest;
use Modules\Course\Http\Requests\TeamModuleRequest;

class TeamModuleController extends Controller
{

    public function show($id, Team $team, Lesson $lesson, Module $module)
    {
        try {
            # Procuro pela turma e seus módulos (caso já tenha cadastrado)
            $show = $team->with(['TeamModules' => function ($q) {
                $q->with('ModuleLessons');
            }])->find($id);

            # Se não encontrar a turma, envia pra exceção com erro
            if (!$show)
                throw new \Exception("Registro não encontrado");

            # Lista 10 aulas para iniciar ao adicionar ao módulo
            $lessons = $lesson->orderBy('name')
                ->limit(10)
                ->get(['id', 'name', 'price', 'sale', 'status']);

            # Verica se existi módulos cadastrados
            $countModules = $module;
            if ($show->TeamModules->count())
                $countModules = $countModules->whereNotIn('id', $show->TeamModules->pluck('id'));

            $countModules = $countModules->count();

        } catch (\Exception $e) {
            # Retorna para a página de listagem de turmas
            return redirect()->route("team.index")->with("error", $e->getMessage());
        }

        return view('course::team.modules.show', compact('show', 'lessons', 'countModules'));
    }

    public function store($id, TeamModuleRequest $request, Team $team, TeamModule $teamModule, Module $module, ModuleLesson $moduleLesson)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {
            # Verifico se a turma que o mesmo está requisitando, existe
            $edit = $team->find($id);

            # Se não existir, envia pra exception
            if (!$edit)
                throw new \Exception("Registro não encontrado");

            # Salva o Módulo
            $moduleQuery = $module->create($request->except(['lessons']));

            # Relaciona o Módulo a turma
            $teamModule->create([
                'team_id' => $edit->id,
                'module_id' => $moduleQuery->id,
                'price' => $request->get('price'),
                'sale' => $request->get('sale'),
                'release_date' => $request->get('release_date'),
                'expiration_date' => $request->get('expiration_date')
            ]);

            # Se vir as aulas, vincula elas ao módulo que está sendo criado
            if ($request->has('lessons')) {
                $i = 0;
                foreach ($request->get('lessons') as $lesson) {
                    # Alteração, está vindo o "id" da aula, apenas, alterado para "lesson_id"
                    $lesson['lesson_id'] = $lesson['id'];
                    # Removo o ID
                    unset($lesson['id']);
                    # Alteração, está vindo o "id" da aula, apenas, alterado para "lesson_id"

                    # Salva o vínculo
                    $moduleLesson->create($lesson + ['module_id' => $moduleQuery->id, 'order' => $i]);

                    $i++;
                }
            }

            # Conclui a transação
            DB::commit();
            return response()->json(["error" => false, "message" => "Módulo salvo com sucesso e já vinculado a Turma <strong>{$edit->name}</strong>."]);
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return response()->json(["error" => true, "message" => $e->getMessage()], 400);
        }
    }

    public function update($id, Request $request, Team $team, Module $module, ModuleLesson $moduleLesson, TeamModule $teamModule)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Procuro pela relação de turma e módulo
            $teamModuleQuery = $teamModule->where([
                'team_id' => $id,
                'module_id' => $request->get('id')
            ])->first();

            # Se não existir, informo o usuário
            if (!$teamModuleQuery)
                throw new \Exception("Não foi possível editar o módulo, entre em contato conosco e informe como ocorreu este erro.");

            # Procuro o módulo
            $updateQuery = $module->find($teamModuleQuery->module_id);
            # Atualizo o módulo vinculado a turma
            $updateQuery->update($request->only(['name', 'release_date', 'expiration_date', 'price', 'sale']));

            $teamModuleQuery->update($request->except(['lessons']));

            if ($request->has('lessons')) {
                # Procuro as aulas do módulo e transformo em Array
                $items = $moduleLesson->where('module_id', '=', $teamModuleQuery->module_id)
                    ->pluck('id', 'lesson_id')->toArray();

                $getRequests = $request->get('lessons');
                $requestItems = [];
                foreach ($getRequests as $getRequest) {
                    $requestItems[$getRequest['id']] = $getRequest;
                }

                # Atualiza as informações da relação
                $updateItems = array_exist_keys($requestItems, $items);
                foreach ($updateItems as $updateItem) {

                    # Facilito e adiciono a aula atual em uma variável referente a index
                    $row = $requestItems[$updateItem];

                    # Procuro a aula referente ao módulo
                    $update = $moduleLesson->where([
                        'module_id' => $teamModuleQuery->module_id,
                        'lesson_id' => $updateItem
                    ])->first();

                    # Se encontrar
                    if ($update) {
                        # Pego a posição do conteúdo e adiciono na ordem
                        $order = array_search($row['id'], array_keys($requestItems));

                        # Atualizo
                        $update->update($row);
                    }
                }

                # Nova relação
                $newItems = array_not_exist_keys($requestItems, $items);
                foreach ($newItems as $newItem) {

                    # Facilito e adiciono o conteúdo atual em uma variável referente a index
                    $row = $requestItems[$newItem];

                    # Pego a posição do conteúdo e adiciono na ordem
                    $order = array_search($newItem, array_keys($requestItems));

                    # Relaciono o novo conteúdo
                    $moduleLesson->create([
                        'module_id' => $teamModuleQuery->module_id,
                        'lesson_id' => $newItem,
                        'release_date' => isset($row['release_date']) ? $row['release_date'] : null,
                        'expiration_date' => isset($row['expiration_date']) ? $row['expiration_date'] : null,
                    ]);
                }

                # Remove
                $destroyItems = array_diff_key($items, $requestItems);
                foreach ($destroyItems as $destroyItem => $value) {
                    # Procuro e deleto
                    $moduleLesson->where([
                        'module_id' => $teamModuleQuery->module_id,
                        'lesson_id' => $destroyItem
                    ])->delete();
                }
            }


            # Conclui a transação
            DB::commit();
            return response()->json(["error" => false, "message" => "Módulo {$request->get('name')} atualizado com sucesso"]);
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return \response()->json(["error" => true, "message" => $e->getMessage()], 400);
        }
    }

    public function destroy($teamModuleId, Request $request, TeamModule $teamModule)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {
            # Verifico se está recebendo o ID da turma, para garantir a integridade de remoção do dado
            if (!$request->has('team_id') || $request->get('team_id') == "")
                throw new \Exception("Registro não encontrado");

            $teamModuleQuery = $teamModule->where('team_id', '=', $request->get('team_id'))
                ->find($teamModuleId);

            # Verifico se existe o módulo relacionado a turma
            if (!$teamModuleQuery)
                throw new \Exception("Registro não encontrado");

            # Removo
            $teamModuleQuery->delete();

            # Inicia a transação
            DB::commit();
            return redirect()->route("team.module.show", $request->get('team_id'))->with("success", "Módulo, removido com sucesso");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->with("error", $e->getMessage());
        }
    }

    public function reOrder($id, Request $request, TeamModule $teamModule)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {
            # Verifico se passou os módulos para ordernar
            if ($request->has('modules')) {
                foreach ($request->get('modules') as $order => $module) {
                    # Procuro o módulo referente a turma e o id do módulo e atualizo
                    $teamModule->find($module['id'])->update(['order' => $order]);
                }
            } else {
                throw new \Exception("Nenhum modulo fornecido.");
            }
            # Conclui a transação
            DB::commit();
            return response()->json(["error" => false, "message" => "Os módulos foram reordenados com sucesso."], 200);
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return response()->json(["error" => true, "message" => $e->getMessage()], 400);
        }
    }

    public function reOrderLessons($id, Request $request, ModuleLesson $moduleLesson)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Verifico se as aulas estão sendo passada na requisição
            if ($request->has('lessons')) {
                foreach ($request->get('lessons') as $order => $lesson) {
                    # Procuro a aula referente ao módulo e atualizo
                    $moduleLesson->where([
                        "module_id" => $id,
                        "lesson_id" => $lesson
                    ])->update(["order" => $order]);
                }
            } else {
                throw new \Exception("Nenhuma aula encontrada na requisição.");
            }
            # Conclui a transação
            DB::commit();
            return response()->json(["error" => false, "message" => "As aulas foram reordenadas com sucesso."]);
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return response()->json(["error" => true, "message" => $e->getMessage()], 400);
        }
    }

    /**
     * @param $id
     * @param ImportModulesRequest $request
     * @param Team $team
     * @param TeamModule $teamModule
     * @param Module $module
     * @param Lesson $lesson
     * @param ModuleLesson $moduleLesson
     * @return \Illuminate\Http\RedirectResponse
     *
     * Caso haja manutenção nessa função, leia com atenção cada passo
     */
    public function importModules($id, ImportModulesRequest $request, Team $team, TeamModule $teamModule, Module $module, Lesson $lesson, ModuleLesson $moduleLesson)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Verifico se a turma que o mesmo está requisitando, existe
            $edit = $team->find($id);

            # Se não existir, envia pra exception
            if (!$edit)
                return redirect()->route("team.index")->with("error", "Registro não encontrado.");

            # O array que está vindo é dividido inicialmente por módulos e dentro deste array existe os id das aulas
            # Exemplo
            # [module_id:value] => array(
            #   [lesson_id] => value
            #   [lesson_id] => value
            # )
            foreach ($request->get('modules') as $module_id) {

                # Procuro o módulo que vai ser clonado
                $cloneModule = $module->find($module_id, ["name"]);

                # Se ele existir
                if ($cloneModule) {

                    # Cria o módulo (com o mesmo nome)
                    $cloneModuleQuery = $module->create($cloneModule->toArray());

                    # Relaciona o Módulo a turma
                    $teamModule->create([
                        'team_id' => $edit->id,
                        'module_id' => $cloneModuleQuery->id,
                        'price' => "0,00",
                        'sale' => "0,00",
                        'release_date' => null,
                        'expiration_date' => null
                    ]);

                    # Se vir as aulas
                    if ($request->has("lessons")) {
                        # Coloco em um variável para poder acesasr as aulas referente ao módulo que está lendo
                        $lessons = $request->get("lessons");

                        # Se existir as aulas referente ao módulo
                        if (isset($lessons[$module_id])) {
                            $i = 0;
                            # Pego todas a aulas referente ao módulo e vínculo ao módulo clonado
                            foreach ($lessons[$module_id] as $lesson_id) {
                                $moduleLesson->create([
                                    "module_id" => $cloneModuleQuery->id,
                                    "lesson_id" => $lesson_id,
                                    "order" => $i,
                                    "release_date" => null,
                                    "expiration_date" => null
                                ]);
                                $i++;
                            }
                        }
                    }

                }
            }

            # Conclui a transação
            DB::commit();
            return redirect()->route("team.module.show", $id)->with("success", "Importação dos módulos efetuada com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->route("team.module.show", $id)->with("error", $e->getMessage());
        }
    }
}
