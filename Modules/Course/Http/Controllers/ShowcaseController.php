<?php

namespace Modules\Course\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Course\Entities\Course;
use Modules\Course\Entities\CourseShowcase;
use Modules\Course\Entities\CourseShowcaseTeam;
use Modules\Course\Http\Requests\CourseShowcaseRequest;

class ShowcaseController extends Controller
{

    public function index(CourseShowcase $courseShowcase)
    {
        # Paginação das vitrines
        $data = $courseShowcase->paginate(20);

        return view('course::showcase.index', compact('data'));
    }

    public function create(Course $c)
    {
        # Listagem dos Cursos
        $courses = $c->whereStatus(1)
            ->orderBy('name')
            ->get(['id', 'name']);

        return view('course::showcase.create', compact('courses'));
    }

    public function store(CourseShowcaseRequest $request, CourseShowcase $courseShowcase, CourseShowcaseTeam $courseShowcaseTeam)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Cria a vitrine
            $courseShowcaseQuery = $courseShowcase->create($request->except(['teams']));
            # Cursos
            foreach ($request->get('courses') as $course) {
                # Converte a turma que está vindo em string para json array
                $course = json_decode($course, true);
                # Vincula o curso a vitrine
                $courseShowcaseTeam->create([
                    'course_showcase_id' => $courseShowcaseQuery->id,
                    'course_id' => $course['id']
                ]);
            }

            # Conclui a transação
            DB::commit();
            return redirect()->route("course.showcase.index")->with("success", "Vitrine criada com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->with("error", $e->getMessage());
        }
    }

    public function edit($id, CourseShowcase $cs, Course $c)
    {
        try {

            # Verifico se a vitrine existe
            $edit = $cs->with(['CourseShowcaseTeam' => function ($q) {
                return $q->with(['Course' => function ($q) {
                    return $q->select(['id', 'name']);
                }])->select(['course_showcase_id', 'course_id']);
            }])->find($id);

            # Se não existir
            if (!$edit)
                throw new \Exception("Registro não encontrado.");

            # Caso exista os cursos na vitrine
            $flash_courses = collect([]);
            foreach ($edit->CourseShowcaseTeam as $course_showcase_team) {
                $flash_courses->push($course_showcase_team->Course);
            }
            # Adiciona a sessão no flash
            session()->flash('courses', $flash_courses);

            # Listagem dos Cursos
            $courses = $c->whereStatus(1)
                ->orderBy('name')
                ->get(['id', 'name']);
        } catch (\Exception $e) {
            return redirect()->route("course.showcase.index")->with("error", $e->getMessage());
        }
        return view('course::showcase.edit', compact('edit', 'courses'));
    }

    public function update($id, CourseShowcaseRequest $request, CourseShowcase $cs, CourseShowcaseTeam $cst)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Verifico se existe a vitrine
            $edit = $cs->find($id);
            if (!$edit)
                return redirect()->route("course.showcase.index")->with("error", "Registro não encontrado.");

            # Pega todos os ids dos cursos da relação
            $relations = $edit->CourseShowcaseTeam->pluck('course_id')->toArray();

            # Cursos
            $delete = $relations;
            if ($request->has('courses')) {
                foreach ($request->get('courses') as $course) {
                    # Converte a turma que está vindo em string para json array
                    $course = json_decode($course, true);

                    # Procuro pela relação
                    $exist = $cst->where([
                        'course_showcase_id' => $edit->id,
                        'course_id' => $course['id']
                    ])->first();

                    $delete = array_diff($relations, [$course['id']]);

                    # Se não existir, cria a relação
                    if (!$exist) {
                        # Vincula o curso a vitrine
                        $cst->create([
                            'course_showcase_id' => $edit->id,
                            'course_id' => $course['id']
                        ]);
                    } else {
                        # Atualizo o vínculo - No caso atual está só "atualizando o que já existe", mas, vai receber futuramente a ordenação
                        # $exist->update([
                        #    'course_showcase_id' => $edit->id,
                        #    'course_id' => $course['id']
                        # ]);
                    }
                }
            } else {
                $cst->whereIn('course_id', $delete)->where('course_showcase_id', '=', $edit->id)->delete();
            }



            # Conclui a transação
            DB::commit();
            return redirect()->route("course.showcase.edit", $edit->id)->with("success", "Vitrine atualizada com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->with("error", $e->getMessage());
        }
    }

    public function destroy($id, CourseShowcaseTeam $cst)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Procuro pela vitrine
            $delete = $cst->find($id);
            # Se não existir, envia pra exceção
            if (!$delete)
                throw new \Exception("Regsitro não encontrado.");

            # Remove a vitrine
            $delete->delete();

            # Conclui a transação
            DB::commit();
            return redirect()->route("course.showcase.index")->with("success", "Vitrine deletada com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->route("course.showcase.index")->with("error", $e->getMessage());
        }
    }
}
