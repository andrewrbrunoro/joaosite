<?php

namespace Modules\Course\Http\Controllers;

use App\Helpers\Redactor;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Course\Entities\Content;
use Modules\Course\Entities\ContentType;
use Modules\Course\Http\Requests\ContentRequest;

class ContentController extends Controller
{

    public function index(Content $content)
    {
        # Paginação dos conteúdos
        $data = $content->orderBy('created_at', 'desc')
            ->paginate(20);

        return view('course::content.index', compact('data'));
    }

    public function create(ContentType $contentType)
    {
        # Listo os tipos de conteúdo
        $contentTypes = $contentType->whereStatus(1)
            ->select(['id', 'name', 'slug', 'element_type'])
            ->get();

        return view('course::content.create', compact('contentTypes'));
    }

    public function store(ContentRequest $request, ContentType $contentType, Content $content, Redactor $redactor)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Procuro pelo tipo de conteúdo, caso não exista, retorna para o formulário com os inputs e informando o erro
            $type = $contentType->find($request->get('content_type_id'));
            if (!$type)
                return redirect()->back()->withInput($request->all())->with("error", "O Tipo de conteúdo não foi encontrado, tente inserir novamente.");

            # Verifica se o tipo de conteúdo necessita de uma validação
            if ($type->validate != '') {
                # Valida o conteúdo de acordo com o tipo do conteúdo
                $validate = \Validator::make($request->all(), [
                    'content' => $type->validate
                ])->setAttributeNames(['content' => 'Conteúdo']);

                # Se falhar, volta pro formulário com os dados enviados e com mensagem de erro
                if ($validate->fails())
                    return redirect()->back()->withInput($request->all())->withErrors($validate);
            }

            # Salvo o conteúdo
            $contentQuery = $content->create($request->all());

            # Se o conteúdo que ta vindo for um arquivo, faz a inserção
            if ($request->hasFile('content')) {
                $redactor->setDir($content->path_image . $contentQuery->id, false)
                    ->requestFile('content');

                # Atualiza o conteúdo adicionando o nome do arquivo
                $contentQuery->update(['content' => $content->path_image . $contentQuery->id . "/" . $redactor->getFileName()]);
            }

            # Inicia a transação
            DB::commit();
            return redirect()->route("content.create")->with("success", "Conteúdo <strong>{$request->get('name')}</strong> foi criado com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->route("content.index")->with("error", $e->getMessage());
        }
    }

    public function show()
    {
        return view('course::content.show');
    }

    public function edit($id, Content $content, ContentType $contentType)
    {
        try {
            # Procuro pelo conteúdo
            $edit = $content->find($id);
            # Caso não encontre, envia pra exception
            if (!$edit)
                throw new \Exception("Registro não encontrado.");

            # Listo os tipos de conteúdo
            $contentTypes = $contentType->whereStatus(1)
                ->select(['id', 'name', 'slug', 'element_type'])
                ->get();

        } catch (\Exception $e) {
            return redirect()->route("content.index")->with("error", "");
        }
        return view('course::content.edit', compact('contentTypes', 'edit'));
    }

    public function update($id, ContentRequest $request, Content $content, ContentType $contentType, Redactor $redactor)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            # Procuro pelo tipo de conteúdo, caso não exista, retorna para o formulário com os inputs e informando o erro
            $type = $contentType->find($request->get('content_type_id'));
            if (!$type)
                return redirect()->back()->withInput($request->all())->with("error", "O Tipo de conteúdo não foi encontrado, tente inserir novamente.");

            # Procuro pelo conteúdo
            $edit = $content->find($id);
            # Caso não encontre, envia pra exception
            if (!$edit)
                return redirect()->route("content.index")->with("error", "Registro não encontrado");

            # Verifica se o tipo de conteúdo necessita de uma validação
            if ($type->validate != '') {

                if ($request->file('content') != '') {
                    # Valida o conteúdo de acordo com o tipo do conteúdo
                    $validate = \Validator::make($request->all(), [
                        'content' => $type->validate
                    ])->setAttributeNames(['content' => 'Conteúdo']);

                    # Se falhar, volta pro formulário com os dados enviados e com mensagem de erro
                    if ($validate->fails())
                        return redirect()->back()->withInput($request->all())->withErrors($validate);
                }
            }

            # Salvo o conteúdo
            $edit->update($request->all());

            # Se o conteúdo que ta vindo for um arquivo, faz a inserção
            if ($request->hasFile('content')) {
                $redactor->setDir($content->path_image . $edit->id, false)
                    ->requestFile('content');

                # Atualiza o conteúdo adicionando o nome do arquivo
                $edit->update(['content' => $content->path_image . $edit->id . "/" . $redactor->getFileName()]);
            }

            # Inicia a transação
            DB::commit();
            return redirect()->back()->with("success", "Conteúdo <strong>{$request->get('name')}</strong> atualizado com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->route("content.index")->with("error", $e->getMessage());
        }
    }

    public function destroy($id, Content $content)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {
            # Consulto pelo conteúdo
            $exist = $content->find($id);
            # Verififco se ele existe
            if (!$exist)
                return redirect()->route("content.index")->with("error", "Registro não encontrado");
            # Remove o conteúdo
            $exist->delete();
            # Inicia a transação
            DB::commit();
            return redirect()->route("content.index")->with("success", "Conteúdo removido com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->route("content.index")->with("error", $e->getMessage());
        }
    }

    public function searchContents(Request $request, Content $content)
    {
        $data = $content;
        if ($request->has('q')) {
            if ($request->get('q') == '*') {
                $data = $content->orderBy('name');
            } else {
                $data = $content->where('name', 'like', "%{$request->get('q')}%")
                    ->orWhere('slug', 'like', "%{$request->get('q')}%")
                    ->orWhere('content', 'like', "%{$request->get('q')}%");
            }
        }

        $results = $data->paginate(20);

        return response()->json(['error' => false, 'data' => $results]);
    }
}
