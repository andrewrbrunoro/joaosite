<?php

namespace Modules\Course\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Course\Entities\Course;
use Modules\Course\Http\Requests\CourseRequest;

class CourseController extends Controller
{

    public function index(Course $course)
    {
        # Paginação dos cursos
        $data = $course->orderBy('created_at', 'desc')
            ->paginate(20);

        return view('course::index', compact('data'));
    }

    public function create()
    {
        return view('course::create');
    }

    public function store(CourseRequest $request, Course $course)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {
            # Cria o curso
            $course->create($request->all());
            # Inicia a transação
            DB::commit();
            return redirect()->route("team.index")
                ->with("success", "Curso <strong>{$request->get('name')}</strong>, adicionado com sucesso. <br />
                                   Você está agora em <strong>Turmas</strong>, clique para <a href='".route("team.create")."'>Cadastrar Turma</a> para vincular ao Novo Curso");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->with("error", $e->getMessage());
        }
    }

    public function show()
    {
        return view('course::show');
    }

    public function edit($id, Course $course)
    {
        try {
            # Procuro pelo Curso
            $edit = $course->find($id);
            # Se não encontrar, evia pra exception
            if (!$edit)
                throw new \Exception("Registro não encontrado.");
        } catch (\Exception $e) {
            # Retorna para a listagem dos cursos
            return redirect()->route("course.index")->with("error", $e->getMessage());
        }
        return view('course::edit', compact('edit'));
    }

    public function update($id, CourseRequest $request, Course $course)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {
            # Procuro pelo Curso
            $edit = $course->find($id);
            # Se não encontrar, retorna pra tela de listagem
            if (!$edit)
                return redirect()->route("course.index")->with("error", "Registro não encontrado");

            # Atualiza os dados do curso
            $edit->update($request->all());
            # Inicia a transação
            DB::commit();
            return redirect()->back()->with("success", "Curso atualizado com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->back()->with("error", $e->getMessage());
        }
    }

    public function destroy($id, Course $course)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {
            # Consulto pelo curso
            $exist = $course->find($id);
            # Verififco se ele existe
            if (!$exist)
                return redirect()->route("course.index")->with("error", "Registro não encontrado");
            # Remove o curso
            $exist->delete();
            # Inicia a transação
            DB::commit();
            return redirect()->route("course.index")->with("success", "Curso removido com sucesso.");
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return redirect()->route("course.index")->with("error", $e->getMessage());
        }
    }
}
