@extends('dashboard::layouts.master')

@section('title', 'Cursos')

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Cursos</h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-12">
                        <a href="{!! route('course.create') !!}" class="btn btn-outline btn-success">
                            <i class="fa fa-plus"></i> Cadastrar Curso
                        </a>
                        <a href="{!! request()->fullUrl() !!}" class="btn btn-outline btn-default">
                            <i class="fa fa-repeat"></i> Atualizar
                        </a>
                    </div>
                </div>

                <hr>

                <div class="row">

                    <div class="col-lg-12">
                        <table class="table table-responsive table-bordered table-stripped">
                            <thead>
                            <tr>
                                <td>#</td>
                                <td>Nome</td>
                                <td>Link</td>
                                <td>Dias Disponíveis</td>
                                <td>Status</td>
                                <td>Opções</td>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($data->count())
                                @foreach($data as $row)
                                    <tr>
                                        <td>{!! $row->id !!}</td>
                                        <td>{!! $row->name !!}</td>
                                        <td>{!! url($row->slug) !!}</td>
                                        <td>{!! $row->days_available !!}</td>
                                        <td>{!! $row->statusLabel !!}</td>
                                        <td>
                                            {!! Form::open(['route' => ['course.destroy', $row->id], 'method' => 'DELETE', 'onsubmit' => 'beforeConfirm(this, event)']) !!}
                                            <a href="{!! route('team.index', ['curso' => $row->id]) !!}" class="btn btn-outline btn-xs btn-success">
                                                <i class="fas fa-align-justify"></i> Turmas
                                            </a>
                                            <a href="{!! route('course.edit', $row->id) !!}" class="btn btn-outline btn-info btn-xs">
                                                <i class="fas fa-pencil-alt"></i> Editar
                                            </a>
                                            <button class="btn btn-outline btn-danger btn-xs" type="submit">
                                                <i class="fa fa-trash"></i> Deletar
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8">Nenhum curso no momento, <a href="{!! route('course.create') !!}">clique aqui</a> adicione um curso</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                        @if (isset($data))
                            {!! $data->links() !!}
                        @endif
                    </div>

                </div>

            </div>
        </div>
    </div>
@stop

