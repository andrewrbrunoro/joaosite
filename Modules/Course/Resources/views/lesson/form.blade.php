<nestable inline-template :contents-prop='{!! session()->has('contents') ? session('contents') : json_encode([]) !!}'>
    <div class="row">

        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="clear">
                            Status <label class="badge badge-danger">Obrigatório</label>
                        </label>
                        <label class="radio-inline">
                            {!! Form::radio('status', 1, true) !!} Ativo
                        </label>
                        <label class="radio-inline">
                            {!! Form::radio('status', 0, false) !!} Inativo
                        </label>
                    </div>
                </div>
            </div>
            <hr>
        </div>

        <div class="col-lg-12 col-xs-12">

            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                        <label for="name">
                            Nome <label class="badge badge-danger">Obrigatório</label>
                        </label>
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                        {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group{!! $errors->first('discipline_id') ? ' has-error' : '' !!}">
                        <label for="discipline_id">
                            Disciplina <label class="badge badge-info">Opcional</label>
                        </label>
                        {!! Form::select('discipline_id', $disciplines, null, ['class' => 'form-control', 'placeholder' => 'Disciplina']) !!}
                        {!! $errors->first('discipline_id', '<span class="text-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="form-group{!! $errors->first('resume') ? ' has-error' : '' !!}">
                        <label for="resume">
                            Resumo/Legenda/Explicação (Máximo 255 caracteres) <label class="badge badge-info">Opcional</label>
                        </label>
                        {!! Form::textarea('resume', null, ['rows' => 2, 'class' => 'form-control no-resize', 'maxlength' => 255, 'placeholder' => 'Resumo/Legenda/Explicação']) !!}
                        {!! $errors->first('resume', '<span class="text-danger">:message</span>') !!}
                    </div>

                </div>
            </div>
        </div>

        <div class="col-lg-12 col-xs-12">
            <h3 class="forum-title bg-primary">
                Gerenciar conteúdos
            </h3>
        </div>

        <div class="col-lg-4 fd-border-right">
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label>
                            Procurar conteúdo <label class="badge badge-success">Clique nas setas para adicionar</label>
                        </label>
                        <input type="text" class="form-control" placeholder="Procure o conteúdo" v-model="search_query" v-on:keyup="getContents"/>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="fd-overflow-lesson">
                        <ul class="list-group" v-if="search_query == ''">
                            @foreach($contents as $content)
                                <li class="list-group-item" v-if="not_read_ids.indexOf({!! $content->id !!}) < 0">
                                    <a href="javascript:void(0);">
                                        {!! $content->name !!}
                                    </a>
                                    <button class="btn btn-xs btn-info pull-right fd-space-pull-right" v-on:click='setContent({!! $content !!})' type="button" title="Adicionar a aula">
                                        <i class="fa fa-arrow-right"></i>
                                    </button>
                                </li>
                            @endforeach
                        </ul>
                        <ul class="list-group" v-if="search_query != ''">
                            <li class="list-group-item" v-if="not_read_ids.indexOf(result.id) < 0" v-for="result in search_result">
                                <a href="javascript:void(0);">
                                    @{{ result.name }}
                                </a>
                                <button class="btn btn-xs btn-info pull-right fd-space-pull-right" v-on:click='setContent(result)' type="button" title="Adicionar a aula">
                                    <i class="fa fa-arrow-right"></i>
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-8">

            <div class="row">
                <div class="col-lg-12" v-show="contents.length">
                    <label>
                        Conteúdos nesta aula <label class="badge badge-success">Segure os itens para ordernar</label>
                    </label>
                    <div class="dd table-responsive">
                        <table class="table table-bordered table-stripped">
                            <thead>
                            <tr>
                                <td width="2%">#</td>
                                <td>Nome</td>
                                {{--<td width="18%">Preço</td>--}}
                                {{--<td width="18%">Promoção</td>--}}
                                <td width="18%" title="Data de lançamento">Dt. Lançamento</td>
                                <td width="18%" title="Data de expiração">Dt. Expiração</td>
                                <td width="2%">#</td>
                            </tr>
                            </thead>
                            <tbody class="dd-list">
                            <tr class="dd-item" v-bind:data-id="content.id" v-for="(content, index) in contents">
                                <td class="arb-drag bg-primary" title="Segure para ordernar"></td>
                                <td>
                                    <input type="hidden" :name="'contents['+content.id+'][content_id]'" :value="content.id"/>
                                    <input type="hidden" :name="'contents['+content.id+'][name]'" :value="content.name"/>
                                    @{{ content.name }}
                                </td>
                                <?php /*
                                <td>
                                    <input :name="'contents['+content.id+'][price]'" type="text" class="form-control"
                                           v-on:focusout="updateContentValue(index, 'price')"
                                           :value="content.price"
                                           v-money="{decimal: ',', thousands: '.', precision: 2, masked: false}"/>
                                </td>
                                <td>
                                    <input :name="'contents['+content.id+'][sale]'" type="text" class="form-control"
                                           v-on:focusout="updateContentValue(index, 'sale')"
                                           :value="content.sale"
                                           v-money="{decimal: ',', thousands: '.', precision: 2, masked: false}"/>
                                </td>
                                */ ?>
                                <td>
                                    <input :name="'contents['+content.id+'][release_date]'" placeholder="dia/mês/ano"
                                           v-on:focusout="updateContentValue(index, 'release_date')"
                                           :value="content.release_date"
                                           type="text" class="form-control date-picker"/>
                                </td>
                                <td>
                                    <input :name="'contents['+content.id+'][expiration_date]'" placeholder="dia/mês/ano"
                                           v-on:focusout="updateContentValue(index, 'expiration_date')"
                                           :value="content.expiration_date"
                                           type="text" class="form-control date-picker"/>
                                </td>
                                <td>
                                    <button class="btn-xs btn btn-danger" style="margin-top: 18%" type="button"
                                            title="Remover o conteúdo desta aula" v-on:click="destroyContent(content.id)">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-lg-12">
            <hr>
        </div>

        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <button type="submit" class="btn btn-primary pull-right">
                        <i class="fa fa-check"></i> Salvar a Aula
                    </button>
                </div>
            </div>
        </div>
    </div>
</nestable>
@section('script')
    {!! Html::script('assets/vendors/nestable/nestable.min.js') !!}
    {!! Html::script(Module::asset('course:js/components/nestable.js')) !!}
@endsection