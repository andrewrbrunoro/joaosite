@extends('dashboard::layouts.master')

@section('title', 'Suporte a erros reportados')

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Suporte a erros reportados</h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-12">
                        <a href="{!! route('lesson.index') !!}" class="btn btn-outline btn-warning">
                            <i class="fa fa-arrow-left"></i> Listar todos as aulas
                        </a>
                        <a href="{!! request()->fullUrl() !!}" class="btn btn-outline btn-default">
                            <i class="fa fa-repeat"></i> Atualizar
                        </a>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-lg-12 col-xs-12 table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Erro</th>
                                    <th>Aula</th>
                                    <th>Usuário</th>
                                    <th>Turma</th>
                                    <th>Ajustado</th>
                                    <th>Opções</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($data) && $data->count())
                                    @foreach($data as $row)
                                        <tr>
                                            <td>{!! $row->id !!}</td>
                                            <td>
                                                {!! str_limit(strip_tags($row->text)) !!}
                                            </td>
                                            <td>
                                                {!! $row->Lesson->name !!}
                                            </td>
                                            <td>
                                                {!! $row->User->name !!}
                                            </td>
                                            <td>
                                                {!! $row->Team->name !!}
                                            </td>
                                            <td>
                                                {!! $row->fixed_label !!}
                                            </td>
                                            <td>
                                                <a href="{!! route('support.lesson.error.edit', [$row->lesson_id, $row->id]) !!}" class="btn btn-outline btn-xs btn-success">
                                                    <i class="fas fa-pencil-alt"></i> Editar ou Responder
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5">Nenhum erro reportado no momento.</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
        
                        @if (isset($data))
                            {!! $data->links() !!}
                        @endif
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@stop
