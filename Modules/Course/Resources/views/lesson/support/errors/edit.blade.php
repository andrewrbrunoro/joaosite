@extends('dashboard::layouts.master')

@section('title', 'Editar ou Responder - Erro Reportado')

@section('content')
    <div class="row">
        {!! Form::model($edit, ['route' => ['support.lesson.error.update', $edit->lesson_id, $edit->id], 'method' => 'PATCH']) !!}
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Editar  - <strong></strong></h5>
                </div>

                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="{!! route('support.lesson.error.index', $edit->lesson_id) !!}" class="btn btn-outline btn-warning">
                                <i class="fa fa-arrow-left"></i> Listar todos os erros
                            </a>
                        </div>
                    </div>
                    <hr>
                    @include('course::lesson.support.errors.form')
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@stop
