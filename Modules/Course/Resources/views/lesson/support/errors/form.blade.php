<div class="row">

    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="clear">
                        Ajustado <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('fixed', 1, true) !!} Ativo
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('fixed', 0, false) !!} Inativo
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-xs-12">

        <div class="row">
            <div class="col-lg-3 col-xs-3">
                <div class="clear">
                    <label class="control-label">
                        Usuário
                    </label>
                    <br>
                    {!! $edit->User->name !!}
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="form-group">
                    <label class="clear">
                        Turma
                    </label>
                    <br>
                    {!! $edit->Team->name !!}
                </div>
            </div>
            <div class="col-lg-6 col-xs-6">
                <div class="clear">
                    <label class="control-label">
                        Aula
                    </label>
                    <br>
                    <a href="{!! $edit->url !!}">{!! $edit->url !!}</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="clear">
                    <label>
                        Descrição do erro
                    </label>
                    <br>
                    {!! $edit->text !!}
                </div>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group{!! $errors->first('answer') ? ' has-error' : '' !!}">
                    <label for="answer">
                        Notificar o usuário (max: <strong class="text-danger">300</strong> caracteres) <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::textarea('answer', null, ['class' => 'form-control no-resize', 'placeholder' => 'Exemplo:. O erro foi ajustado']) !!}
                    {!! $errors->first('answer', '<span class="text-danger">:message</span>') !!}
                </div>
                
            </div>
        </div>

        <hr>
    </div>

    <div class="col-lg-6 col-sm-6">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check"></i> Salvar os Dados
        </button>
    </div>
</div>