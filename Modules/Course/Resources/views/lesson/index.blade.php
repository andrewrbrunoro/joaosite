@extends('dashboard::layouts.master')

@section('title', 'Aulas')

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Aulas</h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-12">
                        <a href="{!! route('lesson.create') !!}" class="btn btn-outline btn-success">
                            <i class="fa fa-plus"></i> Cadastrar Aula
                        </a>
                        <a href="{!! request()->fullUrl() !!}" class="btn btn-outline btn-default">
                            <i class="fa fa-repeat"></i> Atualizar
                        </a>
                    </div>
                </div>

                <hr>

                <div class="row">

                    <div class="col-lg-12">
                        <table class="table table-responsive table-bordered table-stripped">
                            <thead>
                            <tr>
                                <th width="3%">#</th>
                                <th>Nome</th>
                                <th>Pontuação</th>
                                <th width="6%">Dúvidas</th>
                                <th width="6%">Acessos</th>
                                <th width="8%">Status</th>
                                <th width="25%">Opções</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($data->count())
                                @foreach($data as $row)
                                    <tr>
                                        <td>{!! $row->id !!}</td>
                                        <td>{!! $row->name !!}</td>
                                        <td>{!! $row->rating !!}</td>
                                        <td>
                                            <label class="badge badge-info">
                                                {!! $row->LessonQuestion->where('status', '=', 0)->count() !!}
                                            </label>
                                        </td>
                                        <td>
                                            <label class="badge badge-info">
                                                {!! $row->LessonView->sum('quantity') !!}
                                            </label>
                                        </td>
                                        <td>{!! $row->statusLabel !!}</td>
                                        <td>
                                            {!! Form::open(['route' => ['lesson.destroy', $row->id], 'method' => 'DELETE', 'onsubmit' => 'beforeConfirm(this, event)']) !!}
                                            <a href="{!! route('support.lesson.error.index', $row->id) !!}" class="btn btn-xs btn-outline btn-danger">
                                                {!! $row->LessonError->where('fixed', '=', 0)->count() !!} - Erro(s)
                                            </a>
                                            <a href="{!! route('lesson.edit', $row->id) !!}" class="btn btn-outline btn-info btn-xs">
                                                <i class="fas fa-pencil-alt"></i> Editar
                                            </a>
                                            <button class="btn btn-outline btn-danger btn-xs" type="submit">
                                                <i class="fa fa-trash"></i> Deletar
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">Nenhuma aula no momento, <a href="{!! route('lesson.create') !!}">clique aqui</a> adicione uma aula</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                        @if (isset($data))
                            {!! $data->links() !!}
                        @endif
                    </div>

                </div>

            </div>
        </div>
    </div>
@stop

