@extends('dashboard::layouts.master')

@section('title', 'Nova Aula')

@section('content')
    <div class="row">
        {!! Form::open(['route' => 'lesson.store']) !!}
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5>Nova Aula</h5>
            </div>

            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('lesson.index') !!}" class="btn btn-outline btn-warning">
                            <i class="fa fa-arrow-left"></i> Listar todas as aulas
                        </a>
                    </div>
                </div>

                <hr>


                @include('course::lesson.form')
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop
