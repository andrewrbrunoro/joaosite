<div class="row">

    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="clear">
                        Status <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('status', 1, true) !!} Ativo
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('status', 0, false) !!} Inativo
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
            <label for="name">
                Nome <label class="badge badge-danger">Obrigatório</label>
            </label>
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
            {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
        </div>
    </div>

    <div class="col-lg-12">
        <div class="form-group{!! $errors->first('resume') ? ' has-error' : '' !!}">
            <label for="resume">
                Resumo (máximo 255 caracteres) <label class="badge badge-info">Opcional</label>
            </label>
            {!! Form::textarea('resume', null, ['rows' => 2, 'maxlength' => 255, 'class' => 'form-control', 'placeholder' => 'Resumo', 'style' => 'resize: none;']) !!}
            {!! $errors->first('resume', '<span class="text-danger">:message</span>') !!}
        </div>
    </div>

    <div class="col-lg-12">
        <div class="row">

            <div class="col-lg-3">
                <div class="form-group{!! $errors->first('days_available') ? ' has-error' : '' !!}">
                    <label for="days_available">
                        Dias disponíveis <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::text('days_available', null, ['class' => 'form-control', 'placeholder' => 'Dias disponíveis']) !!}
                    {!! $errors->first('days_available', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>

            <div class="col-lg-9">
                <div class="form-group{!! $errors->first('video_comercial') ? ' has-error' : '' !!}">
                    <label for="video_comercial">
                        Vídeo Comercial <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::text('video_comercial', null, ['class' => 'form-control', 'placeholder' => 'Vídeo Comercial']) !!}
                    {!! $errors->first('video_comercial', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>

        </div>
    </div>

    <div class="col-lg-12">

        <div class="form-group{!! $errors->first('commercial_call') ? ' has-error' : '' !!}">
            <label for="commercial_call">
                Chamada Comercial <label class="badge badge-info">Opcional</label>
            </label>
            {!! Form::textarea('commercial_call', null, ['class' => 'form-control redactor-editor', 'placeholder' => 'Chamada Comercial']) !!}
            {!! $errors->first('commercial_call', '<span class="text-danger">:message</span>') !!}
        </div>

        <div class="form-group{!! $errors->first('terms') ? ' has-error' : '' !!}">
            <label for="terms">
                Termos de Aquisição <label class="badge badge-info">Opcional</label>
            </label>
            {!! Form::textarea('terms', null, ['class' => 'form-control redactor-editor', 'placeholder' => 'Termos de Aquisição']) !!}
            {!! $errors->first('terms', '<span class="text-danger">:message</span>') !!}
        </div>

    </div>

</div>

<div class="row">
    <div class="col-lg-6 col-sm-6">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check"></i> Salvar o curso
        </button>
    </div>
</div>