@extends('dashboard::layouts.master')

@section('title', 'Conteúdos')

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Conteúdos</h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-12">
                        <a href="{!! route('content.create') !!}" class="btn btn-outline btn-success">
                            <i class="fa fa-plus"></i> Cadastrar Conteúdo
                        </a>
                        <a href="{!! request()->fullUrl() !!}" class="btn btn-outline btn-default">
                            <i class="fa fa-repeat"></i> Atualizar
                        </a>
                    </div>
                </div>

                <hr>

                <div class="row">

                    <div class="col-lg-12">
                        <table class="table table-responsive table-bordered table-stripped">
                            <thead>
                            <tr>
                                <td>#</td>
                                <td>Nome</td>
                                <td>Tipo</td>
                                <td>Link</td>
                                <td>Status</td>
                                <td width="15%">Opções</td>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($data->count())
                                @foreach($data as $row)
                                    <tr{!! $row->fileExists == false ? ' class="bg-danger" title="O arquivo não existe mais"' : '' !!}>
                                        <td>{!! $row->id !!}</td>
                                        <td>{!! $row->name !!}</td>
                                        <td>{!! $row->ContentType->name !!}</td>
                                        <td>{!! url($row->slug) !!}</td>
                                        <td>{!! $row->statusLabel !!}</td>
                                        <td>
                                            {!! Form::open(['route' => ['content.destroy', $row->id], 'method' => 'DELETE', 'onsubmit' => 'beforeConfirm(this, event)']) !!}
                                            <a href="{!! route('content.edit', $row->id) !!}" class="btn btn-outline btn-info btn-xs">
                                                <i class="fas fa-pencil-alt"></i> Editar
                                            </a>
                                            <button class="btn btn-outline btn-danger btn-xs" type="submit">
                                                <i class="fa fa-trash"></i> Deletar
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">Nenhum conteúdo no momento, <a href="{!! route('content.create') !!}">clique aqui</a> adicione um conteúdo</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                        @if (isset($data))
                            {!! $data->links() !!}
                        @endif
                    </div>

                </div>

            </div>
        </div>
    </div>
@stop

