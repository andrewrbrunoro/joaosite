@extends('dashboard::layouts.master')

@section('title', 'Editar Conteúdo')

@section('content')
    <div class="row">
        {!! Form::model($edit, ['route' => ['content.update', $edit->id], 'files' => true, 'method' => 'PATCH']) !!}
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5>Editar Conteúdo - <strong class="text-success">{!! $edit->name !!}</strong></h5>
            </div>

            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('content.index') !!}" class="btn btn-outline btn-warning">
                            <i class="fa fa-arrow-left"></i> Listar todos os conteúdos
                        </a>
                    </div>
                </div>

                <hr>


                @include('course::content.form')
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop
