@extends('dashboard::layouts.master')

@section('title', 'Novo Conteúdo')

@section('content')
    <div class="row">
        {!! Form::open(['route' => 'content.store', 'files' => true]) !!}
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5>Novo Conteúdo</h5>
            </div>

            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('content.index') !!}" class="btn btn-outline btn-warning">
                            <i class="fa fa-arrow-left"></i> Listar todos os conteúdos
                        </a>
                    </div>
                </div>

                <hr>


                @include('course::content.form')
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop
