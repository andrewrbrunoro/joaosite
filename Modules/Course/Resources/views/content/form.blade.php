<content-manage inline-template :elements='{!! $contentTypes !!}'>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="clear">
                            Status <label class="badge badge-danger">Obrigatório</label>
                        </label>
                        <label class="radio-inline">
                            {!! Form::radio('status', 1, true) !!} Ativo
                        </label>
                        <label class="radio-inline">
                            {!! Form::radio('status', 0, false) !!} Inativo
                        </label>
                    </div>
                </div>
            </div>
            <hr>
        </div>

        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                        <label for="name">
                            Nome <label class="badge badge-danger">Obrigatório</label>
                        </label>
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                        {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group{!! $errors->first('content_type_id') ? ' has-error' : '' !!}">
                        <label for="content_type_id">
                            Tipo de conteúdo <label class="badge badge-danger">Obrigatório</label>
                        </label>
                        {!! Form::select('content_type_id', $contentTypes->pluck('name', 'id'), null, ['class' => 'form-control', 'v-on:change' => 'elementType', 'id' => 'ContentType', 'placeholder' => 'Selecione o Tipo de Conteúdo']) !!}
                        {!! $errors->first('content_type_id', '<span class="text-danger">:message</span>') !!}
                    </div>
                </div>

            </div>
        </div>

        <div class="col-lg-12" v-if="element != 'video'">
            <div class="form-group{!! $errors->first('video') ? ' has-error' : '' !!}">
                <label for="video">
                    Vídeo <label class="badge badge-info">Opcional</label>
                </label>
                {!! Form::text('video', null, ['class' => 'form-control', 'placeholder' => 'Vídeo']) !!}
                {!! $errors->first('video', '<span class="text-danger">:message</span>') !!}
            </div>
        </div>

        <div class="col-lg-12">
            <hr v-if="element != ''">
        </div>

        @if (isset($edit) && $edit->ContentType->element_type == 'file')
        <div class="col-lg-12">
            <div class="form-group">
                <label class="label label-success ">
                    Arquivo atual: <a href="{!! url($edit->content) !!}" class="text-white" target="_blank">{!! url($edit->content) !!}</a>
                </label>
            </div>
        </div>
        @endif

        <div class="col-lg-12" v-if="element == ''">
            <div class="alert alert-info">
                Selecione um Tipo de conteúdo
            </div>
        </div>

        @foreach($contentTypes as $contentType)
            <div class="col-lg-12" v-if="element === '{!! $contentType->slug !!}'">
                <div class="form-group{!! $errors->first('content') ? ' has-error' : '' !!}">
                    <label for="'content'">
                        {!! $contentType->name !!} <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    {!! Form::{$contentType->element_type}('content', null, ['class' => 'form-control' . ($contentType->element_type == 'textarea' ? ' content-redactor-editor' : ''), 'placeholder' => $contentType->name]) !!}
                    {!! $errors->first('content', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        @endforeach

        <div class="col-lg-12">
            <hr>
        </div>

        <div class="col-lg-12" >
            <div class="form-group{!! $errors->first('available_weekday') ? ' has-error' : '' !!}">
                <label for="'available_weekday'">
                    Disponível em qual dia da semana <label class="badge badge-danger">Obrigatório</label>
                </label>
                  {{ Form::select('available_weekday', [
                   '' => 'Selecione',
                   '0' => 'Domingo',
                   '1' => 'Segunda',
                   '2' => 'Terça',
                   '3' => 'Quarta',
                   '4' => 'Quinta',
                   '5' => 'Sexta',
                   '6' => 'Sábado',
                   '7' => 'De Acordo com a Lição'],
                   null, array('class' => 'form-control')
                    )
                }}
                {!! $errors->first('available_weekday', '<span class="help-block">:message</span>') !!}
            </div>
        </div>

        <div class="col-lg-12">
            <label for="'duration'">
                Duração <label class="badge badge-info">Opcional</label>
            </label>
            <div class="form-group">

                <div class="range-slider">
                     {!! Form::text('duration', null, ['class' => 'js-range-slider', 'placeholder' => 'Vídeo']) !!}
                </div>

            </div>
        </div>

        <div class="col-lg-6 col-sm-6">
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-check"></i> Salvar o Conteúdo
            </button>
        </div>

    </div>
</content-manage>
@section('main-js', false)
@section('script')
    {!! Html::script(Module::asset('course:js/components/content.js')) !!}
@endsection