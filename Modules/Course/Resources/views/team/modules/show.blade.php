@extends('dashboard::layouts.master')

@section('title', 'Gerenciar Módulos')

@section('content')

    <import-content inline-template :not='{!! $show->TeamModules->pluck('id') !!}' :modal="{!! session()->has('modal') ? 1 : 0 !!}">
        <div id="importContent" class="modal inmodal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                {!! Form::open(['route' => ['team.module.import.modules', $show->id]]) !!}
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            &times;
                        </button>
                        <h4 class="modal-title">Importar Módulos</h4>
                    </div>

                    <div class="modal-body">

                        <div class="sk-spinner sk-spinner-wave" v-if="modules.length <= 0">
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>

                        <div class="row" v-if="modules.length">
                            <div class="col-lg-12 col-xs-12 table-responsive">
                                <div class="alert alert-info">
                                    <p><strong>*</strong> Selecione os módulos para poder importar todo o seu conteúdo</p>
                                </div>
                                @if (session()->has("error-modal"))
                                    <div class="alert alert-danger">
                                        <p>{!! session("error-modal") !!}</p>
                                    </div>
                                @endif
                                <div v-for="module in modules">
                                    <table class="table table-bordered table-stripped" style="margin-bottom: 0">
                                        <tbody>
                                            <tr style="background-color: #fbfbeb" :title="'Módulo - ' + module.name">
                                                <td width="5%">
                                                    <input type="checkbox" name="modules[]" :value="module.id" title="Selecione todas as aulas do módulo" v-on:click="checkModule(module.id)" />
                                                </td>
                                                <td>
                                                    @{{ module.name }}
                                                </td>
                                            </tr>
                                            <tr v-for="lesson in module.module_lessons">
                                                <td>
                                                    <input type="checkbox" :name="'lessons['+module.id+'][]'"
                                                           :value="lesson.id"
                                                           v-bind:data-module="module.id"
                                                           v-bind:data-lesson="lesson.id" />
                                                </td>
                                                <td>
                                                    @{{ lesson.name }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">
                            Fechar
                        </button>
                        <button type="submit" class="btn btn-primary" v-if="modules.length">
                            <i class="fas fa-check"></i> Importar
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </import-content>

    {{-- Hack component, englobando toda a tela de Turma Módulos --}}
    <team-modules inline-template team="{!! $show->id !!}" xmlns:v-on="http://www.w3.org/1999/xhtml" xmlns:v-bind="http://www.w3.org/1999/xhtml">
        <div>
            @include('course::team.modules.modal')

            <div class="row">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Gerenciar Módulos da Turma - <strong class="text-success">{!! $show->name !!}</strong></h5>
                    </div>
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-sm-12">
                                <a href="{!! route('team.index') !!}" class="btn btn-outline btn-warning">
                                    <i class="fa fa-arrow-left"></i> Listar todas a Turmas
                                </a>
                                <a href="#" class="btn btn-outline btn-success" data-open-modal>
                                    <i class="fa fa-plus"></i> Adicionar Módulos
                                </a>
                                @if ($countModules)
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#importContent">
                                        <i class="fas fa-asterisk"></i> Importar Módulos
                                    </button>
                                @endif
                            </div>
                        </div>

                        <hr>

                        <div class="col-lg-12 col-sm-12 table-responsive dd" id="nestable-modules">
                            <table class="table table-bordered table-stripped">
                                <thead>
                                <tr>
                                    <td width="2%">#</td>
                                    <td>Nome</td>
                                    <td>Preço</td>
                                    <td>Promoção</td>
                                    <td title="Data de lançamento">Dt. Lançamento</td>
                                    <td title="Data de expiração">Dt. Expiração</td>
                                    <td width="18%">
                                        Opções
                                    </td>
                                </tr>
                                </thead>
                                <tbody class="dd-list">
                                @if ($show->TeamModules->count())
                                    @foreach($show->TeamModules as $teamModule)
                                        {!! $teamModule->teamModuleEdit !!}
                                        <tr title="Módulo {!! $teamModule->name !!}" class="dd-item" data-id="{!! $teamModule->id !!}">
                                            <td title="Segure para ordenar o módulo" class="bg-primary arb-drag"></td>
                                            <td>{!! $teamModule->name !!}</td>
                                            <td>R$ {!! $teamModule->pivot->price !!}</td>
                                            <td>R$ {!! $teamModule->pivot->sale !!}</td>
                                            <td>{!! $teamModule->pivot->release_date !!}</td>
                                            <td>{!! $teamModule->pivot->expiration_date !!}</td>
                                            <td>
                                                {!! Form::open(['route' => ['team.module.destroy', $teamModule->id], 'method' => 'DELETE', 'onsubmit' => 'beforeConfirm(this, event)']) !!}

                                                {!! Form::hidden('team_id', $show->id) !!}
                                                <a href="#" class="btn btn-outline btn-info btn-xs" v-on:click='edit({!! $teamModule !!})'>
                                                    <i class="fas fa-pencil-alt"></i> Editar
                                                </a>

                                                <button class="btn btn-outline btn-danger btn-xs" type="submit">
                                                    <i class="fa fa-trash"></i> Deletar
                                                </button>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7">Nenhum módulo no momento, <a href="#" data-open-modal>clique aqui</a> e adicione um novo módulo</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </team-modules>
    {{-- Hack component, englobando toda a tela de Turma Módulos --}}
@stop

@section('script')
    {!! Html::script('assets/vendors/nestable/nestable.min.js') !!}
    {!! Html::script(Module::asset('course:js/components/team-modules.js')) !!}
    <script type="text/javascript">
        let showTeamId = '{!! $show->id !!}';
    </script>
    {!! Html::script(Module::asset('course:js/components/import-modules.js')) !!}
    {!! Html::script(Module::asset('course:js/main.js')) !!}
@endsection