<div class="floating-modal">
    <div class="container-fluid">
        <div class="row">
            <div class="ibox float-e-margins">
                <div class="ibox-content">

                    <div class="row">

                        <div class="col-lg-12 col-xs-12">

                            <h3 class="forum-title bg-primary">
                                @{{ edit_mode ? 'Editar Módulo - ' + this.store.name : 'Novo Módulo' }}
                            </h3>

                            <div class="row">
                                <div class="col-lg-12">

                                    <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                                        <label for="name">
                                            Nome <label class="badge badge-danger">Obrigatório</label>
                                        </label>
                                        {!! Form::text('name', null, ['class' => 'form-control', 'v-model' => 'store.name', 'placeholder' => 'Nome']) !!}
                                        {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group{!! $errors->first('price') ? ' has-error' : '' !!}">
                                        <label for="price">
                                            Preço <label class="badge badge-info">Opcional</label>
                                        </label>
                                        {!! Form::text('price', null, ['class' => 'form-control', 'v-model' => 'store.price', 'v-money' => "{decimal: ',', thousands: '.', precision: 2, masked: false}", 'placeholder' => 'Preço']) !!}
                                        {!! $errors->first('price', '<span class="text-danger">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="form-group{!! $errors->first('sale') ? ' has-error' : '' !!}">
                                        <label for="sale">
                                            Promoção <label class="badge badge-info">Opcional</label>
                                        </label>
                                        {!! Form::text('sale', null, ['class' => 'form-control', 'v-model' => 'store.sale', 'v-money' => "{decimal: ',', thousands: '.', precision: 2, masked: false}", 'placeholder' => 'Preço']) !!}
                                        {!! $errors->first('sale', '<span class="text-danger">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="form-group{!! $errors->first('release_date') ? ' has-error' : '' !!}">
                                        <label for="release_date">
                                            Dt. Lançamento <label class="badge badge-info">Opcional</label>
                                        </label>
                                        {!! Form::text('release_date', null, [
                                        'class' =>
                                        'form-control date-picker',
                                        'v-on:focusout' => 'setDatePickerValue',
                                        'v-model' => 'store.release_date',
                                        'placeholder' => 'Dt. Lançamento'
                                        ]) !!}
                                        {!! $errors->first('release_date', '<span class="text-danger">:message</span>') !!}
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="form-group{!! $errors->first('expiration_date') ? ' has-error' : '' !!}">
                                        <label for="expiration_date">
                                            Dt. Expiração <label class="badge badge-info">Opcional</label>
                                        </label>
                                        {!! Form::text('expiration_date', null, [
                                        'class' => 'form-control date-picker',
                                        'v-on:focusout' => 'setDatePickerValue',
                                        'v-model' => 'store.expiration_date',
                                        'placeholder' => 'Dt. Expiração'
                                        ]) !!}
                                        {!! $errors->first('expiration_date', '<span class="text-danger">:message</span>') !!}
                                    </div>
                                </div>
                            </div>

                            <hr>

                        </div>

                        <div class="col-lg-12 col-xs-12">

                            <div class="row">

                                <div class="col-lg-4 col-xs-4">
                                    <div class="form-group">
                                        <label>
                                            Procurar aulas <label class="badge badge-success">Clique nas setas para adicionar</label>
                                        </label>
                                        <input type="text" class="form-control" placeholder="Procure pelo nome da aula" />
                                    </div>
                                    <ul class="list-group">
                                        @foreach($lessons as $lesson)
                                            <li class="list-group-item" v-if="not_read_ids.indexOf({!! $lesson->id !!}) < 0">

                                                <a href="javascript:void(0);">
                                                    {!! $lesson->name !!}
                                                </a>

                                                <button class="btn btn-xs btn-info pull-right fd-space-pull-right"
                                                        v-on:click='setLesson({!! $lesson !!})'
                                                        type="button"
                                                        title="Adiciona ao módulo">
                                                    <i class="fa fa-arrow-right"></i>
                                                </button>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>

                                <div class="col-lg-8 col-xs-8 table-responsive">
                                    <label>
                                        Aulas deste módulo <label class="badge badge-success">Segure os itens para ordernar</label>
                                    </label>
                                    <div class="dd table-responsive" id="nestable-modal">
                                        <table class="table table-bordered table-stripped">
                                            <thead>
                                            <tr>
                                                <td width="2%">#</td>
                                                <td>Nome</td>
                                                <td title="Data de lançamento">Dt. Lançamento</td>
                                                <td title="Data de expiração">Dt. Expiração</td>
                                                <td width="2%">#</td>
                                            </tr>
                                            </thead>
                                            <tbody class="dd-list">
                                                <tr class="dd-item" v-bind:data-id="lesson.id" v-for="(lesson, index) in store.lessons">
                                                    <td class="arb-drag bg-primary" title="Segure para ordernar"></td>
                                                    <td>@{{ lesson.name }}</td>
                                                    <td>
                                                        <input placeholder="dia/mês/ano"
                                                               v-on:focusout="updateValue(index, 'release_date')"
                                                               :value="lesson.release_date"
                                                               type="text" class="form-control date-picker"/>
                                                    </td>
                                                    <td>
                                                        <input placeholder="dia/mês/ano"
                                                               v-on:focusout="updateValue(index, 'expiration_date')"
                                                               :value="lesson.expiration_date"
                                                               type="text" class="form-control date-picker"/>
                                                    </td>
                                                    <td>
                                                        <button class="btn-xs btn btn-danger" style="margin-top: 18%" type="button"
                                                                title="Remover a aula deste módulo"
                                                                v-on:click="destroy(lesson.id)">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>

                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12">

                            <hr>

                            <button type="button" class="btn btn-primary" v-on:click="save">
                                <i class="fa fa-check"></i> @{{ !wait ? "Salvar o Módulo" : "Aguarde..." }}
                            </button>

                            <button type="button" class="btn btn-danger" v-on:click="closeModal">
                                <i class="fa fa-times"></i> Fechar
                            </button>

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>