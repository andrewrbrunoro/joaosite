@extends('dashboard::layouts.master')

@section('title', 'Turmas')

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Turmas</h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-12">
                        <a href="{!! route('team.create') !!}" class="btn btn-outline btn-success">
                            <i class="fa fa-plus"></i> Cadastrar Turma
                        </a>
                        <a href="{!! request()->fullUrl() !!}" class="btn btn-outline btn-default">
                            <i class="fa fa-repeat"></i> Atualizar
                        </a>
                    </div>
                </div>

                <hr>

                <div class="row">

                    <div class="col-lg-12">
                        <table class="table table-responsive table-bordered table-stripped">
                            <thead>
                            <tr>
                                <td>#</td>
                                <td>Nome</td>
                                <td>Tipo</td>
                                <td>Curso</td>
                                <td>Link</td>
                                <td>Preço</td>
                                <td>Preço Promoção</td>
                                <td>Status</td>
                                <td>Opções</td>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($data->count())
                                @foreach($data as $row)
                                    <tr{!! $row->principal == 1 ? ' title="Turma está como principal" class="bg-weak-green"' : '' !!}>
                                        <td>{!! $row->id !!}</td>
                                        <td>{!! $row->name !!}</td>
                                        <td>{!! $row->teamType->name !!}</td>
                                        <td>
                                            <a href="{!! route('course.edit', $row->course->id) !!}">{!! $row->course->name !!}</a>
                                        </td>
                                        <td>{!! url($row->slug) !!}</td>
                                        <td>R$ {!! $row->price !!}</td>
                                        <td>R$ {!! $row->sale !!}</td>
                                        <td>{!! $row->statusLabel !!}</td>
                                        <td>
                                            {!! Form::open(['route' => ['team.destroy', $row->id], 'method' => 'DELETE', 'onsubmit' => 'beforeConfirm(this, event)']) !!}
                                            <a href="{!! route('team_user.show', $row->id) !!}" title="Gerenciar Alunos" class="btn btn-outline btn-xs btn-default">
                                                <i class="fas fa-users"></i> G. Alunos
                                            </a>
                                            <a href="{!! route('team.module.show', $row->id) !!}" class="btn btn-outline btn-success btn-xs" title="Gerenciar as Aulas">
                                                <i class="fa fa-align-justify"></i> G. Aulas
                                            </a>
                                            <a href="{!! route('team.edit', $row->id) !!}" class="btn btn-outline btn-info btn-xs">
                                                <i class="fas fa-pencil-alt"></i> Editar
                                            </a>
                                            <button class="btn btn-outline btn-danger btn-xs" type="submit">
                                                <i class="fa fa-trash"></i> Deletar
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="10">Nenhuma turma no momento, <a href="{!! route('team.create') !!}">clique aqui</a> adicione uma turma</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                        @if (isset($data))
                            {!! $data->links() !!}
                        @endif

                    </div>

                </div>

            </div>
        </div>
    </div>
@stop

