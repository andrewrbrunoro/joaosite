<div class="row">

    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="clear">
                        Principal <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('principal', 1, false) !!} Ativo
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('principal', 0, true) !!} Inativo
                    </label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="clear">
                        Status <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('status', 1, true) !!} Ativo
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('status', 0, false) !!} Inativo
                    </label>
                </div>
            </div>
        </div>
        <hr>
    </div>

    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                    <label for="name">
                        Nome <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome', 'maxlength' => 255]) !!}
                    {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('team_type_id') ? ' has-error' : '' !!}">
                    <label for="team_type_id">
                        Tipo de Turma <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    {!! Form::select('team_type_id', $teamTypes, null, ['class' => 'form-control', 'placeholder' => 'Tipo de Turma']) !!}
                    {!! $errors->first('team_type_id', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('course_id') ? ' has-error' : '' !!}">
                    <label for="team_type_id">
                        Curso <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    {!! Form::select('course_id', $courses, null, ['class' => 'form-control', 'placeholder' => 'Curso']) !!}
                    {!! $errors->first('course_id', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group{!! $errors->first('price') ? ' has-error' : '' !!}">
                    <label for="price">
                        Preço <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::text('price', null, ['class' => 'form-control', 'v-money' => "{decimal: ',', thousands: '.', precision: 2, masked: false}", 'placeholder' => 'Preço']) !!}
                    {!! $errors->first('price', '<span class="help-block">:message</span>') !!}
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group{!! $errors->first('sale') ? ' has-error' : '' !!}">
                    <label for="sale">
                        Promoção <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::text('sale', null, ['class' => 'form-control', 'v-money' => "{decimal: ',', thousands: '.', precision: 2, masked: false}", 'placeholder' => 'Preço']) !!}
                    {!! $errors->first('sale', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group{!! $errors->first('resume') ? ' has-error' : '' !!}">
                    <label for="resume">
                        Resumo (<strong class="text-success">Máximo 255 caracteres</strong>) <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::textarea('resume', null, ['rows' => 2, 'class' => 'form-control no-resize', 'placeholder' => 'Resumo', 'maxlength' => 255]) !!}
                    {!! $errors->first('resume', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group{!! $errors->first('content') ? ' has-error' : '' !!}">
                    <label for="content">
                        Conteúdo <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::textarea('content', null, ['class' => 'form-control redactor-editor', 'placeholder' => 'Conteúdo']) !!}
                    {!! $errors->first('content', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
        </div>

        <hr>
    </div>

</div>


<div class="row">
    <div class="col-lg-6 col-sm-6">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check"></i> Salvar a Turma
        </button>
    </div>
</div>