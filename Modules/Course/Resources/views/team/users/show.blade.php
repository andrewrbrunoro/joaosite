@extends('dashboard::layouts.master')

@section('title', 'Gerenciar Alunos')

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Gerenciar Alunos</h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-12">
                        <a href="{!! route("team.index") !!}" class="btn btn-outline btn-warning">
                            <i class="fas fa-arrow-left"></i> Lista todas as Turmas
                        </a>
                        <a href="{!! request()->fullUrl() !!}" class="btn btn-outline btn-default">
                            <i class="fa fa-repeat"></i> Atualizar
                        </a>
                    </div>
                </div>

                <hr>

                <div class="row">
                    {!! Form::open(['route' => ['team_user.store', $team->id]]) !!}
                    <div class="col-lg-12 col-xs-12">

                        <div class="row">
                            <div class="col-lg-9">
                                <div class="form-group{!! $errors->first('user_id') ? ' has-error' : '' !!}">
                                    <label for="user_id">
                                        Usuário <label class="badge badge-danger">Obrigatório</label>
                                    </label>
                                    {!! Form::select('user_id[]', $listUsers, null, ['class' => 'form-control selectpicker', 'data-live-search' => "true", 'multiple', 'required']) !!}
                                    {!! $errors->first('user_id', '<span class="text-danger">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group{!! $errors->first('expiration_date') ? ' has-error' : '' !!}">
                                    <label for="expiration_date" title="Data de Validade">
                                        Dt. de validade <label class="badge badge-info">Opcional</label>
                                    </label>
                                    {!! Form::text('expiration_date', null, ['class' => 'form-control date-picker', 'placeholder' => 'Data de validade']) !!}
                                    {!! $errors->first('expiration_date', '<span class="text-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fas fa-check"></i> Matricular
                                </button>
                            </div>
                        </div>
                        <hr>
                    </div>
                    {!! Form::close() !!}
                    <div class="col-lg-12 col-xs-12 table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nome</th>
                                    <th>E-mail</th>
                                    <th width="15%">Dt. de validade</th>
                                    <th width="8%">Opções</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($data) && $data->count())
                                    @foreach($data as $row)
                                        <tr>
                                            <td>{!! $row->id !!}</td>
                                            <td>{!! $row->User->name !!}</td>
                                            <td>{!! $row->User->email !!}</td>
                                            <td>
                                                <input type="text" class="form-control date-picker update-registration" data-registry="{!! $row->id !!}" value="{!! $row->expiration_date !!}" />
                                            </td>
                                            <td>
                                                {!! Form::open(['route' => ['team_user.destroy', $team->id, $row->id], 'method' => 'DELETE', 'onsubmit' => 'beforeConfirm(this, event)']) !!}
                                                <button class="btn btn-outline btn-danger btn-xs" type="submit">
                                                    <i class="fa fa-trash"></i> Deletar
                                                </button>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5">Nenhum aluno matrículado no momento.</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>

                        @if (isset($data))
                            {!! $data->links() !!}
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop

@section("css")
    {!! Html::style('assets/vendors/silvio-moreto/css/bootstrap-select.min.css') !!}
@endsection

@section("script")
    {!! Html::script('assets/vendors/silvio-moreto/js/bootstrap-select.min.js') !!}
    {!! Html::script('assets/vendors/silvio-moreto/js/i18n/defaults-pt_BR.min.js') !!}

    <script type="text/javascript">
        var TEAM_ID = '{!! $team->id !!}';
        $(document).ready(function(){
            $('.update-registration').on('change', function(){
                var self = $(this);
                axios.patch(CMS_URL + 'turma-usuario/' + TEAM_ID + '/atualiza-matricula', {
                    registration_id: self.data('registry'),
                    expiration_date: self.val()
                }).then(function (r) {
                    toastr.success(r.data.message);
                }).catch(function (r) {
                    r = r.response.data;
                    toastr.error(r.message);
                });
            });
        });
    </script>
@endsection
