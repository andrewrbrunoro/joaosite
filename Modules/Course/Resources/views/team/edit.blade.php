@extends('dashboard::layouts.master')

@section('title', 'Editar Turma')

@section('content')
    <div class="row">
        {!! Form::model($edit, ['route' => ['team.update', $edit->id], 'method' => 'PATCH']) !!}
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5>Editar Turma {!! $edit->name !!}</h5>
            </div>

            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('team.index') !!}" class="btn btn-outline btn-warning">
                            <i class="fa fa-arrow-left"></i> Listar todas as turmas
                        </a>
                    </div>
                </div>

                <hr>
                @include('course::team.form')
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop
