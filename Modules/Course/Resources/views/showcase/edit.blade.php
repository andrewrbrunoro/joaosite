@extends('dashboard::layouts.master')

@section('title', 'Editar Vitrine')

@section('content')
    <div class="row">
        {!! Form::model($edit, ['route' => ['course.showcase.update', $edit->id], 'method' => 'PATCH']) !!}
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5>Editar Vitrine - {!! $edit->name !!}</h5>
            </div>

            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('course.showcase.index') !!}" class="btn btn-outline btn-warning">
                            <i class="fa fa-arrow-left"></i> Listar todas as Vitrines
                        </a>
                    </div>
                </div>

                <hr>

                @include('course::showcase.form')
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop
