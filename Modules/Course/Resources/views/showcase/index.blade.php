@extends('dashboard::layouts.master')

@section('title', 'Vitrines')

@section('content')
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Vitrines</h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-12">
                        <a href="{!! route('course.showcase.create') !!}" class="btn btn-outline btn-success">
                            <i class="fa fa-plus"></i> Cadastrar Vitrine
                        </a>
                        <a href="{!! request()->fullUrl() !!}" class="btn btn-outline btn-default">
                            <i class="fa fa-repeat"></i> Atualizar
                        </a>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-lg-12 col-xs-12 table-responsive">
                        <table class="table table-bordered table-stripped">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Nome</td>
                                    <td>Status</td>
                                    <td width="5%" title="Quantida de Turmas na vitrine">Qtd.</td>
                                    <td title="Data de Lançamento">Dt. Lançamento</td>
                                    <td title="Data de Encerramento">Dt. Encerramento</td>
                                    <td width="15%">Opções</td>
                                </tr>
                            </thead>
                            <tbody>
                                @if (isset($data) && $data->count())
                                    @foreach($data as $row)
                                        <tr>
                                            <td>{!! $row->id !!}</td>
                                            <td>{!! $row->name !!}</td>
                                            <td>{!! $row->statusLabel !!}</td>
                                            <td>{!! $row->CourseShowcaseTeam->count() !!}</td>
                                            <td>{!! $row->release_date !!}</td>
                                            <td>{!! $row->expiration_date !!}</td>
                                            <td>
                                                {!! Form::open(['route' => ['course.showcase.destroy', $row->id], 'method' => 'DELETE', 'onsubmit' => 'beforeConfirm(this, event)']) !!}
                                                <a href="{!! route('course.showcase.edit', $row->id) !!}" class="btn btn-outline btn-info btn-xs">
                                                    <i class="fas fa-pencil-alt"></i> Editar
                                                </a>
                                                <button class="btn btn-outline btn-danger btn-xs" type="submit">
                                                    <i class="fa fa-trash"></i> Deletar
                                                </button>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">Nenhuma vitrine cadastrada no momento. <a href="{!! route('course.showcase.create') !!}">clique aqui</a> e adicione uma vitrine.</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop
