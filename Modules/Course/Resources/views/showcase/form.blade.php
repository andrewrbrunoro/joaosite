<div class="row">

    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="clear">
                        Status <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('status', 1, true) !!} Ativo
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('status', 0, false) !!} Inativo
                    </label>
                </div>
            </div>
        </div>
        <hr>
    </div>

    <div class="col-lg-12 col-xs-12">

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                    <label for="name">
                        Nome <label class="badge badge-danger">Obrigatório</label>
                    </label>
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                    {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group{!! $errors->first('release_date') ? ' has-error' : '' !!}">
                    <label for="release_date">
                        Data de Lançamento <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::text('release_date', null, ['class' => 'form-control date-picker', 'placeholder' => 'Data de lançamento']) !!}
                    {!! $errors->first('release_date', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group{!! $errors->first('expiration_date') ? ' has-error' : '' !!}">
                    <label for="expiration_date">
                        Data de Expiração <label class="badge badge-info">Opcional</label>
                    </label>
                    {!! Form::text('expiration_date', null, ['class' => 'form-control date-picker', 'placeholder' => 'Data de expiração']) !!}
                    {!! $errors->first('expiration_date', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
        </div>
        <hr>
    </div>

    <course-showcase inline-template :exist-courses='{!! session()->has('courses') ? session('courses') : collect([]) !!}'>
        <div class="col-lg-12 col-xs-12">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="form-group">
                        <label>
                            Cursos <label class="badge badge-info">Selecione ou procure pelo Curso</label>
                        </label>
                        <select class="form-control" v-on:change="addCourse">
                            <option value="">Selecione o Curso</option>
                            @foreach($courses as $course)
                                <option v-if="hideCourses.indexOf({!! $course->id !!}) < 0" value='{!! $course !!}'>{!! $course->name !!}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-12 col-xs-12 table-responsive">
                    <h4 class="forum-title forum-title-without-margin bg-primary">
                        Cursos
                    </h4>
                    <table class="table table-bordered table-stripped">
                        <thead>
                        <tr>
                            <td>Nome</td>
                            <td width="5%">Opções</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-if="courses.length > 0" v-for="course in courses">
                            <td>
                                <input type="hidden" name="courses[]" :value="JSON.stringify(course)" />
                                @{{ course.name }}
                            </td>
                            <td>
                                <button type="button" class="btn btn-danger btn-xs" v-on:click="unsetCourse(course.id)">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        <tr v-if="courses.length <= 0">
                            <td colspan="4">Nenhum turma na vitrine no momento.</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </course-showcase>

    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-check"></i> Salvar Vitrine
                </button>
            </div>
        </div>
    </div>
</div>

@section('script')
    <script type="text/javascript">
        Vue.component('course-showcase', {
            props: {
                existCourses: {
                    type: Array,
                    default: function () {
                        return []
                    }
                }
            },
            data: function () {
                return {
                    courses: this.existCourses,
                    hideCourses: []
                }
            },
            methods: {
                unsetCourse: function (id) {
                    try {

                        // remove dos itens escondidos
                        this.hideCourses = this.hideCourses.filter(function(o){
                            return o != id;
                        });

                        // removo das turmas adicionadas
                        this.courses = this.courses.filter(function(o){
                            return o.id != id;
                        });

                    } catch(e) {
                        toastr.error(e);
                    }
                },
                addCourse: function () {
                    try {
                        let target = $(event.target || event.srcElement);
                        // se não estiver vazio
                        if (target.val() != "") {
                            let value = JSON.parse(target.val());
                            // Adiciona a turma a listagem de turmas da vitrine
                            this.courses.unshift(value);
                            // Puxa apenas o ID da Turma para poder esconder na listagem de seleção
                            this.hideCourses.push(value.id);
                            // Seleciono o valor nulo
                            target.val('');
                        }
                    } catch (e) {
                        toastr.error(e);
                    }
                }
            },
            created: function () {
                this.courses.map(function(o){
                    this.hideCourses.push(o.id);
                }.bind(this));
            }
        });
    </script>
@endsection