@extends('dashboard::layouts.master')

@section('title', 'Nova Vitrine')

@section('content')
    <div class="row">
        {!! Form::open(['route' => 'course.showcase.store']) !!}
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5>Nova Vitrine</h5>
            </div>

            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('course.showcase.index') !!}" class="btn btn-outline btn-warning">
                            <i class="fa fa-arrow-left"></i> Listar todas as Vitrines
                        </a>
                    </div>
                </div>

                <hr>

                @include('course::showcase.form')
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop
