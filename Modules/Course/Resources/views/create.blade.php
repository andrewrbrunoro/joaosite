@extends('dashboard::layouts.master')

@section('title', 'Novo Curso')

@section('content')
    <div class="row">
        {!! Form::open(['route' => 'course.store']) !!}
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5>Novo Curso</h5>
            </div>

            <div class="ibox-content">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{!! route('course.index') !!}" class="btn btn-outline btn-warning">
                            <i class="fa fa-arrow-left"></i> Listar todos os cursos
                        </a>
                    </div>
                </div>

                <hr>


                @include('course::form')
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop
