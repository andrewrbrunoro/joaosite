Vue.component('import-content', {
    props: {
        modal: {
            type: Number,
            default: function () {
                return 0;
            }
        },
        not: {
            type: Array,
            default: function () {
                return [];
            }
        }
    },
    data: function () {
        return {
            modules: []
        }
    },
    methods: {
        checkModule: function (module_id) {
            let dom = $(this.$el);
            let target = $(event.target || event.srcElement);
            let lessons = dom.find('[data-module="' + module_id + '"]');
            if (target.is(':checked')) {
                lessons.prop('checked', true);
            } else {
                lessons.prop('checked', false);
            }
        },
        getModules: function () {
            let self = this;
            axios.get(CMS_URL + 'modulo/todos/modulos', {
                params: {
                    not: this.not
                }
            }).then(function(r){
                self.modules = r.data.data;
            }).catch(function(r){
                let response = r.response;
                toastr.error(response.data.message);
            });
        }
    },
    created: function () {
        this.getModules();
    },
    mounted: function () {
        if (this.modal == 1) {
            $('[data-target="#importContent"]').trigger('click');
        }
    }
});