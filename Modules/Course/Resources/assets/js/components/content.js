var $content = Vue.extend({
    props: {
        elements: Array
    },
    data: function () {
        return {
            element: ''
        }
    },
    methods: {
        elementType: function (e) {
            var target = e.target||e.srcElement;
            this.setElement(target.value);
        },
        setElement: function (id) {
            this.elements.map(function(a) {
                if (a.id == id) {
                    this.element = a.slug;
                    if (a.element_type == 'textarea') {
                        setTimeout(function(){
                            $('.redactor-editor').redactor({
                                minHeight: 500,
                                toolbarFixed: true,
                                toolbarFixedTopOffset: 80, // pixels
                                fileUpload: CMS_URL + 'upload/file',
                                imageUpload: CMS_URL + 'upload/image'
                            })
                        }, 100);
                    }
                }
            }.bind(this));
        }
    },
    mounted: function (){
        var $contentType = $("#ContentType");
        if ($contentType.val() != "")
            this.setElement($contentType.val());
    }
});
Vue.component('content-manage', $content);