Vue.component('team-modules', {
    props: {
        team: "",
        modulesProp: {
            type: Object,
            default: function () {
                return {};
            }
        },
        editProp: {
            type: Object,
            default: function () {
                return {
                    name: "",
                    price: 0.00,
                    sale: 0.00,
                    release_date: "",
                    expiration_date: "",
                    lessons: []
                }
            }
        }
    },
    data: function () {
        return {
            wait: false,
            modules: this.modulesProp,
            store: this.editProp,
            not_read_ids: [],
            edit_mode: false
        }
    },
    methods: {
        save: function () {
            // Remove todos os erros na tela
            removeAllErrors();
            // Verifico se não está em requisição
            if (!this.wait) {
                if (this.edit_mode) {
                    this.update();
                } else {
                    this.storeModule();
                }
            } else {
                toastr.info("Aguarde, uma requisição está sendo processada.");
            }
        },
        storeModule: function () {
            var self = this;
            // Seto que está requisitando, para não exisir um flood
            this.wait = true;
            // Requisita a criação do módulo com as aulas
            axios.post(CMS_URL + 'turma/' + this.team + '/criar/modulo', this.store).then(function (r) {
                // Tudo certo, informa o usuário
                toastr.success(r.data.message);
                // Finalizo a requisição
                self.wait = false;
                // Limpo o formulário
                self.clear();
            }).catch(function(r) {
                // No catch, é necessário retorna a "variável da função no caso (r)" e junto response. Se não, ele retorna apenas o motivo do erro e não todos os dados
                var response = r.response;
                // Se existir errors que vem da validação
                if (response.data.errors) {
                    // Seto os erros em uma variável
                    var errors = response.data.errors;
                    // Faço a leitura do Objeto e transformo em array e adiciono o efeito de erro
                    Object.keys(errors).map(function(object, index){
                        setError(object, errors[object]);
                    });
                }
                toastr.error("Não foi possível salvar o módulo, verifique se os dados foram digitados corretamente.");
                // Finalizo a requisição
                self.wait = false;
            });
        },
        update: function () {
            var self = this;
            // Seto que está requisitando, para não exisir um flood
            this.wait = true;
            // Requisita a criação do módulo com as aulas
            axios.patch(CMS_URL + 'turma/' + this.team + '/editar/modulo', this.store).then(function (r) {
                // Tudo certo, informa o usuário
                toastr.success(r.data.message);
                // Finalizo a requisição
                self.wait = false;
            }).catch(function(r) {
                // No catch, é necessário retorna a "variável da função no caso (r)" e junto response. Se não, ele retorna apenas o motivo do erro e não todos os dados
                var response = r.response;
                // Se existir errors que vem da validação
                if (response.data.errors) {
                    // Seto os erros em uma variável
                    var errors = response.data.errors;
                    // Faço a leitura do Objeto e transformo em array e adiciono o efeito de erro
                    Object.keys(errors).map(function(object, index){
                        setError(object, errors[object]);
                    });
                }
                toastr.error("Não foi possível salvar o módulo, verifique se os dados foram digitados corretamente.");
                // Finalizo a requisição
                self.wait = false;
            });
        },
        setDatePickerValue: function () {
            var target = event.target || event.srcElement;
            this.store[target.name] = target.value;
        },
        setLesson: function (lesson) {
            // Adiciona a aula na inserção do módulo
            this.store.lessons.unshift(lesson);
            // Adiciona o ID que não precisa mais listar
            this.not_read_ids.push(lesson.id);

            setTimeout(function () {
                $('.date-picker').datepicker({
                    format: "dd/mm/yyyy",
                    todayHighlight: true
                });
            }, 200);
        },
        updateValue: function (id, key) {
            var target = event.target || event.srcElement;
            if (this.store.lessons.length) {
                this.store.lessons[id][key] = target.value;
            }
        },
        destroy: function (id) {
            this.not_read_ids = this.not_read_ids.filter(function (current) {
                return current != id;
            });
            this.store.lessons = this.store.lessons.filter(function (current) {
                return current.id != id;
            });
        },
        clear: function () {
            this.store = {
                name: "",
                price: 0.00,
                sale: 0.00,
                release_date: "",
                expiration_date: "",
                lessons: []
            };
            this.not_read_ids = [];
        },
        // Função criada, para evitar uma manutenção maior na listagem dos módulos
        closeModal: function () {
            $('.floating-modal').toggleClass('active');
            window.location.reload();
        },
        edit: function (module) {
            this.store = {
                id: module.id,
                name: module.name,
                price: module.pivot ? module.pivot.price : '0,00',
                sale: module.pivot ? module.pivot.sale : '0,00',
                release_date: module.pivot ? module.pivot.release_date : null,
                expiration_date: module.pivot ? module.pivot.expiration_date : null,
                lessons: []
            };

            if (module.module_lessons) {
                module.module_lessons.map(function(object){
                    this.store.lessons.push({
                        id: object.id,
                        name: object.name,
                        release_date: object.pivot ? object.pivot.release_date : null,
                        expiration_date: object.pivot ? object.pivot.expiration_date : null
                    });
                    this.not_read_ids.push(object.id);
                }.bind(this));
            }
            // Abre a modal para a crição ou atualização de módulo
            $('.floating-modal').toggleClass('active');
            // Editar, ativo
            this.edit_mode = true;

            setTimeout(function () {
                $('.date-picker').datepicker({
                    format: "dd/mm/yyyy",
                    todayHighlight: true
                });
            }, 200);
        },
        // Necessário para quando atualizar, alterar as posições
        hackChange: function (e) {
            let list = e.length ? e : $(e.target);
            if (window.JSON) {
                axios.patch(CMS_URL + 'turma/' + this.store.id + '/ordernar/aulas/modulo', {
                    lessons: JSON.parse(list.nestable('serialize'))
                });
            }
        }
    },
    mounted: function () {
        $('#nestable-modal').nestable({
            handleClass: 'arb-drag'
        }).on('change', this.hackChange);
    }
});