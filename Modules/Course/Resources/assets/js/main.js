$(document).ready(function(){
    $('#nestable-modules').nestable({
        handleClass: 'arb-drag'
    }).on('change', function(e) {
        let list = e.length ? e : $(e.target);
        if (window.JSON) {
            axios.patch(CMS_URL + 'turma/' + showTeamId + '/ordernar/modulos', {
                modules: JSON.parse(list.nestable('serialize'))
            }).then(function(r) {
                toastr.success(r.data.message);
            }).catch(function(r){
                toastr.error("Não foi possível ordernar os módulos, tente novamente. Caso o erro persista, entre em contato conosco.");
            });
        }
    });
});