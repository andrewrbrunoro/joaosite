<?php

namespace Modules\Course\Helpers;

use Illuminate\Contracts\Support\MessageBag;

/**
 * v 0.1
 *
 * Class ImportContent
 * @package Modules\Course\Helpers
 */
class ImportContent
{

    /**
     * @var MessageBag
     */
    private $bag;

    /**
     * ImportContent constructor.
     * @param MessageBag $bag
     *
     * Instância BAG para evitar um erro fatal
     */
    public function __construct(MessageBag $bag)
    {
        $this->bag = $bag;
    }

    /**
     * @param $name
     * @param $arguments
     * @return ImportContent
     *
     * Cria o metódo mágico __call que interceptar todos os eventos que a classe recebe
     */
    public function __call($name, $arguments): self
    {
        if (preg_match('/^get(.+)/', $name, $matches)) {
            $var_name = strtolower($matches[1]);
            return $this->$var_name ? $this->$var_name : $arguments[0];
        }
        if (preg_match('/^set(.+)/', $name, $matches)) {
            $var_name_sneak = Inflector::underscore($matches[1]);
            $var_name = strtolower($var_name_sneak);
            $this->$var_name = $arguments[0];
        }
        return $this;
    }

    /**
     * @param $property
     * @return ImportContent
     *
     * Metodo mágico que retorna a propriedade se caso ela existir
     */
    public function __get($property): self
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        } else {
            $this->bag->add("__get-{$property}", 'Não foi dinamicamente criado ou não existe na classe.');
        }
        return $this;
    }

    /**
     * @param $property
     * @param $value
     * @return ImportContent
     *
     * Intercepta todos os metódos que iniciam com set
     */
    public function __set($property, $value): self
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }


    /**
     * @param string $value
     * @return ImportContent
     *
     * Seta a localidade do arquivo, para a leitura da classe
     */
    public function setFile(string $value): self
    {
        try {
            if (!is_file(public_path($value)))
                throw new \Exception("Arquivo não encontrado");
        } catch (\Exception $e) {
            $this->bag->add($e->getCode(), $e->getMessage());
        }
        return $this;
    }

}