<?php

namespace Modules\Course\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Course\Entities\ContentType;

class ConteTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $contentType = new ContentType();

        if ($contentType->where('slug', '=', 'texto')->count() < 1) {
            $contentType->create([
                'name' => 'Texto',
                'slug' => 'texto',
                'status' => 1,
                'element_type' => 'textarea',
                'validate' => 'required'
            ]);
        }

        if ($contentType->where('slug', '=', 'pdf-material-de-apoio')->count() < 1) {
            $contentType->create([
                'name' => 'PDF - Material de Apoio',
                'slug' => 'pdf-material-de-apoio',
                'status' => 1,
                'element_type' => 'file',
                'validate' => 'required|file'
            ]);
        }

        if ($contentType->where('slug', '=', 'ppt-material-de-apoio')->count() < 1) {
            $contentType->create([
                'name' => 'PPT - Material de Apoio',
                'slug' => 'ppt-material-de-apoio',
                'status' => 1,
                'element_type' => 'file',
                'validate' => 'required|file'
            ]);
        }

        // $this->call("OthersTableSeeder");
    }
}
