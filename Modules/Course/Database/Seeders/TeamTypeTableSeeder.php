<?php

namespace Modules\Course\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Course\Entities\TeamType;

class TeamTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $teamType = new TeamType();

        if ($teamType->whereSlug('ead')->count() < 1) {
            $teamType->create([
                'name' => 'EAD'
            ]);
        }

        if ($teamType->whereSlug('presencial')->count() < 1) {
            $teamType->create([
                'name' => 'Presencial'
            ]);
        }
    }
}
