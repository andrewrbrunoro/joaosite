<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamUserConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_user_configs', function (Blueprint $table) {

            $table->integer('id');
            $table->integer('user_id');
            $table->integer('team_id');
            $table->integer('last_lesson_id')->nullable();
            $table->integer('speed')->nullable()->default(1);
            $table->integer('material_position')->nullable()->default(0);
            $table->tinyInteger('lesson_status')->nullable()->default(1);
            $table->tinyInteger('light')->nullable()->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_user_configs');
    }
}
