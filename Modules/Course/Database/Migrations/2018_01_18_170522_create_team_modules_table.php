<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_modules', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('team_id');
            $table->integer('module_id');

            $table->integer('order')->default(0);

            $table->decimal('price', 10, 2)->default(0.00);
            $table->decimal('sale', 10, 2)->default(0.00);

            $table->dateTime('release_date')->nullable();
            $table->dateTime('expiration_date')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_modules');
    }
}
