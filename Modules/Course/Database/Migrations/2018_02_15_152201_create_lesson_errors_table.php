<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_errors', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('team_id');
            $table->integer('user_id');
            $table->string('url');
            $table->integer('lesson_id')->nullable();

            $table->text('text');
            $table->longText('answer')->nullable();

            $table->tinyInteger('fixed')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_errors');
    }
}
