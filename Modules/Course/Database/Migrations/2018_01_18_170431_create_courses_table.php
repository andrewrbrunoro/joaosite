<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('days_available')->nullable();

            $table->string('name');
            $table->string('slug');
            $table->string('resume', 255)->nullable();

            $table->text('video_call')->nullable();
            $table->longText('commercial_call')->nullable();
            $table->longText('terms')->nullable();

            $table->tinyInteger('status')->default(1);

            $table->dateTime('release_date')->nullable();
            $table->dateTime('expiration_date')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
