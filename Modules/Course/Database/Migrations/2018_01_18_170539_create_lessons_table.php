<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('discipline_id')->nullable();

            $table->string('name');
            $table->string('slug');

            $table->string('resume')->nullable();

            $table->integer('days_available')->nullable();

            $table->decimal('price', 10, 2)->nullable()->default(0.00);
            $table->decimal('sale', 10, 2)->nullable()->default(0.00);

            $table->tinyInteger('status')->default(1);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessons');
    }
}
