<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_contents', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('lesson_id');
            $table->integer('content_id');

            $table->integer('order')->nullable();

            $table->decimal('price', 10, 2)->nullable()->default(0.00);
            $table->decimal('sale', 10, 2)->nullable()->default(0.00);

            $table->dateTime('release_date')->nullable();
            $table->dateTime('expiration_date')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_contents');
    }
}
