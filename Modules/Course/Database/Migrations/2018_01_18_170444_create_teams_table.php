<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('course_id')->nullable();
            $table->integer('team_type_id')->nullable();

            $table->string('name');
            $table->string('slug');
            $table->string('resume')->nullable();

            $table->decimal('price', 10, 2)->default(0.00);
            $table->decimal('sale', 10, 2)->default(0.00);

            $table->longText('content')->nullable();

            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('principal')->default(0);

            $table->dateTime('release_date')->nullable();
            $table->dateTime('expiration_date')->nullable();

            $table->integer('views')->nullable()->default(1);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
