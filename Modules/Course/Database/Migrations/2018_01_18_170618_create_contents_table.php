<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('content_type_id');

            $table->string('name');
            $table->string('slug');

            $table->decimal('price', 10, 2)->default(0.00);
            $table->decimal('sale', 10, 2)->default(0.00);
            $table->decimal('duration', 10, 1)->default(0.0);

            $table->text('video')->nullable();

            $table->longText('content')->nullable();

            $table->integer('available_weekday');

            $table->tinyInteger('status')->default(1);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
