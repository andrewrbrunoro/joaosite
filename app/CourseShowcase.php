<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseShowcase extends Model
{

    use SoftDeletes;

    public function getShowcases()
    {
        $showcases = [];
        try {

            if (!cache()->has("showcases")) {
                # Procuro as vitrines disponíveis
                # A vitrine só vai existir SE ( Existir cursos na relação E que os cursos tenham turmas como PRINCIPAL )
                $showcases = $this->has('VitrineCursos')
                    ->with(['VitrineCursos' => function ($q) {
                        $q->with(["Turma" => function ($q) {
                            $q->select(["id", "course_id", "team_type_id", "name", "slug", "price", "sale"]);
                        }])->select(["courses.id", "name", "slug"]);
                    }])->get(["id", "name"]);

                # Instâncio uma collect para poder manipular os dados com facilidade
                $data = collect([]);
                # Faço a leitura nas vitrines
                foreach ($showcases as $showcase) {
                    $courses = [];
                    # Leitura do Curso que está na relação
                    foreach ($showcase->VitrineCursos as $row) {
                        # Pra diminuir o uso de instância, eu seto os dados da turma na variável "team"
                        $team = $row->Turma;
                        # Crio o array com os dado do curso
                        $courses[] = [
                            "titulo" => $row->name,
                            "imagem" => "http://davi-andre.fd/assets/images/cursos/edital-concurso-policia-civil-ma.jpg",
                            "valor_label" => "R$ <strong>{$team->buy_price}</strong>",
                            "tipo" => $team->TeamType->name,
                            "q_aulas" => "{$team->countLessons} aulas",
                            "link" => url("{$row->slug}/{$team->TeamType->slug}/{$team->slug}")
                        ];
                    }
                    $data->push([
                        'id' => $showcase->id,
                        'curso' => $showcase->name,
                        'turmas' => $courses
                    ]);
                }

                cache()->put("showcases", $data, Carbon::now()->addDay());
                $showcases = $data;
            } else {
                $showcases = cache("showcases");
            }

        } catch (\Exception $e) {
            # Avisa que deu um erro e que em breve estara consertado
            # A probabilidade de cair na exception é quase nula
            session()->flash("error", ":( Um erro inesperado aconteceu, mas, estamos resolvendo o mais rápido possível.");
        }

        return $showcases;
    }

    public function VitrineCursos()
    {
        return $this->belongsToMany(Course::class, 'course_showcase_teams', 'course_showcase_id', 'course_id')
            ->has('Turma')
            ->with('Turma')
            ->whereNull('course_showcase_teams.deleted_at');
    }

}
