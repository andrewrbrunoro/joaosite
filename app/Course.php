<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{

    use SoftDeletes;

    public function Turma()
    {
        return $this->hasOne(Team::class, 'course_id', 'id')->wherePrincipal(1);
    }

    public function Turmas()
    {
        return $this->hasMany(Team::class, 'team_id');
    }

}
