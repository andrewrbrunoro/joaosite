<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LessonView extends Model
{

    protected $fillable = ["user_id", "team_id", "lesson_id", "quantity"];

    /**
     * @param $lesson_id
     * @param $team_id
     *
     * Cria uma visualização (útlimo acesso)
     */
    public function incrementView($lesson_id, $team_id)
    {
        $view = $this->where([
            'team_id' => $team_id,
            'user_id' => auth()->user()->id,
            'lesson_id' => $lesson_id
        ])->first();

        if ($view) {
            $view->update([
                'quantity' => $view->quantity + 1
            ]);
        } else {
            $this->create([
                'team_id' => $team_id,
                'user_id' => auth()->user()->id,
                'lesson_id' => $lesson_id,
                'quantity' => 1
            ]);
        }

    }

    /**
     * @param $user_id
     * @param $team_id
     * @param int $qt
     * @return mixed
     *
     * Pega os últimos 3 acessos registrado
     */
    public function lastAccess($user_id, $team_id, $qt = 3)
    {
        return $this->where([
            'team_id' => $team_id,
            'user_id' => $user_id
        ])->orderBy('created_at', 'desc')
            ->limit(3)
            ->get();
    }

    public function views($user_id, $team_id, $lesson_id)
    {
        $views = $this->where([
            'team_id' => $team_id,
            'user_id' => $user_id,
            'lesson_id' => $lesson_id
        ])->first(['quantity']);
        return $views ? $views->quantity : 0;
    }

    public function Lesson()
    {
        return $this->belongsTo(Lesson::class, 'lesson_id');
    }
}
