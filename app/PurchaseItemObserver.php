<?php

namespace App;

class PurchaseItemObserver
{
    private $purchase;

    private $total = 0.00;

    private $sub_total = 0.00;

    public function sumTotal(float $total = 0.00, $quantity = 1): self
    {
        $this->total += ($total * (is_null($quantity) ? 1 : $quantity));
        return $this;
    }

    public function subTotal(float $total = 0.00, $quantity = 1): self
    {
        $this->total -= ($total * (is_null($quantity) ? 1 : $quantity));
        return $this;
    }

    public function sumSubTotal(float $subTotal = 0.00, $quantity = 1): self
    {
        $this->sub_total += ($subTotal * (is_null($quantity) ? 1 : $quantity));
        return $this;
    }

    public function subSubTotal(float $subTotal = 0.00, $quantity = 1): self
    {
        $this->sub_total -= ($subTotal * (is_null($quantity) ? 1 : $quantity));
        return $this;
    }

    public function setPurchase(Purchase $purchase): self
    {
        $this->purchase = $purchase;
        return $this;
    }

    public function created(PurchaseItem $purchaseItem)
    {
        # Seta o pedido
        $this->setPurchase($purchaseItem->Purchase);

        # Adiciona os valores que o pedido já tem
        $this->sumTotal($this->purchase->total);
        $this->sumSubTotal($this->purchase->sub_total);

        # Adiciona os valores do item
        $this->sumTotal(coinToBco($purchaseItem->original_price), $purchaseItem->quantity);
        $this->sumSubTotal(coinToBco($purchaseItem->price), $purchaseItem->quantity);

        # Salva os novos valores do pedido
        $this->purchase->update([
            'total' => $this->total,
            'sub_total' => $this->sub_total
        ]);
    }

    public function deleted(PurchaseItem $purchaseItem)
    {
        # Seta o pedido
        $this->setPurchase($purchaseItem->Purchase);

        # Adiciona os valores que o pedido já tem
        $this->sumTotal($this->purchase->total);
        $this->sumSubTotal($this->purchase->sub_total);

        # Remove os valores do item
        $this->subTotal(coinToBco($purchaseItem->original_price), $purchaseItem->quantity);
        $this->subSubTotal(coinToBco($purchaseItem->price), $purchaseItem->quantity);

        # Salva os novos valores do pedido
        $this->purchase->update([
            'total' => $this->total,
            'sub_total' => $this->sub_total
        ]);
    }
}
