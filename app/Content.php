<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{

    use SoftDeletes;

    /**
     * @param $value
     * @return \Illuminate\Contracts\Routing\UrlGenerator|mixed|string
     *
     * Faço o tratamento automático da informação que vai ir
     *
     * Se for aqruivo, adiciona a URL completa
     */
    public function getContentAttribute($value)
    {
        return $this->ContentType->element_type === "file" ? url($value) : $value;
    }

    public function ContentType()
    {
        return $this->belongsTo(ContentType::class, 'content_type_id');
    }

}
