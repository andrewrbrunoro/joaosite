<?php

namespace App;

use App\Traits\Price;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{

    use SoftDeletes, Price;

    protected $fillable = [
        "user_id", "user_create_id", "payment_method_id", "cookie",
        "sub_total", "total", "total_with_coupon", "closed"
    ];

    public static function boot()
    {
        parent::boot();

        static::created(function ($model) {
            session(["purchase_id" => $model->id]);
        });
    }

    public function PurchaseItem()
    {
        return $this->hasMany(PurchaseItem::class, "purchase_id");
    }

}
