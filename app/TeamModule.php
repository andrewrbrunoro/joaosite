<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TeamModule extends Model
{

    public function teamCountLessons($team_id)
    {
        try {
            $cache_key = "{$team_id}_count_lessons";
            # Verifico se não existe um cache que faz a contagem
            if (!cache()->has($cache_key)) {
                # Procuro todos os módulos referente a turma e trago apenas os ids
                $modules = $this->where('team_id', '=', $team_id)
                    ->whereRaw("( 
                        ( DATE(`release_date`) <= DATE(?) OR `release_date` IS NULL ) 
                            AND 
                        ( DATE(`expiration_date`) >= DATE(?) OR `expiration_date` IS NULL ) 
                    )", [Carbon::now(), Carbon::now()])
                    ->pluck('module_id');

                # Conto quantas aulas existem
                $count = ModuleLesson::whereIn('module_id', $modules)->count();

                # Crio o cache da quantidade
                cache()->put($cache_key, $count, Carbon::now()->addDay());

            } else {
                $count = cache($cache_key);
            }
        } catch (\Exception $e) {
            $count = 0;
        }
        return $count;
    }

}
