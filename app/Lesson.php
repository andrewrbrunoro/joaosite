<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lesson extends Model
{

    use SoftDeletes;

    /**
     * @param $slug
     * @return mixed
     *
     * Procuro a aula pelo slug
     */
    public function getBySlug($slug)
    {
        try {

            # Crio a chave para o cache
            $cache_key = "lesson_{$slug}";
            # Se o cache não existir
            if (!cache()->has($cache_key)) {
                # Procuro a Aula referente ao slug
                $lesson = $this->whereSlug($slug)->first();
                # Se não existir, retorna null
                if (!$lesson)
                    return null;
                # Grava a Aula no cache
                cache()->put($cache_key, $lesson, Carbon::now()->addHour());
            } else {
                # O cache já existir, retorna com a Aula
                $lesson = cache($cache_key);
            }

        } catch (\Exception $e) {
            # Um erro aconteceu, retorna nulo
            return null;
        }
        return $lesson;
    }

}
