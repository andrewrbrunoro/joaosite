<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{

    use SoftDeletes;

    /**
     * @return array
     * Dados formatado para retornar de acordo com as Regras do Pagseguro
     */
    public function getPagseguroUserData()
    {
        if (auth()->check()) {
            $user = auth()->user();
            $phone = collect(str_phone($user->phone));
            return [
                "senderName" => str_limit($user->name, 50, ''),
                "senderCPF" => str_to_number($user->cpf_cnpj),
                "senderAreaCode" => $phone->get('ddd'),
                "senderPhone" => $phone->get('phone'),
                "senderEmail" => $user->email
            ];
        }
        return [];
    }

    /**
     * @return array
     * Dados formatado para retornar de acordo com as Regras do Pagseguro
     */
    public function getPagseguroShippingData()
    {
        if (auth()->check() && auth()->user()->UserAddress) {
            $address = auth()->user()->UserAddress;
            return [
                "shippingAddressStreet" => str_limit($address->street, 80, ''),
                "shippingAddressNumber" => str_limit($address->street_number, 20, ''),
                "shippingAddressDistrict" => str_limit($address->district, 60, ''),
                "shippingAddressPostalCode" => str_to_number($address->zip_code),
                "shippingAddressCity" => str_limit($address->city, 60, ''),
                "shippingAddressState" => str_limit($address->state, 2, '')
            ];
        }
        return [];
    }

    /**
     * @return array
     * Dados formatado para retornar de acordo com as Regras do Pagseguro
     */
    public function getPagseguroBillingData()
    {
        if (auth()->check() && auth()->user()->UserAddress) {
            $address = auth()->user()->BillingAddress ? auth()->user()->BillingAddress : auth()->user()->UserAddress;
            return [
                "billingAddressStreet" => str_limit($address->street, 80, ''),
                "billingAddressNumber" => str_limit($address->street_number, 20, ''),
                "billingAddressDistrict" => str_limit($address->district, 60, ''),
                "billingAddressPostalCode" => str_to_number($address->zip_code),
                "billingAddressCity" => str_limit($address->city, 60, ''),
                "billingAddressState" => str_limit($address->state, 2, '')
            ];
        }
        return [];
    }

    public function UserAddress()
    {
        return $this->hasOne(UserAddress::class, "user_id")->whereStatus(1);
    }

    public function BillingAddress()
    {
        return $this->hasOne(UserAddress::class)->whereStatus(1)->whereCollection(1);
    }

}
