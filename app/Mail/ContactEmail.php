<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $name;

    public $email;

    public $fromEmail;

    public $ccEmails;

    public $extra;

    public $subjectEmail;

    public function __construct($name, $email, $subject, $cc = "", $extra = [])
    {
        $this->name = $name;
        $this->email = $email;
        $this->fromEmail = $email;
        $this->subjectEmail = $subject;
        $this->ccEmails = $cc;
        $this->extra = $extra;
    }


    public function build()
    {
        return $this->from($this->from)
            ->cc(explode(';', $this->ccEmails))
            ->subject("Contato - " . $this->subjectEmail)
            ->view('mails.contact');
    }
}
