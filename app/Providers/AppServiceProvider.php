<?php

namespace App\Providers;

use App\PurchaseItem;
use App\PurchaseItemObserver;
use Illuminate\Validation\Concerns\ValidatesAttributes;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    use ValidatesAttributes;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $this->customValidators();

        # Observação do Pedido
        PurchaseItem::observe(PurchaseItemObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    private function customValidators()
    {
        \Validator::extend('custom_dimensions', function ($attribute, $value, $parameters, $validator) {

            if (!$this->isValidFileInstance($value) || !$sizeDetails = @getimagesize($value->getPathname())) {
                return false;
            }

            $this->requireParameterCount(1, $parameters, 'dimensions');

            list($width, $height) = $sizeDetails;

            $parameters = $this->parseNamedParameters($parameters);

            if ($this->failsBasicDimensionChecks($parameters, $width, $height) ||
                $this->failsRatioCheck($parameters, $width, $height)) {
                return false;
            }

            return true;
        });

        \Validator::extend('cpf', function ($attribute, $value, $parameters, $validator) {
            if ($value == "" || empty($value) || is_null($value))
                return false;

            return validateCPF($value);
        });
    }
}
