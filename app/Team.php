<?php

namespace App;

use App\Traits\Price;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Schema;

class Team extends Model
{

    use SoftDeletes, Price;

    protected $appends = ["buy_price"];

    protected $fillable = ["views"];

    public static function routeRegister($route)
    {
        if (Schema::hasTable('teams')) {
            $teams = self::has('Course')->with(['Course', 'TeamType'])
                ->whereRaw("( 
                ( DATE(`release_date`) <= DATE(?) OR `release_date` IS NULL ) 
                    AND 
                ( DATE(`expiration_date`) >= DATE(?) OR `expiration_date` IS NULL ) 
            )", [Carbon::now(), Carbon::now()])
                ->whereStatus(1)
                ->get();

            foreach ($teams as $team) {
                $route->get("{$team->Course->slug}/{$team->TeamType->slug}/$team->slug", [
                    "uses" => "CourseController@show",
                    "course" => $team->Course->id,
                    "type" => $team->TeamType->id,
                    "team" => $team->id
                ]);

                $route->get("{$team->Course->slug}/{$team->TeamType->slug}/$team->slug/estudar/{lesson?}", [
                    "uses" => "LearnController@show",
                    "course" => $team->Course->id,
                    "type" => $team->TeamType->id,
                    "team" => $team->id
                ]);
            }
        }
    }

    public function getTeam($team_id, $course_id, $type_id)
    {
        try {

            # Instancio a variável cache
            $cache_key = "team_{$team_id}";
            # Se o cache desta turma não existir
            if (!cache()->has($cache_key)) {
                # Procuro pela turma e verifico se o id do curso e o tipo são referentes a relação
                $team = $this->whereHas('Course', function ($q) use ($course_id) {
                    $q->where('id', '=', $course_id);
                })->whereHas('TeamType', function ($q) use ($type_id) {
                    $q->where('id', '=', $type_id);
                })->with(['Course', 'TeamType'])
                    ->find($team_id);
                # Se não encontrar a turma
                if (!$team)
                    $team = false;
                else {
                    # Crio o cache de um dia
                    cache()->put($cache_key, $team, Carbon::now()->addDay());
                }
            } else {
                # Faço a leitura do cache
                $team = cache($cache_key);
            }
        } catch (\Exception $e) {
            $team = false;
        }
        return $team;
    }

    /**
     * @return mixed
     *
     * Retorna o verdadeiro valor do curso
     */
    public function getBuyPriceAttribute()
    {
        if ($this->sale > 0)
            $return = $this->sale;
        else {
            $return = $this->price;
        }
        return $return;
    }

    public function Course()
    {
        return $this->belongsTo(Course::class, "course_id");
    }

    public function TeamType()
    {
        return $this->belongsTo(TeamType::class, "team_type_id");
    }
}
