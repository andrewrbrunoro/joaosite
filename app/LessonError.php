<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessonError extends Model
{

    use SoftDeletes;

    protected $fillable = ["user_id", "lesson_id", "team_id", "url", "text"];

}
