<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeamUser extends Model
{

    use SoftDeletes;

    /**
     * @param $team_id
     * @param $user_id
     * @return bool
     *
     * Verifico se o usuário está cadastrado na turma
     */
    public function registration($team_id, $user_id)
    {
        # Procuro pelo usuário e turma e verifico se a data de validade dele já não expirou
        $check = $this->where([
            'team_id' => $team_id,
            'user_id' => $user_id
        ])->whereRaw('( DATE(`expiration_date`) >= DATE(?) || `expiration_date` IS NULL)', [Carbon::now()])->count();
        return $check > 0;
    }

}
