<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseError extends Model
{

    use SoftDeletes;

    protected $fillable = ["purchase_id", "user_id", "cookie", "url", "referer", "message", "exception"];

}
