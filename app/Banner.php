<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{

    use SoftDeletes;

    # Adicionar a imagem padrão, caso o banner não exista
    private $notExist = "";

    # Caminho em que se encontra os banners
    private $path = "assets/uploads/banners/";

    public $appends = ["image"];

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     *
     * Crio a URL completa da imagem do banner
     */
    public function getImageAttribute()
    {
        if (is_file(public_path($this->path . "{$this->id}/{$this->name}")))
            return url($this->path . "{$this->id}/{$this->name}");
        else {
            return url($this->notExist);
        }
    }

    public function getBanners()
    {
        # Instâncio uma variável do tipo array
        $banners = [];
        try {

            # Se não existir o cache nomeado `banners`
            if (!cache()->has('banners')) {
                # Procuro os banners que estiverem entre o intervalo de data ou se os mesmo estiverem NULL
                $banners = $this
                    ->whereRaw("( 
                        ( DATE(`release_date`) <= DATE(?) OR `release_date` IS NULL ) 
                            AND 
                        ( DATE(`expiration_date`) >= DATE(?) OR `expiration_date` IS NULL ) 
                    )", [Carbon::now(), Carbon::now()])
                    ->whereStatus(1)
                    ->get(['id', 'name', 'alt', 'url']);

                # Adiciono os banners no cache e adiciono uma hora
                cache()->put('banners', $banners, Carbon::now()->addHour(1));
            } else {
                # O Cache já existe
                $banners = cache('banners');
            }

        } catch (\Exception $e) {

            # Avisa que deu um erro e que em breve estara consertado
            # A probabilidade de cair na exception é quase nula
            session()->flash("error", ":( Um erro inesperado aconteceu, mas, estamos resolvendo o mais rápido possível.");

        }
        return $banners;
    }

}
