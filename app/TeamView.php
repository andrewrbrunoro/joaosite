<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamView extends Model
{

    protected $fillable = ["team_id"];

    # Adiciona uma visualização na turma
    public function incrementView($team_id)
    {
        $this->create(["team_id" => $team_id]);
    }

}
