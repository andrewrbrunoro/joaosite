<?php

namespace App;

use App\Traits\Price;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseItem extends Model
{

    use SoftDeletes, Price;

    protected $fillable = ["purchase_id", "table", "item_id", "name", "slug", "price", "sale", "quantity", "url"];

    /**
     * Se estiver em promoção, vai retornar o valor de promoção
     */
    public function getOriginalPriceAttribute()
    {
        if ($this->sale > 0) {
            return $this->sale;
        } else {
            return $this->price;
        }
    }

    public function getStyleValueAttribute()
    {
        $value = explode(",", $this->original_price);

        return "R$ <strong>{$value[0]},<small>{$value[1]}</small></strong>";
    }

    public function Purchase()
    {
        return $this->belongsTo(Purchase::class, 'purchase_id');
    }
}
