<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeamUserConfig extends Model
{

    use SoftDeletes;

    protected $fillable = ["user_id", "team_id", "last_lesson_id", "speed", "material_position", "lesson_status", "light"];

    public function Lesson()
    {
        return $this->belongsTo(Lesson::class, "last_lesson_id");
    }

    public function Team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
