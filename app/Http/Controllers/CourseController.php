<?php

namespace App\Http\Controllers;

use App\Course;
use App\Team;
use App\TeamModule;
use App\TeamType;
use App\TeamUser;
use App\TeamView;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CourseController extends Controller
{

    private $course_id;

    private $team_id;

    private $type_id;

    public function show(Request $request, Course $c, Team $t, TeamType $tt, TeamModule $tm, TeamView $tv, TeamUser $tu)
    {

        try {

            # Instancio na variavel o id do curso
            $this->course_id = $request->route()->getAction('course');
            # Instancio na variavel o id da turma
            $this->team_id = $request->route()->getAction('team');
            # Instancio na variavel o id do tipo
            $this->type_id = $request->route()->getAction('type');

            # Verifico a integridade do acesso
            if (!$this->team_id || !$this->course_id || !$this->type_id)
                throw new \Exception("Alguns dados não foram passado por parâmetro.");

            # Procuro a turma, porém, verifico se os ids são referentes as relações
            $team = $t->getTeam($this->team_id, $this->course_id, $this->type_id);

            # Se não existir, envia pra exceção
            if (!$team)
                throw new \Exception("Turma não encontrada Curso: {$this->course_id}, Turma: {$this->team_id}, Tipo: {$this->type_id}.");

            # Incremento uma visualização na turma
            $tv->incrementView($team->id);

            # Conta quantas aulas tem na turma
            $countLessons = $tm->teamCountLessons($this->team_id);

            # Verifico se o usuário já está matriculado
            $registration = false;
            if (!auth()->guest())
                $registration = $tu->registration($team->id, auth()->user()->id);

        } catch (\Exception $e) {
            # Seja qual for o erro informa para o usuário uma instabilidade
            return redirect()->route('front.home.index')
                ->with("error", "Ops! <br> Nosso sistema está passando por uma instabilidade, por favor, aguarde. <br> Desculpe o transtorno.");
        }

        return view("course.show", compact("team", "countLessons", "registration"));
    }

}
