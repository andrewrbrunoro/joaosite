<?php

namespace App\Http\Controllers;

use App\Helpers\PurchaseErrorHelper;
use App\Purchase;
use App\User;
use App\UserAddress;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Modules\Purchase\Helpers\PagseguroHelper;

class CartController extends Controller
{

    public function index(Purchase $p)
    {
        $data = $p->with('PurchaseItem')->where("id", "=", session("purchase_id"))
            ->first();

        return view("cart.index", compact("data"));
    }

    public function payment(Purchase $p, PagseguroHelper $ph, PurchaseErrorHelper $peh)
    {
        $data = $p->with('PurchaseItem')->where("id", "=", session("purchase_id"))
            ->first();

        if (!$data->PurchaseItem || $data->PurchaseItem->count() < 1)
            return redirect()->route("front.cart.index")->with("error", "O carrinho está vazio.");


        try {

            # Pego o id da sessão do pagseguro
            $session_js = $ph->session()
                ->getSessionID();

            # Pego a url da library de javascript do pagseguro que se encontra na posição `3`
            $pagseguro_library = $ph->getUrl(3);

        } catch (\Exception $e) {
            # Adiciono o erro no pedido
            $peh->setError($e->getMessage(), $e->getTraceAsString());
            # Retorno pra listagem do carrinho
            return redirect()->route("front.cart.index")->with("error", "Não foi possível iniciar a sessão.");
        }


        return view("cart.payment", compact("data", "session_js", "pagseguro_library"));
    }

    public function endPayment(Request $request, User $u, UserAddress $ua, PagseguroHelper $psh, Purchase $p)
    {
        try {

            $request->merge($u->getPagseguroShippingData())
                ->merge($u->getPagseguroBillingData())
                ->merge($u->getPagseguroUserData())
                ->merge(["creditCardHolderAreaCode" => $request->get("senderAreaCode"), "creditCardHolderPhone" => $request->get("senderPhone")])
                ->merge($psh->clearedCreditData($request->all()));

            $purchase = $p->find(session("purchase_id"));

            foreach ($purchase->PurchaseItem as $item) {
                $pay = $psh->setItem([
                    "item_id" => $item->item_id,
                    "name" => $item->name,
                    "price" => coinToBco($item->original_price),
                    "quantity" => $item->quantity
                ]);
            }

            $pay->validate($request->all())
                ->output();

        } catch (ClientException $e) {

            $response = $e->getResponse();
            $body = $response->getBody();

            return redirect()->back()->withErrors($psh->readErrorsXML($body->getContents()));

        } catch (\Exception $e) {
            dd("ex", $e);
            exit;
        }
    }

}
