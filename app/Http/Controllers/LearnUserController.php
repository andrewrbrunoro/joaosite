<?php

namespace App\Http\Controllers;

use App\LessonError;
use App\LessonFavorite;
use App\LessonQuestion;
use App\LessonRating;
use App\TeamUserConfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LearnUserController extends Controller
{

    public function configure(Request $request, TeamUserConfig $tuc)
    {
        $this->validate($request, ["team_id" => "required", "key" => "required", "value" => "required"]);

        # Inicia a transação
        DB::beginTransaction();
        try {
            $configExist = $tuc->where(["user_id" => auth()->user()->id, "team_id" => $request->get("team_id")]);

            if (!$configExist->count()) {
                # Crio ou pego a configuração do usuário, referente a turma
                $tuc->create(["user_id" => auth()->user()->id, $request->get('key') => $request->get('value'), "team_id" => $request->get("team_id")]);
            } else {
                # Atualizo a configuração
                $configExist->update([$request->get('key') => $request->get('value')]);
            }

            # Conclui a transação
            DB::commit();
            return response()->json(["error" => false, "message" => "Configuração setada com sucesso"], 200);
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return response()->json(['error' => true, 'message' => 'Erro ao salvar a configuração'], 400);
        }
    }

    public function lessonQuestion(Request $request, LessonQuestion $lq)
    {
        $this->validate($request, ["lesson_id" => "required", "text" => "required"]);
        # Inicia a transação
        DB::beginTransaction();
        try {

            $lq->create([
                "user_id" => auth()->user()->id,
                "lesson_id" => $request->get("lesson_id"),
                "text" => strip_tags($request->get("text"))
            ]);

            # Conclui a transação
            DB::commit();
            return response()->json(["error" => false, "message" => "Sua dúvida será respondida em breve."], 200);
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return response()->json(["error" => true, "message" => "Não foi possível enviar sua dúvida, por favor, tente novamente."], 400);
        }
    }

    public function lessonRating(Request $request, LessonRating $lr)
    {
        $this->validate($request, ["lesson_id" => "required", "rating" => "required"]);

        # Inicia a transação
        DB::beginTransaction();
        try {

            # Verifico se o usuário já não avaliou a aula
            $exist = $lr->where(["user_id" => auth()->user()->id, "lesson_id" => $request->get("lesson_id")])
                ->first();

            # Se não avaliou, cria a avaliação
            if (!$exist)
                $lr->create(["user_id" => auth()->user()->id, "lesson_id" => $request->get("lesson_id"), "points" => $request->get("rating"), "text" => $request->get("text")]);
            else {
                $exist->update(["lesson_id" => $request->get("lesson_id"), "points" => $request->get("rating"), "text" => $request->get("text")]);
            }

            # Conclui a transação
            DB::commit();
            return response()->json(["error" => false, "message" => "Sua avaliação foi computada."], 200);
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return response()->json(["error" => true, "message" => "Não foi possível enviar sua avaliação, por favor, tente novamente."], 400);
        }
    }

    public function lessonFavorite(Request $request, LessonFavorite $lf)
    {
        $this->validate($request, ["lesson_id" => "required"]);

        # Inicia a transação
        DB::beginTransaction();
        try {

            # Verifico se existe
            $exist = $lf->where([
                "team_id" => $request->get("team_id"),
                "lesson_id" => $request->get("lesson_id"),
                "user_id" => auth()->user()->id
            ]);

            # Se não existir, favorito a aula pro aluno
            if (!$exist->count()) {
                $lf->firstOrCreate([
                    "team_id" => $request->get("team_id"),
                    "lesson_id" => $request->get("lesson_id"),
                    "user_id" => auth()->user()->id
                ]);

                $message = "Aula favoritada";

                $status = true;
            } else {
                # Se existir, significa que ele está desfavoritando
                $exist->delete();

                $message = "Aula desfavoritada";

                $status = false;
            }

            # Conclui a transação
            DB::commit();
            return response()->json(["error" => false, "message" => $message, "status" => $status], 200);
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return response()->json(["error" => true, "message" => "Não foi possível adicionar aos favoritos, tente novamente."], 400);
        }
    }

    public function lessonError(Request $request, LessonError $le)
    {
        $this->validate($request, ["team_id" => "required", "url" => "required", "text" => "required"]);

        # Inicia a transação
        DB::beginTransaction();
        try {

            # Crio o erro
            $le->create([
                "user_id" => auth()->user()->id,
                "lesson_id" => $request->get("lesson_id"),
                "url" => $request->get("url"),
                "team_id" => $request->get("team_id"),
                "text" => trim(strip_tags($request->get("text")))
            ]);

            # Conclui a transação
            DB::commit();
            return response()->json(["error" => false, "message" => "Recebemos o seu problema técnico e será solucionado o mais rápido possível."], 200);
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            return response()->json(["error" => true, "message" => "Não foi possível receber seu problema, por favor, tente novamente."], 400);
        }

    }
}
