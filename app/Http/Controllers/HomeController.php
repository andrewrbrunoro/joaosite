<?php

namespace App\Http\Controllers;

use App\Banner;
use App\CourseShowcase;

class HomeController extends Controller
{

    public function index(Banner $banner, CourseShowcase $courseShowcase)
    {
        # Carrego os banners
        $banners = $banner->getBanners();

        # Carrego as vitrines
        $showcases = $courseShowcase->getShowcases();

        return view("index", compact('banners', 'showcases'));
    }

}
