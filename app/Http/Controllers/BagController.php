<?php

namespace App\Http\Controllers;

use App\Helpers\PurchaseErrorHelper;
use App\Helpers\PurchaseHelper;
use App\PurchaseItem;
use App\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BagController extends Controller
{

    /**
     * @param Request $request
     * @param Team $t
     * @param PurchaseHelper $purchaseHelper
     * @param PurchaseItem $pi
     * @return \Illuminate\Http\RedirectResponse
     *
     * Função feita para os projetos de Cursos Online
     */
    public function addTeam(Request $request, Team $t, PurchaseHelper $purchaseHelper, PurchaseItem $pi)
    {
        # Valida se está recebendo o ID da turma
        $validator = validator($request->all(), ["team_id" => "required|numeric"]);
        # Se falhar, volta avisando que não encontrou o curso
        if ($validator->fails())
            return redirect()->back()->with("error", "Curso não encontrado.");

        # Procuro a turma pelo ID
        $team = $t->find($request->get("team_id"));
        # Se não existir, retorna para a página anterior
        if (!$team)
            return redirect()->back()->with("error", "Curso não encontrado.");

        # Crio ou utilizo um que já existe, mas, adiciono o item no carrinho
        $purchase = $purchaseHelper->setItem([
            "table" => "teams",
            "item_id" => $team->id,
            "name" => "{$team->Course->name} - {$team->name}",
            "slug" => $team->slug,
            "price" => $team->price,
            "sale" => $team->sale,
            "url" => url("{$team->Course->slug}/{$team->TeamType->slug}/{$team->slug}")
        ]);

        # Se existir alguma falha no helper, avisa o primeiro erro ~ Olhar a HELPER ~
        if ($purchase->fails()) {
            return redirect()->back()->with("error", $purchase->getErrors()->first());
        } else {
            return redirect()->route("front.cart.index")->with("success", "O Curso {$team->Course->name} foi adicionado no seu carrinho.");
        }
    }

    /**
     * @param Request $request
     * @param PurchaseItem $pi
     * @param PurchaseHelper $ph
     * @return \Illuminate\Http\RedirectResponse
     *
     * Remove o item do pedido
     */
    public function removeItem(Request $request, PurchaseItem $pi, PurchaseHelper $ph)
    {
        $validator = validator($request->all(), ["item_id" => "required|numeric"]);
        if ($validator->fails()) {
            return redirect()->back()->with("error", "Não foi possível remover o item do carrinho, estamos solucionando o problema o mais rápido possível. <br> Tente novamente em alguns minutos.");
        }

        # Verifico se o Item existe no pedido
        $item = $pi->where([
            "purchase_id" => session("purchase_id")
        ])->find($request->get("item_id"));

        # Se não existir, retorna
        if (!$item)
            return redirect()->back()->with("error", "Registro não encontrado.");
        # Remove o item do pedido
        $ph->removeItem($item->id, session("purchase_id"));
        # Se existir falhas na hora da remoção, retorna e avisa o erro
        if ($ph->fails()) {
            return redirect()->back()->with("error", $ph->getErrors()->first());
        } else {
            return redirect()->route("front.cart.index")->with("success", "O Curso foi removido do carrinho.");
        }
    }

}
