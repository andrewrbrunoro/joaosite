<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use Modules\Sac\Jobs\LeadJob;

class ContactController extends Controller
{

    # Define qual categoria será inserido o LEAD
    private $category = "contato";

    public function index()
    {
        return view("contact");
    }

    public function store(ContactRequest $request)
    {
        dispatch(new LeadJob($request->get("nome"), $request->get("email"), $request->get("assunto"), $this->category, $request->except(["nome", "_token", "email"])));
        return redirect()->route("front.contact.index")->with("success", "Em breve entraremos em contato com você.");
    }

}
