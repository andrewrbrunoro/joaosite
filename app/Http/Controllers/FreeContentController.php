<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Http\Request;

class FreeContentController extends Controller
{

    public function index(Team $t)
    {
        $degustacoes = $t->has('Course')->with('Course')
            ->where([
                "price" => 0,
                "sale" => 0,
                "status" => 1
            ])->get();

        return view("course.free", compact('degustacoes'));
    }

}
