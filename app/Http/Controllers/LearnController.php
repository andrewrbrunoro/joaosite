<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\LessonContent;
use App\LessonFavorite;
use App\LessonQuestion;
use App\LessonRating;
use App\LessonView;
use App\ModuleLesson;
use App\Team;
use App\TeamModule;
use Illuminate\Http\Request;

class LearnController extends Controller
{

    private $course_id;

    private $team_id;

    private $type_id;

    public function show(Request $request, Team $t, TeamModule $tm, ModuleLesson $ml, LessonFavorite $lf, LessonContent $lc, Lesson $l, LessonView $lv, LessonRating $lr)
    {

        try {

            # Instancio na variavel o id do curso
            $this->course_id = $request->route()->getAction('course');
            # Instancio na variavel o id da turma
            $this->team_id = $request->route()->getAction('team');
            # Instancio na variavel o id do tipo
            $this->type_id = $request->route()->getAction('type');

            # Verifico a integridade do acesso
            if (!$this->team_id || !$this->course_id || !$this->type_id)
                throw new \Exception("Alguns dados não foram passado por parâmetro.");

            # Procuro a turma, porém, verifico se os ids são referentes as relações
            $team = $t->getTeam($this->team_id, $this->course_id, $this->type_id);

            # Se não existir, envia pra exceção
            if (!$team)
                throw new \Exception("Turma não encontrada Curso: {$this->course_id}, Turma: {$this->team_id}, Tipo: {$this->type_id}.");

            # Lista os favoritos do usuário, nesta turma
            $favorites = $lf->getTeamUserFavorites(auth()->user()->id, $team->id);

            $modules = $tm->where('team_id', '=', $this->team_id)
                ->pluck('module_id')
                ->toArray();

            $lessons_ids = $ml->whereIn('module_id', $modules)
                ->pluck('lesson_id')
                ->toArray();

            $lessonContents = $lc->has('Content')
                ->has('Lesson')
                ->with(['Content', 'Lesson'])
                ->whereIn('lesson_id', $lessons_ids)
                ->orderBy('order', 'asc')
                ->get();

            $lessons = collect([]);
            foreach ($lessonContents as $index => $lessonContent) {
                $countView = $lv->views(auth()->user()->id, $team->id, $lessonContent->Lesson->id);
                $lessons->push([
                    "id" => $lessonContent->Lesson->id,
                    "aula" => $lessonContent->Lesson->name,
                    "aula-slug" => $lessonContent->Lesson->slug,
                    "professor" => "Professor Teste",
                    "assistido" => $countView > 0,
                    "qtdes_assistido" => $countView,
                    "favorito" => in_array($lessonContent->Lesson->id, $favorites),
                    "url" => url("{$team->Course->slug}/{$team->TeamType->slug}/{$team->slug}/estudar/{$lessonContent->Lesson->id}"),
                    "video" => $lessonContent->Content->video,
                    "material" => $lessonContent->Content->content
                ]);
            }
            $learn['nome'] = $team->Course->name;
            $learn['tipo'] = $team->TeamType->name;

            if (!$request->route()->hasParameter('lesson')) {
                $learn['atual'] = $lessons->first();
                $learn['aulas'] = $lessons->except(0)->toArray();
            } else {

                $lesson_id = $request->route()->parameter('lesson');

                $actualLesson = $lessons->filter(function ($q) use ($lesson_id) {
                    return $q['id'] == $lesson_id;
                })->first();

                $learn['atual'] = $actualLesson;
                $learn['aulas'] = $lessons->filter(function ($q) use ($lesson_id) {
                    return $q['id'] != $lesson_id;
                })->toArray();
            }

            $learn = collect($learn);

            # Adiciono uma visualização para gerar histórico de acesso
            $lv->incrementView($learn['atual']['id'], $team->id);

            # Lista os 3 últimos acessos
            $lastAccesses = $lv->lastAccess(auth()->user()->id, $team->id);

            # ID DA AULA ATUAL
            $lesson_id = $learn['atual']['id'];

            # SLUG DA AULA ATUAL
            $lesson_slug = $learn['atual']['aula-slug'];

            # Avaliações disponiveis
            $availableRatings = $lr->getRatings();

            # Verifico se já foi avaliado
            $lessonEvaluated = $lr->hasEvaluated(auth()->user()->id, $lesson_id);

        } catch (\Exception $e) {
            # Seja qual for o erro informa para o usuário uma instabilidade
            return redirect()->route('front.home.index')
                ->with("error", "Ops! <br> Nosso sistema está passando por uma instabilidade, por favor, aguarde. <br> Desculpe o transtorno.");
        }

        return view("learn.show", compact("team", "learn", "lastAccesses", "lesson_id", "lesson_slug", "lessonEvaluated", "availableRatings"));
    }

    public function questionGallery($slug, Request $request, LessonQuestion $lq, Lesson $l)
    {
        # Procuro a Aula referente ao SLUG
        $lesson = $l->getBySlug($slug);
        # Não existe, envia de volta de onde veio
        if (!$lesson)
            return redirect()->back()->with("error", "Registro não encontrado.");

        # Procuro as dúvidas referente a aula
        $gallery = $lq->has('Lesson')
            ->with('Lesson')
            ->where('lesson_id', '=', $lesson->id)
            ->whereRaw('( `status` = 1 || `user_id` = ? )', [auth()->user()->id]);

        # Se o usuário pesquisar por uma informação
        if ($request->filled('pesquisar')) {
            $search = strip_tags($request->get('pesquisar'));
            $gallery = $gallery->whereRaw(' ( `answer` LIKE "%' . $search . '%" || `text` LIKE "%' . $search . '%"  ) ');
        }

        # Retorna todos as dúvidas
        $gallery = $gallery->get();
        return view("learn.question-gallery", compact("gallery", "lesson"));
    }

}
