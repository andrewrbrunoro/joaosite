<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * @param LoginRequest $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * Faz a autenticação do usuário
     */
    public function login(LoginRequest $request)
    {
        # Valido os dados do usuário e verifico se ele está ativo (status)
        if (auth()->attempt(['email' => $request->get('email'), 'password' => $request->get('password'), 'status' => 1])) {
            $user = auth()->user();
            return redirect()->back()->with("success", "Olá {$user->name}, <br> * Nunca em hipotese alguma nós iremos pedir sua senha.");
        } else {
            return redirect()->route("front.home.index")
                ->with("error", "Verifique se digitou os dados corretamente.")
                ->withInput($request->only(['email']))
                ->withErrors([
                'email' => 'Verifique se digitou o e-mail corretamente',
                'password' => 'Verifique se digitou a senha corretamente'
            ]);
        }
    }

    public function logout()
    {
        # Elimino o UUID do usuário do token
        cookie()->forget("uuid");
        # Remove a sessão
        auth()->logout();
        # Redireciona pra home
        return redirect()->route("front.home.index")->with("success", "Você foi desconectado com sucesso.");
    }
}
