<?php

namespace App\Http\Middleware;

use App\Purchase;
use Closure;

class HasPurchase
{
    protected $Purchase = Purchase::class;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session()->has("purchase_id")) {

            $exist = app($this->Purchase)->where('closed', '=', 0)
                ->whereId(session("purchase_id"))
                ->count();

            if (!$exist)
                return redirect()->back()->with("error", "O carrinho está vazio.");
        } else {
            return redirect()->back()->with("error", "O carrinho está vazio.");
        }
        return $next($request);
    }
}
