<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseShowcaseTeam extends Model
{

    use SoftDeletes;

    public function Curso()
    {
        return $this->belongsTo(Course::class, "course_id");
    }

}
