<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessonFavorite extends Model
{

    protected $fillable = ["user_id", "team_id", "lesson_id"];

    public function getTeamUserFavorites($user_id, $team_id)
    {
        return $this->where([
            'user_id' => $user_id,
            'team_id' => $team_id
        ])->pluck('lesson_id')->toArray();
    }

}
