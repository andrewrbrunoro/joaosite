<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessonQuestion extends Model
{

    use SoftDeletes;

    protected $fillable = ["user_id", "lesson_id", "text", "status"];

    public function Lesson()
    {
        return $this->belongsTo(Lesson::class, 'lesson_id');
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function Teacher()
    {
        return $this->belongsTo(User::class, 'user_answer_id');
    }
}
