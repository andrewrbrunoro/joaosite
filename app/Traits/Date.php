<?php

namespace App\Traits;

/**
 * Trait Contents
 * @package App\Traits
 *
 * Gerencia todos os campos que são referentes a data e que necessitam de tratamento
 *
 * Caso a MODEL tenha campos na tabela que são refernte a mesma ação, adiciona a função com o nome do campo set{Campo}Attribute
 */
trait Date
{

    /**
     * @param $value
     * @return string
     *
     * Converte a data SQL para a da configuração
     */
    public function getReleaseDateAttribute($value)
    {
        if ($value)
            return dateToSql($value, 'Y-m-d H:i:s', config('app.date_format'));
        return "";
    }

    /**
     * @param $value
     * @return string
     *
     * Converte a data SQL para a da configuração
     */
    public function getExpirationDateAttribute($value)
    {
        if ($value)
            return dateToSql($value, 'Y-m-d H:i:s', config('app.date_format'));
        return "";
    }

    /**
     * @param $value
     *
     * Converto a data para a da configuração
     */
    public function setReleaseDateAttribute($value)
    {
        if ($value && typeDate($value, config('app.date_format')))
            $this->attributes['release_date'] = dateToSql($value, config('app.date_format'));
        else {
            $this->attributes['release_date'] = null;
        }
    }

    /**
     * @param $value
     *
     * Converto a data para a da configuração
     */
    public function setExpirationDateAttribute($value)
    {
        if ($value && typeDate($value, config('app.date_format')))
            $this->attributes['expiration_date'] = dateToSql($value, config('app.date_format'));
        else {
            $this->attributes['expiration_date'] = null;
        }
    }

}