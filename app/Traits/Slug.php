<?php

namespace App\Traits;

/**
 * Trait Slug
 * @package App\Traits
 *
 * Gerencia todos os campos que exigem virar uma slug
 *
 * Caso a MODEL tenha campos na tabela que são refernte a mesma ação, adiciona a função com o nome do campo set{Campo}Attribute
 */
trait Slug
{

    /**
     * @param $value
     *
     * Cria o SLUG de acordo com o objeto que está requisitando
     */
    public function setNameAttribute($value)
    {
        # Seta o slug no atributo para salvar
        $this->attributes['slug'] = slugToMe($value, $this);
        # Seta o nome tratado no atributo
        $this->attributes['name'] = $value;
    }

}