<?php

namespace App\Traits;

/**
 * Trait Contents
 * @package App\Traits
 *
 * Gerencia todos os campos que são referentes a texto e que necessitam de tratamento
 *
 * Caso a MODEL tenha campos na tabela que são refernte a mesma ação, adiciona a função com o nome do campo set{Campo}Attribute
 */
trait Contents
{

    private $maxLength = 255;

    /**
     * @param $value string
     * Por padrão um resumo não pode passar de 255 caracteres
     *
     * Limpa a variável de qualquer TAG HTML
     */
    public function setResumeAttribute($value)
    {
        if ($value)
            $this->attributes['resume'] = substr(trim(strip_tags($value)), 0, $this->maxLength);
    }

}