<?php

namespace App\Traits;

/**
 * Trait Contents
 * @package App\Traits
 *
 * Gerencia todos os campos que são referentes a texto e que necessitam de tratamento e precisam ser limpos
 *
 */
trait TextClear
{

    /**
     * @param $value string
     * Por padrão um resumo não pode passar de 255 caracteres
     *
     * Limpa a variável de qualquer TAG HTML
     */
    public function setTextAttribute($value)
    {
        if ($value)
            $this->attributes['text'] = trim(strip_tags($value));
    }

    /**
     * @param $value string
     * Por padrão um resumo não pode passar de 255 caracteres
     *
     * Limpa a variável de qualquer TAG HTML
     */
    public function setAnswerAttribute($value)
    {
        if ($value)
            $this->attributes['answer'] = trim(strip_tags($value));
    }

}