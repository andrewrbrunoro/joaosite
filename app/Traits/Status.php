<?php

namespace App\Traits;

trait Status
{

    /**
     * @return string
     *
     * Pega o status e transforma em um HTML
     */
    public function getStatusLabelAttribute()
    {
        switch ($this->status) {
            case 0:
                return '<label class="badge badge-danger">Inativo</label>';
                break;
            case 1:
                return '<label class="badge badge-primary">Ativo</label>';
                break;
            default:
                return '<label class="badge badge-danger">Inativo</label>';
                break;
        }
    }
}