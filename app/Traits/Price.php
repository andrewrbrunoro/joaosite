<?php

namespace App\Traits;

/**
 * Trait Price
 * @package App\Traits
 *
 * Caso a model tenha como base "price, sale" a trait irá adicionar por padrão.
 *
 * Para utilizar essa trait, basta chamar na model.
 *
 * Caso a MODEL tenha campos na tabela que são refernte a mesma ação, adiciona a função com o nome do campo set{Campo}Attribute
 */
trait Price
{

    /**
     * @param $value
     *
     * Recebe o preço e valida se o mesmo é um valor na moeda REAL 0,00
     */
    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = coinToBco($value);
    }

    /**
     * @param $value
     *
     * Recebe a promoção e valida se o mesmo é um valor na moeda REAL 0,00
     */
    public function setSaleAttribute($value)
    {
        $this->attributes['sale'] = coinToBco($value);
    }

    /**
     * @param $value
     * @return string
     *
     * Retorna o valor no formato moeda REAL 0,00
     */
    public function getPriceAttribute($value)
    {
        return bcoToCoin($value);
    }

    /**
     * @param $value
     * @return string
     *
     * Retorna o valor no formato moeda REAL 0,00
     */
    public function getSaleAttribute($value)
    {
        return bcoToCoin($value);
    }

    /**
     * @return mixed
     *
     * Retorna o valor no formato do database
     */
    public function getPriceDBAttribute()
    {
        return coinToBco($this->price);
    }

    /**
     * @return mixed
     *
     * Retorna o valor no formato do database
     */
    public function getSaleDBAttribute()
    {
        return coinToBco($this->sale);
    }

    /**
     * @return mixed
     *
     * Converte o float para moeda Real R$
     */
    public function getTotalBrlAttribute()
    {
        return bcoToCoin($this->total);
    }

    /**
     * @return mixed
     *
     * Converte o float para moeda Real R$
     */
    public function getTotalWithCouponBrlAttribute()
    {
        return bcoToCoin($this->total_with_coupon);
    }

    /**
     * @return mixed
     *
     * Converte o float para moeda Real R$
     */
    public function getSubTotalBrlAttribute()
    {
        return bcoToCoin($this->sub_total);
    }

    /**
     * @param $value
     *
     * Converte a moeda Real para float decimal 10,2
     */
    public function setTotalAttribute($value)
    {
        $this->attributes['total'] = coinToBco($value);
    }

    /**
     * @param $value
     *
     * Converte a moeda Real para float decimal 10,2
     */
    public function setSubTotalAttribute($value)
    {
        $this->attributes['sub_total'] = coinToBco($value);
    }

    /**
     * @param $value
     *
     * Converte a moeda Real para float decimal 10,2
     */
    public function setTotalWithDiscountAttribute($value)
    {
        $this->attributes['total_with_discount'] = coinToBco($value);
    }
}