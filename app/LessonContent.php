<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessonContent extends Model
{

    use SoftDeletes;

    public function Lesson()
    {
        return $this->belongsTo(Lesson::class, 'lesson_id');
    }

    public function Content()
    {
        return $this->belongsTo(Content::class, 'content_id');
    }

}
