<?php

namespace App;

use App\Traits\TextClear;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessonRating extends Model
{
    use SoftDeletes, TextClear;

    protected $fillable = ["user_id", "lesson_id", "points", "text"];

    public function hasEvaluated($user_id, $lesson_id)
    {
        return $this->where([
            'user_id' => $user_id,
            'lesson_id' => $lesson_id
        ])->first(["points", "text"]);
    }

    public function getRatings()
    {
        return [
            [
                "name" => "Ruim",
                "value" => 1,
                "icon" => "ruim.svg"
            ],
            [
                "name" => "Regular",
                "value" => 2,
                "icon" => "regular.svg"
            ],
            [
                "name" => "Bom",
                "value" => 3,
                "icon" => "bom.svg"
            ],
            [
                "name" => "Ótima",
                "value" => 4,
                "icon" => "otima.svg"
            ]
        ];
    }

}
