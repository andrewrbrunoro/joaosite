<?php

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

/**
 * PHP version 7.x
 *
 * Class Redactor
 * @package App\Helpers
 *
 * Helper que ajuda a manipular arquivos e imagens do Redactor
 */
class Redactor
{
    /**
     * @var
     *
     * Data em que está rodando o diretório
     */
    private $dirDate;

    /**
     * @var int
     *
     * Qualidade da imagem salva
     */
    private $quality = 80;

    /**
     * @var
     * Diretório que será manipulado
     */
    private $dir = 'assets/uploads';

    /**
     * @var
     *
     * Nome do arquivo
     */
    private $name;

    /**
     * @var array
     *
     * Regras para inserir a imagem
     */
    private $validate_image = ['file' => 'required|mimes:jpeg,jpg,png'];

    private $validate_file = ['file' => 'required|mimes:pdf,doc,xlsx,xlx'];

    /**
     * @var
     *
     * Gerencia a requisição
     */
    private $request;

    /**
     * Redactor constructor.
     * @param Request $request
     *
     * Carrega a data para o diretório
     * Seta na classe a requisição feita pelo servidor
     */
    public function __construct(Request $request)
    {
        $this->dirDate = Carbon::now()->format('d-m-Y');
        $this->dir = $this->dir . '/' . $this->dirDate;
        $this->request = $request;
    }

    /**
     * @param int $value
     * @return self
     *
     * Seta a qualidade
     */
    public function setQuality(int $value): self
    {
        $this->quality = $value;
        return $this;
    }

    /**
     * @return int
     *
     * Retorna a qualidade
     */
    public function getQuality(): int
    {
        return $this->quality;
    }

    /**
     * @param string $dir
     * @param bool $dirDate
     * @return Redactor
     *
     * Seta o diretório
     */
    public function setDir(string $dir, bool $dirDate = true): self
    {
        if ($dirDate)
            $this->dir = $dir . '/' . $this->dirDate;
        else {
            $this->dir = $dir;
        }
        return $this;
    }

    /**
     * @return string
     *
     * Retorna o diretório que está setado na classe
     */
    public function getDir(): string
    {
        return $this->dir;
    }

    /**
     * @return bool
     *
     * Verifica se o diretório setado na classe é um diretório
     */
    public function isDir()
    {
        return is_dir(public_path($this->getDir()));
    }

    /**
     * @return string
     *
     * Retorna o diretório com o public_path
     */
    public function getPublicPath(): string
    {
        return public_path($this->getDir());
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     *
     * Retorna o link total do arquivo
     */
    public function getLink()
    {
        return url($this->getDir() . '/' . $this->getFileName());
    }

    /**
     * @param string $value
     * @return self
     *
     * Seta o nome do arquivo
     */
    public function setFileName(string $value): self
    {
        try {
            # limpa a string
            list($file_name, $extension) = explode('.', $value);
            $value = str_slug($file_name, '-') . ".{$extension}";

            # contador
            $i = 0;
            do {
                # Se o contador for maior que 0 (zero), renomeia a imagem
                if ($i) {
                    $name = str_replace('.', "-{$i}.", $value);
                } else {
                    $name = $value;
                }
                # incrementa +1
                $i++;
                # Enquanto o arquivo existir
            } while (is_file($this->getPublicPath() . '/' . $name));

            # Seta o nome do arquivo
            $this->name = $name;
        } catch (\Exception $e) {
        }
        return $this;
    }

    /**
     * @return mixed
     *
     * Retorna o valor
     */
    public function getFileName()
    {
        return $this->name;
    }

    /**
     * @param string $dir
     *
     * Cria o diretório, se não existir...
     */
    public function createDirectory(string $dir = '')
    {
        try {
            # Se passar um diretório, adiciona na classe, senão, utiliza o default
            if (!empty($dir)) $this->setDir($dir);

            # Se não for um diretório, cria
            if (!$this->isDir())
                mkdir($this->getPublicPath(), 755, true);

        } catch (\Exception $e) {
        }
    }

    /**
     * @param string $input
     * @param array $proportions
     *
     * Manipula a imagem que vem pelo request
     */
    public function requestIntervationImage(string $input = 'file', array $proportions = [])
    {
        try {
            # Se o campo não está sendo passado, informar erro
            if (!$this->request->hasFile($input))
                throw new \Exception("O Campo {$input}, não foi encontrado.");

            # Cria o diretório ou pega
            $this->createDirectory();

            # Seta o nome da imagem
            $this->setFileName($this->request->file($input)->getClientOriginalName());

            # Move o arquivo para o diretório como temporário e em seguida, ajusta a qualidade dele
            $temp_name = "temp_" . $this->getFileName();
            $this->request->file($input)->move($this->getPublicPath(), $temp_name);

            # Salva a imagem, com a qualidade setada
            Image::make($this->getPublicPath() . '/' . $temp_name)
                ->save($this->getPublicPath() . '/' . $this->getFileName(), $this->getQuality());

            # Se vier ratios, faz a leitura e cria as imagens referente ao ratio
            if (count($proportions)) {
                foreach ($proportions as $prop) {
                    if (isset($prop['width']) || isset($prop['height'])) {

                        # Caso não seja passado largura ou altura, seto como NULL
                        $width = isset($prop['width']) ? $prop['width'] : null;
                        $height = isset($prop['height']) ? $prop['height'] : null;

                        # Resize na imagem, referente as proporções passadas
                        Image::make($this->getPublicPath() . '/' . $temp_name)->resize($width, $height, function ($c) {
                            $c->aspectRatio();
                            $c->upsize();
                        })->save($this->getPublicPath() . "/{$prop['name']}-" . $this->getFileName(), 100);
                    }
                }
            }
            # Remove o arquivo temporário
            unlink($this->getPublicPath() . '/' . $temp_name);

        } catch (\Exception $e) {
        }
    }

    /**
     * @param string $input
     *
     * Manipula a imagem que vem pelo request
     */
    public function requestFile(string $input = 'file')
    {
        try {
            # Se o campo não está sendo passado, informar erro
            if (!$this->request->hasFile($input))
                throw new \Exception("O Campo {$input}, não foi encontrado.");

            # Cria o diretório ou pega
            $this->createDirectory();

            # Seta o nome do arquivo
            $this->setFileName($this->request->file($input)->getClientOriginalName());

            # Salva a imagem, com a qualidade setada
            $this->request->file($input)->move($this->getPublicPath(), $this->getFileName());

        } catch (\Exception $e) {
        }
    }

    /**
     * @return $this|\Illuminate\Http\JsonResponse
     *
     * Adiciona a imagem do editor
     */
    public function uploadImage()
    {
        # Verifica se a imagem é válida
        $this->request->validate($this->validate_image);
        try {
            # Salva a imagem
            $this->requestIntervationImage();

            # Seta o content como Html para a leitura do Redactor
            return response()
                ->json(['filelink' => $this->getLink()])
                ->header('Content-Type', 'text/html; charset=UTF-8');

        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => $e->getMessage()], 400);
        }
    }

    /**
     * @return $this|\Illuminate\Http\JsonResponse
     *
     * Adiciona o arquivo a um editor
     */
    public function uploadFile()
    {
        # Verifica se a imagem é válida
        $this->request->validate($this->validate_file);
        try {
            # Salva a imagem
            $this->requestFile();

            # Seta o content como Html para a leitura do Redactor
            return response()
                ->json(['filelink' => $this->getLink()])
                ->header('Content-Type', 'text/html; charset=UTF-8');

        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => $e->getMessage()], 400);
        }
    }

}