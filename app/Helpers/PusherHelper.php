<?php

namespace App\Helpers;


use Modules\User\Entities\Group;
use Modules\User\Entities\User;
use Pusher\Pusher;

class PusherHelper extends Pusher
{

    # Configurações do Pusher
    private $PUSHER_APP_ID;
    private $PUSHER_APP_KEY;
    private $PUSHER_APP_SECRET;
    private $PUSHER_CLUSTER;

    private $data = [];

    public function __construct()
    {
        $this->setAppID(env('PUSHER_APP_ID'));
        $this->setAppKEY(env('PUSHER_APP_KEY'));
        $this->setAppSECRET(env('PUSHER_APP_SECRET'));
        $this->setAppCluster(env('PUSHER_CLUSTER'));

        parent::__construct($this->PUSHER_APP_KEY, $this->PUSHER_APP_SECRET, $this->PUSHER_APP_ID, ['cluster' => $this->PUSHER_CLUSTER, 'encrypted' => true]);
    }

    /**
     * @param $value
     * @return PusherHelper
     *
     * Adiciona o ID do APP Pusher
     */
    public function setAppID(string $value): self
    {
        $this->PUSHER_APP_ID = $value;
        return $this;
    }

    /**
     * @param $value
     * @return PusherHelper
     *
     * Adiciona a KEY do APP Pusher
     */
    public function setAppKEY($value): self
    {
        $this->PUSHER_APP_KEY = $value;
        return $this;
    }

    /**
     * @param $value
     * @return PusherHelper
     *
     * Adiciona a Secret Key do APP Pusher
     */
    public function setAppSECRET($value): self
    {
        $this->PUSHER_APP_SECRET = $value;
        return $this;
    }

    /**
     * @param $value
     * @return PusherHelper
     *
     * Adiciona o Cluster do APP Pusher
     */
    public function setAppCluster($value): self
    {
        $this->PUSHER_CLUSTER = $value;
        return $this;
    }

    /**
     * @param $value
     * @return PusherHelper
     *
     * Adiciona uma mensagem
     */
    public function setMessage($value): self
    {
        $this->data['message'] = $value;
        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @return PusherHelper
     *
     * Cria um dado randômico de acordo com a key
     */
    public function setData($key, $value): self
    {
        $this->data[$key] = $value;
        return $this;
    }

    /**
     * @param $user_id
     * @return boolean
     * @throws \Exception
     *
     * Caso queira enviar uma mensagem para um canal específico
     */
    public function notifyUser($user_id)
    {
        # Procuro o usuário
        $user = User::find($user_id);
        # Se não encontrar, envio pra execeção
        if (!$user)
            throw new \Exception("Usuário não encontrado");

        return $this->trigger($user->token, 'notification', $this->data);
    }

    /**
     * @param $group_slug
     * @param string $event
     * @return array|bool
     * @throws \Exception
     *
     * Envia os dados para um grupo específico o grupo é encontrado pela slug dele
     */
    public function sendToGroup($group_slug, $event = 'notification')
    {
        $group = Group::whereSlug($group_slug)
            ->first();
        if (!$group)
            throw new \Exception("Grupo não encontrado.");

        return $this->trigger("group-{$group->id}-{$group->slug}", $event, $this->data);
    }

    /**
     * @param $event string
     * @return array|bool
     *
     * Envia os dados pro canal privado ( apenas para pessoas que tem acesso administrativo )
     */
    public function sendToPrivateChannel($event = 'notification')
    {
        return $this->trigger('private01', $event, $this->data);
    }

    /**
     * @param $user_id
     * @param $event
     * @return array|bool
     * @throws \Exception
     *
     * Envia a notificação pro usuário, porém, é necessário ditar qual o nome do evento
     */
    public function notifyUserEvent($user_id, $event)
    {
        # Procuro o usuário
        $user = User::find($user_id);
        # Se não encontrar, envio pra execeção
        if (!$user)
            throw new \Exception("Usuário não encontrado");

        return $this->trigger($user->token, $event, $this->data);
    }

    /**
     * @param $channel
     * @param $event
     * @return bool
     *
     * Envia a mensagem
     */
    public function send($channel, $event)
    {
        return $this->trigger($channel, $event, $this->data);
    }

}