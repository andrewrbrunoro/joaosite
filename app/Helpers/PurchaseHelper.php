<?php

namespace App\Helpers;

use App\Purchase;
use App\PurchaseItem;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;

/**
 * Class PurchaseHelper
 * @package App\Helpers
 *
 * Essa classe vai manipular o objeto de pedido
 */
class PurchaseHelper extends PurchaseErrorHelper
{

    /**
     * @var MessageBag
     *
     * Irá adicionar os erros que irão ocorrer no decorrere do uso da classe
     */
    private $bag;

    /**
     * @var Purchase
     *
     * Recebe a Model Purchase
     */
    private $Purchase;

    /**
     * @var PusherHelper|bool
     * Recebe a helper do pusher, notificação em tempo real
     */
    private $Pusher = false;

    /**
     * @var
     *
     * Recebe o pedido do usuário
     */
    private $UserPurchase;

    private $cookieToken;

    /**
     * @var array
     *
     * Validação do produto que será inserido
     */
    private $item_validator = [
        'table' => 'required',
        'item_id' => 'required',
        'name' => 'required',
        'slug' => 'required',
        'price' => 'required',
        'sale' => 'required',
        'url' => 'required|min:10'
    ];

    public function __construct(MessageBag $bag)
    {
        $this->bag = $bag;
        $this->Purchase = new Purchase();
        $this->Pusher = new PusherHelper();
    }

    public function sendNotifyPurchase($id = null)
    {
        $this->Pusher->setMessage("Um pedido está em andamento, <a href='" . route('purchase.show', !is_null($id) ? $id : $this->UserPurchase->id) . "'>clique aqui</a> para poder acompanhar")
            ->sendToPrivateChannel();
    }

    public function getErrors()
    {
        return $this->bag;
    }

    public function fails()
    {
        return $this->bag->count() > 0;
    }

    /**
     * @param array $item
     * @param bool $unique
     * @return $this
     *
     * Adiciono um item no pedido
     */
    public function setItem(array $item, $unique = true)
    {
        # Valido se o item é um produto válido
        $validator = validator($item, $this->item_validator);
        # Se falhar, adiciona na bad de erro
        if ($validator->fails())
            $this->bag->add("item.{$this->bag->count()}", "Produto não é válido");
        else {
            # Crio um pedido ou pego o já existente
            $this->setPurchase();
            if (session()->has("purchase_id")) {

                # Inicio uma transação com o banco de dados
                DB::beginTransaction();
                try {
                    # Verifico se o item tem que ser único no pedido
                    if ($unique) {
                        # Verifico se ele existe no pedido
                        if ($this->existItem($item["table"], $item["item_id"]) < 1) {
                            $this->UserPurchase->PurchaseItem()->saveMany([new PurchaseItem($item)]);
                        } else {
                            $this->bag->add("item.{$item['item_id']}", "Produto já constá no carrinho,");
                        }
                    } else {
                        $this->UserPurchase->PurchaseItem()->saveMany([new PurchaseItem($item)]);
                    }
                    # Conclui a transação
                    DB::commit();
                } catch (\Exception $e) {
                    # Cancela a transação
                    DB::rollback();
                    # Adiciono a mensagem do erro na tabela de erro do pedido
                    $this->setError($e->getMessage(), $e->getTraceAsString());
                    # Pra não exibir um erro grotesco, deixa fixo a mensagem
                    $this->bag->add("item.error", "Não foi possível adicionar o {$item['name']} no carrinho, tente novamente, caso o erro persista entre em contato conosco.");
                }
            } else {
                $this->bag->add("fatal", "Sessão do pedido não foi iniciada.");
            }
        }
        return $this;
    }

    /**
     * Crio um pedido
     */
    public function setPurchase()
    {
        # Início a transação
        DB::beginTransaction();
        try {

            # Se existir uma sessão com o pedido
            if (session()->has("purchase_id")) {
                # Procuro pelo pedido
                $this->UserPurchase = $this->Purchase->where([
                    "id" => session("purchase_id"),
                    "closed" => 0
                ])->first();
                # Se o pedido que está na sessão já foi fechado, pode remover ele da sessão
                if (!$this->UserPurchase) {
                    # Remove o pedido da sessão
                    session()->forget("purchase_id");
                    # Crio um novo pedido, afinal, não existe mais sessão
                    $this->setPurchase();
                } else {
                    # Se estiver autenticado e não existir o id do usuário no pedido, apenas atualiza com o id dele
                    if (auth()->check() && $this->UserPurchase->user_id == "") {
                        $this->UserPurchase->update(["user_id" => auth()->user()->id]);
                    }
                }
            } else {
                # Se estiver autenticado, cria um pedido referente ao id do usuário
                # Senão, cria um pedido apenas com UUID dele
                auth()->check() ? $this->setAuthPurchase() : $this->setGuestPurchase();
            }

            DB::commit();
        } catch (\Exception $e) {
            # Cancelo a transação
            DB::rollback();
            # Adiciono a mensagem do erro na tabela de erro do pedido
            $this->setError($e->getMessage(), $e->getTraceAsString());
            # Pra não exibir um erro grotesco, deixa fixo a mensagem
            $this->bag->add("purchase.error", "Não foi possível criar o seu pedido, tente novamente, caso o erro persista, tente novamente mais tarde.");
        }
        return $this;
    }

    /**
     * Crio um pedido para um usuário anônimo
     */
    private function setGuestPurchase()
    {
        $this->UserPurchase = $this->Purchase->create([
            "cookie" => Cookie::get("uuid"),
            "closed" => 0
        ]);

        $this->sendNotifyPurchase();
    }

    /**
     * Crio um pedido para um usuário autenticado
     */
    private function setAuthPurchase()
    {
        $this->UserPurchase = $this->Purchase->create([
            "cookie" => Cookie::get("uuid"),
            "user_create_id" => auth()->user()->id,
            "user_id" => auth()->user()->id,
            "closed" => 0
        ]);

        $this->sendNotifyPurchase();
    }

    /**
     * @param $table
     * @param $item_id
     * @return mixed
     *
     * Verifico se o item já existe no pedido
     */
    public function existItem($table, $item_id)
    {
        return PurchaseItem::where([
            "table" => $table,
            "item_id" => $item_id,
            "purchase_id" => $this->UserPurchase->id
        ])->count();
    }

    public function removeItem($item_id, $pedido_id = null)
    {
        # Inicia a transação
        DB::beginTransaction();
        try {

            $item = PurchaseItem::where("purchase_id", "=", $pedido_id)->find($item_id);
            if (!$item)
                throw new \Exception("Pedido item não encontrou o id {$item_id}.");

            # Remove o item
            $item->delete();

            # Conclui a transação
            DB::commit();
        } catch (\Exception $e) {
            # Cancela a transação
            DB::rollback();
            # Adiciono a mensagem do erro na tabela de erro do pedido
            $this->setError($e->getMessage(), $e->getTraceAsString());
            # Pra não exibir um erro grotesco, deixa fixo a mensagem
            $this->bag->add("remove_item", "Não foi possível remover, tente novamente.");
        }
    }

}