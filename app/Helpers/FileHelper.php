<?php namespace App\Helpers;

use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Modules\Blog\Entities\PostImage;

class FileHelper
{

    # Nome do campo
    private $input;

    # Nome do arquivo
    private $name;

    # Diretório que será manipulado
    private $dir = 'assets/uploads';

    private $quality = 100;

    /**
     * @param int $value
     * @return self
     *
     * Seta a qualidade
     */
    public function setQuality(int $value): self
    {
        $this->quality = $value;
        return $this;
    }

    /**
     * @return int
     *
     * Retorna a qualidade
     */
    public function getQuality(): int
    {
        return $this->quality;
    }

    /**
     * @param string $dir
     * @return FileHelper
     *
     * Seta o diretório
     */
    public function setDir(string $dir): self
    {
        $this->dir = $dir;
        return $this;
    }

    /**
     * @return string
     *
     * Retorna o diretório que está setado na classe
     */
    public function getDir(): string
    {
        return $this->dir;
    }

    /**
     * @return bool
     *
     * Verifica se o diretório setado na classe é um diretório
     */
    public function isDir()
    {
        return is_dir(public_path($this->getDir()));
    }

    /**
     * @return string
     *
     * Retorna o diretório com o public_path
     */
    public function getPublicPath(): string
    {
        return public_path($this->getDir());
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     *
     * Retorna o link total do arquivo
     */
    public function getLink()
    {
        return url($this->getDir() . '/' . $this->getFileName());
    }

    /**
     * @param string $name
     * @return FileHelper
     * @throws \Exception
     *
     * Seta o nome do arquivo
     */
    public function setInput(string $name): self
    {
        # Verifica se existe o campo no request
        $this->validateInput($name);
        # Seta o valor
        $this->input = request()->file($name);
        return $this;
    }

    /**
     * @param string $name
     * @throws \Exception
     *
     * Validação do input do request
     */
    public function validateInput(string $name)
    {
        try {
            if (!request()->hasFile($name))
                throw new \Exception("Campo `{$name}` não encontrado.");
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param string $dir
     * @throws \Exception
     *
     * Cria o diretório, se não existir...
     */
    public function createDirectory(string $dir = ''): void
    {
        try {
            # Se passar um diretório, adiciona na classe, senão, utiliza o default
            if (!empty($dir)) $this->setDir($dir);

            # Se não for um diretório, cria
            if (!$this->isDir())
                mkdir($this->getPublicPath(), 755, true);

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param string $value
     * @return FileHelper
     * @throws \Exception
     *
     * Seta o nome do arquivo
     */
    public function setFileName(string $value): self
    {
        try {
            # limpa a string
            list($file_name, $extension) = explode('.', $value);
            $value = str_slug($file_name, '-') . ".{$extension}";

            # contador
            $i = 0;
            do {
                # Se o contador for maior que 0 (zero), renomeia a imagem
                if ($i) {
                    $name = str_replace('.', "-{$i}.", $value);
                } else {
                    $name = $value;
                }
                # incrementa +1
                $i++;
                # Enquanto o arquivo existir
            } while (is_file($this->getPublicPath() . '/' . $name));

            # Seta o nome do arquivo
            $this->name = $name;
        } catch (\Exception $e) {
            throw $e;
        }
        return $this;
    }

    /**
     * @return mixed
     *
     * Retorna o valor
     */
    public function getFileName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     * @throws \Exception
     *
     * Salva o arquivo no diretório fornecido com o nome setado
     */
    public function save()
    {
        if (!$this->input)
            throw new \Exception("O campo do arquivo não foi fornecido, utilizar ->setInput(nome do campo)");

        # Cria o diretório
        $this->createDirectory();

        return $this->input->move($this->getPublicPath(), $this->getFileName());
    }

    /**
     * Manipula a imagem que vem pelo request
     */
    public function requestIntervationImage()
    {
        try {
            # Se o campo não está sendo passado, informar erro
            if (!$this->input)
                throw new \Exception("O campo do arquivo não foi fornecido, utilizar ->setInput(nome do campo)");

            # Cria o diretório ou pega
            $this->createDirectory();

            # Seta o nome da imagem
            $this->setFileName($this->input->getClientOriginalName());

            # Move o arquivo para o diretório como temporário e em seguida, ajusta a qualidade dele
            $temp_name = "temp_" . $this->getFileName();
            $this->input->move($this->getPublicPath(), $temp_name);

            # Salva a imagem, com a qualidade setada
            Image::make($this->getPublicPath() . '/' . $temp_name)
                ->save($this->getPublicPath() . '/' . $this->getFileName(), $this->getQuality());

            # Remove o arquivo temporário
            unlink($this->getPublicPath() . '/' . $temp_name);

        } catch (\Exception $e) {
        }
    }

}