<?php

namespace App\Helpers;

use App\PurchaseError;
use Illuminate\Support\Facades\Cookie;

/**
 * Class PurchaseErrorHelper
 *
 * Helper que irá salvar os erros do pedido
 */
class PurchaseErrorHelper
{

    /**
     * @var
     * Dados de criação do erro
     */
    private $create;


    /**
     * Verifica se já não existe um purchase_id na sessão, se sim, salva no erro
     * Verifico se o usuário está autenticado e se ele tem erros referente ao cookie dele
     * O uuid do cookie sempre renova após o fim da compra
     */
    private function managerErrors()
    {
        # Se existir a sesão do pedido
        if (session()->has("purchase_id")) {
            # Procuro os erros referente ao o UUID e se não existe id do pedido
            $errors = PurchaseError::where([
                "cookie" => Cookie::get("uuid"),
                "purchase_id" => null
            ]);
            # Verifico se existe
            if ($errors->count()) {
                # Atuailizo todos eles, adicionando o id do pedido
                $errors->update(["user_id" => auth()->check() ? auth()->user()->id : null, "purchase_id" => session("purchase_id")]);
            }
        }
        # Se tive autenticado
        if (auth()->check()) {
            # Procuro os erros referente ao o UUID
            $errors = PurchaseError::where([
                "cookie" => Cookie::get("uuid"),
                "user_id" => null
            ]);

            # Verifico se existe
            if ($errors->count()) {
                # Atuailizo todos eles, adicionando o id do usuário
                $errors->update(["user_id" => auth()->user()->id]);
            }
        }
    }

    /**
     * @return $this
     * Adiciona o IP do usuário
     */
    private function purchaseErrorGuest()
    {
        $this->create["cookie"] = Cookie::get("uuid");
        return $this;
    }

    /**
     * @return $this
     * Adiciona o ID do usuário caso esteja autenticado
     */
    private function purchaseErrorAuth()
    {
        $this->create["user_id"] = auth()->check() ? auth()->user()->id : null;
        return $this;
    }

    /**
     * @return $this
     * Adiciona o ID do pedido caso exista
     */
    public function purchaseErrorPurchaseID()
    {
        $this->create["purchase_id"] = session("purchase_id");
        return $this;
    }

    /**
     * @return $this
     * Pego a URL que está sendo requisitada e de qual URl o usuário veio
     */
    public function purchaseErrorUrlReferer()
    {
        $this->create["url"] = request()->fullUrl();
        $this->create["referer"] = request()->server('HTTP_REFERER');
        return $this;
    }

    /**
     * @param $value
     * @return $this
     * Manipulo o valor da mensagem
     */
    public function purchaseErrorMessage($value)
    {
        $this->create["message"] = strip_tags($value);
        return $this;
    }

    /**
     * @param $value
     * @return $this
     * Manipulo a exceção
     */
    public function purchaseErrorException($value)
    {
        $this->create["exception"] = strip_tags($value);
        return $this;
    }

    /**
     * @param $message
     * @param null $exception
     * Crio o erro do pedido
     */
    public function setError($message, $exception = null)
    {
        $this->managerErrors();

        $this->purchaseErrorGuest()
            ->purchaseErrorPurchaseID()
            ->purchaseErrorAuth()
            ->purchaseErrorUrlReferer()
            ->purchaseErrorMessage($message)
            ->purchaseErrorException($exception);

        PurchaseError::create($this->create);
    }

}