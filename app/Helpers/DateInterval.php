<?php

namespace App\Helpers;


use Carbon\Carbon;

class DateInterval
{
    /**
     * @var static
     * DateTime Carbon
     */
    private $today;
    /**
     * @var
     *
     * Data de lançamento
     */
    private $releaseDate;
    /**
     * @var
     *
     * Data de encerramento
     */
    private $expirationDate;
    /**
     * @var \Illuminate\Config\Repository|mixed
     *
     * Formato da data na hora das validações
     */
    private $dateFormat;
    /**
     * @var string
     *
     * A classe irá usar para adicionar uma mensagem e poder ser requisitada no final
     */
    private $statusMessage = "";
    /**
     * @var bool
     *
     * A classe irá utilizar para setar o status de uma validação
     */
    private $status = false;

    public function __construct()
    {
        # Data atual
        $this->today = Carbon::now();

        # Adiciona o formato da data que será lido
        $this->dateFormat = config('app.date_format');
    }

    /**
     * @param $date
     * @return bool
     *
     * Verifica se a data está no formato válido
     */
    public function typeDate($date): bool
    {
        if (!$date) return false;
        $d = Carbon::createFromFormat($this->getDateFormat(), $date);
        return $d && $d->format($this->getDateFormat()) === $date;
    }

    /**
     * @param string $value
     * @return DateInterval
     *
     * Seta um formato de data que será lido
     */
    public function setDateFormat($value = 'Y-m-d H:i:s'): self
    {
        $this->dateFormat = $value;
        return $this;
    }

    /**
     * @return \Illuminate\Config\Repository|mixed
     *
     * Formato de data que está setado
     */
    public function getDateFormat(): string
    {
        return $this->dateFormat;
    }

    /**
     * @param $value
     * @return DateInterval
     *
     * Adiciona a data de lançamento na classe para poder manipular a mesma
     */
    public function setReleaseDate($value = ""): self
    {
        if ($this->typeDate($value)) {
            $this->releaseDate = Carbon::createFromFormat($this->getDateFormat(), $value);
        } else {
            $this->releaseDate = $value;
        }
        return $this;
    }

    /**
     * @return mixed
     *
     * Retorna o valor da data de lançamento
     */
    public function getReleaseDate()
    {
        return $this->releaseDate;
    }

    /**
     * @param $value
     * @return DateInterval
     *
     * Adiciona a data de encerramento na classe para poder manipular a mesma
     */
    public function setExpirationDate($value = ""): self
    {
        if ($this->typeDate($value)) {
            $this->expirationDate = $value != "" ? Carbon::createFromFormat($this->getDateFormat(), $value) : $value;
        } else {
            $this->expirationDate = "";
        }
        return $this;
    }

    /**
     * @return mixed
     *
     * Retorna o valor da data de encerramento
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * @return string
     *
     * Pega a string do status da mensagem
     */
    public function getStatusMessage()
    {
        return $this->statusMessage;
    }

    /**
     * @return bool
     *
     * Pega o status do que foi analisado
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return DateInterval
     *
     * Faz análise do intervalo das datas
     */
    public function checkReleaseAndExpiration(): self
    {
        # Se a data de lançamento estiver vazia, o sistema entende que é lançamento imediato
        # Senão se a diferença for negativa ou igual a zero, significa que a data de hoje é menor ou igual que a de lançamento, permitido aparecer
        # Senão ainda não foi lançado o curso
        if ($this->getReleaseDate() == "") {
            $release = true;
        } else if ($this->today->diffInDays($this->getReleaseDate(), false) <= 0) {
            $release = true;
        } else {
            $release = false;
        }

        # Se a data de encerramento estiver vazia, o sistema entende que é lançamento imediato
        # Senão se a diferença for positiva, significa que a data de hoje é menor que a data de encerramento
        # Senão já expirou
        if ($this->getExpirationDate() == "") {
            $expiration = true;
        } else if ($this->today->diffInDays($this->getExpirationDate(), false) > 0) {
            $expiration = true;
        } else {
            $expiration = false;
        }

        # Se a data de lançamento e encerramento estiverem criando um intervalo, retorna verdadeiro
        # Senão se a data de lançamento estiver certa e a de encerramento já estiver negativo, significa que já expirou
        # Senão se a data de lançamento estiver negativa e a de encerramento positiva, significa que ainda não foi lançado
        # Senão está indisponível
        if ($release == true && $expiration == true) {
            $this->statusMessage = 'disponivel';
            $this->status = true;
        } else if ($release == true && $expiration == false) {
            $this->statusMessage = 'já expirou';
            $this->status = false;
        } else if ($release == false && $expiration == true) {
            $this->statusMessage = 'ainda não foi lançado';
            $this->status = false;
        } else {
            $this->statusMessage = 'indisponível';
            $this->status = false;
        }
        return $this;
    }

    /**
     * @param string $release_date
     * @param string $expiration_date
     * @return bool
     *
     * Valida se existe um intervalo
     */
    public function validIntervalDate($release_date = "", $expiration_date = ""): bool
    {
        # Alimenta a data de lançamento e de encerramento
        $this->setReleaseDate($release_date)->setExpirationDate($expiration_date);

        # Análise as datas disponíveis
        $this->checkReleaseAndExpiration();

        # Retorna true or false
        return $this->getStatus();
    }

}