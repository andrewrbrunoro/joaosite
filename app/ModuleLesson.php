<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ModuleLesson extends Model
{

    use SoftDeletes;

    public function Module()
    {
        return $this->belongsTo(Module::class, 'module_id');
    }

    public function Lesson()
    {
        return $this->belongsTo(Lesson::class, 'lesson_id');
    }

}
