<?php

# Rotas que qualquer um vai acessar
Route::group([
    'middleware' => 'settings',
    'as' => 'front.',
    'namespace' => 'App\Http\Controllers'
], function ($r) {

    # Home / Página inicial
    $r->get("/", "HomeController@index")->name("home.index");

    # Conteúdo Gratuito
    $r->get('conteudo-gratuito', 'FreeContentController@index')->name("content.free");

    ## Página de Contato ##
    $r->get("contato", "ContactController@index")->name("contact.index");
    $r->post("contato", "ContactController@store")->name("contact.store");
    ## Página de Contato ##

    ## Serviço do usuário ##
    $r->post("autenticacao", "Auth\LoginController@login")->name("auth.login");
    ## Serviço do usuário ##

    ## Carrinho de compras ##
    $r->get("carrinho", "CartController@index")->name('cart.index');
    ## Carrinho de compras ##

    ## Controlado da sacola de compras ##
    $r->post('comprar-curso', 'BagController@addTeam')->name('bag.add.team');
    ## Controlado da sacola de compras ##

    ## Registrar rota de Cursos ##
    \App\Team::routeRegister($r);
    ## Registrar rota de Cursos ##

});

# Rotas que apenas pessoas com autenticação irá acessar
Route::group([
    'middleware' => ['settings', 'auth'],
    'namespace' => 'App\Http\Controllers',
    'as' => 'front.'
], function ($r) {

    $r->get('sair', 'Auth\LoginController@logout')->name('auth.logout');


    # Aulas - Usuário
    $r->get('{slug}/galeria-de-duvidas', 'LearnController@questionGallery')->name('team.gallery-questions');
    $r->post('configuracao/turma', 'LearnUserController@configure')->name('team.user.config');
    $r->post('avaliar/aula', 'LearnUserController@lessonRating')->name('user.rating.lesson');
    $r->post('favoritar/aula', 'LearnUserController@lessonFavorite')->name('user.favorite.lesson');
    $r->post('reportar/duvida/aula', 'LearnUserController@lessonQuestion')->name('user.report.question');
    $r->post('reportar/erro/aula', 'LearnUserController@lessonError')->name('user.report.error');
    # Aulas - Usuário

});

# Rotas do carrinho que necessitam do pedido
Route::group([
    'prefix' => 'carrinho',
    'middleware' => ['settings', 'auth', 'has-purchase'],
    'namespace' => 'App\Http\Controllers',
    'as' => 'front.'
], function ($r) {
    # Carrinho de compras
    $r->get('pagamento', 'CartController@payment')->name('cart.payment');
    $r->post('pagamento', 'CartController@endPayment')->name('cart.end.payment');
    $r->delete('remove-item', 'BagController@removeItem')->name('cart.remove');
    # Carrinho de compras
});

####################################################################################################
####################################################################################################
######################De uso EXCLUSIVO para a área de gereciador do site############################
######################De uso EXCLUSIVO para a área de gereciador do site############################
######################De uso EXCLUSIVO para a área de gereciador do site############################
######################De uso EXCLUSIVO para a área de gereciador do site############################
####################################################################################################
####################################################################################################

# Adicionar no arquivo .env a variável SYSTEM_CMS_PREFIX e o valor que irá ser interpretado pelo HTTP
# Exemplo: admin -> {url}/admin/{rotas}
$prefix_cms = env('SYSTEM_CMS_PREFIX');

# O grupo inicia com o padrão que é a autenticação e as configurações
Route::group([
    'prefix' => $prefix_cms,
    'middleware' => ['settings', 'auth']
], function () {

    # Link Global
    Route::post('upload/image', '\App\Helpers\Redactor@uploadimage')->name('redactor.upload_image');
    Route::post('upload/file', '\App\Helpers\Redactor@uploadFile')->name('redactor.upload_file');
    # Link Global

    /* Declare os módulos que serão utilizados no arquivo .env */
    $modules = trim(env('SYSTEM_MODULES'));
    foreach (explode(',', $modules) as $module) {
        $path = base_path("Modules/{$module}/Http/routes.php");
        if (is_file($path)) {
            include $path;
        } else {
            if (env('APP_DEBUG') == true) {
                die("Módulo {$module} - {$path}. Não encontrada.");
            }
        }
    }
    /* Declare os módulos que serão utilizados no arquivo .env */

});

# Grupos que não podem ter autenticação, referente ao admin
Route::group([
    'prefix' => $prefix_cms,
    'middleware' => ['settings']
], function () {

    $auth_path = base_path("Modules/Auth/Http/routes.php");
    if (is_file($auth_path))
        include $auth_path;

});


# Grupos identependetes
/* Declare os módulos que serão utilizados no arquivo .env */
$modules = trim(env('SYSTEM_MODULES'));
foreach (explode(',', $modules) as $module) {
    $path = base_path("Modules/{$module}/Http/na-routes.php");
    if (is_file($path)) {
        include $path;
    }
}
/* Declare os módulos que serão utilizados no arquivo .env */

####################################################################################################
####################################################################################################
######################De uso EXCLUSIVO para a área de gereciador do site############################
######################De uso EXCLUSIVO para a área de gereciador do site############################
######################De uso EXCLUSIVO para a área de gereciador do site############################
######################De uso EXCLUSIVO para a área de gereciador do site############################
####################################################################################################
####################################################################################################